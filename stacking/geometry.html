<HTML>
<HEAD>
<TITLE>Neo900 Stacking and Measurements</TITLE>
</HEAD>
<BODY>
<H1>Neo900 Stacking and Measurements</H1>

The following images show parts of the vertical stacking of Neo900,
and various measurements of the geometry of N900 elements that affect
this stacking. The raw data is from
<A href="http://neo900.org/stuff/werner/scans/">meshes obtained by 3D
scans</A> (<A href="https://neo900.org/git/scans">repository</A>).
The measurements were taken with the
<A href="https://neo900.org/git/misc/tree/meme">"meme" mesh
measurement</A> program.
<A href="http://meshlab.sourceforge.net/">MeshLab</A> was used for 3D
rendering of the mesh.
<P>
In the screenshots below, the mesh is shown in grey, with darkness indicating
depth. Where the contrast is increased to better show fine structures,
parts of the mesh exceeding the available range of grey tones are
shown as yellow. Blue lines at the bottom and the right side are sometimes
added to show the height profile along the axes of the cursor (red).
<P>
Sections:
<UL>
  <LI><A href="#vertical-stacking">Vertical stacking</A>
  <LI>Case edge height:
    <A href="#case-edge-height-rear">rear</A>,
    <A href="#case-edge-height-front">front</A>
  <LI>Case wall thickness:
    <A href="#case-clear-thick-front">front</A>,
    <A href="#case-wall-thick-rear">rear</A>
  <LI>PCB to case clearance:
    <A href="#case-clear-thick-front">front</A>,
    <A href="#case-clear-rear">rear</A>
  <LI>Location of case features:
    <A href="#loc-feat-front">front</A>,
    <A href="#loc-feat-rear">rear</A>
  <LI><A href="#slider-vert">Vertical structure of the slider</A>
  <LI><A href="#spacer-on-light">Spacer on light spreader</A>
  <LI><A href="#slider-clear">Slider to case clearance</A>
  <LI><A href="#loc-slider-feat">Location of slider features</A>
</UL>

<A name="vertical-stacking">
<H2>Vertical stacking</H2>

The following drawing shows part of the vertical stacking of Neo900,
including the PCBs, the upper edge of the case, and the slider mechanism
of the display module.
<P>
<A href="stacking.pdf"><IMG src="stacking.png"></A>
<P>
All dimensions are in millimeters. The following sections explain how the
measurements used in this drawing were obtained.


<A name="case-edge-height-rear">
<H2>Case edge height (rear side)</H2>

The following set of measurements shows the height of the case edge over
the top surface (S2) of the PCB. The green measurements lines indicate
the pairs of points whose Z difference is being measured.
<P>
<IMG src="lower-edge.png">
<P>
The result is that the case edge is typically between 1.9 to 2.0 mm above
the PCB surface.


<A name="case-edge-height-front">
<H2>Case edge height (front side)</H2>

We repeat the same measurements on the front (keyboard) side.
<P>
<IMG src="lower-wall-front.png">
<P>
The case edge is typically about 2.0-2.2 mm above the PCB surface. The
"step" on which the keyboard frame rests has a depth of about 1.1 to 1.2 mm
and is thus roughly 1 mm above the PCB.


<A name="case-wall-thick-rear">
<H2>Case wall thickness (rear side)</H2>

For designing the side walls of the spacer frame and for calculating
clearances (in the next section), we need to know the thickness of the
case wall.
<P>
<IMG src="case-wall.png">
<P>
The measurements are between opposite points where the sensed height
markedly falls off, and suggest a thickness of 1.4-1.5 mm. Given that the
top of the wall is slightly curved, which can cause a drop to appear in
the scan before the wall falls off in reality, some measurements are
likely to be too short by up to one grid step (0.1 mm). We therefore
consider the nominal wall thickness to be 1.5 mm.


<A name="case-clear-rear">
<H2>PCB to case clearance (rear side)</H2>

This set of measurements shows the horizontal clearance between the
PCB (LOWER) and the case. The green measurement lines show the distances
being measured.
<P>
<IMG src="pcb-case.png">
<P>
The result is that the clearance varies considerably but is never smaller
than 0.5 mm.


<A name="case-clear-thick-front">
<H2>PCB to case clearance and wall thickness (front side)</H2>

This set of measurements shows the horizontal clearance between the
PCB (LOWER) and the front side of the case, and it also shows the
thickness of the wall.
The green measurement lines indicate the distances being measured.
<P>
<IMG src="pcb-front.png">
<P>
We obtain an overall wall thickness of 1.5 to 1.6 mm, and a "step"
width of 0.4 to 0.5 mm. The PCB is closer to the wall than on the
rear side, only 0.3 to 0.4 mm.

<HR>

<A name="loc-feat-rear">
<H2>Location of rear (button) side features, vertical clearance near wall</H2>

This set of measurements show there where features on the rear side
are placed along the X axis. It also indicates the vertical distance
between the top of the case wall and the most prominent rear-side features.
<P>
<IMG src="rear-line.png">
<P>
The first 0.5 to 0.6 mm below top of the case wall are unobstructed.
Then the IR window begins, followed by the volume button with a peak
at about 0.75 mm and the rest at 1.3 mm,
and the camera button at about 0.9 mm.


<A name="loc-feat-front">
<H2>Location of front (keyboard) side features</H2>

The following set of measurements shows where features on the front side
are placed along the X axis:
<P>
<IMG src="front-line.png">
<P>
The middle "hook" seems to be centered at the middle of the case. Most
of these measurements are between edges that not perpendicular to the
measurement direction and sometimes even rounded, and may therefore be
fairly inexact.

<HR>

<A name="slider-vert">
<H2>Vertical structure of the slider</H2>

The height (or depth) of details of the metal structure of the slider
mechanism is examined in the following set of measurements. Special
attention is given to the height of the bends at the upper end of the
slider. The measurement
lines indicate the points whose heights are being compared.
<P>
<IMG src="slider-depth.png">
<P>
The general observation is that that basic shape of this metal structure
is a plane with some cut-outs into which shallow details have been
stamped, and from which larger structures protrude. We can therefore
measure all heights relative to this plane.
<P>
The following different features are being measured here:
<UL>
  <LI>The height of the large base of the columns joining the display
      with PCB and case. This base acts as a spacer and thus determines
      the height of the metal structure above the PCB.<BR>
      We obtain a value of 2.1-2.2 mm, with an important outlier of only
      2.0 mm in the bottom left corner. This is further examined in a
      separate set of measurements, below.
  <LI>The height of the large "hump". It is 1.3 mm above the plane,
      placing it roughly 0.7-0.8 mm above the PCB.
  <LI>The height of the metal plane above the display case. We obtain
      a value of 0.6 mm.
  <LI>The metal structure is bent away from the display and towards the
      PCB (i.e., "up" in this scan) on almost all the upper edge. There
      are two types of such bends: a tall bend with a height of 0.5 mm
      where the metal sheet extends all the way to the case edge, and a
      less pronounced bend with a height of only 0.2 mm where the sheet
      stops well before the case.
</UL>


<A name="spacer-on-light">
<H2>Spacer on light spreader</H2>

The outlier observed above can be explained by closer examination of where
the spacer touches the PCB. The following image zooms into the
PCB-with-case scan we have used before, showing the lower right corner,
right above the keyboard:
<P>
<IMG src="elevated-spacer.png">
<P>
At this location, the spacer is not in contact with the PCB directly,
but rests on the light spreader, which has a thickness of about 150 &mu;m.
One can also see that the spacer has compressed the light spreader by
about 50 &mu;m.


<A name="slider-clear">
<H2>Slider to case clearance</H2>

In order to explore possibilities for adding stabilizing elements to the
spacer frame, we need to determine the horizontal gap between case and
slider mechanism. We measure this indirectly by determining the distance
between the steep outer side of the display and the edge of the slider.
Using the case wall thickness, the gap between slider and wall can then
be calculated.
<P>
<IMG src="slider-lateral.png">
<P>
These measurements show a systematic difference to the wall thickness of
1.5 mm. This can be explained by the shape of the display case, which
transitions from the plateau to the wall through a small step. This step
is almost invisible in the mesh at the top edge but can be clearly seen
at the bottom edge:
<P>
<IMG src="disp-inside-snapshot.jpg">
<P>
With the scanning equipment equipment used, such a step is difficult to
measure accurately due to the conical shape of the scanning needle.
Combining results from the top and the bottom
edge (below), we estimate the step to have a width of about 0.4&pm;0.1 mm.
<P>
<IMG src="slider-lateral-bottom.png">
<P>
We show distances corrected for the step width in parentheses after the
original measurement. The minimum distance between slider metal and the
case is thus 0.1 mm, with the gap sometimes growing to about 0.5 mm (zones A
a and B) or even 0.8 mm (zone C).
<P>
The measurements also show a significant difference between the distance
between slider and the outside wall of the display on the left and on the
right side. We did not examine this further, but consider it possible that
the difference may have been caused the 0.4 mm step entering some of the
measurement while being absent from the others.

<HR>

<A name="loc-slider-feat">
<H2>Location of slider features</H2>

The following set of measurements shows where features on the slider are
are located along the X axis. While some measurements were made between
points that also differ in the Y direction, we only use the X difference.
<P>
<IMG src="slider-line.png">
<P>
Note that, while the scan shows a clearly visible rotation in the XY
plane, its effect on measurements is negligible. (E.g., the real
distance between the two columns is only about 0.02% larger than its
X distance.)

<P>
<HR>
<P>
{DATE}
</BODY>
</HTML>
