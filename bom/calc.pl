#!/usr/bin/perl
#
# bom/calc.pl - Calculate device cost based on stock database
#
# Written 2014-2015, 2017 by Werner Almesberger
# Copyright 2014-2015, 2017 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


$cache = ".cache";


sub usage
{
	print STDERR <<"EOF";
usage: $0 [-b] [-c cache_dir] [-d] [-e file.equiv] [-g|-s]
       [-v] [-x exclude ... [-C] [-h]] units [quantity supplier part ...]
EOF
	exit(1);
}


sub one_part
{
	local ($qty, $src, $name) = @_;
	my $file;

	($file = $name) =~ tr|/|_|;
	$file = $cache."/".$src."_".$file;

	return unless -r $file;

	open(FILE, $file) || die "$file: $!";
	my @a = split(/\s+/, <FILE>);
	my $desc = <FILE>;
	chop $desc;
	close FILE;

	if ($#exclude != -1) {
		$best_desc = $desc if length $desc > length $best_desc;
		return;
	}

	splice(@a, 0, 2);	# drop supplier and part name
	my $stock = shift @a;
	my $want = $qty * $units;

	$src_stock{$src} = $stock if $stock > $src_stock{$src};
	$avail += $stock;

	return if $stock < $want && $found;
	undef $best if $stock >= $want && !$found;

	while ($#a != -1) {
		die "processing $src $name" unless $#a & 1;
		my $break = shift @a;
		my $price = shift @a;
		my $n = $want < $break ? $break : $want;

		next if $best < $n * $price && defined $best;
		next if $best == $n * $price && $best_n >= $n && defined $best;
		$best = $n * $price;
		$best_n = $n;
		$best_src = $src;
		$best_name = $name;
		$best_desc = $desc;
		$best_break = $break;
		$found = 1 if $want <= $stock;
	}
}


sub try_parts
{
	local ($qty, $src, $name) = @_;
	my @a = ();

	return if defined $tried{"$src $name"};
	$tried{"$src $name"} = 1;

	&one_part(@_);
	if ($#exclude == -1 || !grep($_ eq $src, @exclude)) {
		@a = ("$src $name");
		$found = 1;
	}

	for (@{ $equiv{"$src $name"}}) {
		@a = (@a, &try_parts($qty, @{ $_ }));
	}
	return @a;
}


sub print_csv_desc
{
	local ($comma_follows) = @_;

	return unless $descriptions;

	my $tmp = $best_desc;

	$tmp =~ s/"/""/g;
	print "," unless $comma_follows;
	print "\"$tmp\"";
	print "," if $comma_follows;
}


sub part
{
	local ($qty, $src, $name, $group) = @_;
	local $best = undef;
	local $best_desc = undef;
	local $found = 0;
	local $avail = 0;

	undef %tried;
	undef %src_stock;
	my @a = &try_parts(@_);

	if ($#exclude != -1) {
		if (!$found) {
			warn "no matching part found for $src $name\n";
			$missing = 1;
		} else {
			if ($csv) {
				print "$qty,";
				&print_csv_desc(1);
				print join(",", map("\"$_\"", sort @a)), "\n";
			} else {
				printf("%3d ", $qty);
				print join(", ", sort @a), "\n";
				print "\t$best_desc\n"
				    if $descriptions && $best_desc ne "";
			}
		}
		return;
	}

	my $all = 0;
	for (values %src_stock) {
		$all += $_;
	}
	print STDERR "$src $name: sum $avail != peaks $all\n"
	    if $avail != $all && $stock;

	if ($stock) {
		die "clash on name \"$name\"" if defined $stock{$name};
		$stock{$avail == $all ? $name : "*$name"} =
		    [ $qty, $all, sort { @{ $b }[1] <=> @{ $a }[1] }
		      map([ $_, $src_stock{$_} ], keys %src_stock) ]
		    if $qty;
		return;
	}

	my $want = $qty * $units;
	if ($found && $avail < $want) {
		warn "want $want $src $name, known stock may be $avail\n";
		$missing = 1;
	}
	return unless defined $best;

	if ($csv) {
		print "$best_break," if $show_break;
		print "$best_n,\"$group\",\"$best_src $best_name\",$best";
		&print_csv_desc(0);
		print "\n";
	} elsif ($verbose) {
		$max_src = 9;
		$max_name = 40;
		$max_num = 9;

		$max_name -= 7 if $show_break;

		$max_name += $max_src - length $best_src
		    if length $best_src > $max_src;
		$max_name = 0 if $max_name < 0;

		$max_num += $max_name - length $best_name
		    if length $best_name > $max_name;
		$max_num = 0 if $max_num < 0;

		printf("%6d/", $best_break) if $show_break;
		printf("%6d ", $best_n);
		if (length $group > 9) {
			printf("%s\n%*s", $group, 16 + 7 * $show_break, "");
		} else {
			printf("%-9s", $group);
		}
		printf(" %-${max_src}s %-${max_name}s %${max_num}.4f\n",
		    $best_src, $best_name, $best);
		printf("%-16s %s\n", "", $best_desc)
		    if $descriptions && $best_desc ne "";
	}
	$sum += $best;
	$sum{$group ne "" ? $group : "Other"} += $best;
}


sub equiv
{
	local ($name) = @_;
	local $eq, @eq;

	undef $eq;
	open(EQUIV, $name) || die "$name: $!";
	while (<EQUIV>) {
		last if /^END\s*$/;
		s/\s*#[^#]+#\s*$//;
		next if /^#/;
		next if /^\s*$/;
		die unless /^(\s*)(\S+)\s+(\S+)\s*$/;
		if ($1 eq "") {
			$eq = "$2 $3";
			@eq = ($2, $3)
		} else {
			die unless defined $eq;
			push(@{ $equiv{$eq} }, [$2, $3]);
			push(@{ $equiv{"$2 $3"} }, [ @eq ]);
		}
	}
	close EQUIV;
}


while ($ARGV[0] =~ /^-/) {
	if ($ARGV[0] eq "-c") {
		shift @ARGV;
		$cache = $ARGV[0];
		&usage unless defined $cache;
	} elsif ($ARGV[0] eq "-b") {
		$show_break = 1;
	} elsif ($ARGV[0] eq "-d") {
		$descriptions = 1;
	} elsif ($ARGV[0] eq "-e") {
		shift @ARGV;
		&usage unless defined $ARGV[0];
		&equiv($ARGV[0]);
	} elsif ($ARGV[0] eq "-g") {
		$groups = 1;
	} elsif ($ARGV[0] eq "-h") {
		$header = 1;
	} elsif ($ARGV[0] eq "-s") {
		$stock = 1;
	} elsif ($ARGV[0] eq "-C") {
		$csv = 1;
	} elsif ($ARGV[0] eq "-v") {
		$verbose = 1;
	} elsif ($ARGV[0] eq "-x") {
		&usage unless defined $ARGV[0];
		shift @ARGV;
		push(@exclude, $ARGV[0]);
	} else {
		&usage;
	}
	shift @ARGV;
}

$units = shift @ARGV;
&usage unless defined $units || $stock;
&usage if defined $units && $stock;

if ($header) {
	if ($#exclude == -1) {
		print '"break",' if $show_break;
		print "\"quantity\",\"group\",\"distributor part\",\"price\"";
		print ',"description"' if $descriptions;
		print "\n";
	} else {
		print '"number",';
		print '"description",' if $descriptions;
		print "\"manufacturer part(s)\"\n";
	}
}

if ($#ARGV == -1) {
	while (<>) {
		next if /^#/;
		next if /^\s*$/;
		die unless /^(\d+)\s+(\S+)\s+(\S+)\s*(\S*)/;
		&part($1, $2, $3, $4) if $1;
	}
} else {
	while ($#ARGV >= 2) {
		&part(splice(@ARGV, 0, 3));
	}
	&usage unless $#ARGV == -1;
}

if ($#exclude != -1) {
	printf STDERR "Warning: some parts are missing\n" if $missing;
	exit;
}

if ($groups) {
	for (sort { $sum{$a} <=> $sum{$b} } keys %sum) {
		print "$_ $sum{$_}\n";
	}
	exit(0);
}

if ($stock) {
	for (sort
	    { $stock{$b}[1] / $stock{$b}[0] <=> $stock{$a}[1] / $stock{$a}[0] }
	    keys %stock) {
		my $s;
		my $qty = $stock{$_}[0];

		($s = $_) =~ s|^(.+)/.*$|$1|;
		print substr($s.(" " x 30), 0, 30);

		print " ", $stock{$_}[1] / $qty;
		for $i (2..$#{ $stock{$_} }) {
			print " ", $stock{$_}[$i][0], " ",
			    $stock{$_}[$i][1] / $qty
			    if defined $stock{$_}[$i][0];
		}
		print "\n";
	}
	exit;
}

printf("Total: %.2f, %.2f each\n", $sum, $sum / $units) unless $csv;
printf STDERR "Warning: some parts are unavailable\n" if $missing;
