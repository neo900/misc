#!/usr/bin/perl
#
# bom/ann2bom.pl - Generate BOM from annotations
#
# Written 2014, 2016 by Werner Almesberger
# Copyright 2014, 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


$cache = ".cache";


sub usage
{
	print STDERR "usage: $0 [-e|-u] [file.ann ...]\n";
	exit(1);
}


sub flush
{
	return unless defined $curr;

	for my $id (keys %alt) {
		my $p = $alt{$id}[0];
		my $n = 1;

		for (keys %mult) {
			next if index((split(/\s/, $p))[1], $_) == -1;
			$n *= $mult{$_};
			delete $mult{$_};
		}
		$bom{$p} += $n;
		$group{$p} = $group;
	}
	for (keys %mult) {
		print STDRR "$curr: unused multiplier $mult{$_} x $_\n";
	}

	for (keys %alt) {
		my $p = $alt{$_}[0];
		$use{$p}{$curr} = 1;
	}

	undef $curr;
	undef $group;
	undef %mult;
	undef %alt;
}


sub ref
{
	local ($id, $vendor, $part) = @_;
	my $p = "$vendor $part";
	my $f = $alt{$id}[0];

	if (defined $f) {
		$eq{$f}{$p} = 1;
		$eq{$p}{$f} = 1;
	}
	push(@{ $alt{$id} }, $p);
}


sub collect
{
	local ($p) = @_;
	my @res = ($p);
	my @eq = keys %{ $eq{$p} };
	
	delete $eq{$p};
	for (@eq) {
		next unless exists $eq{$_};
		@res = (@res, &collect($_));
	}
	return @res;
}


if ($ARGV[0] eq "-e") {
	$equiv = 1;
	shift @ARGV;
} elsif ($ARGV[0] eq "-u") {
	$use = 1;
	shift @ARGV;
} else {
	$bom = 1;
}
&usage if $ARGV[0] =~ /^-/;

while (<>) {
	next if /^#/;
	if (/^\s*$/) {
		&flush;
		next;
	}
	if (/^(\S+):\s*$/) {
		&flush;
		$curr = $1;
		next;
	}
	die unless /^\s+/;

	my $tmp = $_;
	while ($tmp =~ /(\d+)\sx\s([A-Z][A-Z0-9]*)/) {
		$mult{$2} = $1;
		$tmp = $';
	}

	&ref($2, "DIGI-KEY", $3) if /\s+DK=(\((.*?)\)\s+)?(.*?)\s*$/;
	&ref($2, "NEWARK", $3) if /\s+NEWARK=(\((.*?)\)\s+)?(.*?)\s*$/;
	&ref($2, "ARROW", $3) if /\s+ARROW=(\((.*?)\)\s+)?(.*?)\s*$/;
	&ref($2, "FUTEL", $3) if /\s+FUTEL=(\((.*?)\)\s+)?(.*?)\s*$/;
	&ref(undef, "MANUAL", $1) if /\s+MANUAL=(.*?)\s*$/;
	$group = $1 if /\s+GROUP=(\S+)/;
}
&flush;

if ($bom) {
	for (sort keys %bom) {
		print "$bom{$_} $_ $group{$_}\n";
	}
}

if ($equiv) {
	for (keys %eq) {
		print join("\n\t", &collect($_)), "\n" if exists $eq{$_};
	}
}

if ($use) {
	for (sort keys %use) {
		print "$bom{$_} $_ ".join(",", sort keys %{ $use{$_} })."\n";
	}
}
