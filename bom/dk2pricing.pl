#!/usr/bin/perl
#
# bom/dk2pricing.pl - Obtain stock and pricing for Digi-Key parts
#
# Written 2014, 2016-2017 by Werner Almesberger
# Copyright 2014, 2016-2017 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

require "cache.pl";


$TAG = "DIGI-KEY";
$ID_URL_BASE = "http://www.digikey.com/product-search/download.csv?k=";
$URL_BASE = "http://www.digikey.com/product-detail/en/";


sub csv
{
        local ($s) = @_;
        local (@a) = ();

	$s =~ s/\s*\xef\xbb\xbf//g;	# Byte Order Mark U+FEFF, in UTF-8
        while ($s =~ /^\s*("(([^"]|"")*?)"|(-?(\d|\.)*))\s*(,|$)/) {
                push(@a, $2) if defined $2;
                push(@a, $4) if defined $4;
                $s = $';
                last if $s eq "";
        }
        die "non-csv ending '$s'" unless $s eq "";
        return @a;
}


sub long_id
{
	local ($name) = @_;

	$name =~ s|/|%2F|g;
	my $url = $ID_URL_BASE . $name;

	my $short = $name;
	$short =~ s|%2F|/|g;

	open(PIPE, "wget --user-agent= -O - '$url' |") || die "$url: $!";
	while (<PIPE>) {
		chop;
		my @a = &csv($_);
		if ($a[2] eq "$short") {
			close PIPE;
			my $mid = $a[3];
			$mid =~ s|/|%2F|g;
			return $mid . "/" . $name;
		}
	}
	close PIPE;

	die "can't find short name $short";
}


sub retrieve
{
	local ($name) = @_;

	$name = &long_id($name) if $always_short || $name !~ m|/|;

	my $url = $URL_BASE . $name;
	open(PIPE, "wget --user-agent= -O - '$url' |") || die "$url: $!";
	my $page = join("", <PIPE>);
	close PIPE;

	return $page;
}


sub lookup
{
	local ($page) = @_;

	my $desc = 'itemprop="description">\s*([^<]*)\s*<';

	# Price table

	my @p = undef;

	if ($page =~ /<table\s+id="product-dollars".*?>(.*?)<\/table>/s) {
		$p = $1;

		while ($p =~
		    /<tr>\s*<td.*?>((\d+,)*\d+)<.*?>((\d+,)*\d*\.\d*)</s) {
			local ($b, $u) = ($1, $3);

			$b =~ tr/,//d;
			$u =~ tr/,//d;

			push(@p, $b, $u);
			$p = $';
		}
	} else {
		die unless $page =~ /Obsolete item/;
	}

	local $t;

	# Quantity Available

	if ($page =~ /<span\s+id="dkQty">((\d+,)*\d+)<\/span>/s) {
		$t = $1;
	} else {
		$t = 0;
	}

	$t =~ tr/,//d;
	unshift(@p, $t);

	# Description

	die unless $page =~ /$desc/s;
	$t = $1;
	$t =~ s/\s+$//;
	unshift(@p, $t);

	return @p;
}


&main("-s", \$always_short);
