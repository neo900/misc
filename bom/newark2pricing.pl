#!/usr/bin/perl
#
# bom/newark2pricing.pl - Obtain stock and pricing for Newark parts
#
# Written 2014, 2016-2017 by Werner Almesberger
# Copyright 2014, 2016-2017 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

require "cache.pl";


$TAG = "NEWARK";
#$URL_BASE = "http://www.newark.com/webapp/wcs/stores/servlet/Search?st=";
$URL_BASE = "http://www.newark.com/search?st=";


sub retrieve
{
	local ($name) = @_;

	my $url = $URL_BASE . $name;
	open(PIPE, "wget -O - '$url' |") || die "$url: $!";
	my $page = join("", <PIPE>);
	close PIPE;

	return $page;
}

sub lookup
{
	local ($page) = @_;
	local $t, $p;

	my $qty = 'class="availability".*?>[^<]*?((\d+,)*\d+)';
	my $price = 'class="tableProductDetailPrice.*?>(.*?)</table';
	my $entry = '(\d+)(&nbsp;-&nbsp;\d+|\s*\+).*?\$(\d+\.\d+)';

	my @p = undef;

	if ($page =~ /$qty/s) {
		($t = $1) =~ tr/,//d;
		push(@p, $t);
	} else {
		push(@p, 0);
	}

	if ($page =~ /$price/s) {
		$p = $1;

		while ($p =~ /$entry/s) {
			local ($b, $u) = ($1, $3);
	
			$b =~ tr/,//d;
			$u =~ tr/,//d;

			push(@p, $b, $u);
			$p = $';
		}
	} else {
		die unless $page =~ /Pricing is unavailable/;
	}

	my $d = "";

	if ($page =~ /<h1>\s*([^<]*)\s*</s) {
		$d = $1;
		$d =~ s/&nbsp;/ /gs;
		$d =~ s/\s+/ /gs;
	}

	unshift(@p, $d);

	return @p;
}


&main;
