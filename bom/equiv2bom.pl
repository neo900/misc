#!/usr/bin/perl
#
# bom/equiv2bom.pl - Generate pseudo BOM entries from equivalences
#
# Written 2014 by Werner Almesberger
# Copyright 2014 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


while (<>) {
	next if /^#/;
	next if /^\s*$/;
	die unless /^\s*(\S+)\s+(\S+)\s*$/;
	print "0 $1 $2\n";
}
