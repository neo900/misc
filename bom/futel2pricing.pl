#!/usr/bin/perl
#
# bom/futel2pricing.pl - Obtain stock and pricing for Future Electronics parts
#
# Written 2016 by Werner Almesberger
# Copyright 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

require "cache.pl";


$TAG = "FUTEL";
$URL_BASE = "http://www.futureelectronics.com/en/Technologies/Product.aspx?ProductID=";


sub retrieve
{
	local ($name) = @_;

	my $url = $URL_BASE . $name;
	open(PIPE, "wget -O - '$url' |") || die "$url: $!";
	my $page = join("", <PIPE>);
	close PIPE;

	return $page;
}


sub lookup
{
	local ($page) = @_;
	local $t, $p;

	my $qty = 'class="qtyInStock".*?class="qty">\s*((\d+,)*\d+)';
'>Availability:<[^<]*?((\d+,)*\d+)';
	my $price = 'trProductListPrice">(.*?)</tbody>';
	my $entry = '<th><b>(\d+).*?priceBreakPrice">\$(\d+\.\d+)';

	my @p = undef;

	if ($page =~ /$qty/s) {
		($t = $1) =~ tr/,//d;
		push(@p, $t);
	} else {
		push(@p, 0);
	}

	die unless $page =~ /$price/s;
	$p = $1;

	while ($p =~ /$entry/s) {
		local ($b, $u) = ($1, $2);

		$b =~ tr/,//d;
		$u =~ tr/,//d;

		push(@p, $b, $u);
		$p = $';
	}

	my $d = "";

	if ($page =~ /class="desc">\s*(.*?)<\/p>/s) {
		$d = $1;
		$d =~ s/&nbsp;/ /gs;
		$d =~ s/\s+/ /gs;
	}

	unshift(@p, $d);

	return @p;
}


&main;
