#!/usr/bin/perl
#
# bom/arrow2pricing.pl - Obtain stock and pricing for Arrow parts
#
# Written 2014, 2016 by Werner Almesberger
# Copyright 2014, 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

require "cache.pl";


$TAG = "ARROW";
$URL_BASE = "http://components.arrow.com/part/detail/";


sub prices
{
	local ($url) = @_;

	$url =~ s/\&amp;/\&/g;
	open(PIPE, "wget -O - '$url' |") || die "$url: $!";
	my $p  = join("", <PIPE>);
	close PIPE;

	my $entry = '"minqty":"([^"]+)","price":"\$([^"]+)"';

	my @p = ();
	
	while ($p =~ /$entry/s) {
		local ($b, $u) = ($1, $2);

		$b =~ tr/,//d;
		$u =~ tr/,//d;

		push(@p, $b, $u);
		$p = $';
	}

	return @p;
}


sub retrieve
{
	local ($name) = @_;

	my $url = $URL_BASE . $name;
	open(PIPE, "wget -O - '$url' |") || die "$url: $!";
	my $page = join("", <PIPE>);
	close PIPE;

	return $page;
}


sub lookup
{
	local ($page) = @_;
	local $t, $p;

	my $qty = 'class="li_inv"[^>]*>(\d+)';
	my $price1 = 'class="li_price"[^>]*>\s*\$([0-9.]*)';
	my $priceq = 'class="li_price".*?title="([^"]*)"';

	my @p = undef;

	if ($page =~ /$qty/s) {
		($t = $1) =~ tr/,//d;
		push(@p, $t);
	} else {
		push(@p, 0);
	}

	if ($page =~ /$price1/s) {
		push(@p, 1, $1);
	} else {
		die unless $page =~ /$priceq/s;
		push(@p, &prices($1));
	}
	die unless $page =~ /$price/s;
	$p = $1;

	my $d = "";

	if ($page =~ /<h4>\s*([^<]*)\s*</s && $1 ne "1") {
		$d = $1;
		$d =~ s/\s+/ /gs;
	}

	unshift(@p, $d);

	return @p;
}


&main;
