#!/usr/bin/perl
#
# bom/manual2pricing.pl - Obtain stock and pricing for manually sourced parts
#
# Written 2014, 2016 by Werner Almesberger
# Copyright 2014, 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


$cache = ".cache";


sub usage
{
	print STDERR "usage: $0 [-c cache_dir] [-u] [file ...]\n";
	exit(1);
}


while ($ARGV[0] =~ /^-/) {
	if ($ARGV[0] eq "-c") {
		shift @ARGV;
		$cache = $ARGV[0];
		&usage unless defined $cache;
	} elsif ($ARGV[0] eq "-u") {
		$update = 1;
	} else {
		&usage;
	}
	shift @ARGV;
}


while (<>) {
	next if /^#/;
	next if /^\s*$/;
	die unless /^(\S+)\s+(\S+)\s+(\d+)(\s+\d+\s+\d+\.?\d*)*\s*$/;

	($file = $2) =~ tr|/|_|;
	$file = $cache . "/${1}_" . $file;

	open(FILE, ">$file") || die "$file: $!";
	print FILE || die "$file: $!";
	close FILE || die "$file: $!";
}
