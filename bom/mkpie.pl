#!/usr/bin/perl
#
# bom/mkpie.pl - Make a pie chart from group totals
#
# Written 2014-2015 by Werner Almesberger
# Copyright 2014-2015 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


sub usage
{
	print STDERR
"usage: $0 [-c currency [-u]] [-g file.png] [-p file.pdf] [-t threshold]\n".
"       [-T title] units [bom-file]\n";
	exit(1);
}


while ($ARGV[0] =~ /^-/) {
	if ($ARGV[0] eq "-c") {
		shift @ARGV;
		$curr = $ARGV[0];
		&usage unless defined $curr;
	} elsif ($ARGV[0] eq "-g") {
		shift @ARGV;
		$png = $ARGV[0];
		&usage unless defined $png;
	} elsif ($ARGV[0] eq "-p") {
		shift @ARGV;
		$pdf = $ARGV[0];
		&usage unless defined $pdf;
	} elsif ($ARGV[0] eq "-t") {
		shift @ARGV;
		$thres = $ARGV[0];
		&usage unless defined $thres;
	} elsif ($ARGV[0] eq "-T") {
		shift @ARGV;
		$title = $ARGV[0];
		&usage unless defined $title;
	} elsif ($ARGV[0] eq "-u") {
		$unit = 1;
	} else {
		&usage;
	}
	shift @ARGV;
}

$N = shift @ARGV;
&usage unless defined $N;

while (<>) {
	next if /^#/;
	next if /^\s*$/;
	die unless /^(\S+)\s+(\d+.?\d*)\s*$/;
	($g, $c) = ($1, $2);
	if ($g =~ /^(\S+=\d+,)*\S+=\d+$/) {
		for (split(",", $g)) {
			$_ =~ $_;
			$g{$1} += $2;
			$sum += $2;
		}
	} else {
		$g{$g} += $c;
		$sum += $c;
	}
}

for (keys %g) {
	next if $_ eq "Other";
	$v = $curr ? $unit ? $g{$_} / $N : $g{$_} : 100 * $g{$_} / $sum;
	if ($v < $thres) {
		$g{"Other"} += $g{$_};
		delete $g{$_};
	}
}

open(PIPE, "| gnuplot --persist") || die "gnuplot: $!";
if (defined $png) {
	print PIPE "set term pngcairo size 600, 600\n";
	print PIPE "set output \"$png\"\n";
} elsif (defined $pdf) {
	print PIPE "set term pdfcairo size 10cm, 10cm\n";
	print PIPE "set output \"$pdf\"\n";
} else {
	print PIPE "set term wxt size 600, 600\n";
}
print PIPE <<EOF;
set size square
set style fill solid 1.0 border -1
EOF

$tr = 0.35;

$o = 0;
$cumul = 90;
for (sort { $g{$a} <=> $g{$b} } keys %g) {
	$o++;
	$next = $cumul + 360 / $sum * $g{$_};
	print PIPE "set object $o circle at screen 0.5, 0.5 size screen 0.4 ".
	    "arc [$cumul:$next] lw 2\n";
	$mid = ($cumul + $next) / 2;
	$a = $mid / 180 * 3.1415926;
	$x = cos($a) * $tr + 0.5;
	$y = sin($a) * $tr + 0.5;
	$rot = $mid < 270 ? $mid - 180 : $mid;
	$ref = $mid < 270 ? "left" : "right";
	$label = $_ eq "Other" ? "" : "$_ ";
	$v = $curr ? $unit ? $g{$_} / $N : $g{$_} : 100 * $g{$_} / $sum;
	if ($v && $v >= $thres) {
		$label .= defined $curr ?
		    sprintf("(%s%.2f)", $curr, $v) : sprintf("(%.1f%%)", $v);
		print PIPE "set label \"$label\" at screen $x, $y $ref ".
		    "rotate by $rot\n";
	}
	$cumul = $next;
}

print PIPE "set label \"".sprintf("Total %s%.2f", $curr,
    $unit ? $sum / $N : $sum).
    "\" at screen 0.95, 0.1 right\n" if defined $curr;

$title = "Component cost breakdown for $N unit".($N == 1 ? "" : "s")
    unless defined $title;

print PIPE <<EOF;
unset border
unset xtics
unset ytics
unset key
set parametric
set title "$title"
plot [t=0:1] 0, 0 with dots lc rgb "#ffffff00"
set output
EOF
