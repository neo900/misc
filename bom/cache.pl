#!/usr/bin/perl
#
# bom/cache.pl - Common functions for maintaining the parts cache
#
# Written 2014-2016 by Werner Almesberger
# Copyright 2014-2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

$DEFAULT_CACHE = ".cache";
$cache = $DEFAULT_CACHE;


sub usage
{
	print STDERR <<"EOF";
usage: $0 [-c cache_dir] [-d] [-u] [part/name ...]
       $0 -o out_file part/name
       $0 [-c cache_dir] [-d] [-u] file

  -c cache_dir
       use the specified directory (default: $DEFAULT_CACHE)
  -d   debug mode. Show information retrieved but do now write to cache.
  -o out_file
  -u   update. Overwrite existing cache entries.
EOF
	exit(1);
}


sub part
{
	local ($name) = @_;
	local ($file = $name) =~ tr|/|_|;

	$file = $cache."/${TAG}_".$file;
	return if -r $file && !$update && !$debug;

	my $page;
	if (-r $name) {
		open(FILE, $name) || die "$name: $!";
		$page = join("", <FILE>);
		close FILE;
	} else {
		$page = &retrieve($name);
	}

	if (defined $out_file) {
		open(FILE, ">$out_file") || die "$out_file: $!";
		print FILE $page || die "$out_file: $!";
		close FILE || die "$out_file: $!";
		print STDERR "page written to $out_file\n";
		exit;
	}

	my @p = lookup($page);

	die if $#p == -1;

	my $d = splice(@p, 0, 1);

	if ($debug) {
		print "$TAG $name ", join(" ", @p), "\n$d\n";
	} else {
		open(FILE, ">$file") || die "$file: $!";
		print FILE "$TAG $name ", join(" ", @p), "\n$d\n" ||
		    die "$file: $!";
		close FILE || die "$file: $!";
	}
}


sub main
{
	local @args = @_;

	local $out_file = undef;

	OPTION: while ($ARGV[0] =~ /^-/) {
		if ($ARGV[0] eq "-c") {
			shift @ARGV;
			$cache = $ARGV[0];
			&usage unless defined $cache;
		} elsif ($ARGV[0] eq "-d") {
			$debug = 1;
		} elsif ($ARGV[0] eq "-o") {
			shift @ARGV;
			$out_file = $ARGV[0];
			&usage unless defined $out_file;
		} elsif ($ARGV[0] eq "-u") {
			$update = 1;
		} else {
			for (my $i = 0; $i <= $#args; $i += 2) {
				if ($ARGV[0] eq $args[$i]) {
					${ $args[$i + 1] } = 1;
					shift @ARGV;
					next OPTION;
				}
			}
			&usage;
		}
		shift @ARGV;
	}

	if ($#ARGV == -1) {
		while (<>) {
			next if /^#/;
			next if /^\s*$/;
			die unless /^\d+\s+(\S+)\s+(\S+)/;
			next unless $1 eq $TAG;

			&part($2);
		}
	} else {
		for $name (@ARGV) {
			&part($name);
		}
	}
}


return 1;
