\documentclass[11pt]{article}
\usepackage{a4}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{fp}
\usepackage{xfrac}
\usepackage{titlesec}	% for \sectionbreak
\usepackage{parskip}
\usepackage[hang,bottom]{footmisc}
\usepackage{footnote}
\usepackage[all]{nowidow}
\usepackage[numbib]{tocbibind}	% add references to PDF TOC
\usepackage[hidelinks,bookmarksnumbered=true,bookmarksopen,
    bookmarksopenlevel=2]{hyperref}


% Copyright notice in the footer (first page only)
\usepackage{fancyhdr}
\fancypagestyle{plain}{
  \fancyfoot{}
  \fancyfoot[L]{Copyright \textcopyright\ by the authors.}}
\renewcommand{\headrulewidth}{0pt}

\bibliographystyle{unsrt}
\newcommand{\sectionbreak}{\clearpage}
\renewcommand{\footnotemargin}{1.2em}

\def\FPfmt#1{\FPeval{fpfmttmp}{#1}\fpfmttmp{}}
\def\FPrnd#1#2{\FPfmt{round((#2):#1)}}

\def\iic{$\hbox{I}^2\hbox{C}$}

\title{Neo900 SIM Switch}
\author{J\"org Reisenweber%
\footnote{Concept and design requirements.}
%~\url{<joerg@openmoko.org>} \\
,
Werner Almesberger%
\footnote{Specification details and illustrations.}
%~\url{<werner@almesberger.net>}
}
\date{June XX, 2016}

\newenvironment{tab}{\vskip4mm\qquad\begin{minipage}{430pt}}%
{\end{minipage}\vskip4mm\noindent}

\begin{document}
\phantomsection\pdfbookmark{Neo900 SIM Switch}{firstpage}
\maketitle

Neo900 supports up to two SIM cards, one under the battery and one in a
slot-in holder accessible from the outside. The hardware supports the
following operations:
\begin{itemize}
  \item The modem accessing either SIM, under CPU control,%
\footnote{To the modem, this looks like the user removing one SIM and
inserting another. Depending on the availability of modem variants natively
supporting two SIMs, details of which are not yet available at the time of
writing, additional functionality may be available.}
  \item generic smart card reader mode by the CPU, and
  \item independent access of NFC (through SWP) to a secure element contained
    in the SIM.
\end{itemize}

This document specifies how access to these two cards is implemented
in the Neo900 hardware.

The following drawing illustrates the general situation: we have modem,
CPU, and NFC that each may need to access one of the SIM cards, be it
for communication, for supplying power, or both.

\begin{center}
\includegraphics[scale=0.9]{overview.pdf}
\end{center}

We use A/B for the buses that are affected by the switch, and 1/2 for the
SIM side and for signals or buses that are not affected by the switch.

Depending the module version, the modem may have one or two SIM interfaces.
The system is designed such that no invalid configurations occur even if
the systems involved should fail to coordinate their activities.

Please note that connections that are shown as going to the CPU may in fact
be handled through an IO expander. Furthermore, signal or bus names used
in this document have been assigned somewhat arbitrarily, and may be harmonized
with the naming chosen for the Neo900 schematics at a later point in time.

% -----------------------------------------------------------------------------

\section{Connections}
\label{connections}

The following drawing shows the eight contacts of a typical SIM card:

\begin{center}
\includegraphics[scale=0.9]{sim.pdf}
\end{center}

We discuss the characteristics of the various signals and how they
relate to the Neo900 hardware design in the next sections.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Power (C1, C5)}

The SIM card is powered by the terminal. Of the supply voltage classes
specified in section 6.2.1 of \cite{UICC}, we support class B
(nominally 3~V) and class C (1.8~V).

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Card detection}

Access to a SIM card must be stopped before it can be safely removed
or swapped.
In order to ensure that the entity controlling a card can perform a
clean shutdown, a card detection signal may warn of imminent card removal.

Of the two SIM holders in Neo900, SIM \#2 is equipped with a card detection
switch. SIM \#1 has no such switch but its location ensures that the SIM
can only be released after battery removal.

We may introduce an added safeguard in the form of a battery detection
signal that acts as card detection for SIM \#1.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{SIM data interface (C2, C3, C7)}
\label{dataif}

This is the principal communication interface of a SIM card. It
is used by the modem to perform all the authentication, storage,
etc., operations that are used in mobile telephony.
For lack of a better term, we call this the ``SIM data interface''.

This interface can also be accessed by the CPU
for generic smart card reader purposes.
In this case, card removal is signaled to the modem and both SIMs
are then disconnected from it.

The voltage levels on these signals are defined in section
5 of \cite{UICC} as functions of the supply voltage. Therefore,
level shifting is required when the CPU accesses a class B (3~V)
card.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{SWIO (C6)}

SWP (Single Wire Protocol) provides a channel that operates in
parallel with and that is largely independent from the principal SIM
data interface.
It is used by NFC for communication with a Secure Element that is
(optionally) contained in the SIM card.

SWP is specified in \cite{SWP}. We discuss various implementation
considerations in section 6 of \cite{Neo900NFC}.

While there are small differences between class B (3~V) and class C
(1.8~V),%
\footnote{See section 6.1 of \cite{Neo900NFC}.}
SWP basically operates in the 1.8~V domain in either case.
It connects only to the
NFC subsystem which also operates at 1.8~V. Therefore, no level shifting
is required for SWP. The one signal SWP uses (in addition to power) is
named SWIO.
We use the terms SWP and SWIO largely interchangeably in this document.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Unused contacts (C4, C8)}

Contacts C4 and C8 are reserved for the USB interface specified in
\cite{USBUICC}. We do not support this functionality and leave these
contacts (if present) unconnected.

% -----------------------------------------------------------------------------

\section{Terminal}

While the Neo900 appears as a single terminal to a SIM card, the
entity that speaks to the SIM card may be the modem, the CPU, or
NFC -- the latter either alone or concurrently with modem or CPU.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Modem}
\label{termmodem}

The modem connects to all the contacts described in the section
\ref{connections}, except for SWP and the USB interface. I.e.,
there is power, the SIM data interface, and card detection.

Depending on which modem version will be available at the time the
design is implemented, the modem may have one or two SIM interfaces.
If the modem has only one interface, the CPU will coordinate access
to the SIM cards, and a card switch will appear to the modem like
the removal of the old card followed by the insertion of the new
card:

\begin{center}
\includegraphics[scale=0.9]{modem-single.pdf}
\end{center}

In addition to the card change signals from the SIM cards, the hardware
generates a card change indication before the switch is operated.

In case the modem supports two SIM interfaces, the switch can still be
used to swap the SIMs, should such functionality be desired. Also in this
case a card removal indication is generated before any switch configuration
changes are carried out:
\footnote{At the time of writing, it is not clear whether the modem
would use only one SIM at a time in this case, i.e., similar to the
functionality we provide if the modem has only a single SIM interface,
or whether it would be able to use both SIMs in parallel.
It will also be necessary to determine, when using a modem module
that is not dual-SIM capable, whether the pads used for the second SIM bus
can be safely connected to the SIM switch or whether the signals will have
to be isolated.}

\begin{center}
\includegraphics[scale=0.9]{modem-dual.pdf}
\end{center}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{CPU}

Like the modem, the CPU connects to the SIM data interface (as defined
in section \ref{dataif}), to card
detection, and it can request power to be provided to the SIM
(see section \ref{cpupwr}).

CPU access is mutually exclusive with the modem. This gets ensured on a
hardware level by giving only one of these two subsystems access to the SIMs
at a time. Further details regarding the implementation can be found in
section \ref{simmux}.

In the following sections, we discuss each type of connection and the
circuits needed before going to the main switch.


\subsubsection{CPU-controlled power}
\label{cpupwr}

The CPU requests power
delivery to the SIM from the power selection logic described in section
\ref{pwrsel}.


\subsubsection{CPU data}
\label{cpudata}

Since the SIM data interface may operate at either 1.8 or 3~V while the CPU
always operates at 1.8~V, we need to provide level shifting. Tables
5.8 and 5.12 in sections 5.2.4 and 5.3.4 of \cite{UICC}, respectively,
consider the use of a 20~k$\Omega$ pull-up resistor on the IO contact.
We can therefore use the bi-directional level shifting circuit
described in \cite{LvlShift-Short}:

\begin{center}
\includegraphics[scale=0.9]{shift.pdf}
\end{center}

For simplicity, we can also use the same type of level shifter for
RST and CLK, given that they use the same voltage levels and operate
at the same or a lower frequency.

In order to reduce the circuit footprint and the number of components,
integrated level shifters from the TI LSF family \cite{TILSF} could be used
instead of the discrete circuit shown above.
The programmable pull-up resistors built into the CPU
can be used for R1, further reducing the component count.

Section \ref{shiftvolt} describes how to obtain VSIMCPU.


\subsubsection{CPU card detect}

In order to ensure that the CPU can properly shut down any SIM card
it is accessing when that card is being removed, 
the CPU needs to have access to the card detect signals.

Since the CPU controls the state of the switch, software can simply decide
which card detect signal belongs to which SIM card, and no hardware
selection is necessary.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{NFC}
\label{termnfc}

Since the NFC subsystem provides two separate SWP channels and SWP
is not used by anything else, it can simply connect to both SIM
cards and let software select which channel to use.

The power management subsystem monitors the two SWP lines and
supplies power (if not already present) if detecting activity.
This power supply remains active until explicitly reset by a
signal from the NFC subsystem. This is explained in more detail
in section \ref{pwrsel}.

% -----------------------------------------------------------------------------

\section{The switch}
\label{switch}

The switch consists of three parts: a pair of analog switches for the SIM
data interface and the card detection signals, the power selection logic,
and the power distribution circuit. The following diagram refines the
overview from the introduction and shows how the various elements are related.

\begin{center}
\includegraphics[scale=0.9]{swover.pdf}
\end{center}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Data switching and card detection}
\label{simmux}

The following diagram shows the switching of data and card detection
signals. A pair of
Fairchild FSA2866 analog switches \cite{FSA2866} performs the actual
switching.

\begin{center}
\includegraphics[scale=0.9, angle=90]{simmux.pdf}
\end{center}

The control logic ensures that a card change is signaled to the modem
during at least
500 ms before the switch configuration is changed. For regulatory
considerations,
this logic should be implemented with discrete logic.

The level shifters for CPU\_SIM are discussed in section \ref{cpudata}.

Please note that, in the FSA2866 serving the modem, we use the lines
intended for switching power for the card detection signal instead.
This is to ensure that the switching of data and of card detection
can never disagree.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Power selection}
\label{pwrsel}

The following diagram shows the power selection logic.
The entire circuit can be implemented with a single
Silego SLG46533 mixed-signal array \cite{SLG46533}.

\begin{center}
\includegraphics[scale=0.9]{pwrsel.pdf}
\end{center}

Since power can be requested by various entities, we do not route
modem power directly through the switch but instead measure the
voltage the modem outputs and request the corresponding voltage
from the following stages. (Block ``Voltage detection''.)

Since the analog input voltage must not be greater than VDD, the voltages
output by the modem must be divided by at least $\div 2$.

The resulting request signals, corresponding to the modem's
SIM buses A and B, are then routed (block ``Switch'') towards the
respective SIM, according to the setting of the data switch (SEL\_Q).

The per-SIM request signals from the modem are then merged with the
request signals from CPU and NFC, and for each SIM, the highest
selected voltage is requested from the power distribution subsystem.
(Block ``Voltage selection''.)

The CPU simply indicates whether it wants to request SIM power,
and which voltage. This information is then routed to the corresponding
SIM according to the switch setting (SEL). The CPU must keep CPU\_PWR\_EN
low while CPU\_nMODEM\_Q is low.

NFC is not affected by the switch setting, and activity on the
respective SWIO line generates an 1.8 V request for the respective SIM.
This request remains active until explicitly reset by asserting
SWP\_nRESET.

Regarding the outputs, we show a configuration where each channel has
an enable signal and a voltage selection signal, suitable for interfacing
with the circuit described in section \ref{pwrdist}. Alternatively, one
could use one enable signal for each voltage, e.g., to control individual
switches.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Power distribution}
\label{pwrdist}

SIM power is provided by voltage regulators -- one for each SIM -- that
are configured according to what the power selection subsystem requests.
As an example, the Texas Instruments TPS728180300 \cite{TPS728},
available in a $1.4~{\rm mm}^2$ 5-BGA package, could be used:

\begin{center}
\includegraphics[scale=0.9]{ldo.pdf}
\end{center}

This regulator outputs either 1.8 V or 3.0 V. When disabled, it discharges
its output through a $60~\Omega$ resistor.

There are also similar dual-voltage regulators specifically designed for
use with SIM cards, with enhanced ESD protection, and that also include
level shifters, e.g., the
Texas Instruments TXS4555 \cite{TXS4555}.%
\footnote{Many other companies offer 4555 chips as well. However, most use
a $9~{\rm mm}^2$ 16-QFN footprint while -- for space reasons -- we use the
$3.4~{\rm mm}^2$ 12-QFN package only offered by Texas Instruments.}

We will use this regulator with built-in level shifters, but since our
design is more complex than the type of SIM applications this chip is
designed for, the level shifting capabilities are of no use and are
ignored.

\begin{center}
\includegraphics[scale=0.9]{4555.pdf}
\end{center}

\iffalse
SIM power is provided from system power rails according to what
the power selection subsystem requests. For power distribution,
switches like the Fairchild FPF1321 \cite{FPF1320} can be used:

\begin{center}
\includegraphics[scale=0.9]{pwrsw.pdf}
\end{center}

This switch can select either one of the power rails or it can
disconnect the output. The EN (enable) input should be held low after
reset to prevent powering any SIM cards before the system is in a defined
state.

There are two variants of this switch: the FPF1320 just switches power
while the FPF1321 discharges the output through a $65~\Omega$ resistor
when the switch is disabled. Section 4.5.3 of \cite{USBUICC} only mandates
that the leakage current be below $\rm 40~\mu A$, which is the case with both
variants, but we prefer the FPF1321 in order to avoid
long-lived residual charges.

The valid range of the class B supply voltage is 2.7 to 3.3~V. It
may therefore be possible to just use the 3.3~V rail if no 3.0~V rail
is available.
\fi

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Level shifter voltage}
\label{shiftvolt}

The last missing piece is the high-side voltage for the level shifters
between CPU and the switch. Since this voltage has to match the supply
voltage of the currently selected SIM, we can simply pass the SIM
supply voltage
back through the analog switch that also selects the data signals.

The analog switch serving the CPU shown in section \ref{simmux}
can therefore be completed as follows:

\begin{center}
\includegraphics[scale=0.9]{shiftpwr.pdf}
\end{center}

Note: to keep things clear and simple, these power connections are not
shown in the overview diagram in section \ref{switch}.

% -----------------------------------------------------------------------------

\section{SIM monitoring}

In order to monitor SIM activity, be it commanded from an outside
source or initiated by the SIM itself, we monitor the current
consumption of the SIM cards.
The circuit consists of a differential
amplifier whose output is sampled by the monitoring ADC (MADC) in
the companion chip.

For simplicity, we measure the total current for both SIMs. To monitor
a single SIM, the other should be powered down or removed.%
\footnote{PCB space and available ADC inputs permitting, a variant of the
same circuit could also be used to monitor each SIM individually.
We briefly discuss this in section \ref{dual}.}


% kOhm
\FPeval{R1}{0.002}
\FPeval{R2}{180}
\FPeval{R3}{100}
\FPeval{R4}{180}
\FPeval{R5}{105}
\FPeval{R6}{1100}


\FPeval{Rlow}{1/(1/R5+1/R6)}

\FPeval{Rminus}{R1 + R4 + Rlow}
\FPeval{Rplus}{R2 + R3}

\FPeval{Vbat}{4.3}
\FPeval{Vplus}{Vbat * R3 / Rplus}
\FPeval{Vminus}{Vbat * Rlow / Rminus}
\FPeval{Vmax}{max(Vplus, Vminus)}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Current measuring circuit}
\label{isim}

The basic idea is to measure the voltage drop the SIM current produces
across a shunt resistor. The maximum supply current is 50~mA per SIM,%
\footnote{Table 6.3 in section 6.2.3 of \cite{UICC}: 50~mA for class B
(3.0~V), 30~mA for class C (1.8~V). Sections 5.2.1 and 5.3.1 allow for
spikes of up to 60~mA for either class, with a maximum duration of 400~ns.
We consider accurate measurement of such such short spikes well beyond the
design objectives for this circuit. (See also section \ref{resolution}.)}
yielding a total of 100~mA for both SIMs.
Assuming the SIM regulators are supplied from the 3.3~V rail, the shunt
resistor can drop up to 200~mV given the 100~mV dropout voltage of the
regulator itself.%
\footnote{Parameter $\rm V_{DO}$ on page 6 of \cite{TXS4555}.}
The maximum value for the shunt resistor is therefore $2~\Omega$.

\begin{center}
\includegraphics[scale=0.9]{isim.pdf}
\end{center}

To match the allowed voltage range of the MADC input,%
\footnote{Table 5-75 in section 5.6.2 of \cite{TPS65950} specifies
  the maximum input voltage of the MADC as 2.5~V.
  According to section 4.1, the absolute maximum rating for an input
  is given by the voltage of the corresponding power supply.
  In the case of  to ADCIN2 to ADCIN7, the supply is
  VINTANA2 (table 3-1 in section 3.2), with a software-selectable
  voltage of 2.5 or 2.75~V (section 4.6).
  We designed the SIM current sensing
  circuit such that it is safe to use with 2.5~V.}
we operate the opamp at the VINTANA2 voltage.%
\footnote{Supplying the opamp from VINTANA2 also ensures that the ADC input is
  grounded when VINTANA2 is turned off, as required in note (1) on
  table 5-76 in section 5.6.3 of \cite{TPS65950}.
  The VINTANA2 regulator provides up to 250~mA (section 4.6).}
Given that VINTANA2 may be as low as 2.5~V, and
the regulator input is either \FPrnd{2}{Vbat}~V (maximum battery voltage) 
or 3.3~V,
we use voltage dividers to lower the voltage at the opamp inputs to
a maximum of \FPrnd{2}{Vmax}~V.

We designed the circuit for opamps with ``rail-to-rail'' outputs,
corresponding to the characteristics of the ADC input.
The $2~\Omega$ and $\rm 1.1~M\Omega$ resistor values were selected from the
E24 scale,
and $\rm 105~k\Omega$ is from the E48 scale. Resistors for all these values
are expected to have good availability at 1\% tolerance.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\FPeval{adcstepv}{2.5 / 1024}
\FPeval{avconv}{R1 * 1000 * Rlow / Rminus * R6 / R5}
\FPeval{adcstepi}{adcstepv / avconv}

\subsection{Measurement resolution and sample rate}
\label{resolution}

The MADC has a resolution of 10 bits%
\footnote{Table 5-75 in section 5.6.2 of \cite{TPS65950}.}
over the input range 0--2.5~V,
giving us a resolution of
\FPrnd{2}{adcstepv * 1000}~mV.
The opamp circuit has a current-to-voltage conversion ratio of
$\rm \FPrnd{2}{avconv}~\sfrac{V\!}{A}$.
The smallest current difference the ADC can distinguish is therefore
$\rm \FPrnd{0}{adcstepi * 1000000}~\mu A$.

We note in passing that the actual power consumption of an active SIM card
may be well below the 30 to 50~mA allowed by \cite{UICC}. While we
could not find any publicly accessible document specifying the current
consumption
of SIM cards, one example of a Java Card employing similar technology
is specified with an idle current of $\rm 100~\mu A$ and an active
current of 10~mA.%
\footnote{\url{http://www.smart-ecard.com/pdf/7400021F_CLXSU512KJ3-DIJ_TechBrief.pdf}}

The MADC can sample all 16 channels in 288--529~$\rm\mu s$
and a single channel in 18--33~$\rm\mu s$.%
\footnote{Table 5-77 in section 5.6.3.1 of \cite{TPS65950}.}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\FPeval{Rleak}{1/(1 / Rminus + 1 / Rplus)}

\subsection{Leakage}

The supply-to-ground resistance across R1 to R6 is
$\rm\FPrnd{0}{Rleak}~k\Omega$. The Leakage current is therefore
$\rm\FPrnd{0}{3.3 / Rleak * 1000}~\mu A$ at 3.3~V or
$\rm\FPrnd{0}{Vbat / Rleak * 1000}~\mu A$ at the \FPrnd{1}{Vbat}~V
battery.

Each regulator consumes at most $\rm 35~\mu A$ when enabled and idle,
$\rm 3.5~\mu A$ when disabled.%
\footnote{$\rm I_{GND}$ and $\rm I_{SHDN}$ on page 6 of \cite{TXS4555}.}

Current consumption of the opamp is expected to be significantly below 1~mA.
We discuss opamp characteristics in section \ref{opamp}.

When VINTANA2 is turned off, the inputs of the opamp draw
$\rm \FPrnd{0}{Vbat / 180 * 1000}~\mu A$ each through the clamp diodes
in the opamp.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Simulation}

We analyzed the performance of the circuit
and the effect of component tolerances on its characteristics
with the following simulation:

\begin{center}
% trim: left bottom right top
\includegraphics[scale=0.9, trim=30 250 300 30, clip]{isense.pdf}
\end{center}

The plot on the right side shows the output voltage as a function of the
current across
the shunt resistor R1, for all combinations of the resistors R2 to R6
each being at either the minimum or maximum allowed resistance value,
given a 1\% tolerance.

Due to the large common mode voltage on the sense inputs, even small
component tolerances have a large effect on the resulting output.
This results in a DC offset on the output signal that will differ from
device to device. This offset can be trivially compensated by measuring
the voltage when the regulator is turned off.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Opamp selection}
\label{opamp}

The most important selection criteria for the opamp are:
\begin{itemize}
  \item operating voltage includes the 2.5--2.75~V range,
  \item lowest output voltage $\rm\le 40~mV$ (see below),
  \item compact package,
  \item low idle current, and
  \item reasonable price and availability.
\end{itemize}

The 40~mV output voltage minimum of our circuit is obtained from the
simulation, where
the ideal opamp produces this voltage for a load current of 0~A, given
worst-case resistor tolerances. A real-life opamp introduces the
following limitations:

\begin{itemize}
  \item The minimum output voltage $\rm V_{OL}(max)$ is typically a few
    mV to a few dozen mV above 0~V, even if the opamp is ``rail-to-rail'', and
  \item the output can be shifted up or down by the maximum input offset
    voltage ($\rm V_{OS}$) multiplied by the loop gain of the opamp circuit.
\end{itemize}

We therefore need an opamp that fulfills the following criteria,
in addition to the operating voltage range of 2.5--2.75~V:
$\rm V_{OL}(max)\le 40~mV$ and $\rm |V_{OS}(max)|\cdot \beta\le 40~mV$,
with a loop gain of $\rm\beta = \sfrac{R6}{R5} = \FPrnd{1}{R6 / R5}$.

Furthermore, since inputs of the opamp can raise above its supply voltage
when VINTANA2 is disabled, the inputs must be clamped to the positive
supply voltage.%
\footnote{Some opamps (e.g., the microchip MCP6071 series) only clamp their
inputs to ground and rely on a Zener diode (or similar) to suppress
excursions above the positive supply voltage. Such a chip would be unsuitable
for our purposes.}

The Analog Devices ADA4505-1 \cite{ADA4505-1} fulfills all the above
criteria, draws only $\rm 10~\mu A$, and is available in a space-saving
$\rm 1.4\times 0.9\times 0.6~mm^3$ chip scale package.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Individual monitoring of each SIM}
\label{dual}

As mentioned above, the circuit could be extended to monitor each
SIM individually.
To do so,
a second opamp is needed (e.g., by replacing the ADA4505-1 with the
dual-channel ADA4505-2), and R1 and R4 to R6 have to be
duplicated for the second channel. R2 and R3 can be shared. Since
the maximum load current is then 50~mA instead of 100~mA, R1 should be
increased to $4~\Omega$. To avoid clipping at the maximum current,
R6 may be reduced to $\rm 1~M\Omega$.

% -----------------------------------------------------------------------------

\appendix
\section{Placement of second SIM holder}

We use the Amphenol 10100271 \cite{10100271} SIM card holder with an
overall thickness of 1.95 mm, for standard Mini-SIM of
$\rm 25~mm\times 15~mm\times 0.76~mm$.
The card is pushed in to insert and pushed again to eject.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Vertical stacking}

The card holder is placed such that the opening for the SIM card consists
of one or two U-shaped cut-outs in the (former) N900 case and/or the spacer
frame. It would be preferable to have the cut-out only in the spacer frame,
over which we have full design and manufacturing control, but no suitable
location is available.

We therefore place it on the S2 surface, i.e., the top side of the LOWER
PCB, as shown in the following drawing:

\begin{center}
\includegraphics[scale=0.9]{stack.pdf}
\end{center}

Additional dimensions and the underlying measurement data can be found on
the ``Stacking and measurements'' page at
\url{https://neo900.org/stuff/werner/stacking/}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Distance to wall}

The card holder is placed such that its edge is 0.7 mm from the inside
of the case wall. The following table shows the nominal positions relative
to card holder and either side of the wall for different card states:

\begin{tabular}{l|ccc}
  State	& \multicolumn{3}{c}{Position of card edge (mm)} \\
	& \multicolumn{3}{c}{relative to $\ldots$} \\
	& Card	& \multicolumn{2}{c}{Case wall} \\
	& holder & Inside & Outside \\
  \hline
  Ejected	& 6.0	& 5.3	& 3.8 \\
  Inserted	& 2.0	& 1.3	& $-0.2$ \\
  Pushed	& 0.8	& 0.1	& $-1.4$ \\
\end{tabular}

Ensuring that the position relative to the inside of the wall never becomes
negative prevents the card from sliding up inside the case, and getting stuck.


% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{X placement}

The placement of the second SIM card holder along the Y and Z axes is
determined by clearly defined mechanical constraints. We have more leeway
on the X axis. The following drawing suggests an approximate placement of
the three tall components that are located on S2:

\begin{center}
\includegraphics[scale=0.9]{big3.pdf}
\end{center}

The basic idea is to place these components under the keyboard area.
When typing on the keyboard, the PCB flexes a little. This causes
mechanical stress on chips mounted on the other side of the PCB, and
on their solder joints. Such stress may eventually lead
to device failure. We therefore want to avoid placing components under
the keyboard on S3, especially no large chips.

Meanwhile, the modem and WLAN modules and the second SIM card holder
are too tall to leave much room for other components on the PCB above
them. We should therefore try to overlap the area where we
do not wish to place large components as much as possible with
the area where we can only place very thin (if any) components.

The above placement is merely a suggestion. Layout will encounter
further constraints and therefore has the final say.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Case cut-out}

{\bf To Do}

% -----------------------------------------------------------------------------

\clearpage
\begin{thebibliography}{8}
\input simsw.bbl
\end{thebibliography}

\end{document}
