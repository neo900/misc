#!/usr/bin/perl

require "lib.pl";


# ===== Model =================================================================

#
# Positions are described as viewed from above onto the keyboard
#


sub conv_nocorr
{
	local ($x, $y) = @_;

	return ($ox + $x, -($oy + $y));
}


sub conv_corr
{
	local ($x, $y) = @_;

	# correct for 2.5D scan X error
	return &conv_nocorr($x / 0.998, $y);
}


sub conv_eagle
{
	local ($x, $y) = @_;

	return &conv_nocorr(-$x, $y);
}


#
# @@@ This is reversed with respect to main.pl because we used the optical
# scan as the principal reference.
#
$outconv = \&conv_nocorr;

&eagle if $ARGV[0] eq "-e";
$outconv = \&conv_corr if $ARGV[0] eq "-c";


if ($eagle) {
	$outconv = \&conv_eagle;
	($ox, $oy) = (136.0, 9.7);
	$flip = 1;
} else {
	($ox, $oy) = (1.4, 37.5);
}


# ----- Bottom edge -----------------------------------------------------------

&rlineto(0, -33.5);

# ---- Lower right corner -----------------------------------------------------

&rcbend(0.4, -0.4);

# ----- Right edge ------------------------------------------------------------

&rlineto(10.0, 0);
&rcbend(0.8, -0.4);
&rlineto(7.8, 0);
&rcbend(0.3, 0.2);
&rdbend(3.0, 1.6);
&rlineto(0.65, 0);

# 90 degree corner !

# ----- Top edge --------------------------------------------------------------

&rlineto(0, 14.25)

# ----- Upper left corner of main section -------------------------------------

&rcbend(-0.4, 0.4);

# ----- Right side of bay -----------------------------------------------------

&rlineto(-1.75, 0);
&rdbend(-0.8, 0.8);
&rlineto(0, 1.2);
&rcbend(-0.4, 0.4);
&rlineto(-1.2, 0);
&rdbend(-0.4, -0.4);
&rlineto(0, -0.15);
&rcbend(-0.8, -0.8);
&rlineto(-11.2, 0);

# ----- Lower right corner of the bay ------------------------------------------

&rdbend(-0.8, 0.8);

# ----- Bottom edge of the bay ------------------------------------------------

&rlineto(0, 11.65);

# ----- Lower left corner of the bay ------------------------------------------

&rcbend(0.8, 0.8);

# ----- Left side of the bay --------------------------------------------------

&rlineto(10.4, 0);

# ----- Upper right corner of the flash section -------------------------------

&rdbend(0.5, 0.5);

# ----- Top edge of the flash section -----------------------------------------

&rlineto(0, 3.05);

# ----- Upper left corner of the flash section --------------------------------

&rcbend(-0.4, 0.4);

# ----- Left edge -------------------------------------------------------------

&rlineto(-16.1, 0);

# ----- Lower left corner -----------------------------------------------------

&rdbend(-0.4, -0.4);

&pos;
