#!/usr/bin/perl


#
# Variables:
#
# cx, cy 	current position
# ox, oy	origin
# tx, ty	beginning of polyline
#

$tx = $cx = 0;
$ty = $cy = 0;


# ----- Output ----------------------------------------------------------------


sub outconv_default
{
	return @_;
}


$outconv = \&outconv_default;


sub end
{
	$tx = $cx;
	$ty = $cy;
	return unless $drawing;
	if ($eagle) {
		print ";\n";
	} else {
		print "\n";
	}
	$drawing = 0;
}


sub line
{
	if (!$drawing) {
		my ($x, $y) = &{$outconv}($tx, $ty);
		if ($eagle) {
			print "Wire ($x $y)";
		} else {
			print "$x $y\n";
		}
		$drawing = 1;
	}

	my ($x, $y) = &{$outconv}($cx, $cy);
	if ($eagle) {
		print " ($x $y)";
	} else {
		print "$x $y\n";
	}
}


# ----- Eagle script mode -----------------------------------------------------


sub eagle
{
	$eagle = 1;
#	print "board;\n";
	print "Grid 1 mm;\n";
	print "Layer Dimension;\n";
	print "Set Wire_Bend 2;\n";
}


# ----- Arcs ------------------------------------------------------------------


$ARC_STEP = 0.02;
$PI = 3.141592653;


sub hypot
{
	local ($x, $y) = @_;

	return sqrt($x * $x + $y * $y);
}


sub arc_common
{
	local ($ax, $ay, $bx, $by, $ex, $ey, $cw) = @_;

	my $r = &hypot($ax - $bx, $ay - $by) / 2;
	my $mx = ($ax + $bx) / 2;
	my $my = ($ay + $by) / 2;
	my $a0 = atan2($ay - $my, $ax - $mx);
	my $a1 = atan2($ey - $my, $ex - $mx);
	$a1 += $PI * 2 if $a1 < $a0;
	($a0, $a1) = ($a1, $a0) if $cw;
	$a1 += $PI * 2 if $a1 < $a0;
	my $a = $a1 - $a0;
	my $n = int(($r * $a / 2 / $PI) / $ARC_STEP);

	$n = 2 if $n < 2;
	&moveto($mx + $r * cos($a0), $my + $r * sin($a0));
	for (my $i = 1; $i <= $n; $i++) {
		my $f = $i / $n;
		&lineto($mx + $r * cos($a0 + $a * $f),
		    $my + $r * sin($a0 + $a * $f));
	}
	&moveto($ex, $ey);
}


sub eagle_arc
{
	local ($dir, $ax, $ay, $bx, $by, $ex, $ey) = @_;

	$dir = $dir eq "cw" ? "ccw" : "cw" unless $flip;

	($ax, $ay) = &{$outconv}($ax, $ay);
	($bx, $by) = &{$outconv}($bx, $by);
	($ex, $ey) = &{$outconv}($ex, $ey);

	print "arc $dir ($ax $ay) ($bx $by) ($ex $ey);\n";
}


sub arc_ccw
{
	if ($eagle) {
		&end;
		&eagle_arc("ccw", @_);
	} else {
		&arc_common(@_, 0);
	}
}


sub arc_cw
{
	if ($eagle) {
		&end;
		&eagle_arc("cw", @_);
	} else {
		&arc_common(@_, 1);
	}
}


sub tan
{
	local ($x) = @_;

	return sin($x) / cos($x);
}


sub arc_curve
{
	local ($ax, $ay, $bx, $by, $c) = @_;

	my $dx = $bx - $ax;
	my $dy = $by - $ay;
	my $mx = ($ax + $bx) / 2;
	my $my = ($ay + $by) / 2;
	my $d = &hypot($dx, $dy);

	my $f = 0.5 / &tan(abs($c) * $PI / 360);

	# V is the center of the circle from which we want an arc

	my $vx;
	my $vy;
	if ($c > 0) {
		$vx = $mx - $dy * $f;
		$vy = $my + $dx * $f;
	} else {
		$vx = $mx + $dy * $f;
		$vy = $my - $dx * $f;
	}

	# W is the diameter point

	my $wx = $ax + 2 * ($vx - $ax);
	my $wy = $ay + 2 * ($vy - $ay);
	if ($c > 0) {
		&arc_ccw($ax, $ay, $wx, $wy, $bx, $by);
	} else {
		&arc_cw($ax, $ay, $wx, $wy, $bx, $by);
	}
	&moveto($bx, $by);
}


#
# Continuous bend: continuous at starting point, discontinuous at end point
#

sub rcbend
{
	local ($x, $y) = @_;

	my $h = $x * $x + $y * $y;

	my ($tx, $ty) = ($cx, $cy);
	$cx += $x;
	$cy += $y;
	my ($ex, $ey) = ($cx, $cy);

	my @a = ();

	if ($x > 0) {
		if ($y > 0) {
			if ($x > $y) {
				@a = (0, 0, $h / $y);
			} else {
				@a = (1, $h / $x, 0);
			}
		} else {
			if ($x > -$y) {
				@a = (1, 0, $h / $y);
			} else {
				@a = (0, $h / $x, 0);
			}
		}
	} else {
		if ($y > 0) {
			if (-$x > $y) {
				@a = (1, 0, $h / $y);
			} else {
				@a = (0, $h / $x, 0);
			}
		} else {
			if (-$x > -$y) {
				@a = (0, 0, $h / $y);
			} else {
				@a = (1, $h / $x, 0);
			}
		}
	}

	my $cw = shift @a;

	@a = ($tx, $ty, $tx + $a[0], $ty + $a[1], $cx, $cy);
	if ($cw) {
		&arc_cw(@a);
	} else {
		&arc_ccw(@a);
	}

	&moveto($ex, $ey);
}


#
# Discontinuous bend: continuous at starting point, discontinuous at end point
#

sub rdbend
{
	local ($x, $y) = @_;

	&rmoveto($x, $y);
	&rcbend(-$x, -$y);
	&rmoveto($x, $y);
}


if (0) {
	#&moveto(0, 0);
	#&lineto(1, 0);
	#&arc_common(0, 0, 0, -2, 1, -1, 0);
	#&rsarc(1, -2);
	# "fox" shape

	&moveto(2, 1);
	&rcbend(2, 1);
	&rcbend(1, 2);
	&rcbend(-2, 1);
	&rcbend(-1, -2);
	&rcbend(-2, -1);
	&rcbend(1, -2);
	&rcbend(2, -1);
	&rcbend(-1, 2);

	# "boot" shape

	&moveto(6, -1);
	&rdbend(2, 1);
	&rdbend(1, 2);
	&rdbend(-2, 1);
	&rdbend(-1, -2);
	&rdbend(-2, -1);
	&rdbend(1, -2);
	&rdbend(2, -1);
	&rdbend(-1, 2);

	&end;
	exit(1);
}


# ----- Circle ----------------------------------------------------------------


sub circle
{
	local ($mx, $my, $r) = @_;

	if ($eagle) {
		my ($rx, $ry) = &{$outconv}($mx + $r, $my);
		($mx, $my) = &{$outconv}($mx, $my);
		print "circle ($mx $my) ($rx $ry);\n";
		return;
	}

	my $n = int(($r * 2 / $PI) / $ARC_STEP);

	$n = 2 if $n < 2;

	&moveto($mx + $r, $my);
	for (my $i = 1; $i <= $n; $i++) {
		my $a = 2 * $PI * $i / $n;
		&lineto($mx + $r * cos($a), $my + $r * sin($a));
	}
}


# ----- Postscript-line commands ----------------------------------------------


sub origin
{
	$ox = $cx;
	$oy = $cy;
}


sub moveto
{
	local ($x, $y) = @_;

	$cx = $x;
	$cy = $y;
	&end;
}


sub lineto
{
	local ($x, $y) = @_;

	$cx = $x;
	$cy = $y;
	&line;
}


sub rmoveto
{
	local ($x, $y) = @_;

	$cx += $x;
	$cy += $y;
	&end;
}


sub rlineto
{
	local ($x, $y) = @_;

	$cx += $x;
	$cy += $y;
	&line;
}


sub pos
{
	print STDERR "$cx $cy\n";
}


return 1;
