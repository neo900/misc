#!/usr/bin/perl

require "lib.pl";


# ===== Model =================================================================

#
# Positions are described as viewed from above onto the keyboard
#


sub conv_nocorr
{
	local ($x, $y) = @_;

	return ($ox + $x, -($oy + $y));
}


sub conv_corr
{
	local ($x, $y) = @_;

	# correct for 2.5D scan X error
	return &conv_nocorr($x * 0.998, $y);
}


$outconv = \&conv_corr;

&eagle if $ARGV[0] eq "-e";
$outconv = \&conv_nocorr if $ARGV[0] eq "-c";

($ox, $oy) = (8.4, 57.15) if !$eagle;
($ox, $oy) = (6.05, -0.3) if $eagle;


# ----- Bottom edge -----------------------------------------------------------

#
# General structure: straight baseline with
# - long recesses of a depth of 0.5 mm and
# - short protrusions of 0.3 mm.
#

# near Shift Z (key col 1 2)

&rlineto(16.2, 0);

# near X C (key col 3 4)

&rdbend(1.1, -0.5);
&rlineto(7.4, 0);
&rcbend(1.1, 0.5);

# near V (key col 5)

&rlineto(8.7, 0);

# near B (key col 6)

&rcbend(0.8, 0.3);
&rlineto(4.0, 0);
&rdbend(0.8, -0.3);
&rlineto(2.1, 0);
&rdbend(1.1, -0.5);

# near N (key col 7)

&rlineto(7.65, 0);

# near M (key col 8), approaching Space

&rcbend(1.0, 0.5);
&rlineto(2.1, 0);
&rcbend(0.8, 0.3);
&rlineto(4.25, 0);
&rdbend(0.8, -0.3);

# near Space (key col 9 and 10)

&rlineto(8.4, 0);
&rdbend(0.8, -0.5);

# near Left (key col 11)

&rlineto(7.85, 0);
&rcbend(1.1, 0.5);

# near Down Right (key col 12 13)

&rlineto(15.35, 0);

# ----- Lower right corner ----------------------------------------------------

&rdbend(0.5, -0.5);	# AMBIGUOUS
&rlineto(0, -6.35);
&rcbend(2.2, -2.2);	# AMBIGUOUS
&rlineto(3.7, 0);
&rdbend(0.6, -0.6);	# AMBIGUOUS

# ----- Right edge ------------------------------------------------------------

# Height of keyboard

&rlineto(0, -9.8);

# Right "bay"

&rdbend(-0.4, -0.8);
&rlineto(0, -16.3);
&rcbend(0.45, -0.8);

# Slightly inclined top end

&rlineto(-0.4, -12.9);	# inclined !

# ----- Upper right corner ----------------------------------------------------

&rdbend(-6.0, -5.5);	# CORRECT ASYMMETRY (consider inclination ?)

# ----- Top edge --------------------------------------------------------------

# Protrusion near some atenna

&rlineto(-9.8, 0);
&rdbend(-1.3, 0.95);

# Near IR

&rlineto(-7.00, 0);

# Camera capture button

&rdbend(-0.5, 0.5);	# AMBIGUOUS
&rlineto(0, 0.15);
&rcbend(-0.9, 0.9);	# AMBIGUOUS

&rlineto(-1.2, 0);

# EVQ-Q0G03K: cutout 5.1 x 2.85 mm, corner radius <= 1 mm

sub evq_q0g03k
{
	my $depth = 2.85 + 0.2;	# extra 0.2 mm

	&rdbend(-0.5, 0.5);
	&rlineto(0, $depth - 0.5 - 0.5);
	&rcbend(-0.5, 0.5);
	&rlineto(-5.1 + 2 * 0.5, 0);
	&rdbend(-0.5, -0.5);
	&rlineto(0, -$depth + 0.5 + 0.5);
	&rcbend(-0.5, -0.5);
}

&evq_q0g03k;

&rlineto(-3.1, 0);

&rdbend(-0.9, -0.9);	# AMBIGUOUS
&rlineto(0, -0.15);
&rcbend(-0.5, -0.5);	# AMBIGUOUS

# Near camera connector, passing POWER button

&rlineto(-14.25, 0);

# EVQ-P42B3M: cutout 5.1 x 1.65 mm, corner radius <= 0.5 mm

sub evq_p42b3m
{
	&rdbend(-0.5, 0.5);
	&rlineto(0, 1.65 - 0.5 - 0.4);
	&arc_curve($cx, $cy, $cx - 0.7, $cy + 0.4, 130);
	&rlineto(-5.1 + 2 * 0.7, 0);
	&arc_curve($cx, $cy, $cx - 0.7, $cy - 0.4, 130);
	&rlineto(0, -1.65 + 0.5 + 0.4);
	&rcbend(-0.5, -0.5);
}

&evq_p42b3m;

&rlineto(-3.95, 0);

# Small stepped "bay"

&rdbend(-0.7, 0.3);
&rlineto(-5.25, 0);
&rcbend(-0.8, -0.3);
&rlineto(-0.1, 0);
&rcbend(-0.9, -0.4);

# Volume button (right)

&rlineto(-2.6, 0);

&evq_p42b3m;

#exit;
&rlineto(-1.4, 0);

# Opening for rocker button

&rdbend(-0.5, 0.5);	# AMBIGUOUS
&rlineto(0, 0.25);
&rcbend(-0.9, 0.9);	# AMBIGUOUS
&rlineto(-0.4, 0);
&rdbend(-0.9, -0.9);	# AMBIGUOUS
&rlineto(0, -0.25);
&rcbend(-0.5, -0.5);	# AMBIGUOUS

# Volume button (left)

&rlineto(-1.4, 0);
&evq_p42b3m;
&rlineto(-2.25, 0);

# Protrusion near elongated hole

&rcbend(-1.0, -0.5);
&rlineto(-5.8, 0);
&rcbend(-0.4, 0.3);
&rdbend(-0.9, 0.55);
&rlineto(-0.4, 0);

# ----- Upper left corner -----------------------------------------------------

&rdbend(-5.2, 5.2);	# AMBIGUOUS

# ----- Left edge -------------------------------------------------------------

# Long straight edge to the BOB

&rlineto(0, 35.2);

# BOB connection

&rcbend(0.6, 0.6);	# AMBIGUOUS
&rlineto(1.05, 0);
&rdbend(0.4, 0.4);	# AMBIGUOUS
&rlineto(0, 7.5);

# ----- Lower left corner -----------------------------------------------------

&rcbend(0.6, 0.6);	# AMBIGUOUS
&rlineto(1.0, 0);
&rdbend(1.9, 1.9);	# AMBIGUOUS
&rlineto(0, 2.95);
&rcbend(0.5, 0.5);	# AMBIGUOUS

&pos;

# ----- Camera cut-out --------------------------------------------------------

#
# This is only a rough approximation since our camera has no SMIA95 socket.
#

$r = 1.0;
&moveto(1.75 + $r, -23.3);
&rlineto(12 - 2 * $r, 0);
&arc_curve($cx, $cy, $cx + $r, $cy - $r, -180);
&rlineto(0, -12.3 + 2 * $r);
&arc_curve($cx, $cy, $cx - $r, $cy - $r, -180);
&rlineto(-12 + 2 * $r, 0);
&arc_curve($cx, $cy, $cx - $r, $cy + $r, -180);
&rlineto(0, 12.3 - 2 * $r);
&arc_curve($cx, $cy, $cx + $r, $cy + $r, -180);

# ----- Round holes -----------------------------------------------------------

&circle(-1.55, -26.35, 1.5);
&circle(95.7, -26.35, 1.4);
&circle(95.7, -52.4, 1.5);

# ----- Elongated hole --------------------------------------------------------

$lx = -1.1;
$ly = $lx / 4;

$wy = -2.5;
$wx = -$wy / 4;

&moveto(-0.6, -51.0);
&rlineto($lx, $ly);
&arc_curve($cx, $cy, $cx + $wx, $cy + $wy, 180);
&rlineto(-$lx, -$ly);
&arc_curve($cx, $cy, $cx - $wx, $cy - $wy, 180);

# ----- Small positioning holes -----------------------------------------------

$d = 1.0;
$m = 1.6 - $d;

&moveto(7.25, -2.35);
&rlineto($m, 0);
&arc_curve($cx, $cy, $cx, $cy + $d, 180);
&rlineto(-$m, 0);
&arc_curve($cx, $cy, $cx, $cy - $d, 180);

$d = 1.0;
$m = 1.6 - $d;

&moveto(0.95, -6.1);
&rlineto(0, $m);
&arc_curve($cx, $cy, $cx - $d, $cy, 180);
&rlineto(0, -$m);
&arc_curve($cx, $cy, $cx + $d, $cy, 180);

$d = 1.0;
&circle(0.45, -22.9, $d / 2);
