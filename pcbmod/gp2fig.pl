#!/usr/bin/perl

$xo = shift @ARGV if $ARGV[0] =~ /^-?\d+(\.\d*)?/;
$yo = shift @ARGV if $ARGV[0] =~ /^-?\d+(\.\d*)?/;

print <<"EOF";
#FIG 3.2
Landscape
Center
Metric
A4
100.00
Single
-2
1200 2
EOF


sub map
{
	local ($x, $y) = @_;

	$x += $xo;
	$y += $yo;
	$x = $x * 1200.0 / 25.4;
	$y = $y * 1200.0 / 25.4;
	$x = int($x + 0.5);
	$y = int(-$y + 0.5);
	return "$x $y";
}


sub flush
{
	return if $#p <= 0;
	print "2 1 0 2 ";	# obj sub style thick
	print "0 7 50 -1 ";	# fg bg depth pen
	print "-1 0.0 1 1 ";	# fill style_val join(round) cap(round)
	print "-1 0 0 ", $#p + 1, "\n";
				# radius fwd bwd npoints
	print "\t", join(" ", map(&map(@{ $_ }), @p)), "\n";
	undef @p;
}


while (<>) {
	if (/^\s*$/) {
		&flush;
		next;
	}
	die unless /^(-?\d+(\.\d*)?)\s+(-?\d+(\.\d*)?)$/;
	push(@p, [ $1, $3 ]);
}
&flush;
