#!/usr/bin/perl

require "lib.pl";


sub conv
{
        local ($x, $y) = @_;

        return ($ox + $x, $oy + $y);
}


$outconv = \&conv;
($ox, $oy) = (2.362, -57.95);

while (<>) {
	die unless /<wire.*x1="([^"]+)".*y1="([^"]+)".*x2="([^"]+)".*y2="([^"]+)(.*curve="([^"]+))?"/;
	if (defined $6) {
		&arc_curve($1, $2, $3, $4, $6);
	} else {
		&moveto($1, $2);
		&lineto($3, $4);
	}
}
