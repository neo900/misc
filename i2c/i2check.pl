#!/usr/bin/perl
#
# i2c/i2check.pl - Validate I2C address assignments
#
# Written 2016 by Werner Almesberger
# Copyright 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

#
# Entry format:
#
# name	pattern [/ pattern ...] [= pattern]
#
#   name is the name of the respective device
#   pattern / ... is a sequence of one or more choices that correspond
#	to alternate configurations. E.g., 000 / 001 would be either 000
#	or 001.
#   = pattern assigns the specified address to the device.
#
# Characters in I2C address pattern:
#
# 0	bit is always 0
# 1	bit is always 1
# a	bit is configurable (all "a"s are independent)
# b-w	bit is configurable and coordinated with other assignment
# x	device uses 0 and 1
# .	separator, ignored
#
# Choice patterns can contain all of the above. Assignment patterns cannot
# contain "a" or variables, but they can contain "x" (but only if present
# in the corresponding choice pattern).
#
# Patterns can also be two-digit hex numbers in the 0xnn format.
#
# Example:
#
# 0a and 1a can occupy (00, 10), (01, 10), (00, 11), or (01, 11)
# 0b and 1b can occupy (00, 10) or (01, 11)
# x0 occupies 00 and 01
#


#----- Input processing -------------------------------------------------------


$X = "[0-9a-f]";
$P = "[01.a-x]+|0x${X}{2}";


sub dehex
{
	local ($s) = @_;

	if ($s =~ /^0x$X{2}$/) {
		my $v = hex($s);

		die "$s is not a valid I2C address" if $s > 0x7f;
		return sprintf("%07b", $v);
	}
	$s =~ s/\.//g;
	die "expect 7 binary digits" unless length($s) == 7;
	return $s;
}


while (<>) {
	s/#.*//;
	next if /^\s*$/;
	s/\s*$//;

	die unless /^(\S+)\s+(($P)(\s*\/\s*($P))*)\s*(=\s*($P))?$/;

	$name = $1;
	$patterns = $2;
	$fixed{$name} = &dehex($7) unless $7 eq "";

	die "duplicate entry \"$name\"" if exists $p{$name};

	$patterns =~ s/\.//g;
	@p = split(/\s*\/\s*/, $patterns);
	for (@p) {
		$_ = &dehex($_);
	}
	$p{$name} = [ @p ];
}

for (values %p) {
	$s = join("", @{ $_ });
	$s =~ s/[01ax]//g;
	for (split("", $s)) {
		$vars++ unless defined $var{$_};
		$var{$_} = 0;
	}
}


#----- Check combinations -----------------------------------------------------


sub do_device
{
	local ($name) = $name;

	my $fix = $fixed{$name};

	print "\t$name";
	print " = $fix" if defined $fix;
	print "\n";

	for $pattern (@{ $p{$name} }) {
		$as = 0;
		$xs = 0;
		for (split("", $pattern)) {
			$as++ if $_ eq "a";
			$xs++ if $_ eq "x";
		}

		# Expand "a" and variables

		for ($a = 0; $a != 1 << $as; $a++) {
			my $found = 0;

			print "\t\t";

			$pa = "";
			$tmp_a = $a;
			for (split("", $pattern)) {
				if ($_ eq "a") {
					$tmp_a <<= 1;
					$pa .= ($tmp_a >> $as) & 1;
				} elsif (defined $var{$_}) {
					$pa .= $var{$_};
				} else {
					$pa .= $_;
				}
			}

			if (defined $fix && $fix eq $pa) {
				die "duplicate match" if $found;
				$found = 1;
				$found_once{$name} = 1;
			}

			# Expand "x"

			for ($x = 0; $x != 1 << $xs; $x++) {
				$tmp_x = $x;
				$px = "";
				for (split("", $pa)) {
					if ($_ eq "x") {
						$tmp_x <<= 1;
						$px .= ($tmp_x >> $xs) & 1;
					} elsif ($_ =~ /^[01]$/) {
						$px .= $_;
					} else {
						die;
					}
				}
				print "$px ";
				if ($found) {
					die "$name: $px is already taken by " .
					    $taken{$px}
					    if defined $taken{$px};
					$taken{$px} = $name;
					$taken_once{$px} = $name;
				}
			}
			print "\n";
			return 1 if $found;
		}
	}
	return 0;
}


for ($v = 0; $v != 1 << $vars; $v++) {
	$tmp = $v;
	for (sort keys %var) {
		$var{$_} = $tmp & 1;
		print "$_ = $var{$_} ";
		$tmp >>= 1;
	}
	print "\n";

	undef %taken;
	$found_all = 1;
	for $name (sort keys %p) {
		$found_all &= &do_device($name);
	}
	last if $found_all;
}

#----- Show available addresses -----------------------------------------------

print "Always available:\n";
$last = -1;
for (sort keys %taken_once) {
	$this = oct("0b$_");
	if ($last + 2 == $this) {
		printf("\t0x%02x\n", $last + 1);
	} elsif ($last + 2 < $this) {
		printf("\t0x%02x-0x%02x\n", $last + 1, $this - 1);
	}
	$last = $this;
}
if ($last == 126) {
		print "\t0x7f\n";
} elsif ($last < 126) {
		printf("\t0x%02x-0x7f\n", $last + 1);
}

#----- Successful termination -------------------------------------------------

sub flush
{
	if (defined $last_name) {
		if ($last == $first) {
			print "\n";
		} else {
			printf("-0x%02x\n", $last);
		}
	}
}


if ($found_all) {
	printf "Assignments:\n";
	$last_name = undef;
	$first = 999;
	$last = 999;
	for (sort keys %taken) {
		$name = $taken{$_};
		$n = oct("0b$_");
		if ($name ne $last_name || $last != $n - 1) {
			&flush;
			printf("\t$name%s0x%02x",
			    "\t" x (2 - (length($name) >> 3)), $n);
		}
		$first = $n if $last_name ne $name;
		$last_name = $name;
		$last = $n;
	}
	&flush;
	
	exit(0);
}

#----- Report assignments that never worked (0 = 1 or such) -------------------

for (keys %p) {
	warn "$_: $fixed{$_} was never found"
	    if defined $fixed{$_} && !$found_once{$_};
}

die "could never assign all addresses\n";
