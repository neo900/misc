#!/usr/bin/perl
#
# block/anntest.pl - Test expansion of annotation
#
# Written 2014 by Werner Almesberger
# Copyright 2014 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


require "libann.pl";


sub usage
{
	print STDERR
"usage: $0 [-t templates.atp ...] template=text\n";
	exit(1);
}


while ($ARGV[0] =~ /^-./) {
	if ($ARGV[0] eq "-t") {
		shift @ARGV;
		&usage unless defined $ARGV[0];
		push(@atp, shift @ARGV);
	} else {
		&usage;
	}
}

&usage if $#ARGV;

for (@atp) {
	&ann_read_template($_);
}

print &ann_expand($ARGV[0]);
