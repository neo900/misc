#!/usr/bin/perl
#
# block/map2css.pl - Convert AREAs from image map to CSS items
#
# Written 2014-2016 by Werner Almesberger
# Copyright 2014-2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

require "libann.pl";


sub usage
{
	print STDERR <<"EOF";
usage: $0 [-a file.ann ...] [-n name] [-u ythresh] [-t file.atp ...]
       [-w width] [-x x-offset] [-y y-offset] [file ...]
EOF
	exit(1);
}


$map = "map";
$xoffset = $yoffset = "0px";
$width = "300px";
while ($ARGV[0] =~ /^-/) {
	$opt = shift @ARGV;
	if ($opt eq "-a") {
		&usage unless defined $ARGV[0];
		push(@ann, shift @ARGV);
	} elsif ($opt eq "-n") {
		$map = shift @ARGV;
		&usage unless defined $map;
	} elsif ($opt eq "-t") {
		&usage unless defined $ARGV[0];
		push(@atp, shift @ARGV);
	} elsif ($opt eq "-u") {
		$up = shift @ARGV;
		&usage unless defined $up;
	} elsif ($opt eq "-w") {
		$width = shift @ARGV;
		&usage unless defined $width;
	} elsif ($opt eq "-x") {
		$xoffset = shift @ARGV;
		&usage unless defined $xoffset;
	} elsif ($opt eq "-y") {
		$yoffset = shift @ARGV;
		&usage unless defined $yoffset;
	} else {
		&usage;
	}
}
&usage if $ARGV[0] =~ /^-/;

# ----- read annotation templates ---------------------------------------------

for (@atp) {
	&ann_read_template($_);
}

# ----- process annotations ---------------------------------------------------

sub execute
{
	local ($c) = @_;

	if ($c =~ /^reset:$/) {
		delete $css{$curr};
	} elsif ($c =~ /^css:/) {
		$css{$curr} .= $';
	} elsif ($c =~ /^movex:/) {
		$movex{$curr} += $';
	} elsif ($c =~ /^movey:/) {
		$movey{$curr} += $';
        }
	return undef;
}


for (@ann) {
	open(ANN, $_) || die "$_: $!";
	undef $curr;
	while (<ANN>) {
		next if /^\s*#/;
		next if /^\s*$/;
		if (/^(\S+):\s*$/) {
			$curr = $1;
			next;
		}
		die unless /^\s*/;
		die "no current section" unless defined $curr;
		$t = &ann_expand($');
		&ann_commands($t, \&execute);
	}
	close(ANN);
}

# ----- process the map -------------------------------------------------------

while (<>) {
	next unless /COORDS="([^"]+)".*href="([^"]+)" alt=""/;
	@c = split(",", $1);
	$id = $2;
	undef $x0;
	undef $x1;
	undef $y0;
	undef $y1;
	while ($#c != -1) {
		($x, $y) = splice(@c, 0, 2);
		$x0 = $x if $x < $x0 || !defined $x0;
		$x1 = $x if $x > $x1 || !defined $x1;
		$y0 = $y if $y < $y0 || !defined $y0;
		$y1 = $y if $y > $y1 || !defined $y1;
	}
	@c = ($x0, $y0, $x1 - $x0, $y1 - $y0);

	print "\t#$map div.t_${map}_$id {\n";
	for ("left", "top", "width", "height") {
		print "\t\t$_:" . shift(@c)."px;\n";
	}
	print "\t}\n\n";

	print "\t#$map li:hover div.t_${map}_$id {\n";
	print "\t\tbackground-color:rgba(255,255,255,0.333);\n";
	print "\t\tbox-shadow:0px 0px 5px rgba(0,0,0,0.666);\n";
	print "\t}\n\n";

	print "\t#$map li:hover div.t_${map}_$id div {\n";
	if ($y0 > $up && defined $up) {
		$y = $y1 - $y0 > $yoffset ? $yoffset : ($y1 - $y0) / 2;
		$y += $movey{$id};
		print "\t\tposition:absolute;\n";
		print "\t\ttop:initial;\n";
		print "\t\tbottom:$y;\n";
	} else {
		$y = $yoffset + $movey{$id};
		print "\t\tposition:relative;\n";
		print "\t\ttop:$y;\n";
	}
	print "\t\tdisplay:block;\n";
	print "\t\twidth:$width;\n";
	$x = $x1 - $x0 > $xoffset ? $xoffset : ($x1 - $x0) / 2;
	$x += $movex{$id};
	print "\t\tleft:$x;\n";
	print "\t\tborder:2px solid #000;\n";
	print "\t\tbackground:#fff;\n";
	print "\t\tpadding:5px;\n";
	print "\t\tz-index:1;\n";
	print "\t\tbox-shadow:1px 1px 3px 1px rgba(0,0,0,0.5);\n";
	print "\t$css{$id}\n" if defined $css{$id};
	print "\t}\n\n";
}
