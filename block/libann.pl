#!/usr/bin/perl
#
# block/libann.pl - Helper functions to process annotations and templates
#
# Written 2014-2016 by Werner Almesberger
# Copyright 2014-2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

#
# Template syntax:
#
# NAME:
#    expansion
#    ...
#
# NAME: RE
#    expansion
#    ...
#

#
# Annotation syntax:
#
# NAME:
#    content
#    ...
#
# In annotations, templates are of the form NAME=value
# If the template has no RE, every $$ is changed to "value".
# If the template as a RE, it is matched as /^RE/ and $n substitution is
# performed (see below). At the end every $$ is changed to $'.
#
# $n substitution has three forms:
#
# Unconditional:
# $n		is replaced with the value
#
# If:
# ${n:text}	is replaced with "text" if $n is defined, else removed
#
# If-not:
# ${n!text}	is replaced with "text" if $n is not defined, else removed
#
# All $n are processed after "if" and "if-not".
#

#
# Global variables:
#
# $ann_temp{name}	template
# $ann_re{name}		regular expression of template
#

# ----- read annotation templates ---------------------------------------------


sub ann_read_template
{
	local ($file) = @_;
	local ($curr);

	open(ATP, $file) || die "$file: $!";
	undef $curr;
	while (<ATP>) {
		next if /^\s*#/;
		next if /^\s*$/;
		if (/^([A-Z][A-Z0-9_]*|\*):\s*(\s(.+?)\s*)?$/) {
			$curr = $1;
			$ann_re{$curr} = $3;
			next;
		}
		die unless /^\s*/;
		die "no current template" unless defined $curr;
		$ann_templ{$curr} .= $';
	}
	close ATP;
}


# ----- expand an annotation --------------------------------------------------


sub conditionals
{
	local ($x, @a) = @_;

	while (1) {
		my $y = $x;

		for (my $i = 1; $i != 9; $i++) {
			if (defined $a[$i-1]) {
				$x =~ s/\$\{$i:([^{}]*?)\}/\1/g;
				$x =~ s/\$\{$i![^{}]*?\}//g;
			} else {
				$x =~ s/\$\{$i:[^{}]*?\}//g;
				$x =~ s/\$\{$i!([^{}]*?)\}/\1/g;
			}
		}
		return $x if $x eq $y;
	}
}


sub ann_expand
{
	local ($s) = @_;
	local ($t, $changed) = ("");

	while (1) {
		$changed = 0;
		undef $t;
		while ($s =~ /\b([A-Z][A-Z0-9_]+)=("([^"]*)"|([^"].*?))\s*$/m) {
			$t .= $`;
			$s = $';
			my $v = defined $3 ? $3 : $4;
			my $n = $1;
			my $x = $ann_templ{$n};
			die "unknown template \"$1\""
			    unless defined $x;
			if ($ann_re{$n} ne "") {
				my @a = $v =~ /^$ann_re{$n}/;
				die "no RE match for template \"$n\""
				    unless @a;
				$v = $';
				$x = &conditionals($x, @a);
				for (my $i = 1; $i != $#a+2; $i++) {
					$x =~ s/\$$i/$a[$i-1]/g;
				}
			}
			$x =~ s/\$\$/$v/g;
			$t .= $x;
			$changed = 1;
		}
		$s = $t.$s;
		last unless $changed;
	}
	$s =~ s/\\(.)/\1/g;
	return $s;
}


# ----- extract and remove commands -------------------------------------------

#
# Annotation commands:
# [fg:color]			change foreground color of box
# [bg:color]			change background color of box
# [fc:color]			change first text color (all parts)
# [sc:color]			change second text color (all parts)
# [fci:color]			like fc, but only if text is inside the box
# [sci:color]			like sc, but only if text is inside the box
# [ftN:new first text]		change Nth first text (0-based, 0 if no number)
# [stN:new second text]		change Nth second text (idem)
# [ssz:size]			size (points) of second text
# [reset:]			reset annotation
#
# [push:tag:text]		push text on the stack
# [pop:tag]			pop text from stack and insert. Tag must match.
# [drop:tag]			pop text and discard. Tag must match.
# [change:tag:text]		change text in first matching stack entry
#
# [set:var:text]		set/change variable (in the 1st pass)
# [get:var]			expand variable (in the 2nd pass)
#
# [swap:id]			swap current item with named item in sequence
#				of annotations (changes pop-up priority)
#
# [css:text]			additions to the item's CSS section
# [movex:offset]		move pop-up by X offset (can be negative)
# [movey:offset]		move pop-up by Y offset (can be negative)
#
# Color syntax:
# color		any of the xfig color names, in lower case, or a #rrggbb code
# color+N%	"tint", 0% to 100% (white), in steps of 5%.
# color-N%	"shade", 0% to 100% (black), in steps of 5%.
#


sub ann_commands
{
	local ($s, $exec, @user) = @_;
	local $t, @c;

	undef $t;
	undef @c;
	while ($s =~ /\[([a-z][a-z0-9]*):([^]]*)\]/) {
		$t .= $`;
		$s = $';
		local ($c, $v) = ($1, $2);
		die "unrecognized command \"$c\""
		    unless $c eq "fg" || $c eq "bg" ||
		    $c eq "fc" || $c eq "sc" ||
		    $c eq "fci" || $c eq "sci" ||
		    $c =~ /^ft\d*$/ || $c =~ /^st\d*$/ ||
		    $c eq "ssz" ||
		    $c eq "reset" ||
		    $c eq "push" || $c eq "pop" || $c eq "drop" ||
		    $c eq "change" ||
		    $c eq "set" || $c eq "get" ||
		    $c eq "swap" || $c eq "css" ||
		    $c eq "movex" || $c eq "movey";
		push(@c, "$c:$v");
		$t .= &{$exec}("$c:$v", @user) if defined $exec;
	}
	return ($t.$s, @c);
}


# -----------------------------------------------------------------------------


return 1;
