Using a local cache for PDFs
----------------------------

First-time setup
- - - - - - - -

1) Choose a directory for a local copy of the block diagram and go
   there, e.g.,

   mkdir /home/neo900/block
   cd /home/neo900/block

2) Download the HTML, CSS, and PNG parts of the block diagram, e.g.,

   wget https://neo900.org/stuff/block-diagrams/neo900/neo900.html
   wget https://neo900.org/stuff/block-diagrams/neo900/neo900.css
   wget https://neo900.org/stuff/block-diagrams/neo900/neo900.js
   wget https://neo900.org/stuff/block-diagrams/neo900/neo900.png

3) Generate the blank pixel image and obtain a local copy of jQuery:

   convert -size 1x1 xc:transparent blank.gif
   wget http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js

4) Obtain the "localize" script:

   wget -O localize https://neo900.org/git/misc/plain/block/localize
   chmod +x localize

5) Run the "localize" script to download PDFs referenced in the
   block diagram and replace the Web links with local ones:

   ./localize cache <neo900.html >neo900-local.html

   "cache" is the name of the subdirectory to which the files will be
   downloaded. The downloads will take a while.

The localized version of the block diagram can now be accessed
with a Web browser at

file:///home/neo900/block/neo900-local.html


Reporting errors
- - - - - - - -

If any downloads fail, "localize" will report their number at the end
of its run, e.g.,

  Failed downloads: 1

Downloads can fail due to local network issues, a temporary failure
at the remote location, or because the file has been removed or
renamed.

If a file seems to have become unavailable, please report this on
Freenode channel #neo900


Updating the block diagram
- - - - - - - - - - - - -

Delete the old block diagram files (but keep the cache) and repeat
steps 2 and 5 of the setup instructions.


Regenerating the cache
- - - - - - - - - - -

1) Go to the block diagram directory and rename or delete the
   "cache" subdirectory:

   cd /home/neo900/block
   mv cache cache-backup

2) Run "localize" (step 5 of the setup instructions) to populate
   the cache again.
