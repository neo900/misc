#!/usr/bin/perl
#
# block/ann2div.pl - Print annotations as DIVs for CSS-based map
#
# Written 2014 by Werner Almesberger
# Copyright 2014 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

#
# Annotation file syntax:
#
# # whatever		lines beginning with # are ignored
#			blank lines are also ignored
# label:		label of following section
#   text		indented text is copied into section
#   templ=line		template that extends to the end of the line
#   templ="text" more	template using the quoted string
#


require "libann.pl";


# ----- command line processing -----------------------------------------------

sub usage
{
	print STDERR \
"usage: $0 [-n name] [-m map] [-t templates.atp ...] [-u ythresh] [file ...]\n";
	exit(1);
}


$name = "map";
while ($ARGV[0] =~ /^-/) {
	$opt = shift @ARGV;
	if ($opt eq "-n") {
		&usage unless defined $ARGV[0];
		$name = shift @ARGV;
	} elsif ($opt eq "-m") {
		&usage unless defined $ARGV[0];
		$map = shift @ARGV;
	} elsif ($opt eq "-t") {
		&usage unless defined $ARGV[0];
		push(@atp, shift @ARGV);
	} elsif ($opt eq "-u") {
		$up = shift @ARGV;
		&usage unless defined $up;
	} else {
		&usage;
	}
}

# ----- read the map ----------------------------------------------------------

if (defined $map) {
	open(MAP, $map) || die "$map: $!";
	while (<MAP>) {
		next unless /COORDS="([^"]+)".*href="([^"]+)" alt=""/;
		$n = $2;
		@c = split(",", $1);
		undef $x{$n};
		undef $y{$n};
		while ($#c != -1) {
			($x, $y) = splice(@c, 0, 2);
			$x{$n} = $x if $x < $x{$n} || !defined $x{$n};
       	         $y{$n} = $y if $y < $y{$n} || !defined $y{$n};
	        }
	}
	close MAP;
}

# ----- read annotation templates ---------------------------------------------

for (@atp) {
	&ann_read_template($_);
}

# ----- generate annotations --------------------------------------------------

sub flush_curr
{
	for (@curr) {
		undef $ann{$_} if $reset;
		$ann{$_} .= $ann;
	}
	undef @curr;
	undef $ann;
	$reset = 0;
}


#
# Globals:
#
# @stag{id}		Stacked tags
# @stxt{id}		Stacked text
#
# $var{id}{name}	text variables
#


sub execute
{
	local ($c) = @_;

	if ($c =~ /^reset:$/) {
		undef $ann;
		$reset = 1;
	} elsif ($c =~ /^push:([A-Za-z_][A-Za-z0-9_]*):/) {
		push(@{ $stag{$curr} }, $1);
		push(@{ $stxt{$curr} }, $');
	} elsif ($c =~ /^pop:([A-Za-z_][A-Za-z0-9_]*)$/) {
		$tag = pop(@{ $stag{$curr} });
		die "drop: expected tag \"$1\" got \"$tag\"" unless $tag eq $1;
		return pop(@{ $stxt{$curr} });
	} elsif ($c =~ /^drop:([A-Za-z_][A-Za-z0-9_]*)$/) {
		$tag = pop(@{ $stag{$curr} });
		die "drop: expected tag \"$1\" got \"$tag\"" unless $tag eq $1;
		pop(@{ $stxt{$curr} });
	} elsif ($c =~ /^change:([A-Za-z_][A-Za-z0-9_]*):/) {
		for (my $i = $#{ $stag{$curr} }; $i != -1; $i--) {
			if ($stag{$curr}[$i] eq $1) {
				$stxt{$curr}[$i] = $';
				return undef;
			}
		}
		die "change: tag \"$1\" not found";
	} elsif ($c =~ /^set:([A-Za-z_][A-Za-z0-9_]*):/) {
		$var{$curr}{$1} = $';
	} elsif ($c =~ /^get:([A-Za-z_][A-Za-z0-9_]*)$/) {
		return "[$c]";
	} elsif ($c =~ /^swap:/) {
		die "swap: swapping item \"$curr\" twice"
		    if defined $swap{$curr} || defined $swapped{$'};
		$swap{$curr} = $';
		$swapped{$'} = 1;
	}
	return undef;
}


sub get
{
	local ($c, $b) = @_;

	$var{$b}{"_id"} = $b;
	if ($c =~ /^get:([A-Za-z_][A-Za-z0-9_]*)$/) {
		die "undefined variable \"$1\" in \"$b\""
		    unless defined $var{$b}{$1};
		return $var{$b}{$1};
	}
	return undef;
}


undef $curr;
undef @curr;
undef $ann;
$same = 0;
$reset = 0;
while (<>) {
	next if /^\s*#/;
	if (/^\s*$/) {
		$same = 0;
		next;
	}
	if (/^(\S+):\s*$/) {
		&flush_curr if !$same;
		$curr = $1;
		push(@curr, $curr);
		$same = 1;
		next;
	}
	die unless /^\s*/;
	die "no current section" unless defined $curr;
	$t = &ann_expand($');
	$t = (&ann_commands($t, \&execute))[0];
	$ann .= $t;
	$same = 0;
}
&flush_curr;

for $curr (keys %ann) {
	for (@{ $stag{$curr} }) {
		$ann{$curr} .= pop @{ $stxt{$curr} };
	}
}


sub cmp
{
	local ($a, $b) = @_;

	return $x{$b} <=> $x{$a} if $x{$a} != $x{$b};
	return $y{$a} <=> $y{$b}
	    if defined $up && $y{$a} >= $up && $y{$b} >= $up;
	return $y{$b} <=> $y{$a};
}


@k = sort { &cmp($a, $b) } keys %ann;
for ($i = 0; $i <= $#k; $i++) {
	$i{$k[$i]} = $i;
}
for (keys %swap) {
	my $a = $i{$_};
	my $b = $i{$swap{$_}};
	($k[$a], $k[$b]) = ($k[$b], $k[$a]);
}
for (@k) {
	print "<LI><DIV class=\"t_${name}_$_\"><DIV>";
	$t = (&ann_commands($ann{$_}, \&get, $_))[0];
	print $t;
	print "</DIV></DIV></LI>\n";
}
