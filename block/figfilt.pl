#!/usr/bin/perl
#
# block/figfilt.pl - Filter a FIG file by layers
#
# Written 2014, 2016 by Werner Almesberger
# Copyright 2014, 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

# ----- Read and classify FIG file --------------------------------------------


sub flush
{
	return unless defined $curr;

	die unless $curr =~ /^(-?\d)\s/;
	@a = split(/ +/, $curr);

	local $f;

	if ($a[0] == 0 || $a[0] == 6 || $a[0] == -6) {
		# color, compound (begin, end)
		$f = undef;
	} elsif ($a[0] == 1 || $a[0] == 2 || $a[0] == 3 || $a[0] == 5) {
		# ellipse, polyline, spline, arc
		$f = 6;
	} elsif ($a[0] == 4) {
	    	# text
		$f = 3;
	} else {
		die "unrecognized object type $a[0]";
	}

	if (!defined $f) {
		$out .= $comment.$curr;
	} elsif ($show[$a[$f]]) {
		$a[$f] %= $mod;
		$out .= $comment.join(" ", @a);
	}

	undef $curr;
	undef $comment;
}


sub usage
{
	print STDERR "usage: $0 [-m modulo] layer[-layer][,...] [file.fig]\n";
	exit(1);
}


$mod = 1000;
while ($ARGV[0] =~ /^-/) {
	my $opt = shift @ARGV;

	if ($opt eq "-m") {
		$mod = shift @ARGV;
		&usage unless $mod;
	}
}

$layers = shift @ARGV;
&usage unless defined $layers;

while ($layers =~ /^(\d+(-\d+)?)([,:]|$)/) {
	$e = defined $2 ? -$2 : $1;
	die "inverted range" if $1 > $e;
	$layers = $';
	for ($1..$e) {
		$show[$_] = 1;
	}
}

&usage if $layers ne "";

while (<>) {
	$out .= $_;
	last if /^\d+\s+\d\s+/;
}

while (<>) {
	if (/^#/) {
		&flush if defined $curr;
		$comment .= $_;
		next;
	}
	if (/^-?\d\s/) {
		&flush if defined $curr;
	} else {
		die unless /^\s/;
	}
	$curr .= $_;
}
&flush;

print $out;
