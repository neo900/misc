#!/usr/bin/perl
#
# block/annotate.pl - Annotate and enhance block diagrams
#
# Written 2014-2016 by Werner Almesberger
# Copyright 2014-2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

#
# Visual markup conventions:
#
# - polygons that are not inside other polygons are "special", except
# - if a polygon is cross-hatched (pattern 45), then it is not "special"
# - text in Helvetica Narrow Bold is "special", too
#

#
# General:
# - all lines are changed to thickness 3
#
# Regular polygon handling:
# - hatch (if any) is removed
#
# "Special" polygon handling:
# - highlighted in yellow and moved to layer 999
#
# "Special" text handling:
# - for simplicity, the bounding box of each polygon represents its extent
#   for the following operations
# - within each "special" polygon, the text closest to the center is searched
# - this text becomes the "first" text
# - the search then continues to additional text in the polygon, by distance
#   from the center
# - if text that has the same font size and that continues the already located
#   text is found, it is added to the first text. Continuation is defined by
#   a text that's higher up (Y coordinate) to begin with an upper-case letter
#   and the lower text beginning with a lower-case letter, or by the higher
#   text ending in a comma.
# - if text with a different font size is found, this text is considered the
#   second text and the same search for continuation is performed.
# - all other text in the polygon is ignored
# - after this, text outside of polygons is processed in the same way by order
#   of distance from a polygon. Polygons that already have a first and second
#   text are ignored.
# - once all text has been found, the one with a larger font is considered the
#   part description while one with the smaller font is considered the part
#   number. If both have the same size, the higher is the description. If both
#   are also at the same height, the one on the left is the description.
#
# Note that these rules mean that every component or subsystem must have both
# a description and a part number. If one is missing, the algorithm will,
# erroneously, find something else.
#


require "libann.pl";


$TEXT_HEIGHT	= 95;	# about 2 mm at 1200 units/in

$BOX_COLOR	= 6;	# yellow
$BOX_FILL	= 33;	# solid, pale (+65%)


# ----- Helper functions for editing ------------------------------------------


sub get_fields
{
	local ($s) = @_;

	die unless $s =~ /^((#[^\n]*\n)*)([^\n]*)\n/;
	return split(/\s+/, $3);
}


sub edit_field
{
	local ($s, @e) = @_;
	local ($a, $b);
	local ($i, $v);
	local @a;

	die unless $s =~ /^((#[^\n]*\n)*)([^\n]*)\n/;
	($c, $a, $b) = ($1, $3, $');

	@a = split(/\s+/, $a);
	while ($#e != -1) {
		$i = shift @e;
		$v = shift @e;
		$a[$i] = $v;
	}

	return $c.join(" ", @a)."\n".$b;
}


sub text
{
	local ($s) = @_;

	die unless $s =~ /^(\S+\s+){13}/;
	die unless $' =~ /\\001/;
	return $`;
}


sub comment
{
	local ($s, $c) = @_;

	return "# $c\n$s";
}


sub edit_fg
{
	local ($s, $color, $pattern) = @_;

	my @a = get_fields($s);
	if ($a[0] == 4) {	# text
		return &edit_field($s, 2, $color);
	} else {
		return &edit_field($s, 4, $color);
	}
}


sub edit_bg
{
	local ($s, $color, $pattern) = @_;

	return &edit_field($s, 5, $color, 8, $pattern);
}


sub edit_size
{
	local ($s, $points) = @_;

	return &edit_field($s, 6, $points);
}


sub edit_text
{
	local ($s, $n) = @_;

	die unless $s =~ /^(\S+\s+){13}/;
	return "$&$n\\001\n";
}


# ----- output annotated drawing ----------------------------------------------


sub output
{
	print $head;
	for (sort values %col) {
		print "0 $_ $col[$_]\n";
	}
	print @box, @raw, @circ, @arc, @text;
}


# ----- Read and classify FIG file --------------------------------------------


sub flush
{
	# widen line if it's ellipse, polyline, or arc
	local $w = &edit_field($comment.$curr, 3, 3);
	return unless defined $curr;

	die unless $curr =~ /^(-?\d)\s/;
	if ($1 eq "0") {	# color
		push(@color, $w);
	} elsif ($1 eq "1") {	# ellipse
		push(@circ, $w);
	} elsif ($1 eq "2") {	# polyline
		push(@poly, $w);
	} elsif ($1 eq "4") {	# text
		push(@text, $comment.$curr);
	} elsif ($1 eq "5") {	# arc
		push(@arc, $w);
	} elsif ($1 eq "6" || $1 eq "-6") {	# compound
		# ignore
	} else {
		die "unrecognized object type $1";
	}
	undef $curr;
	undef $comment;
}


sub usage
{
	print STDERR
"usage: $0 [-d] [-D] [-t templates.atp ...] [-F font] file.fig\n".
"          [annotations.txt ...]\n";
	exit(1);
}


$font = 22;	# special (IDs) font: Helvetica Narrow Bold
while ($ARGV[0] =~ /^-./) {
	if ($ARGV[0] eq "-d") {
		$debug = 1;
		shift @ARGV;
	} elsif ($ARGV[0] eq "-D") {
		$dump = 1;
		shift @ARGV;
	} elsif ($ARGV[0] eq "-F") {
		shift @ARGV;
		&usage unless defined $ARGV[0];
		$font = shift @ARGV;
	} elsif ($ARGV[0] eq "-t") {
		shift @ARGV;
		&usage unless defined $ARGV[0];
		push(@atp, shift @ARGV);
	} else {
		&usage;
	}
}
&usage unless $#ARGV >= 0;
($fig, @ann) = @ARGV;

open(FIG, $fig) || die "$fig: $!";
while (<FIG>) {
	$head .= $_;
	last if /^\d+\s+\d\s+/;
}

while (<FIG>) {
	if (/^#/) {
		&flush if defined $curr;
		$comment .= $_;
		next;
	}
	if (/^-?\d\s/) {
		&flush if defined $curr;
	} else {
		die unless /^\s/;
	}
	$curr .= $_;
}
&flush;

# ----- record colors ---------------------------------------------------------

for (@color) {
	$_ =~ /\n\s*/;
	$prm = $`;
	@a = split(/\s+/, $prm);

	$col[$a[1]] = $a[2];
	$col{$a[2]} = $a[1];
}

# ----- extract coordinates and font size from all texts ----------------------

for (@text) {
	$_ =~ /\n\s*/;
	$prm = $`;
	@a = split(/\s+/, $prm);

	if ($a[5] != $font && defined $font) {
		push(@raw, $_);
		next;
	}

	push(@tmp, $_);
	push(@tsz, $a[6]);	# size
	push(@tx, $a[11]);	# X
	push(@ty, $a[12]);	# Y
}
@text = @tmp;

# ----- extract bounding box from all polygons --------------------------------

for (@poly) {
	/^(#[^\n]*\n)*/;
	$' =~ /\n\s*/;
	$prm = $`;
	@a = split(/\s+/, $prm);

	undef @x;
	undef @y;
	$s = $';
	$s =~ s/.*?\n\s*// if $a[13];	# forward arrow parameters
	$s =~ s/.*?\n\s*// if $a[14];	# backward arrow parameters
	while ($s ne "") {
		die unless $s =~ /^(-?\d+)\s+/;
		push(@x, $1);
		$s = $';
		die unless $s =~ /^(-?\d+)\s+/;
		push(@y, $1);
		$s = $';
	}

	@a = split(/\s+/, $prm);
	if ($a[1] == 1) {
		push(@raw, $_);
		next;
	}

	($x0, $x1, $y0, $y1) = (undef, undef, undef, undef);
	for ($i = 0; $i <= $#x; $i++) {
		$x0 = $x[$i] if $x[$i] < $x0 || !defined $x0;
		$x1 = $x[$i] if $x[$i] > $x1 || !defined $x1;
		$y0 = $y[$i] if $y[$i] < $y0 || !defined $y0;
		$y1 = $y[$i] if $y[$i] > $y1 || !defined $y1;
	}

	push(@box, $_);
	push(@bx0, $x0);
	push(@bx1, $x1);
	push(@by0, $y0);
	push(@by1, $y1);

	if ($a[8] == 45) {	# ignore if right-hatched
		$skip[$#box] = 1;
		$box[$#box] = &edit_field($box[$#box],
		    8, -1,	# de-hatch
		    6, 999);	# depth = 999

	}
}

# ----- extract comment polygons ----------------------------------------------

for ($i = 0; $i <= $#box; $i++) {
	next unless $box[$i] =~ /#\s*comment=(\S+)\s*\n/m;
	$box[$i] = $`.$';	# remove comment tag
	$fd{uc $1} = $i;	# add to dictionary
	$skip[$i] = 1;
}

# ----- mark polygons which are in another polygon's bounding box -------------

for ($i = 0; $i <= $#box; $i++) {
	for ($j = 0; $j <= $#box; $j++) {
		next if $i == $j;
		next if $bx0[$j] > $bx0[$i];
		next if $bx1[$j] < $bx1[$i];
		next if $by0[$j] > $by0[$i];
		next if $by1[$j] < $by1[$i];
		$skip[$i] = 1;
		last;
	}
}

# ----- highlight polygons and match them with text ---------------------------

sub hypot
{
	local ($x, $y) = @_;

	return sqrt($x*$x+$y*$y);
}


sub continues
{
	local ($a, $b) = (&text($text[$_[0]]), &text($text[$_[1]]));

	if (($a =~ /^[A-Z]/ && $b =~ /^[a-z]/) || $a =~ /,\s*$/) {
		return 1;
	} else {
		return 0;
	}
}


sub continuation
{
	local ($s, $last, $next) = @_;

	return 0 if $tsz[$last] != $tsz[$next];
	if ($ty[$last] < $ty[$next]) {
		return 0 unless &continues($last, $next);
		$$s = $$s." ".&text($text[$next]);
	} else {
		return 0 unless &continues($next, $last);
		$$s = &text($text[$next])." ".$$s;
	}
	return 1;
}


sub d
{
	local ($cx, $cy, $x, $y) = @_;

	return sqrt(($x-$cx)*($x-$cx)+($y-$cy)*($y-$cy));
}


sub d_box
{
	local ($x0, $x1, $y0, $y1, $x, $y) = @_;

	if ($x < $x0) {
		return &d($x0, $y0, $x, $y) if $y < $y0;
		return &d($x0, $y1, $x, $y) if $y > $y1;
		return $x0-$x;
	}
	if ($x > $x1) {
		return &d($x1, $y0, $x, $y) if $y < $y0;
		return &d($x1, $y1, $x, $y) if $y > $y1;
		return $x-$x1;
	}
	return $y0-$y if $y < $y0;
	return $y-$y1 if $y > $y1;
	return &d(($x0+$x1)/2, ($y0+$y1)/2, $x, $y);
}


#
# We use the following global variables to keep track of text collection:
#
# @fi[box]	first item of first text found for box (undef if none)
# @fl[box]	last item of first text; undef if done with first text
# @fit[box]	cumulative first text
# @si[box]	first item of second text (undef if none yet)
# @sl[box]	last item of second text; undef if done with second text
# @sit[box]	cumulative second text
#


sub consider
{
	local ($b, $t) = @_;

	if (!defined $fi[$b]) {
		push(@{ $fi[$b] }, $t);
		$fl[$b] = $t;
		$fit[$b] = &text($text[$t]);
		return 1;
	}
	if (defined $fl[$b]) {
		if (&continuation(\$fit[$b], $fl[$b], $t)) {
			push(@{ $fi[$b] }, $t);
			$fl[$b] = $t;
			return 1;
		}
		undef $fl[$b] if $tsz[$t] == $tsz[$fi[$b][0]];
	}
	if (!defined $si[$b]) {
		return 1 if $tsz[$t] == $tsz[$fi[$b][0]];
		push(@{ $si[$b] }, $t);
		$sl[$b] = $t;
		$sit[$b] = &text($text[$t]);
		return 1;
	}
	if (defined $sl[$b]) {
		if (&continuation(\$sit[$b], $sl[$b], $t)) {
			push(@{ $si[$b] }, $t);
			$sl[$b] = $t;
			return 1;
		}
		if ($tsz[$t] == $tsz[$si[$b][0]]) {
			undef $sl[$b];
			return 0;
		}
	}
	return 0;
}


# determine "insideness" of text and calculate distances from boxes

for ($i = 0; $i <= $#box; $i++) {
	next if $skip[$i];

	$box[$i] = &edit_field($box[$i],
	    5, $BOX_COLOR,
	    8, $BOX_FILL,
	    6, 999);	# depth = 999

	undef %d_in;
	undef %d_out;
	for ($j = 0; $j <= $#text; $j++) {
		($x, $y) = ($tx[$j], $ty[$j]);
		$y -= $TEXT_HEIGHT if $y > $by1[$i];
		$d = &d_box($bx0[$i], $bx1[$i], $by0[$i], $by1[$i], $x, $y);
		$d[$i][$j] = $d;
		if ($tx[$j] >= $bx0[$i] && $tx[$j] <= $bx1[$i] &&
		    $ty[$j] >= $by0[$i] && $ty[$j] <= $by1[$i]) {
			$in[$i][$j] = 1;
			$in_any[$j] = 1;
		} else {
			push @dp, [ $i, $j ]
			    unless &text($text[$j]) =~ /^\d{1,2}$/;
		}
	}
}

# manually assign IDs

for ($i = 0; $i <= $#box; $i++) {
	if ($box[$i] =~ /#\s*id=\s*(\S+)\s*\n/m) {
		$fit[$i] = $1;
		$fi[$i] = [ undef ];
	}
}

# associate text inside boxes

for ($i = 0; $i <= $#box; $i++) {
	next if $skip[$i];

	undef @t;
	for ($j = 0; $j <= $#text; $j++) {
		push(@t, $j) if defined $d[$i][$j] && $in[$i][$j];
	}

	next if $#t == -1;

	for (sort { $d[$i][$a] <=> $d[$i][$b] } @t) {
		&consider($i, $_);
		last if defined $fi[$i] && defined $si[$i] &&
		    !defined $fl[$i] && !defined $sl[$i];
	}
}

# associate text outside boxes

for (sort { $d[@{ $a }[0]][@{ $a }[1]] <=> $d[@{ $b }[0]][@{ $b }[1]] } @dp) {
	local ($b, $t) = @{ $_ };

	next if $box[$b] =~ /#\s*inside\s*\n/m;
	next if $in_any[$t];
	next if $used[$t];
	$used[$t] = &consider($b, $t);
}

# order first and second text by size (first is larger)

for ($i = 0; $i <= $#box; $i++) {
	local ($f, $s) = ($fi[$i], $si[$i]);

	next unless defined $s;
	next unless defined @{ $f }[0];
	next if defined $f && $tsz[@{ $s }[0]] < $tsz[@{ $f }[0]];

	($fi[$i], $si[$i]) = ($si[$i], $fi[$i]);
	($fit[$i], $sit[$i]) = ($sit[$i], $fit[$i]);
}

# debug: highlight associated text and exit

if ($debug) {
	for ($i = 0; $i <= $#box; $i++) {
		local ($f, $s) = ($fi[$i][0], $si[$i][0]);

		$text[$f] = &edit_field($text[$f], 2, 4) if defined $f;
		$text[$s] = &edit_field($text[$s], 2, 12) if defined $s;
	}
	&output;
	exit;
}

# ----- build a dictionary ----------------------------------------------------

for ($i = 0; $i <= $#box; $i++) {
	next unless defined $fit[$i];

	($t = $fit[$i]) =~ tr/[A-Za-z0-9_]/ /cs;
	$t =~ s/\s+/ /g;
	@t = split(/ /, $t);

	undef @t2;
	for ($j = 0; $j < $#t; $j++) {
		push(@t2, $t[$j]."_".$t[$j+1]);
		push(@t2, $t[$j]."_".$t[$j+1]."_".$t[$j+2]) unless $j == $#t-1;
	}

	for (@t, @t2) {
		next if /^.{0,2}$/;
		$fd{uc $_} = $i if uc $fit[$i] eq uc $_;
		next if $block{$_};
		if (exists $fd{uc $_}) {
			$block{$_} = 1;
			delete $fd{uc $_} unless
			    defined $fd{uc $_} && uc $fit[$fd{uc $_}] eq uc $_;
		} else {
			$fd{uc $_} = $i;
		}
	}
}

# ----- dump dictionary and exit ----------------------------------------------

if ($dump) {
	for (sort keys %fd) {
		print "$_\n";
	}
	print "-" x 79, "\n";
	for (sort @fit) {
		print "$_\n";
	}
	print "-" x 79, "\n";
	for (sort @sit) {
		print "$_\n";
	}
	exit;
}

# ----- exit if we don't have any annotations ---------------------------------

if ($#ann == -1) {
	&output;
	exit;
}

# ----- load templates --------------------------------------------------------

for (@atp) {
	&ann_read_template($_);
}

# ----- read and assign annotations -------------------------------------------

sub flush_curr
{
	for (@curr) {
		$ann{$_} .= $ann;
	}
	undef @curr;
	undef $ann;
}

for $ann (@ann) {
	open(ANN, $ann) || die "$ann: $!";
	undef $curr;
	undef @curr;
	undef $ann;
	$same = 0;
	while (<ANN>) {
		next if /^\s*#/;
		next if /^\s*$/;
		if (/^\s*$/) {
			$same = 0;
			next;
		}
		if (/^(\S+):\s*$/) {
			&flush_curr if !$same;
			$curr = $fd{uc $1};
			die "unknown label \"$1\"" unless defined $curr;
			die "label changed from \"$label{$curr}\" to \"$1\""
			    if defined $label{$curr} && $label{$curr} ne $1;
			push(@curr, $curr);
			$label{$curr} = $1;
			$same = 1;
			next;
		}
		die unless /^\s*/;
		die "no current section" unless defined $curr;
		$ann .= &ann_expand($');
		$same = 0;
	}
	close ANN;
	&flush_curr;
}

# ----- add annotations as "href" comments, for image map creation ------------

for $b (keys %label) {
	$box[$b] = &comment($box[$b], "href=\"$label{$b}\" alt=\"\"");
}

# ----- execute commands in annotations ---------------------------------------

sub color_by_name
{
	local ($c) = @_;
	local @colors = (
		"black",	"blue",		"green",	"cyan",
		"red",		"magenta",	"yellow",	"white",
		"blue4",	"blue3",	"blue2",	"ltblue",
		"green4",	"green3",	"green2",	"cyan4",
		"cyan3",	"cyan2",	"red4",		"red3",
		"red2",		"magenta4",	"magenta3",	"magenta2",
		"brown4",	"brown3",	"brown2",	"pink4",
		"pink3",	"pink2",	"pink",		"gold");

	if ($c =~ /^#/) {
		return $col{$c} if defined $col{$c};
		for (my $i = 32; $i != 32 + 512; $i++) {
			next if defined $col[$i];
			$col[$i] = $c;
			$col{$c} = $i;
			return $i;
		}
		die "congratulations, you've exceeded the FIG color palette";
	}

	for (my $i = 0; $i <= $#colors; $i++) {
		return $i if $colors[$i] eq $c;
	}
	die "unknown color \"$c\"";
}


sub color
{
	local ($c) = @_;
	die "invalid color \"$c\"" unless
	    $c =~ /^([a-z0-9]+|#[0-9a-fA-F]{6})(([+-])(\d+%))?$/;
	
	return (&color_by_name($c), 20) unless defined $2;
	die "invalid saturation \"$2\"" if $4 > 100 || ($4 % 5);
	return (&color_by_name($1), $3 eq "+" ? 20+$4/5 : 20-$4/5);
}


sub execute
{
	local ($b, $c) = @_;

	if ($c =~ /^fg:/) {
		$box[$b] = &edit_fg($box[$b], &color($'));
	} elsif ($c =~ /^bg:/) {
		$box[$b] = &edit_bg($box[$b], &color($'));
	} elsif ($c =~ /^fc:/) {
		for (@{ $fi[$b] }) {
			$text[$_] = &edit_fg($text[$_], &color($'));
		}
	} elsif ($c =~ /^fci:/) {
		for (@{ $fi[$b] }) {
			$text[$_] = &edit_fg($text[$_], &color($'))
			    if $in_any[$_];
		}
	} elsif ($c =~ /^sc:/) {
		for (@{ $si[$b] }) {
			$text[$_] = &edit_fg($text[$_], &color($'));
		}
	} elsif ($c =~ /^sci:/) {
		for (@{ $si[$b] }) {
			$text[$_] = &edit_fg($text[$_], &color($'))
			    if $in_any[$_];
		}
	} elsif ($c =~ /^ft(\d*):/) {
		die "$label{$b} has no first text"
		    unless defined $fi[$b];
		die "$label{$b} has no first text [$1]"
		    unless defined $fi[$b][$1];
		$text[$fi[$b][$1]] = &edit_text($text[$fi[$b][$1]], $');
	} elsif ($c =~ /^st(\d*):/) {
		die "$label{$b} has no second text"
		    unless defined $si[$b];
		die "$label{$b} has no second text [$1]"
		    unless defined $si[$b][$1];
		$text[$si[$b][$1]] = &edit_text($text[$si[$b][$1]], $');
	} elsif ($c =~ /^reset:$/) {
		# ignore
	} elsif ($c =~ /^ssz:/) {
		for (@{ $si[$b] }) {
			$text[$_] = &edit_size($text[$_], $');
		}
	} elsif ($c =~ /^push:([A-Za-z_][A-Za-z0-9_]*):/) {
		# ignore
	} elsif ($c =~ /^pop:([A-Za-z_][A-Za-z0-9_]*)$/) {
		# ignore
	} elsif ($c =~ /^drop:([A-Za-z_][A-Za-z0-9_]*)$/) {
		# ignore
	} elsif ($c =~ /^change:([A-Za-z_][A-Za-z0-9_]*):/) {
		# ignore
	} elsif ($c =~ /^set:([A-Za-z_][A-Za-z0-9_]*):/) {
		# ignore
	} elsif ($c =~ /^get:([A-Za-z_][A-Za-z0-9_]*)$/) {
		# ignore
	} elsif ($c =~ /^swap:/) {
		# ignore
	} elsif ($c =~ /^css:/) {
		# ignore
	} elsif ($c =~ /^movex:/ || $c =~ /^movey:/) {
		# ignore
	} else {
		die "unrecognized command \"$c\"";
	}
}


if (exists $ann_templ{"*"}) {
	@c = &ann_commands($ann_templ{"*"});
	shift @c;
	for ($b = 0; $b <= $#box; $b++) {
		next if $skip[$b];

		for $c (@c) {
			&execute($b, $c);
		}
	}
}

for $b (keys %ann) {
	@c = &ann_commands($ann{$b});
	shift @c;
	for $c (@c) {
		&execute($b, $c);
	}
}

# ----- generate output -------------------------------------------------------

&output;
