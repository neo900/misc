##### CPU and memories ########################################################


cpu:
	REPLACE=DM3730CBP
	DS=http://www.ti.com/lit/ds/symlink/dm3725.pdf
	DK=DM3730CBP100/296-32627-ND
	GROUP=Core

# TPS65950A3
GAIA:
	KEEP=
	[ft:Companion] [st:"TWL4030"]
	DK=TPS65950A3ZXNR/296-37687-1-ND
	GROUP=Core

eMMC:
	REPLACE=???
	TODO=
	Note: size still to be confirmed.<BR>
	GROUP=Storage

RAM:
	REPLACE=KCE00E00CA
	[ft0:NAND (512 MB)]
	[ft1:RAM (1 GB),]
	RAM and NAND PoP (1 GB + 512 MB; KCE00E00CA)<BR>
	Possible full name of replacement: KCE00E00CA-A506
	TICKET=516
	MANUAL=KCE00E00CA-A506
	GROUP=Core

##### RF ######################################################################

Modem_Navigation:
	NEW=Modem (PHS8/PLS8-E,US/PXS8)
	GSM/UMTS (PHS8), GSM/UMTS/LTE (PLS8), or GSM/UMTS/CDMA2000 (PXS8);<BR>
	GPS/GLONASS (all).<BR>
	DS=(PHS8) http://www.gemalto.com/brochures-site/download-site/Documents/M2M_PHS8_datasheet.pdf
	The PHS8 hardware description can be found via
	<A href="http://web.archive.org/web/20150103113256/http://download.maritex.com.pl/pdfs/wi/phs8.pdf">archive.org</A><BR>
	DS=(PLS8) http://www.gemalto.com/brochures-site/download-site/Documents/M2M_PLS8_datasheet.pdf
	DS=(PXS8) http://www.gemalto.com/brochures-site/download-site/Documents/M2M_PXS8_datasheet.pdf
	MANUAL=PHS8
	GROUP=Telephony

Modem_monitor:
	NEW=Current monitor (3 x INA231) and switch (TPS22963)
	Current monitor:<BR>
	DS=(INA231) http://www.ti.com/lit/ds/symlink/ina231.pdf
	DK=(INA231) INA231AIYFFT/296-38958-2-ND
	MOUSER=(INA231) 595-INA231AIYFFT
	Load switch:<BR>
	DS=(TPS22963) http://www.ti.com/lit/ds/symlink/tps22963c.pdf
	DK=(TPS22963) TPS22963CYZPT/296-39360-1-ND
	MOUSER=(TPS22963) 595-TPS22963CYZPT
	GROUP=Telephony

LNA:
	PART=GPS Low-Noise Amplifier (LNA)
#	Possible component choices:<BR>
#	TriQuint TQM640002.
#	WARN=EOL, last buy 2015-03-10
	Infineon BGM 1034N7<BR>
#	DS=(TQM640002) http://www.mouser.com/catalog/specsheets/tqm640002.pdf
#	MOUSER=(TQM640002) 772-TQM640002
#	DS=(BGM 1034N7) http://www.infineon.com/dgdl/bgm1034N7.pdf?folderId=db3a30431f848401011fcbf2ab4c04c4&fileId=db3a304334fac4c60134fafa93ce0011
	DS=(BGM 1034N7) http://www.mouser.com/ds/2/196/Infineon-BGM1034N7-DS-v03_00-en-767596.pdf
#	DK=(BGM 1034N7) BGM%201034N7%20E6327/BGM%201034N7%20E6327CT-ND
	MOUSER=(BGM 1034N7) 726-BGM1034N7E6327
	GROUP=Telephony

GPS_kill:
	PART=GPS kill RF switch (PE4259)
	DS=http://www.psemi.com/pdf/datasheets/pe4259ds.pdf
	DK=4259-63/1046-1011-1-ND
	GROUP=Telephony
# Alternative:
# http://www.psemi.com/pdf/datasheets/pe4259ds.pdf

Modem_TX_monitor:
	PART=Modem TX monitor (2 x LMV221, CPL-WBF-00D3, SLG46533)
	Signals: Enable signal from CPU, two analog outputs (one per
	direction) to ADC inputs of the companion chips.<P>
	Power detector:<BR>
	DS=(LMV221) http://www.ti.com/lit/ds/symlink/lmv221.pdf
	DK=(LMV221) LMV221SD-NOPB/LMV221SD-NOPBCT-ND/1577074
	Directional coupler:<BR>
	DS=(CPL-WBF-00D3) http://www.st.com/web/en/resource/technical/document/datasheet/DM00059334.pdf
	DK=(CPL-WBF-00D3) CPL-WBF-00D3/497-13698-1-ND
	Mixed-signal array:<BR>
#	DS=(SLG46531) http://www.silego.com/uploads/Products/product_415/SLG46531_DS_r109_10212016.pdf
#	WEB=[Silego shop] http://www.silego.com/buy/index.php?main_page=product_info&cPath=61&products_id=428
	SLG=
	WEB=[Silego shop] http://www.silego.com/buy/index.php?main_page=product_info&cPath=61&products_id=497
	GROUP=Telephony

WLAN:
	REPLACE=WL1837MODGI
	WL1873-based, with 802.11a/b/g/n, BT 4.0<BR>
	TICKET=546
	DS=http://www.ti.com/lit/ds/symlink/wl1837mod.pdf
#	DK=WL1837MODGIMOCT/WL1837MODGIMOCT-ND
	DK=WL1837MODGIMOCT/296-43667-1-ND
	MOUSER=595-WL1837MODGIMOCT
	GROUP=ISM

FMTXRX:
	NEW=FM TX/RX with RDS/RDBS (Si4721)
	DS=http://www.silabs.com/Support%20Documents/TechnicalDocs/Si4720-21-B20.pdf
	<A href="http://www.silabs.com/Support Documents/TechnicalDocs/AN332.pdf">Programming guide</A><BR>
	DK=SI4721-B20-GM/SI4721-B20-GM-ND
	MOUSER=634-SI4721-B20-GM
	GROUP=ISM

Tuning:
	PART=Tuning circuit
	GROUP=ISM

USB_PHY:
	PART=High-speed USB PHY (USB3322)
	DS=http://media.digikey.com/pdf/Data%20Sheets/Microchip%20PDFs/USB332x.pdf
	DK=USB3322C-GL-TR/USB3322C-GL-TR-ND
	MOUSER=886-USB3322C-GL-TR
	ARROW=56938374S8317732N2064
	GROUP=Telephony

NFC:
	PART=Near Field Communication (NFC) controller (TRF7970A, MKL16Z128VFM4)
	<A href="https://neo900.org/stuff/papers/nfc-draft.pdf">Draft paper</A><BR>
	Note: still under consideration.<BR>
	TICKET=539
	NFC transceiver:<BR>
#	DS=(PN544) http://www.mouser.com/ds/2/302/75016890-4042.pdf
#	MOUSER=(PN544) 771-PN5441A2EC20501
	DS=(TRF7970A) http://www.ti.com/lit/ds/symlink/trf7970a.pdf
	DK=(TRF7970A) TRF7970ARHBR/296-29355-1-ND
	MOUSER=(TRF7970A) 595-TRF7970ARHBR
	MCU:<BR>
	DS=(MKL16Z128VFM4) http://cache.freescale.com/files/microcontrollers/doc/data_sheet/KL16P64M48SF5.pdf
	TRM=(MKL16Z128VFM4) http://cache.freescale.com/files/microcontrollers/doc/ref_manual/KL16P80M48SF4RM.pdf
	DK=(MKL16Z128VFM4) MKL16Z128VFM4/MKL16Z128VFM4-ND
	MOUSER=(MKL16Z128VFM4) 841-MKL16Z128VFM4
	GROUP=ISM

SIM_switch:
	PART=SIM card access switch (2 x FSA2866, 2 x TXS4555, SLG46533, LSF0204D, ADA4505-1)
	<A href="https://neo900.org/stuff/papers/simsw.pdf">White paper</A>
	<BR>

	Analog switches:<BR>
	DS=(FSA2866) http://www.fairchildsemi.com/datasheets/FS/FSA2866.pdf
	DK=(FSA2866) FSA2866UMX/FSA2866UMXCT-ND
	MOUSER=(FSA2866) 512-FSA2866UMX

	Voltage regulators:<BR>
	DS=(TXS4555) http://www.ti.com/lit/ds/symlink/txs4555.pdf
	DK=(TXS4555) TXS4555RUTR/296-28451-1-ND
	MOUSER=(TXS4555) 595-TXS4555RUTR

	Mixed-signal array:<BR>
#	DS=(SLG46531) http://www.silego.com/uploads/Products/product_415/SLG46531_DS_r109_10212016.pdf
#	WEB=[Silego shop] http://www.silego.com/buy/index.php?main_page=product_info&cPath=61&products_id=428
	SLG=
	WEB=[Silego shop] http://www.silego.com/buy/index.php?main_page=product_info&cPath=61&products_id=497
#	DS=(SLG46721) http://www.silego.com/uploads/Products/product_243/SLG46721r110_10282015.pdf
#	WEB=[Silego shop] http://www.silego.com/buy/index.php?main_page=product_info&cPath=56&products_id=323
#	MOUSER=(SLG46721) 402-SLG46721V
	MANUAL=SLG46533V

	Level shifters:<BR>
	DS=(LSF0204D) http://www.ti.com/lit/ds/symlink/lsf0204d.pdf
	DK=(LSF0204D) LSF0204DRUTR/296-40402-1-ND
	MOUSER=(LSF0204D) 595-LSF0204DRUTR

	Opamp (for SIM current monitoring):<BR>
	DS=(ADA4505-1) http://www.analog.com/media/en/technical-documentation/data-sheets/ADA4505-1_4505-2_4505-4.pdf
	DK=(ADA4505-1) ADA4505-1ACBZ-R7/ADA4505-1ACBZ-R7CT-ND
	MOUSER=(ADA4505-1)584-ADA4505-1ACBZ-R7

	GROUP=Telephony

Opt_tap:
	<IMG src="opt-tap.png">
	[css:width:658px;]
	[movex:-658]
	[movex:150]
	[bg:white] [fg:white]

##### Battery #################################################################

Charger:
	REPLACE=BQ24297
	DS=http://www.ti.com/lit/ds/symlink/bq24297.pdf
	DK=BQ24297RGET/296-39580-1-ND
	MOUSER=595-BQ24297RGET
	NEWARK=19X9925
	ARROW=61597793S908994N9193
	GROUP=Power

Fuel_gauge:
	PART=Fuel gauge (BQ27200, BQ27421)
        Note: The BQ27200 only supports 100 kHz I2C and therefore limits the
        speed of the bus (I2C&#35;3) it is on.<BR>
	DS=(BQ27200) http://www.ti.com/lit/ds/symlink/bq27200.pdf
        DK=(BQ27200) BQ27200DRKR/296-17521-1-ND
	DS=(BQ27421) http://www.ti.com/lit/ds/symlink/bq27421-g1.pdf
	TRM=(BQ27421) http://www.ti.com/lit/ug/sluuac5c/sluuac5c.pdf
        DK=(BQ27421) BQ27421YZFR-G1A/296-38880-1-ND

Backup:
	REPLACE=PAS3225P 3R3113
	DS=http://www.yuden.co.jp/productdata/catalog/en/capacitor04_e.pdf
	DK=PAS3225P3R3113/587-3260-1-ND
	MOUSER=963-PAS3225P3R3113
	TICKET=573
	GROUP=Power

Battery_contact:
	TODO=
	DS=(HDQ) http://www.ti.com/lit/an/slua408a/slua408a.pdf
	GROUP=Power

Batt_temp:
	TODO=We aren't even sure if we'll want it in the end.
	GROUP=Power

##### Audio ###################################################################

Codec:
	KEEP=

ECI:
	KEEP=

Mic_TV:
	KEEP=

Speakers:
	GROUP=Audio

Speaker:
	KEEP=

Headphone:
	KEEP=

Ring_switch:
	PART=OMTP/AHJ switch (TS3A225)
	DS=http://www.ti.com/lit/ds/symlink/ts3a225e.pdf
	DK=TS3A225ERTER/296-30111-1-ND
	MOUSER=595-TS3A225ERTER
	TICKET=572
	GROUP=Audio

Microphones:
	PART=Digital microphones (2 x SPK0415HM4H)
	DS=http://www.knowles.com/eng/content/download/3897/49396/version/6/file/SPK0415HM4H-B.pdf
	DK=SPK0415HM4H-B/423-1210-1-ND
	MOUSER=721-SPK0415HM4H-B
	GROUP=Audio

Mic_itf:
	REPLACE=Discrete circuit
##	REPLACE=EMIF02-MIC07F3
###	DS=http://www.st.com/web/en/resource/technical/document/datasheet/CD00263938.pdf
##	DS=(EMIF02-MIC07F3) http://media.digikey.com/pdf/Data%20Sheets/ST%20Microelectronics%20PDFS/EMIF02-MIC07F3.pdf
##	DK=(EMIF02-MIC07F3) EMIF02-MIC07F3/497-10699-1-ND
##	MOUSER=(EMIF02-MIC07F3) 511-EMIF02-MIC07F3
##	<HR>
##	Alternative part: <B>EMIF02-MIC06F3</B><BR>
##	EMIF02-MIC06F3 seems identical to EMIF02-MIC07F3, except for
##	lacking integrated DC blocking capacitors. Such capacitors can be
##	easily added externally.<BR>
##	DS=(EMIF02-MIC06F3) http://www.st.com/resource/en/datasheet/emif02-mic06f3.pdf
##	MOUSER=(EMIF02-MIC06F3) 511-EMIF02-MIC06F3
	GROUP=Audio

ECI:
	REPLACE=TLV1702
	DS=http://www.ti.com/lit/ds/symlink/tlv1701.pdf
	DK=TLV1702AIDGKR/296-37236-1-ND
	MOUSER=595-TLV1702AIDGKR
	GROUP=Audio

##### Illumination, drivers ###################################################

Backlight:
#	REPLACE=TPS61041
#	DS=http://www.ti.com/lit/ds/symlink/tps61040.pdf
#	DK=TPS61041DBVR/296-12686-1-ND
#	MOUSER=595-TPS61041DBVR
	REPLACE=TPS61160
	Note: there is also a TPS61160A, which seems identical except that
	it lacks the programmable internal PWM.<BR>
	DS=http://www.ti.com/lit/ds/symlink/tps61160.pdf
	<A href="http://www.ti.com/lit/an/slva471/slva471.pdf">Analog dimming</A><BR>
	DK=TPS61160DRVR/296-22942-1-ND
	MOUSER=595-TPS61160DRVR
	GROUP=Lights

LED_drivers:
	REPLACE=2 x LP55281
	DS=http://www.ti.com/lit/ds/symlink/lp55281.pdf
	DK=LP55281TL-NOPB/LP55281TL-NOPBCT-ND/1836173
	MOUSER=926-LP55281TL/NOPB
	TICKET=541
	GROUP=Lights

# Assuming QFN part (LP55231).
# BGA part (LP5523) would have better availability.
Fancy_RGB:
	PART=RGB and Aux LED driver (LP55231)
	[st:LP55231]
	DS=http://www.ti.com/lit/ds/symlink/lp55231.pdf
	DK=LP55231SQE%2FNOPB/296-39059-1-ND
	MOUSER=595-LP55231SQE/NOPB
	GROUP=Lights

IR_logic:
	PART=IR control logic (SLG46533, NX3V1G66)
	<A href="https://neo900.org/stuff/papers/ir.pdf">White paper</A><BR>

#	DS=(SLG46531) http://www.silego.com/uploads/Products/product_415/SLG46531_DS_r109_10212016.pdf
#	WEB=[Silego shop] http://www.silego.com/buy/index.php?main_page=product_info&cPath=61&products_id=428
	SLG=
	WEB=[Silego shop] http://www.silego.com/buy/index.php?main_page=product_info&cPath=61&products_id=497
	DS=(NX3V1G66) http://www.nxp.com/documents/data_sheet/NX3V1G66.pdf
	DK=(NX3V1G66) NX3V1G66GM,132/568-4759-1-ND
	GROUP=Lights

##### Illumination, LEDs ######################################################

Privacy:
	[reset:]
	PART=Privacy indicator (SMLP36RGB2W3)
	[st:SMLP36RGB2W3]
#	DS=http://rohmfs.rohm.com/en/products/databook/datasheet/opto/led/chip_multi/smlp36.pdf
	DS=http://www.rohm.com/web/global/datasheet/SMLP36RGB2W(R)
	DK=SMLP36RGB2W3/846-1097-1-ND
	MOUSER=755-SMLP36RGB2W3
	GROUP=Lights

Flash_LEDs:
#	FOUND=2 x CLN6A-MKW
#	DS=http://www.cree.com/~/media/Files/Cree/LED%20Components%20and%20Modules/HB/Data%20Sheets/CLN6A(979).pdf
#	DK=CLN6A-MKW-CJ0K0233/CLN6A-MKW-CJ0K0233CT-ND
	FOUND=2 x L130-5780002011001
	LUXEON 3020 series.
	<A href="http://talk.maemo.org/showthread.php?p=1500744">CCT 5700 K, CRI 80%.</A><BR>
	DS=http://www.lumileds.com/uploads/461/DS209-pdf
	DK=L130-5780002011001/1416-1337-1-ND
	FUTEL=L1305780002011001PHILIPSLUMILEDS9046314
	GROUP=Lights

IR_LED:
	[reset:]
	PART=IR LED (VSMB2948SL ?)
	DS=http://www.vishay.com/docs/83498/vsmb2948sl.pdf
	DK=VSMB2948SL/VSMB2948SLCT-ND
	MOUSER=78-VSMB2948SL
	NEWARK=62W1341
	GROUP=Other

IR_sense:
	PART=IR sensor (VEMD10940F)
	DS=http://www.vishay.com/docs/84171/vemd10940f.pdf
	DK=VEMD10940F/VEMD10940FCT-ND
	MOUSER=78-VEMD10940F
	GROUP=Other

Kbd_LEDs:
	FOUND=6 x MSL0201RGB
	[ssz:7]
#	DS=http://rohmfs.rohm.com/en/products/databook/datasheet/opto/led/chip_multi/msl0201rgb.pdf
	DS=http://www.rohm.com/web/global/datasheet/MSL0201RGB
	DK=MSL0201RGBW1/MSL0201RGBW1CT-ND
	TICKET=541
	GROUP=Lights

Aux_LEDs:
	PART=Auxiliary indicator LEDs (2 x MSL0104RGBU1)
#	DS=(MSL0104) http://rohmfs.rohm.com/en/products/databook/datasheet/opto/led/chip_multi/msl0104.pdf
	DS=(MSL0104) http://www.rohm.com/web/global/datasheet/MSL0104RGBW
	DK=(MSL0104) MSL0104RGBU1/MSL0104RGBU1CT-ND
	Alternate part (NRND, but has better availability):<BR>
#	DS=(MSL0101) http://rohmfs.rohm.com/en/products/databook/datasheet/opto/led/chip_multi/msl0101.pdf
	DS=(MSL0101) http://media.digikey.com/pdf/Data%20Sheets/Rohm%20PDFs/MSL0101%20Series.pdf
	DK=(MSL0101) MSL0101RGBU1/846-1189-1-ND
	GROUP=Lights

Mod_LEDs:
	PART=Monochrome LEDs on keyboard
# Red
	DS=(red) http://optoelectronics.liteon.com/upload/download/DS-22-99-0151/S_110_LTST-C190KRKT.pdf
	DK=(red) LTST-C190KRKT/160-1436-1-ND
# Blue
	MOUSER=(red) 859-LTST-C190KRKT
	DS=(blue) http://optoelectronics.liteon.com/upload/download/DS-22-99-0224/S_110_LTST-C190TBKT(0630).pdf
	DK=(blue) LTST-C190TBKT/160-1646-1-ND
	MOUSER=(blue) 859-LTST-C190TBKT
# Yellow-Orange
	DS=(orange) http://optoelectronics.liteon.com/upload/download/DS-22-99-0186/S_110_LTST-C190KFKT.pdf
	DK=(orange) LTST-C190KFKT/160-1434-1-ND
	MOUSER=(orange) 859-LTST-C190KFKT
	GROUP=Lights

##### Connectors ##############################################################

Main_flex:
	KEEP=
	Background information: TICKET=636

Main_cam:
	REPLACE=N97 cam, DF37NB-20DS-0.4V conn.
	TODO=Experimental.<BR>
	TICKET=515
	WEB=http://talk.maemo.org/showthread.php?p=1423867
#	DS=http://www.hirose.co.jp/cataloge_hp/ed_DF37_20140305.pdf
	DS=https://www.hirose.com/product/en/download_file/key_name/DF37/category/Catalog/doc_file_id/49052/?file_category_id=4&item_id=249&is_series=1
	DK=DF37NB-20DS-0.4V(51)/H11849CT-ND
	GROUP=Other

USB_AB:
	PART=Micro USB AB receptacle (47590-0001)
	DS=http://www.molex.com/pdm_docs/sd/475900001_sd.pdf
	DK=0475900001/WM17144CT-ND
	TICKET=518
	GROUP=Other

uSD:
	FOUND=ST1W008S4B
	DS=http://media.digikey.com/PDF/Data%20Sheets/JAE%20PDFs/ST1W008S4BR1500.pdf
	DK=ST1W008S4BR1500/670-2345-1-ND
	MOUSER=656-ST1W008S4BR1500
	GROUP=Storage

SIM_1:
	PART=SIM card holder (SF7W006S4ER1500)
	Hinged type, placed under the battery.<BR>
	[ssz:6]
	DS=http://jae-connector.com/en/pdf/SJ106220.pdf
	DK=SF7W006S4ER1500/670-2336-ND
	MOUSER=656-SF7W006S4ER1500
	GROUP=Telephony

SIM_2:
	PART=SIM card holder (101-00271-82)
	Slot-in type, for outside access.<BR>
	DS=http://media.digikey.com/pdf/Data%20Sheets/Amphenol%20PDFs/101-00271.pdf
	DK=101-00271-82/101-00271-82-1-ND
	GROUP=Telephony

3_5:
	KEEP=[st:Common Nokia spare]
	GROUP=Audio

Hackerbus:
	PART=Hackerbus (M50-3151042)
	<A href="https://neo900.org/stuff/papers/hb.pdf">White paper</A><BR>
	DS=http://harwin.com/includes/pdfs/M50-315.pdf
	DK=M50-3151042/952-1374-5-ND
	MOUSER=855-M50-3151042
	NEWARK=18M4532
	GROUP=Other

B2B_UPPER_LOWER:
	PART=UPPER to LOWER interconnect (2 x DF40HC(2.5)-60DS-0.4V, 2 x DF40C-60DP-0.4V)
#	DS=http://www.hirose.co.jp/cataloge_hp/ed_DF40_20140305.pdf
	DS=https://www.hirose.com/product/en/download_file/key_name/DF40/category/Catalog/doc_file_id/31649/?file_category_id=4&item_id=22&is_series=1
	DK=(DF40HC(2.5)-60DS-0.4V) DF40HC(2.5)-60DS-0.4V(51)/H11774CT-ND/2178737
	MOUSER=(DF40HC(2.5)-60DS-0.4V) 798-DF40HC2560DS04V
	DK=(DF40C-60DP-0.4V) DF40C-60DP-0.4V(51)/H11628CT-ND
	MOUSER=(DF40C-60DP-0.4V) 798-DF40C60DP0.4V51

B2B_LOWER_BOB:
	PART=LOWER to BOB interconnect (DF9-21P-1V, DF9-21S-1V)
	DS=(DF9) http://media.digikey.com/pdf/Data%20Sheets/Hirose%20PDFs/DF9.pdf
	DK=(DF9-21P-1V) DF9-21P-1V(32)/H10415CT-ND
	DK=(DF9-21S-1V) DF9-21S-1V(32)/H10403CT-ND
	<HR>
	Second source: Harwin M40<BR>
	Harwin M40-6002146 == Hirose DF9-21P-1V(32),
	Harwin M40-6202146 == Hirose DF9-21S-1V(32).<BR>
	DS=(M40) http://cdn.harwin.com/pdfs/M40-600.pdf
	DK=(M40-6002146) M40-6002146/952-3053-ND
	DK=(M40-6202146) M40-6202146/952-3057-ND
	<HR>
	Alternative connector family: FCI Conan<BR>
	DS=(Conan) http://portal.fciconnect.com/Comergent//fci/drawing/91900.pdf
	DK=(91911-21221LF) 91911-21221LF/91911-21221LF-ND
	DK=(91931-31121LF) 91931-31121LF/609-3520-1-ND

##### Connectivity ############################################################

HB_USB_PHY:
	PART=High-speed USB PHY (USB3322)
	DS=http://media.digikey.com/pdf/Data%20Sheets/Microchip%20PDFs/USB332x.pdf
	DK=USB3322C-GL-TR/USB3322C-GL-TR-ND
	MOUSER=886-USB3322C-GL-TR
	ARROW=56938374S8317732N2064
	GROUP=Other

IO_Expander:
	PART=IO Expander (2 x XRA1201, XRA1201P)
	<A href="https://neo900.org/stuff/papers/iox.pdf">White paper</A><BR>
	DS=https://www.exar.com/content/document.ashx?id=20780
	DK=(XRA1201) XRA1201IL24-F/1016-1710-ND
	MOUSER=(XRA1201) 701-XRA1201IL24-F
	DK=(XRA1201P) XRA1201PIL24-F/1016-1712-ND
	MOUSER=(XRA1201P) 701-XRA1201PIL24-F
	GROUP=Other

##### LCD, cameras ############################################################

Touch_scrn:
	REPLACE=CRTOUCHB10
	TICKET=527
	DS=http://cache.freescale.com/files/32bit/doc/data_sheet/CRTOUCHDS.pdf
	DK=CRTOUCHB10VFM/CRTOUCHB10VFM-ND
	NEWARK=89T5274
	GROUP=Sensors

Cam_switch:
	KEEP=
	DK=TS3DS26227YZTR/296-22890-1-ND
	MOUSER=595-TS3DS26227YZTR
	GROUP=Sensors

##### Switches ################################################################

Vol:
	FOUND=2 x EVQ-P42B3M
	[ssz:6]
	TICKET=518
	DS=http://industrial.panasonic.com/www-data/pdf/ATK0000/ATK0000CE18.pdf
	DK=EVQ-P42B3M/P15502CT-ND
	MOUSER=667-EVQ-P42B3M
	GROUP=Sensors

Power:
	FOUND=EVQ-P40B3M
	TICKET=518
	DS=http://industrial.panasonic.com/www-data/pdf/ATK0000/ATK0000CE18.pdf
	DK=EVQ-P40B3M/P15501CT-ND
	MOUSER=667-EVQ-P40B3M
	GROUP=Sensors

Lock:
	FOUND=ESE16
	DS=http://industrial.panasonic.com/www-data/pdf/ATB0000/ATB0000CE11.pdf
	DK=ESE-16J001/P14266SCT-ND
	MOUSER=667-ESE-16J001
	TICKET=518
	GROUP=Sensors

Slide:
	FOUND=MLX90248ESE
#	DS=http://www.melexis.com/Asset/MLX90248-(New-Generation)-Datasheet-DownloadLink-5551.aspx
	DS=https://www.melexis.com/-/media/files/documents/datasheets/mlx90248-datasheet-melexis.pdf
	DK=MLX90248ESE-EBA-000-RE/MLX90248ESE-EBA-000-RECT-ND
	GROUP=Sensors

Capture:
	FOUND=EVQQ0G03K
	WARN=EOL, last buy 2015-05-01
	DS=http://industrial.panasonic.com/www-data/pdf/ATK0000/ATK0000CE24.pdf
	DK=EVQ-Q0G03K/P15960SCT-ND
	ARROW=48750364S8392450N1386
	TICKET=518
	GROUP=Sensors

Cover:
	REPLACE=TMD26713
	DS=http://www.ams.com/eng/content/download/250304/976107/142395
	DK=TMD26713/TMD26713-TCT-ND
	GROUP=Sensors

Kbd_scan:
	NEW=Keyboard scanner (TCA8418)
	DS=http://www.ti.com/lit/ds/symlink/tca8418.pdf
	DK=TCA8418RTWR/296-25223-1-ND
	MOUSER=595-TCA8418RTWR
	GROUP=Sensors

##### Sensors #################################################################

lid_mag:
	FOUND=MLX90248ESE
#	DS=http://www.melexis.com/Asset/MLX90248-(New-Generation)-Datasheet-DownloadLink-5551.aspx
	DS=https://www.melexis.com/-/media/files/documents/datasheets/mlx90248-datasheet-melexis.pdf
	DK=MLX90248ESE-EBA-000-RE/MLX90248ESE-EBA-000-RECT-ND
	GROUP=Sensors

Accel:
	KEEP=
	DK=LIS302DLTR/497-5911-1-ND
	MOUSER=511-LIS302DLTR
	GROUP=Sensors

9_Axis:
	NEW=9-Axis sensor (BMX055)
	Acceleration, Gyroscope, Magnetic, and Temperature.<BR>
#	DS=http://ae-bst.resource.bosch.com/media/products/dokumente/bmx055/BST-BMX055-DS000-02.pdf
	DS=https://ae-bst.resource.bosch.com/media/_tech/media/datasheets/BST-BMX055-DS000-02.pdf
#	MOUSER=262-BMX055
	FUTEL=BMX0550273141179BOSCH5040485
	GROUP=Sensors

Baro_humidity:
	NEW=Pressure/Humidity/Temperature (BME280)
#	DS=https://ae-bst.resource.bosch.com/media/products/dokumente/bme280/BST-BME280_DS001-11.pdf
	DS=https://ae-bst.resource.bosch.com/media/_tech/media/datasheets/BST-BME280_DS001-11.pdf
#	MOUSER=262-BME280
	FUTEL=BME280BOSCH2053946
	GROUP=Sensors

Stylus:
	NEW=Stylus presence detector (TMD26713)
	Note: placement still needs to be specified.<BR>
	DK=TMD26713/TMD26713-TCT-ND
	DS=http://www.ams.com/eng/content/download/250304/976107/142395
	GROUP=Sensors

##### Vibrator ################################################################

Vibra:
	REPLACE=304-106
#	DS=https://catalog.precisionmicrodrives.com/order-parts/datasheet/304-106-5mm-vibration-motor-7mm-type
#	DS=https://www.precisionmicrodrives.com/file/5647/download?token=qDFOrtaf
	WEB=https://catalog.precisionmicrodrives.com/order-parts/product/304-106-5mm-vibration-motor-7mm-type
	GROUP=Other

#
# To do:
#

# FX6-20P-0.8SV b2b to uSD
# FX6-20S-0.8SV b2b on uSD

# B2B on upper is
# DF40C-60DS-0.4V 2x
# B2B on lower is
# DF40C-60DP-0.4V

#
# Pending tickets:
#
# Skipped tickets:
# TICKET=526 GPS separation vs. integration
#	Note: no separate GPS shown for Neo900
# TICKET=573 supercap charger (LTC3355)
# 
