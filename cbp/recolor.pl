#!/usr/bin/perl

sub usage
{
	print STDERR <<"EOF";
usage: $0 old_set new_set ... [file]

  set:  fg bg fill
EOF
	exit(1);
}


while ($#ARGV >= 5) {
	@old = splice(@ARGV, 0, 3);
	@new = splice(@ARGV, 0, 3);
	$map{join(" ", @old)} = [ @new ];
}

&usage unless $#ARGV < 1;


while (<>) {
	if ($_ !~ /^1\s+3\s+/) {
		print;
		next;
	}
	@a = split(/\s+/);
	$new = $map{"$a[4] $a[5] $a[8]"};
	if (defined $new) {
		($a[4], $a[5], $a[8]) = @{ $new };
		print join(" ", @a), "\n";
	} else {
		print;
	}
}
