#!/usr/bin/perl
#
# pickpad.pl - Pick and highlight pads by grid position
#
# Written 2016 by Werner Almesberger
# Copyright 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


sub usage
{
	print STDERR <<"EOF";
usage: $0 [-d comment ...] [[style] -a] [[style] pad ...}

  -a		set all circles to the specified style
  -d comment	delete object/compound marked with the comment

style options:

  -b color	background color
  -f color	foreground color
  -r		reset style to change nothing
  -s fill_style	fill style
  -w with	line width
EOF
}


# Read the FIG file and identify coordinates

while ($#ARGV != -1) {
	if ($ARGV[0] eq "-d") {
		shift @ARGV;
		&usage unless defined $ARGV[0];
		push(@delete, $ARGV[0]);
	} else {
		last;
	}
	shift @ARGV;
}

FIG: while (<STDIN>) {
	my @a = split(/\s+/);

	if ($skip) {
		$skip++ if /^6/;
		$skip-- if /^-6/;
		$skip = 0 if $skip == 1;
		next;
	}

	for $d (@delete) {
		if (/^#\s+.*\b$d\b/) {
			$skip = 1;
			next FIG;
		}
	}

	$gx{$1} = $a[11] if /^4\s.*\s(\d\d?)\\001/;
	if (/^4\s.*\s([A-Z][A-Z]?)\\001/) {
		$gy{$1} = $a[12];
		$gh{$1} = $a[9];
	}
	push(@fig, $_);
}

# Associate circles with grid points and "grey out" all circles

for (my $i = 0; $i <= $#fig; $i++) {
	next unless $fig[$i] =~ /^1\s+[34]\s+/;

	my @a = split(/\s+/, $fig[$i]);
	my ($x, $y) = ($a[12], $a[13]);
	my $cx = undef;
	my $cy = undef;

	for (keys %gx) {
		if ($gx{$_} == $x) {
			die if defined $cx;
			$cx = $_;
		}
	}
	for (keys %gy) {
		if ($gy{$_} >= $y && $gy{$_} - $gh{$_} <= $y) {
			die if defined $cy;
			$cy = $_;
		}
	}

	die unless defined $cx && defined $cy;

	die if defined $c{$cx . $cy};
	$c{$cy . $cx} = $i;
}

# Highlight selected circles

sub update
{
	local ($i) = @_;
	my @a = split(/\s+/, $fig[$i]);

	$a[3] = $width if defined $width;
	$a[4] = $fg if defined $fg;
	$a[5] = $bg if defined $bg;
	$a[8] = $fill if defined $fill;

	$fig[$i] = join(" ", @a). "\n";
}


while ($#ARGV != -1) {
	if ($ARGV[0] eq "-a") {
		for (my $i = 0; $i <= $#fig; $i++) {
			next unless $fig[$i] =~ /^1\s+[34]\s+/;
			&update($i);
		}
	} elsif ($ARGV[0] eq "-b") {
		shift @ARGV;
		&usage unless defined $ARGV[0];
		$bg = $ARGV[0];
	} elsif ($ARGV[0] eq "-f") {
		shift @ARGV;
		&usage unless defined $ARGV[0];
		$fg = $ARGV[0];
	} elsif ($ARGV[0] eq "-r") {
		undef $width;
		undef $fg;
		undef $bg;
		undef $fill;
	} elsif ($ARGV[0] eq "-s") {
		shift @ARGV;
		&usage unless defined $ARGV[0];
		$fill = $ARGV[0];
	} elsif ($ARGV[0] eq "-w") {
		shift @ARGV;
		&usage unless defined $ARGV[0];
		$width = $ARGV[0];
	} elsif ($ARGV[0] =~ /^-/) {
		&usage;
	} else {
		die "unknown grid point $_" unless defined $c{$ARGV[$i]};

		&update($c{$ARGV[$i]});
	}
	shift @ARGV;
}

# Write modified FIG file

print join("", @fig);
