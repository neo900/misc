/*
 * dev.c - Changeable device content
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "dev.h"


bool nvm_bit(const struct nvm *nvm, int n)
{
	assert(n >= 0);
	assert(n < NVM_BITS);
	return nvm->nvm[n];
}


int nvm_vec(const struct nvm *nvm, int n, int len)
{
	int res = 0;
	int i;

	for (i = 0; i != len; i++)
		res |= nvm_bit(nvm, n + i) << i;
	return res;
}


bool cm_bit(const struct dev *dev, int in)
{
	int n = nvm_vec(&dev->nvm, in, 6);

	assert(n >= 0);
	assert(n < CM_LINES);
	return dev->cm[n].v;
}


void cm_set(struct dev *dev, int n, int value)
{
	assert(n >= 0);
	assert(n < CM_LINES);

	if (value == -1)
		return;
	if (dev->cm[n].v == value)
		return;
	dev->changed = 1;
	dev->cm[n].v = value;
}


void cm_set_output(struct dev *dev, int n, int value)
{
	assert(n >= 0);
	assert(n < CM_LINES);

	if (dev->cm[n].input && value != -1) {
		if (dev->cm[n].input == im_tap)
			return;
		fprintf(stderr, "trying to set input %d (%s/%s)\n",
		    n, dev->cm[n].name, dev->cm[n].generic);
		exit(1);
	}

	cm_set(dev, n, value);
}


uint8_t cm_find(struct dev *dev, const char *name)
{
	int i;

	for (i = 0; i != CM_LINES; i++) {
		if (dev->cm[i].name && !strcmp(dev->cm[i].name, name))
			return i;
		if (dev->cm[i].generic && !strcmp(dev->cm[i].generic, name))
			return i;
	}
	fprintf(stderr, "unknown signal \"%s\n", name);
	exit(1);
}


void cm_input(struct dev *dev, int n, enum input_mode mode)
{
	assert(n >= 0);
	assert(n < CM_LINES);

	dev->cm[n].input = mode;
}
