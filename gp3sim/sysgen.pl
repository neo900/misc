#!/usr/bin/perl
#
# sysgen.pl - Generate the model-specific C code
#
# Written 2015 by Werner Almesberger
# Copyright 2015 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

# ----- Command line processing ------------------------------------------------

sub usage
{
	print STDERR <<"EOF";
usage: $0 [-i input,...] part.cm part.map design.gp3 [test.tst] [script.scr]
EOF
	exit(1);
}


if ($ARGV[0] eq "-i") {
	@inputs = split(',', $ARGV[1]);
	shift @ARGV;
	&usage unless defined shift @ARGV;
}

&usage if $ARGV[0] =~ /^-/;
&usage unless $#ARGV >= 2 && $#ARGV <= 4;

$cm_file = $ARGV[0];
$map_file = $ARGV[1];
$gp3_file = $ARGV[2];
if (defined $ARGV[3]) {
	if ($ARGV[3] =~ /\.tst$/) {
		$test_file = $ARGV[3];
		$script_file = $ARGV[4];
	} else {
		$script_file = $ARGV[3];
		&usage if $#ARGV == 4;
	}
}

# ----- Read connection matrix definition -------------------------------------

#
# Global variables set here:
#
# $seen[CM]	presence check for CM entries
# $out{NAME}	define which CM signal NAME drives
# $type[CM]	define what item / function block is
# $param[CM]	parameters of the respective instance
#
# CM is the signal number of the connection matrix.
# NAME is the unique name of the item (function block).
#

open(CM, $cm_file) || die "$cm_file: $!";
while (<CM>) {
	s/#.*//;
	next if /^\s*$/;
	next if /^-/;	# @@@
	die "connection matrix syntax" unless /^(\d+)\s+(\S+)\s*/;
	my $cm = $1;
	my $name = $2;
	die "duplicate CM output $1" if defined $seen[$1];
	die "duplicate item $name" if defined $out{$name};
	$seen[$cm] = 1;
	$out{$name} = $cm;
	die "CM type syntax" unless $' =~ /^$|^=\s*([A-Za-z_0-9]+)\s*/;
	$type[$cm] = $1;
	die "CM parameters syntax" unless $' =~ /^$|^\(((\d+,\s*)*\d+)\)\s*$/;
	$param[$cm] = $1;
}
close CM;

# ----- Read GreenPAK designer item map ---------------------------------------

#
# Global variables set here:
#
# $item{CAPTION}	unique item name corresponding to the caption
#
# CAPTION is the item's unique name as used by the GreenPAK tools
#

open(MAP, $map_file) || die "$map_file: $!";
while (<MAP>) {
	s/#.*//;
	next if /^\s*$/;
	die "map syntax" unless /^(\S+)(\s+(\S.*?))?\s*$/;
	die "unknown item $1" unless defined $out{$1};
	next if $2 eq "";
	die "duplicate map entry \"$3\"" if defined $item{$3};
	$item{$3} = $1;
}
close MAP;

# ----- Read design -----------------------------------------------------------

#
# Global variables set here:
#
# $cm_label[CM]		label on the function block driving a CM signal
# $label_cm{LABEL} 	CM signal number belonging to a label
#
# LABEL is the label attached (!) to the function block
#
# @@@ Note: we assume that each function block drives only one signal.
# This is not true in all cases. Fix later.
#

open(GP3, $gp3_file) || die "$gp3_file $!";
while (<GP3>) {
	if (/<nvmData\s+registerLenght=\"(\d+)\"[^>]*>([^<]+)/) {
		$nvm_len = $1;
		$nvm_data = $2;
		next;
	}
#	if (/<wire\s.*output="(\d+)".*wireText="([^"]*)"/) {
#		if (defined $wire[$1]) {
#			die "wire $1 \"$wire[$1]\" vs \"$2\""
#			    unless $wire[$1] eq $2;
#		} else {
#			push(@ids, $1);
#			$wire[$1] = $2;
#		}
#		next;
#	}
	if (/<item\s.*caption="([^"]*)"/) {
		$caption = $1;
		next;
	}
	if (/<\/item>/) {
		undef $caption;
		next;
	}
	if (/<textLabel.*?>([^<]+)</) {
		next unless defined $caption;
		die "cannot map labeled item \"$1\" ($caption)"
		    unless defined $item{$caption};
		my $cm = $out{$item{$caption}};
		if (defined $label_cm{$1}) {
			warn "ignoring output $1 ($caption)";
			next;
		}
		$cm_label[$cm] = $1;
		$label_cm{$1} = $cm;
		next;
	}
}
close GP3;

die "no NVM" unless defined $nvm_data;

# ----- Generate simulator ----------------------------------------------------

print "/* MACHINE-GENERATED. DO NOT EDIT ! */\n\n\n";

# init()

print "static void init(struct dev *dev)\n{\n";
print "\tmemset(dev, 0, sizeof(struct dev));\n";
my $n = 0;
for (split(/\s+/, $nvm_data)) {
	my $v = hex($_);
	for (my $i = 0; $i != 8; $i++) {
		print "\tdev->nvm.nvm[$n] = ", ($v >> $i) & 1, ";\n";
		$n++;
	}
}
for (my $cm = 0; $cm <= $#cm_label; $cm++) {
	next unless defined $cm_label[$cm];
	print "\tdev->cm[$cm].name = \"$cm_label[$cm]\";\n";
	$label{$cm_label[$cm]} = $cm;
}
for (sort keys %out) {
	print "\tdev->cm[$out{$_}].generic = \"$_\";\n";
	die "duplicate CM name $_"
	    if exists $label{$_} && $label{$_} ne $out{$_};
	$label{$_} = $out{$_};
}
print "}\n\n\n";

for (@inputs) {
	die "unknown input $_" unless exists $label{$_};
	$is_input[$label{$_}] = 1;
}

# iterate()

print "static void iterate(struct dev *dev)\n{\n";
print "\tdev->changed = 0;\n";
for (my $cm = 0; $cm <= $#cm_label; $cm++) {
	my $label = $cm_label[$cm];
	next unless defined $label;
	die "unknown type of $name" unless defined $type[$cm];
	my $comma = defined $param[$cm] ? ", " : "";
	print
	    "\tcm_set_output(dev, $cm, op_$type[$cm](dev$comma$param[$cm]));\n";
}
print "}\n\n\n";

# dump()

print "static void dump(struct dev *dev)\n{\n";
for (sort { $a cmp $b } keys %label_cm) {
	my $cm = $label_cm{$_};
	next unless defined $cm;
	print "\tprintf(\"%-20s = %d\\n\", \"$_\", dev->cm[$cm].v);\n";
}
print "}\n\n\n";

# test()



sub defines
{
	local ($all_const) = @_;

	for (sort keys %label) {
		my $cm = $label{$_};
		next unless defined $cm;
		my $const =
		    $is_input[$cm] && !$all_const ? "" : "(const bool) ";
		print "#define $_ $const(dev->cm[$cm].v)\n";
	}
}


sub undefs
{
	for (sort keys %label) {
		my $cm = $label{$_};
		next unless defined $cm;
		print "#undef $_\n";
	}
}


if (defined $test_file) {
	print "#define HAVE_TEST\n\n";

	&defines(1);

	open(TEST, $test_file) || die "$test_file: $!";
	$s = join("", <TEST>);
	close TEST;

	print "\nstatic bool test(struct dev *dev)\n{\n";
	while (1) {
		if ($s =~ m|^\s*//[^\n]*?\n|s) {
			print "$&";
			$s = $';
			next;
		}
		last unless $s =~ /;/m;
		($t0, $t1) = ($`, $');
		if ($t0 =~ /^(\s*)(\S+)\s*==/s) {
			print "$1__TEST($2, $');"
		} else {
			print "$t0;";
		}
		$s = $t1;
	}
	print $s;
	print "\treturn 1;\n";
	print "}\n";

	&undefs;
}

# script()

exit unless defined $script_file;

print "#define HAVE_SCRIPT\n\n";

&defines(0);

open(SCRIPT, $script_file) || die "$script_file: $!";
$s = join("", <SCRIPT>);
close SCRIPT;

print "\nstatic bool script(struct dev *dev)\n{\n";
while (1) {
	if ($s =~ m|^\s*//[^\n]*?\n|s) {
		print "$&";
		$s = $';
		next;
	}
	last unless $s =~ /;/m;
	($t0, $t1) = ($`, $');
	if ($t0 =~ /^(\s*)(\S+)\s*==/s) {
		print "$1__TEST($2, $');"
	} else {
		print "$t0;";
	}
	$s = $t1;
}
print $s;
print "\treturn 1;\n";
print "}\n";
