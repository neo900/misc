/*
 * sys.c - Simulator framework
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "dev.h"
#include "op.h"


static struct dev dev;


static void __attribute__((unused)) update(void);


#define	__TEST(expect, result) \
	do { \
		if ((expect) != (result)) { \
			fprintf(stderr, "%s (%d) != %s (%d)\n", \
			    #expect, expect, #result, result); \
			return 0; \
		} \
	} while (0)


#include "sys.inc"


#ifndef HAVE_TEST

static void test(struct dev *dev)
{
	fprintf(stderr, "no test patterns\n");
	exit(1);
}


#endif


static void solve(struct dev *dev)
{
	int i;

	for (i = 0; i != MAX_ITERATIONS; i++) {
		iterate(dev);
		if (!dev->changed)
			break;
	}
	if (dev->changed) {
		fprintf(stderr, "design does not converge\n");
		exit(1);
	}
}


static void do_test(struct dev *dev)
{
	if (!test(dev)) {
		dump(dev);
		exit(1);
	}
}


static void update(void)
{
	solve(&dev);    /* use of global "dev" is ugly */
#ifdef HAVE_TEST
	do_test(&dev);
#endif
}


static void manual(struct dev *dev, int argc, char **argv)
{
	int i, cm;

	for (i = 1; i != argc; i++) {
		if (*argv[i] == '+') {
			cm = cm_find(dev, argv[i] + 1);
			cm_input(dev, cm, im_tap);
		} else {
			cm = cm_find(dev, argv[i]);
			cm_input(dev, cm, im_input);
		}
		cm_set(dev, cm, 1);
	}

#ifdef HAVE_SCRIPT
	if (!script(dev)) {
		dump(dev);
		exit(1);
	}
#else
	solve(dev);

	dump(dev);
#endif
}


static void automatic(struct dev *dev, int argc, char **argv)
{
	uint8_t cm_map[CM_LINES];
	int active = 0;
	uint64_t i;
	int cm, j;

	while (argc) {
		if (**argv == '+') {
			cm = cm_find(dev, *argv + 1);
			cm_input(dev, cm, im_tap);
		} else {
			cm = cm_find(dev, *argv);
			cm_input(dev, cm, im_input);
		}
		cm_map[active] = cm;
		for (j = 0; j != active; j++)
			if (cm_map[j] == cm) {
				fprintf(stderr,
				    "CM line %d in entries [%d] and [%d]\n",
				     cm, j, active);
				exit(1);
			}
		active++;
		argc--;
		argv++;
	}
	for (i = 0; i != (uint64_t) 1 << active; i++) {
		for (j = 0; j != active; j++)
			cm_set(dev, cm_map[j], (i >> j) & 1);
		solve(dev);
		do_test(dev);
	}

}


int main(int argc, char **argv)
{
	init(&dev);

	if (argc == 1 || strcmp(argv[1], "-t")) {
		manual(&dev, argc, argv);
	} else {
		automatic(&dev, argc - 2, argv + 2);
	}

	return 0;
}
