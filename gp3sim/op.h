/*
 * op.h - Operate function blocks
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef OP_H
#define	OP_H

#include "dev.h"


int op_VDD(const struct dev *dev);

int op_PIN_IN(const struct dev *dev, int pin, int cfg);
int op_PIN_IO(const struct dev *dev, int pin, int in, int cfg);
int op_PIN_SUPER(const struct dev *dev, int pin, int in, int cfg);
int op_PIN_OE(const struct dev *dev, int pin, int in_oe, int cfg);

int op_LUT2(const struct dev *dev, int in, int table);
int op_LUT2_FF(const struct dev *dev, int in, int table, int mode, int ff_cfg);
int op_LUT3(const struct dev *dev, int in, int table);
int op_LUT3_FF(const struct dev *dev, int in, int table, int mode, int ff_cfg);
int op_LUT4_CNT(const struct dev *dev, int in, int table, int mode, int ff_cfg);

int op_INV(const struct dev *dev, int in);

int op_ACMP(const struct dev *dev, int pin, int cfg);

#endif /* !OP_H */
