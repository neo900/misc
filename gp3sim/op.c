/*
 * op.c - Operate function blocks
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <assert.h>

#include "dev.h"


/* ----- VDD --------------------------------------------------------------- */


int op_VDD(const struct dev *dev)
{
	return 1;
}


/* ----- PIN --------------------------------------------------------------- */


int op_PIN_IN(const struct dev *dev, int pin, int cfg)
{
	return -1;
}


int op_PIN_IO(const struct dev *dev, int pin, int in, int cfg)
{
	return -1;
}


int op_PIN_SUPER(const struct dev *dev, int pin, int in, int cfg)
{
	return -1;
}


int op_PIN_OE(const struct dev *dev, int pin, int in_oe, int cfg)
{
	return -1;
}


/* ----- LUT helper functions ---------------------------------------------- */


static int idx_lut2(const struct dev *dev, int in)
{
	return cm_bit(dev, in) + 2 * cm_bit(dev, in + 6);
}


static int idx_lut3(const struct dev *dev, int in)
{
	return idx_lut2(dev, in) + 4 * cm_bit(dev, in + 12);
}


static int idx_lut4(const struct dev *dev, int in)
{
	return idx_lut3(dev, in) + 8 * cm_bit(dev, in + 18);
}


/* ----- LUT --------------------------------------------------------------- */


int op_LUT2(const struct dev *dev, int in, int table)
{
	return nvm_bit(&dev->nvm, table + idx_lut2(dev, in));
}


int op_LUT2_FF(const struct dev *dev, int in, int table, int mode, int ff_cfg)
{
	assert(!nvm_bit(&dev->nvm, mode));
	return op_LUT2(dev, in, table);
}


int op_LUT3(const struct dev *dev, int in, int table)
{
	return nvm_bit(&dev->nvm, table + idx_lut3(dev, in));
}


int op_LUT3_FF(const struct dev *dev, int in, int table, int mode, int ff_cfg)
{
	assert(!nvm_bit(&dev->nvm, mode));
	return op_LUT3(dev, in, table);
}


int op_LUT4_CNT(const struct dev *dev, int in, int table, int mode, int ff_cfg)
{
	assert(!nvm_bit(&dev->nvm, mode));
	return nvm_bit(&dev->nvm, table + idx_lut4(dev, in));
}


/* ----- INV --------------------------------------------------------------- */


int op_INV(const struct dev *dev, int in)
{
	return !cm_bit(dev, in);
}


/* ----- ACMP -------------------------------------------------------------- */


int op_ACMP(const struct dev *dev, int pin, int cfg)
{
	return -1;
}
