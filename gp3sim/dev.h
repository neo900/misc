/*
 * dev.h - Changeable device content
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef DEV_H
#define	DEV_H

#include <stdbool.h>
#include <stdint.h>


#define	NVM_BITS	1024
#define	CM_LINES	64

#define	MAX_ITERATIONS	CM_LINES


struct nvm {
	bool nvm[NVM_BITS];
};


enum input_mode {
	im_none,	/* output or undefined */
	im_input,	/* input */
	im_tap,		/* used as input, ignore element driving it */
};

struct dev {
	struct {
		bool v;
		enum input_mode input;	/* set by simulation framework */
		const char *name;	/* name assigned in design */
		const char *generic;	/* generic name from *.cm */
	} cm[CM_LINES];
	struct nvm nvm;
	bool changed;
};


bool nvm_bit(const struct nvm *nvm, int n);
int nvm_vec(const struct nvm *nvm, int n, int len);

bool cm_bit(const struct dev *dev, int in);
void cm_set(struct dev *dev, int n, int value); /* -1 to leave as it is */
void cm_set_output(struct dev *dev, int n, int value); /* fail if input */
void cm_input(struct dev *dev, int n, enum input_mode mode);

uint8_t cm_find(struct dev *dev, const char *name);

#endif /* !DEV_H */
