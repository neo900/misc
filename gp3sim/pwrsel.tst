// Assertions that must be valid each time the simulation has converged.
//
// This file goes to the body of a C function, so most things that are allowed
// in C can be done here. There are the following exceptions:
//
// - only // comments are supported, and they must be at the beginning
//   of the line or only preceded by whitespace,
//
// - statements of the form
//   name == expression;
//   (which would be unusual in regular C) become assertions.
//
// - macros provide access to the connection matrix, using the names of the
//   elements that drive the respective line of the matrix.
//
// Inputs can be changed by assigning to them. The function update() updates
// the simulation after such changes.
//

// A/B stage

MODEM_1_3V == !SEL_Q ? MODEM_A_3V : MODEM_B_3V;
MODEM_1_1V8 == !SEL_Q ? MODEM_A_1V8 : MODEM_B_1V8;
MODEM_2_3V == SEL_Q ? MODEM_A_3V : MODEM_B_3V;
MODEM_2_1V8 == SEL_Q ? MODEM_A_1V8 : MODEM_B_1V8;

CPU_1_3V == !SEL ? CPU_PWR_EN && CPU_3V_n1V8 : 0;
CPU_1_1V8 == !SEL ? CPU_PWR_EN && !CPU_3V_n1V8 : 0;
CPU_2_3V == SEL ? CPU_PWR_EN && CPU_3V_n1V8 : 0;
CPU_2_1V8 == SEL ? CPU_PWR_EN && !CPU_3V_n1V8 : 0;

// Modem/CPU stage

VSEL_1_3V == CPU_1_3V || MODEM_1_3V;
VSEL_1_EN == VSEL_1_3V || SWP_1 || CPU_1_1V8 || MODEM_1_1V8;
VSEL_2_3V == CPU_2_3V || MODEM_2_3V;
VSEL_2_EN == VSEL_2_3V || SWP_2 || CPU_2_1V8 || MODEM_2_1V8;
