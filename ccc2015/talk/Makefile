SHELL=/bin/bash

PICS_LIGHT = theory.pdf reality.pdf separation.pdf however.pdf \
	rogue.pdf rogue2.pdf unbundle.pdf monitor.pdf switch.pdf
PICS = origins.pdf maemo.pdf thick.pdf sandwich.pdf n900-stacking.pdf \
	spacer.pdf

NAME = talk
LONG_NAME = neo900-$(NAME)

.PHONY:		all p4 spotless clean images upload

all:		$(NAME).pdf
		xpdf $(NAME).pdf

$(NAME).pdf:	$(NAME).tex $(PICS) spacer-ray.png
	# $(PICS_LIGHT) neo900-thickened.png
		TEXINPUTS=../light: pdflatex $(NAME)

$(NAME).ps:	$(NAME).pdf
		pdftops -paper A4 -expand $< $@

p4:		$(NAME).ps
		mpage -4 $< | lpr

%.pdf:		%.fig
		fig2dev -L pdf $< $@ || { rm -f $@; exit 1; }

spacer-ray.png:	spacer-ray.xcf
		xcf2png $< >$@ || { rm -f $@; exit 1; }

# xcf2png from xcftools
neo900-thickened.png: neo900-thickened.xcf
		xcf2png $< >$@ || { rm -f $@; exit 1; }

theory.pdf:	one.fig
		perl ./figfilt.pl <$< one theory | fig2dev -L pdf >$@

reality.pdf:	one.fig
		perl ./figfilt.pl <$< one reality | fig2dev -L pdf >$@

separation.pdf:	two.fig
		perl ./figfilt.pl <$< two | fig2dev -L pdf >$@

however.pdf:	two.fig
		perl ./figfilt.pl <$< two however | fig2dev -L pdf >$@

rogue.pdf:	two.fig
		perl ./figfilt.pl <$< two however rogue | fig2dev -L pdf >$@

rogue2.pdf:	two.fig
		perl ./figfilt.pl <$< two however rogue rogue2 | \
		    fig2dev -L pdf >$@

unbundle.pdf:	two.fig
		perl ./figfilt.pl <$< two unbundle | fig2dev -L pdf >$@

monitor.pdf:	two.fig
		perl ./figfilt.pl <$< two monitor monitor-only | \
		    fig2dev -L pdf >$@

switch.pdf:	two.fig
		perl ./figfilt.pl <$< two monitor switch | fig2dev -L pdf >$@

clean:
		rm -f $(NAME).aux $(NAME).log $(NAME).toc
		rm -f $(NAME).nav $(NAME).out $(NAME).snm
		rm -f $(PICS) $(PICS_LIGHT)
		rm -f neo900-thickened.png
		rm -f origins.pdf maemo.pdf

spotless:
		rm -f $(NAME).pdf
		rm -f $(LONG_NAME).tar.bz2

images:		$(LONG_NAME).tar.bz2

$(LONG_NAME).tar.bz2: $(NAME).pdf
		rm -rf $(LONG_NAME)
		mkdir -p $(LONG_NAME)
		convert -geometry 1920x1080 -density 300x300 -quality 100 $< \
		    -scene 1 $(LONG_NAME)/$(LONG_NAME)-%02d.jpg
		tar cfj $@ $(LONG_NAME)/

upload:		$(NAME).pdf $(LONG_NAME).tar.bz2
		neo900 $(NAME).pdf $(LONG_NAME).tar.bz2 pub/werner/ccc2015talk/
