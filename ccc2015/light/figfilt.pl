#!/usr/bin/perl
for (@ARGV) {
	$select{$_} = 1;
}

$skip = 0;
# 0: don't skip
# 1: skip next
# 2: skip indented
while (<STDIN>) {
	if (/^#\s+/) {
		$tmp = $';
		$tmp =~ s/\s*//g;
		$skip = 1;
		for (split(",", $tmp)) {
			if (/^!/) {
				$skip = 0 unless $select{$'};
			} else {
				$skip = 0 if $select{$_};
			}
		}
		next if $skip;
	}
	if (/^#/) {
		die if $skip;
		print;
		next;
	}
	if (/^\S/) {
		if ($skip == 1) {
			$skip = 2;
			next;
		}
		$skip = 0;
	}
	next if $skip;
	print;
}
