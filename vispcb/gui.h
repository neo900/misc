/*
 * gui.c - User interface
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef GUI_H
#define	GUI_H

#include "ppm.h"
#include "overlay.h"


void gui(const struct ppm *ppm, struct overlay *overlay);

#endif /* !GUI_H */
