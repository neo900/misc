/*
 * class.h - Classify pixels
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef CLASS_H
#define	CLASS_H

#include <stdint.h>


enum classes {
	class_none	= 0,
	class_bg,
	class_pcb,
	n_classes
};

struct classification {
	unsigned x, y;
	uint8_t *data;
};


static inline uint8_t class_get(const struct classification *cls,
    unsigned x, unsigned y)
{
	return cls->data[y * cls->x + x];
}


void analyze(uint8_t r, uint8_t g, uint8_t b);
void classify(struct classification *cls, const struct ppm *ppm);

#endif /* !CLASS_H */
