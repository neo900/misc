/*
 * gui.c - User interface
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>

#include "SDL.h"
#include "SDL_gfxPrimitives.h"

#include "ppm.h"
#include "overlay.h"
#include "class.h"
#include "gui.h"


#define	XRES		640
#define	YRES		480

#define	SDL_SURFACE	SDL_HWSURFACE

#define	TOGGLE_X0	10
#define	TOGGLE_Y0	8
#define	TOGGLE_XSTEP	16
#define	TOGGLE_R	6

#define	CONTRAST_RGBA	0xffffffff
#define	TOGGLE_ON_RGBA	0xff4010ff
#define	TOGGLE_OFF_RGBA	0x606060ff


static Sint16 xres, yres;
static Sint16 xorig, yorig;
static bool hide[n_classes];
static bool contrast = 0;
static bool show_image = 1;
static bool show_overlay = 1;


/* ----- Draw the overlay -------------------------------------------------- */


static Sint16 ovl_x, ovl_y;


static void moveto(void *user, int x, int y)
{
	ovl_x = x - xorig;
	ovl_y = y - yorig;
}


static void lineto(void *user, int x, int y)
{
	SDL_Surface *s = user;

	x -= xorig;
	y -= yorig;
	aalineColor(s, ovl_x, ovl_y, x, y, 0xff2020ff);
	ovl_x = x;
	ovl_y = y;
}


/* ----- Show the image ---------------------------------------------------- */


static void draw_image(SDL_Surface *s, const struct ppm *ppm,
    const struct classification *cls)
{
	Sint16 xend, yend;
	Sint16 x, y;

	xend = xorig + xres > ppm->x ? ppm->x - xorig : xres;
	yend = yorig + yres > ppm->y ? ppm->y - yorig : yres;
	for (y = 0; y != yend; y++)
		for (x = 0; x != xend; x++)
			if (!hide[class_get(cls, xorig + x, yorig + y)])
				pixelColor(s, x, y,
				    contrast ? CONTRAST_RGBA :
				    ppm_rgb(ppm, xorig + x, yorig + y) << 8 |
				    0xff);
}


static void draw_status(SDL_Surface *s)
{
	unsigned i;

	for (i = 0; i != n_classes; i++)
		filledCircleColor(s, TOGGLE_X0 + TOGGLE_XSTEP * i, TOGGLE_Y0,
		    TOGGLE_R, hide[i] ? TOGGLE_OFF_RGBA : TOGGLE_ON_RGBA);
}


static void draw(SDL_Surface *s, const struct ppm *ppm,
    const struct classification *cls, const struct overlay *overlay)
{
	SDL_FillRect(s, NULL, SDL_MapRGB(s->format, 0, 0, 0));
	if (show_image)
		draw_image(s, ppm, cls);
	if (show_overlay && overlay)
		overlay_draw(overlay, moveto, lineto, s);
	draw_status(s);
}


/* ----- Main loop --------------------------------------------------------- */


static void button_event(SDL_MouseButtonEvent *button, const struct ppm *ppm)
{
	Uint16 x = button->x + xorig;
	Uint16 y = button->y + yorig;
	Uint16 xh = xres >> 1;
	Uint16 yh = yres >> 1;

	switch (button->button) {
	case 1:
		if (x < ppm->x && y < ppm->y)
			analyze(ppm_r(ppm, x, y), ppm_g(ppm, x, y),
			    ppm_b(ppm, x, y));
		break;
	case 2:
		xorig = x - xh;
		yorig = y - yh;
		if (xorig + xres > ppm->x)
			xorig = ppm->x - xres;
		if (yorig + yres > ppm->y)
			yorig = ppm->y - yres;
		if (xorig < 0)
			xorig = 0;
		if (yorig < 0)
			yorig = 0;
		break;
	}
}


static bool event_loop(SDL_Surface **surf, const struct ppm *ppm,
    struct overlay *overlay)
{
	//SDL_Surface *s = *surf;
	SDL_Event event;

	while (1) {
		SDL_WaitEvent(&event);

		switch (event.type) {
		case SDL_MOUSEBUTTONDOWN:
			button_event(&event.button, ppm);
			return 0;
		case SDL_VIDEORESIZE:
			xres = event.resize.w;
			yres = event.resize.h;
			*surf = SDL_SetVideoMode(xres, yres, 0,
			    SDL_SURFACE | SDL_RESIZABLE);
			return 0;
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym) {
			case SDLK_c:
				contrast = 1;
				return 0;
			case SDLK_i:
				show_image = !show_image;
				return 0;
			case SDLK_o:
				show_overlay = !show_overlay;
				return 0;
			case SDLK_q:
				return 1;
			case SDLK_r:
				if (overlay)
					overlay_reload(overlay);
				return 0;
			case SDLK_0:
				hide[0] ^= 1;
				return 0;
			case SDLK_1:
				hide[1] ^= 1;
				return 0;
			case SDLK_2:
				hide[2] ^= 1;
				return 0;
#if 0
			case SDLK_4:
				hide[3] ^= 1;
				return 0;
#endif
			default:
				break;
			}
			break;
		case SDL_KEYUP:
			switch (event.key.keysym.sym) {
			case SDLK_c:
				contrast = 0;
				return 0;
			default:
				break;
			}
				break;
		case SDL_QUIT:
			return 1;
		}
	}
	return 0;
}


void gui(const struct ppm *ppm, struct overlay *overlay)
{
	SDL_Surface *surf;
	struct classification cls;

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "SDL_init: %s\n", SDL_GetError());
		exit(1);
	}
	atexit(SDL_Quit);

	surf = SDL_SetVideoMode(XRES, YRES, 0, SDL_SURFACE | SDL_RESIZABLE);
	if (!surf) {
		fprintf(stderr, "SDL_SetVideoMode: %s\n", SDL_GetError());
		exit(1);
	}

	xres = surf->w;
	yres = surf->h;

	classify(&cls, ppm);

	while (1) {
		SDL_LockSurface(surf);
		draw(surf, ppm, &cls, overlay);
		SDL_UnlockSurface(surf);
		SDL_UpdateRect(surf, 0, 0, 0, 0);
		if (event_loop(&surf, ppm, overlay))
			break;
	}
}
