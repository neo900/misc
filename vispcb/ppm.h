/*
 * ppm.h - Read a PPM file
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef PPM_H
#define	PPM_H

#include <stdint.h>
#include <stdio.h>


struct ppm {
	unsigned x, y;
	uint8_t *data;
};


static inline uint8_t ppm_chan(const struct ppm *ppm, unsigned x, unsigned y,
    unsigned chan)
{
	return ppm->data[3 * (ppm->x * y + x) + chan];
}


#define	ppm_r(ppm, x, y)	ppm_chan(ppm, x, y, 0)
#define	ppm_g(ppm, x, y)	ppm_chan(ppm, x, y, 1)
#define	ppm_b(ppm, x, y)	ppm_chan(ppm, x, y, 2)


static inline uint32_t ppm_rgb(const struct ppm *ppm, unsigned x, unsigned y)
{
	const uint8_t *p = ppm->data + 3 * (ppm->x * y + x);

	return p[0] << 16 | p[1] << 8 | p[2];
}


void ppm_load_file(struct ppm *ppm, FILE *file);
void ppm_load(struct ppm *ppm, const char *name);

#endif /* !PPM_H */
