/*
 * ppm.c - Read a PPM file
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdlib.h>
#include <stdio.h>

#include "ppm.h"


void ppm_load_file(struct ppm *ppm, FILE *file)
{
	unsigned maxval, got;
	int n;

	n = fscanf(file, "P6 %u %u %u\n", &ppm->x, &ppm->y, &maxval);
	if (n != 3) {
		fprintf(stderr, "no PPM P6 header\n");
		exit(1);
	}
	if (maxval != 255) {
		fprintf(stderr, "invalid maximum value %u\n", maxval);
		exit(1);
	}
	ppm->data = malloc(ppm->x * ppm->y * 3);
	if (!ppm->data) {
		perror("malloc");
		exit(1);
	}
	got = fread(ppm->data, 3, ppm->x * ppm->y, file);
	if (got != ppm->x * ppm->y) {
		fprintf(stderr, "fread: got only %u pixels\n", got);
		exit(1);
	}
}


void ppm_load(struct ppm *ppm, const char *name)
{
	FILE *file;

	file = fopen(name, "r");
	if (!file) {
		perror(name);
		exit(1);
	}
	ppm_load_file(ppm, file);
	fclose(file);
}
