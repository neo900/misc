/*
 * class.c - Classify pixels
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "ppm.h"
#include "class.h"


struct class {
	const char *name;

	uint8_t r, g, b;	/* RGB color */
	uint8_t r0, r1;		/* intensity */
	uint8_t a;		/* angle, in degrees */

	float a_cos;		/* cosine of angle */
	uint16_t sr0, sr1;	/* intensity, stretched */
	float rad;		/* radius */
};


#define	N_CLASSES	(sizeof(classes) / sizeof(classes[0]))


static struct class classes[] = {
	{
		.name	= "bg",
		.r	= 255,
		.g	= 255,
		.b	= 255,
		.r0	= 200,
		.r1	= 255,
		.a	= 20,
	},
	{
		.name	= "pcb",
		.r	= 0,
		.g	= 255,
		.b	= 200,
		.a	= 20,
		.r0	= 80,
		.r1	= 200,
	},
};


static bool in_cone(const struct class *c, uint8_t r, uint8_t g, uint8_t b)
{
	unsigned scalar;
	float rad;

	if (!r && !g && !b)
		return c->r0 == 0;
	scalar = c->r * r + c->g * g + c->b * b;
	rad = hypotf(hypotf(r, g), b);
	if (scalar < c->a_cos * rad * c->rad)
		return 0;
	if (rad < c->sr0)
		return 0;
	if (rad > c->sr1)
		return 0;
	return 1;
}


void analyze(uint8_t r, uint8_t g, uint8_t b)
{
	float rad;
	unsigned scalar;
	const struct class *c;

	fprintf(stderr, "RGB %u %u %u, ", r, g, b);
	if (!r && !g && !b) {
		fprintf(stderr, "r = 0\n");
		return;
	}
	rad = hypotf(hypotf(r, g), b);
	fprintf(stderr, "r = %g\n", rad);
	for (c = classes; c != classes + N_CLASSES; c++) {
		scalar = c->r * r + c->g * g + c->b * b;
		fprintf(stderr, "\t%s:\t%f\n", c->name,
		    acos(scalar / rad / c->rad) / M_PI * 180);
	}
	
}


static uint16_t stretch(uint8_t rad, uint8_t r, uint8_t g, uint8_t b)
{
	float f = 1, h;

	if (r)
		f /= cos(atan2(r, g));
	h = hypotf(r, g);
	if (h >= 1)
		f /= cos(atan2(b, h));
	return rad * f;
}


static void setup_class(struct class *c)
{
	c->a_cos = cos(c->a /180.0 * M_PI);
	c->rad = hypotf(hypotf(c->r, c->g), c->b);
	c->sr0 = stretch(c->r0, c->r, c->g, c->b);
	c->sr1 = stretch(c->r1, c->r, c->g, c->b);

	fprintf(stderr, "%s:\t%u %u %u, %u-%u, a %u\n",
	    c->name, c->r, c->g, c->b, c->r0, c->r1, c->a);
	fprintf(stderr, "\tcos %g, r %g\n", c->a_cos, c->rad);
	fprintf(stderr, "\tsr %u-%u\n", c->sr0, c->sr1);
}


void classify(struct classification *cls, const struct ppm *ppm)
{
	uint8_t *c;
	unsigned n_pix = ppm->x * ppm->y;
	const uint8_t *p = ppm->data;
	unsigned i;
	uint8_t r, g, b;

	cls->x = ppm->x;
	cls->y = ppm->y;
	cls->data = c = calloc(n_pix, 1);
	if (!c) {
		perror("malloc");
		exit(1);
	}

	for (i = 0; i != N_CLASSES; i++)
		setup_class(classes + i);

	while (c != cls->data + n_pix) {
		r = p[0];
		g = p[1];
		b = p[2];
		p += 3;

		for (i = 0; i != N_CLASSES; i++)
			if (in_cone(classes + i, r, g, b)) {
				*c = i + 1;
				break;
			}
		c++;
	}
}
