/*
 * vispcb.c - Generate PCB outline from optical scan
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "ppm.h"
#include "overlay.h"
#include "gui.h"


#define	MM2PIX(n)	((n) / 25.4 * 600)


/* ----- Transformation matrix --------------------------------------------- */


static float mx[3] = { 1, 0, 0 };
static float my[3] = { 0, 1, 0 };


static void xform(float x, float y, float *res_x, float *res_y)
{
	float px = MM2PIX(x);
	float py = MM2PIX(y);

	*res_x = mx[0] * px + mx[1] * py + mx[2];
	*res_y = my[0] * px + my[1] * py + my[2];
}


static void load_matrix(const char *name)
{
	FILE *file;
	char buf[1024];
	float *m = mx;
	float dummy[3];
	unsigned n;

	file = fopen(name, "r");
	if (!file) {
		perror(name);
		exit(1);
	}
	while (fgets(buf, sizeof(buf), file)) {
		if (*buf == '#')
			continue;
		n = sscanf(buf, "%f %f %f\n", m + 0, m + 1, m + 2);
		switch (n) {
		case -1:
			continue;
		case 3:
			if (m == mx) {
				m = my;
				break;
			}
			if (m == my) {
				m = dummy;
				break;
			}
			/* fall through */
		default:
			fprintf(stderr, "invalid matrix\n");
			exit(1);
		}
	}
	fclose(file);
	if (m != mx && m != dummy) {
		fprintf(stderr, "incomplete matrix\n");
		exit(1);
	}
}


/* ----- Command-line processing ------------------------------------------- */


static void usage(const char *name)
{
	fprintf(stderr,
	    "usage; %s [-m matrix.m] [-o overlay.gp] image.ppm\n", name);
	exit(1);
}


int main(int argc, char **argv)
{
	struct overlay overlay;
	struct ppm ppm;
	bool have_overlay = 0;
	int c;

	 while ((c = getopt(argc, argv, "m:o:")) != EOF)
		switch (c) {
		case 'm':
			load_matrix(optarg);
			break;
		case 'o':
			overlay_load(&overlay, optarg, xform);
			have_overlay = 1;
			break;
		default:
			usage(*argv);
		}

	switch (argc - optind) {
	case 1:
		ppm_load(&ppm, argv[optind]);
		break;
	default:
		usage(*argv);
	}

	gui(&ppm, have_overlay ? &overlay : NULL);
	return 0;
}
