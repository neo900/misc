#!/usr/bin/python

from math import sin, cos, acos, hypot, sqrt, pi
from cad import *
import FreeCADGui, Mesh

#
# Vertical stacking:
#
# Layer	Total	Description
# -	0.0	Top surface of keyboard, spacer frame, original bottom case
# 1.0	1.0	Top wall total thickness
# * keyboard area may be recessed by ~0.125 mm
# 2.3	3.3	Vertical extension to 3.3 mm [1] and [2]
# 1.0	4.3	Keyboard frame, sunken into bottom case
# 0.7	5.0	Flank wall around keyboard (original 0.9 mm)
#
# [1] https://neo900.org/stuff/joerg/spacerframe/sketch.pdf
# [2] http://irclog.whitequark.org/neo900/2015-07-20#13467507
#

# Use cnc = True if CNC-milling from a 5.0 mm workpiece.
# Use cnc = False if 3D-printing.

cnc = False

extension = 3.3

top_t = 1.0
extension_t = extension - top_t
frame_t = 1.0
flank_t = 0.7		# original 0.9 mm

top_z = -top_t
extension_z = top_z - extension_t
frame_z = extension_z - frame_t
flank_z = frame_z - flank_t

piece_t = 5.0
tool_d = 25.4 / 8.0 + 0.2

# ----- Trigonometrical helper functions --------------------------------------


#
# We have an arc that emerges from a horizontal tangent at point X and passes
# through a given point P, at offset (x, y) from X. We calculate the radius of
# the arc.
#
#         |
#         |\ r
#       r |  \
#         |   P
# --------X
#

def arc_tang_p(x, y):
	return (x * x + y * y) / (2 * y)

#
# We are given an arc with radius r that starts at (0, 0) and ends at (x, y).
# We calculate a middle point M that is on the arc just between (0, 0) and
# (x, y).
#
# Below, the arc is shown with A at (0, 0), B at (x, y), C as the arc's center,
# and M as the desired middle point. P is an auxiliary point halfway between
# A and B.
#
#       C
#      /|\
#     / | \
#   r/  |  \r
#   /   |   \
#  /    |    \
# A-----P-----B
#  --__ | __--
#      -M-
#
# We assume that A-M-B are placed in a counter-clockwise direction.
#

def arc_p_mid(x, y, r):
	ab2 = x * x + y * y
	cp2 = r * r - ab2 / 4.0
	mp = r - sqrt(cp2)
	f = mp / sqrt(ab2)
	mx = x / 2.0 + y * f
	my = y / 2.0 - x * f
	return mx, my


#
# Two arcs, one has radius r, begins with a horizontal tangent at (0, 0), and
# ends at point T.  The other begins at T and ends with a vertical tangent at
# (x, y). Both arcs share a tangent at T. Calculate T.
#
#            |
#    ___q____|(x, y)
#    \       |
#     \      |
#     |\    |
#     | \  |
#    r|  \/
#     |__/T
# -----
#

# (
def bi_arc(x, y, r):
	pass


# ----- Helpers for Gnuplot output --------------------------------------------


def arc(cx, cy, r, a0 = 0, a1 = 90, n = 360):
	for i in range(0, n + 1):
		a =  (a0 + (a1 - a0) / (n + 0.0) * i) / 180.0 * pi
		x = cx + r * cos(a)
		y = cy + r * sin(a)
		print x, y
	print


def circ(cx, cy, r, n = 360):
	arc(cx, cy, 0, 360, n)
	for i in range(0, n):
		a = 2.0 * pi / n * i
		x = cx + r * cos(a)
		y = cy + r * sin(a)
		print x, y
	print


def line(a, b):
	print a[0], a[1]
	print b[0], b[1]
	print



# ----- Helpers for FreeCAD output --------------------------------------------


def line(a, b):
	return Part.LineSegment(v(a[0], a[1], 0), v(b[0], b[1], 0))


def arc_2pr(a, b, r):
	x, y = arc_p_mid(b[0] - a[0], b[1] - a[1], r)
	return Part.Arc(v(a[0], a[1], 0), v(x + a[0], y + a[1], 0),
	    v(b[0], b[1], 0))


# ----- Rounded rectangle generators ------------------------------------------


def rrect(w, h, r, t):
	w2 = w / 2.0
	h2 = h / 2.0

	items = [
	    arc_2pr((w2, h2 - r), (w2 - r, h2), r),
	    line((w2 - r, h2), (-w2 + r, h2)),
	    arc_2pr((-w2 + r, h2), (-w2, h2 - r), r),
	    line((-w2, h2 - r), (-w2, -h2 + r)),
	    arc_2pr((-w2, -h2 + r), (-w2 + r, -h2), r),
	    line((-w2 + r, -h2), (w2 - r, -h2)),
	    arc_2pr((w2 - r, -h2), (w2, -h2 + r), r),
	    line((w2, -h2 + r), (w2, h2 - r)) ]

	s = Part.Shape(items)

	return extrude_shape(s, t)


# ----- Filler to reduce machining time ---------------------------------------


def filler(w, h, r, t):
	my_w = w - 2 * tool_d
	my_h = h - 2 * tool_d
	if r > tool_d:
		return rrect(my_w, my_h, r - tool_d, t)
	else:
		f = rect(my_w, my_h, t)
		move(f, -my_w / 2.0, -my_h / 2.0, 0)
		return f


# ----- Outline generators ----------------------------------------------------


def outline_gp(w, h, r):
	p = []
	for dx in (-1, 1):
		tmp = 0
		if dx < 0:
			tmp = 180
		for dy in (-1, 1):
			if dx == dy:
				a0 = a - tmp
				a1 = 90 * dy
			else:
				a0 = 90 * dy
				a1 = tmp - a
			arc(mx + dx * (w / 2.0 - r - dw),
			    my + dy * (h / 2.0 - r), r,
			    a0, a1)
			p.append((mx + dx * (w / 2.0 - r - dw),
			    my + dy * h / 2.0))
		arc(mx + dx * (w / 2.0 - q), my, q, tmp - a, tmp + a)

	line(p[0], p[2])
	line(p[1], p[3])


def outline_cad(w, h, r, t):
	a_rad = a / 180.0 * pi

	# Endpoints of the flat sides
	f = []
	for dx in (-1, 1):
		for dy in (-1, 1):
			f.append((mx + dx * (w / 2.0 - r - dw),
			    my + dy * h / 2.0))

	# Endpoints of the side arcs
	e = []
	for dx in (-1, 1):
		for dy in (-1, 1):
			e.append((mx + dx * (w / 2.0 - q * (1 - cos(a_rad))),
			    my + dy * q * sin(a_rad)))

	items = [
	    arc_2pr(e[3], f[3], r),
	    line(f[3], f[1]),
	    arc_2pr(f[1], e[1], r),
	    arc_2pr(e[1], e[0], q),
	    arc_2pr(e[0], f[0], r),
	    line(f[0], f[2]),
	    arc_2pr(f[2], e[2], r),
	    arc_2pr(e[2], e[3], q) ]

	s = Part.Shape(items)

	return extrude_shape(s, t)


# ----- Assembly --------------------------------------------------------------


# Height:
#  outside	-2, -62	-> -32 +/- 30
# Width:
#  outside	1.5, 112.5 -> 57 +/- 55.5

mx = 57.2
my = -32.5

case_w = 110.9
case_h = 59.8
corner_r = 7.8		# outside corner radius (observed; nominal may be 8.0)

wall = 2.3		# main side wall
frame_border = 1.3	# wall around original keyboard frame (at surface)
wall_flank = 0.8
wall_rear = 1.5

q = 500		# left/right wall radius (outside)
dw = 0.5	# corner offset

# More precise calculation
q = (dw * dw + (case_h / 2.0 - corner_r) ** 2) / 2 / dw

# Keyboard frame

key_frame_side_h = 30.0	# from outside edge of bottom shell
key_frame_side_wall = 0.5

# Inner wall between keyboard and display

mid_wall_h = 1.2	# original is 1.0 mm but seems to have plenty of slack
mid_wall_t = 0.4

# Inner walls on the side of the keyboard

left_wall_x = 3.0	# from edge of keyboard
left_wall_w = 0.8
left_wall_y0 = 8.6
left_wall_t = 1.0

right_wall_x = 0.7	# from edge of keyboard
right_wall_w = 0.7
right_wall_y0 = 7.4	# from bottom edge of keyboard
right_wall_h = 7.7
right_wall_t = 1.0

# Keyboard opening

# @@@ approximative
kbd_w = 93.6
kbd_h = 18.3
kbd_bottom_border = 2.6
kbd_r = 1.5

# "Clamp" sheets at the bottom of the dome sheet
# (Note: we're unlikely to have anything like this, but being compatible to
# the original case makes it easier to verify and to demo our design.)

clamp_x0 = -16.0	# roughly measured
clamp_x1 = 2.0
clamp_t = 0.4

# Dents in the flank wall

dent_x = 34.5		# offset from left/right keyboard edge
dent_w = 4.0

# Barrel for screws holding the keyboard frame

barrel_r_x = 2.4	# center offset from edge of keyboard opening
barrel_r_y = 6.1
barrel_l_x = 2.9
barrel_l_y = 2.8

# Original screws are M1.4 x 4 mm
# We'll want x 7 or x 8 mm

barrel_out_r = 1.4	# exactly 1.4 * 2 / 2
barrel_out_r = 1.7	# the original barrels are conical; use base width
barrel_in_r = 0.6	# 1.2 mm, close enough to 1.4 * 0.8 / 2

barrel_t = 1.2			# original size
barrel_t = 1.2 + extension	# what we'd want nominally
barrel_t = 5.0 - top_t		# what we'll use for now to save material

# Display opening

disp_w = case_w - 2 * 3.8
disp_off = 24.4		# offset of display hole from bottom edge
disp_h = case_h - wall_rear - disp_off
disp_r = 4.5

# rear stabilizer

#
# For a 1/8 in endmill, we need about 1.2 mm to clear a 0.5 mm bump.
# Adding 0.5 tolerance, we get an offset of about 2 mm on each side.
#

stab_rear_y = 0.5
stab_rear_t = 0.8
stab_rear_off = 1.2 + 0.5
stab_rear_r = 0.2

# front stabilizer

stab_in_w = 9.2 - 2.0
stab_in_y = 0.1		# limited by overhang (0.3 mm) and key mat
			# metal base (about 0.1 mm)
stab_in_h = stab_in_y + wall_flank
stab_out_w = 7.0 - 3.0
stab_out_y = 0.2
stab_t = 2.5		# this makes the stabilizers protrude about
			# 0.5 mm below the PCB. We have room for about
			# 1.0 mm.

if cnc:
	stab_t = 1.7	# if working with a 5.0 mm raw piece, the total
			# depth is limited. We'd need at least 6.0 mm
			# to extend the front stabilizers.

a = acos((q - dw) / q) / pi * 180

if False:
	outline(case_w, case_h, corner_r)

	wall = 1.5
	outline(case_w - 2 * wall, case_h - 2 * wall, corner_r - wall)

if True:
	import FreeCAD, Part
	from FreeCAD import Base

	doc = FreeCAD.newDocument()

	mx = 0
	my = 0

	frame = outline_cad(case_w, case_h, corner_r, top_t + extension_t)
	move(frame, 0, 0, extension_z)

	diff = wall - frame_border
	rf = outline_cad(case_w - 2 * diff, case_h - 2 * diff, corner_r - diff,
	    frame_t)
	move(rf, 0, 0, frame_z)

	diff = wall - wall_flank
	flank = outline_cad(case_w - 2 * diff, case_h - 2 * diff,
	    corner_r - diff, flank_t)
	move(flank, 0, 0, flank_z)

	for dx in (-1, 1):
		cut = rect(-dx * dent_w, wall, flank_t)
		move(cut, dx * (kbd_w / 2.0 - dent_x), -case_h / 2.0, flank_z)
		flank = flank.cut(cut)

	for dx in (-1, 1):
		# @@@ + 0.5 so what we don't get an impossibly thin residue due
		# to the curvature of the corner.
		cut = rect(dx * (clamp_x1 - clamp_x0), wall + 0.5, clamp_t)
		move(cut, dx * (kbd_w / 2.0 + clamp_x0), -case_h / 2.0, flank_z)
		flank = flank.cut(cut)

	rf = rf.fuse(flank)

	keep = rect(case_w, key_frame_side_h, frame_t + flank_t)
	move(keep, -case_w / 2.0, -case_h / 2.0, flank_z)
	rf = rf.common(keep)
	frame = frame.fuse(rf)

	cut = outline_cad(case_w - 2 * wall, case_h - 2 * wall,
	    corner_r - wall, extension_t + frame_t + flank_t)
	move(cut, 0, 0, flank_z)
	frame = frame.cut(cut)

	kbd = rrect(kbd_w, kbd_h, kbd_r, top_t)
	move(kbd, 0, (kbd_h - case_h) / 2.0 + kbd_bottom_border, top_z)
	frame = frame.cut(kbd)

	xl = -kbd_w / 2.0
	xr = kbd_w / 2.0
	yl = -case_h / 2.0 + kbd_bottom_border

	for c in ((xl - barrel_l_x, yl + barrel_l_y),
	    (xr + barrel_r_x, yl + barrel_r_y)):
		outer = cylinder(barrel_out_r, barrel_t)
		inner = cylinder(barrel_in_r, barrel_t)
		outer = outer.cut(inner)
		move(outer, c[0], c[1], top_z - barrel_t)
		frame = frame.fuse(outer)
		
	disp = rrect(disp_w, disp_h, disp_r, top_t + extension_t)
	move(disp, 0, (disp_h - case_h) / 2.0 + disp_off, extension_z)
	frame = frame.cut(disp)

	mid_wall = rect(case_w - 2 * wall, -mid_wall_h, -mid_wall_t)
	move(mid_wall, -case_w / 2.0 + wall, disp_off - case_h / 2.0, top_z)
	frame = frame.fuse(mid_wall)

	kbd_y0 = kbd_bottom_border - case_h / 2.0

	left_wall_y1 = disp_off - mid_wall_h - case_h / 2.0
	left_wall_h = left_wall_y1 - (kbd_y0 + left_wall_y0)
	left_wall = rect(-left_wall_w, left_wall_h, -left_wall_t)
	move(left_wall, -kbd_w / 2.0 - left_wall_x, kbd_y0 + left_wall_y0,
	    top_z)
	frame = frame.fuse(left_wall)

	right_wall = rect(right_wall_w, right_wall_h, -right_wall_t)
	move(right_wall, kbd_w / 2.0 + right_wall_x, kbd_y0 + right_wall_y0,
	    top_z)
	frame = frame.fuse(right_wall)

	kbd = filler(kbd_w, kbd_h, kbd_r, piece_t)
	move(kbd, 0, (kbd_h - case_h) / 2.0 + kbd_bottom_border, -piece_t)

	disp = filler(disp_w, disp_h - stab_rear_y, disp_r, piece_t)
	move(disp, 0, (disp_h - case_h) / 2.0 + disp_off - stab_rear_y / 2.0,
	    -piece_t)
	fill = kbd.fuse(disp)

	#
	# Locations based on
	# https://neo900.org/stuff/werner/stacking/#loc-slider-feat
	#
	# offset: 96.3 / 2 = 48.15
	#
	# -offset + 10.6	-37.55
	# + 18.3		-19.25
	#
	# offset - 3.3		44.85
	# - 15.4 - 4.3 - 4.2	20.95
	#

	for stab in [(-37.55, -19.25), (20.95, 44.85)]:
		x0 = stab[0] + stab_rear_off
		x1 = stab[1] - stab_rear_off
		s_base = isotrap(x1 - x0, stab_rear_y, extension)
		move(s_base, x0, case_h / 2.0 - wall_rear - stab_rear_y,
		    extension_z)
		s_top = rrect(x1 - x0, stab_rear_y, stab_rear_r, stab_rear_t)
		move(s_top, (x0 + x1) / 2.0,
		    case_h / 2.0 - wall_rear - stab_rear_y / 2.0,
		    extension_z - stab_rear_t)
		frame = frame.fuse(s_base)
		frame = frame.fuse(s_top)

	#
	# Locations based on
	# https://neo900.org/stuff/werner/stacking/#loc-feat-front
	#
	# offset, from center: 9.3 / 2 = 4.65
	#
	# -offset - 4.4 - 2.7 - 9.6	-21.35
	# -9.1				-30.45
	#
	# -offset			-4.65
	# + 2 * offset			4.65
	#
	# offset + 4.2 + 2.7 + 9.9	21.45
	# + 9.5				30.95
	#

	for stab in [(-30.45, -21.35), (-4.65, 4.65), (21.45, 30.95)]:
		x = (stab[0] + stab[1]) / 2.0
		s_in = rect(stab_in_w, stab_in_h, extension + stab_t - top_t)
		s_out = rect(stab_out_w, stab_out_y, extension + stab_t - top_t)
		move(s_in, x - stab_in_w / 2.0,
		    -case_h / 2.0 + wall - wall_flank,
		    extension_z - stab_t)
		move(s_out, x - stab_out_w / 2.0,
		    -case_h / 2.0 + wall - wall_flank - stab_out_y,
		    extension_z - stab_t)
		frame = frame.fuse(s_in)
		frame = frame.fuse(s_out)

	if not cnc:
		frame.rotate(v(0, 0, 0), v(0, 1, 0), 180)
	visualize(doc, frame, "Frame", (1.0, 1.0, 1.0), 50)
	if cnc:
		visualize(doc, fill, "Fillers", (1.0, 1.0, 1.0), 80)

	if cnc:
		export = frame.fuse(fill)
		export.exportStl("spacer.stl")
	else:
		ch = FreeCAD.ActiveDocument.addObject("Part::Chamfer",
		    "Chamfer")
		fr = FreeCAD.ActiveDocument.Frame
		ch.Base = fr
		ch.Edges = [(148, 0.60, 1.20)]
#		fr.Visibility = False
		FreeCADGui.ActiveDocument.Frame.Visibility = False
		FreeCAD.ActiveDocument.recompute() 

		Mesh.export([ FreeCAD.ActiveDocument.Chamfer ], "spacer.stl")

#		fi = FreeCAD.ActiveDocument.addObject("Part::Fillet", "Fillet")
#		fi.Base = ch
#		fi.Edges = [(58, 0.5, 0.5)]
##		ch.Visibility = False
#		FreeCADGui.ActiveDocument.Chamfer.Visibility = False
#		FreeCAD.ActiveDocument.recompute() 
#
#		Mesh.export([ FreeCAD.ActiveDocument.Fillet ], "spacer.stl")
