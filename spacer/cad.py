#!/usr/bin/python
#
# spacer/cad.py - CAD helper functions
#
# Written 2015 by Werner Almesberger
# Copyright 2015 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


import FreeCAD, Part
from FreeCAD import Base


# ----- Common primitives -----------------------------------------------------


def v(x, y, z):
	return Base.Vector(x, y, z)


def extrude_shape(shape, z):
	wire = Part.Wire(shape.Edges)
	face = Part.Face(wire)

	return face.extrude(v(0, 0, z))


def rect(x, y, z):
	bottom = Part.LineSegment(v(0, 0, 0), v(x, 0, 0))
	top = Part.LineSegment(v(0, y, 0), v(x, y, 0))
	left = Part.LineSegment(v(0, 0, 0), v(0, y, 0))
	right = Part.LineSegment(v(x, 0, 0), v(x, y, 0))

	s = Part.Shape([ bottom, top, left, right ])
	return extrude_shape(s, z)


def cylinder(r, z):
	return Part.makeCylinder(r, z)


def move(obj, x, y, z):
	obj.translate(v(x, y, z))


#
# Convex isosceles trapezoid:
#
#  _____________ ___
#  \           /  ^
#   \         /   | y
#    \       /    v
# ----*-------------
# | y |  x  |
# |<->|<--->|
#
# * = origin
#

def isotrap(x, y, z):
	bottom = Part.LineSegment(v(0, 0, 0), v(x, 0, 0))
	top = Part.LineSegment(v(-y, y, 0), v(x + y, y, 0))
	left = Part.LineSegment(v(0, 0, 0), v(-y, y, 0))
	right = Part.LineSegment(v(x, 0, 0), v(x + y, y, 0))

	s = Part.Shape([ bottom, top, left, right ])
	return extrude_shape(s, z)


# ----- Visualization helper --------------------------------------------------


def visualize(doc, shape, name, color, trans):
	obj = doc.addObject("Part::Feature", name)
	obj.Shape = shape
	obj.ViewObject.ShapeColor = color
	obj.ViewObject.Transparency = trans
