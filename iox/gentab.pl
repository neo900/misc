#!/usr/bin/perl

sub usage
{
	print STDERR
	    "usage: $0 [-f board] [-l n900.map] [-m iox.map] [iox.tex]\n";
	exit(1)
}


while ($ARGV[0] =~ /^-/) {
	$opt = shift @ARGV;
	if ($opt eq "-f") {
		$filter = shift @ARGV;
		&usage unless defined $filter;
		$filter{$filter} = 1;
	} elsif ($opt eq "-l") {
		$legacy = shift @ARGV;
		&usage unless defined $legacy;
	} elsif ($opt eq "-m") {
		$map = shift @ARGV;
		&usage unless defined $map;
	} else {
		&usage;
	}
}


#----- Scan document for IO signal tables -------------------------------------


$s = join("", <>);
$t = "";

while ($s =~ /\n\\begin\{(ioxdef|ioxgen)\}(.*?)\\end\{\1\}/s) {
	$s = $';
	$t .= $2;
}

$s = "";
while ($t =~ /\\footnote/) {
	$s .= $`;
	$t = $';
	while ($t =~ /^([^}]*){[^}]*}/) {
		$t = $1 . $';
	}
}
$s .= $t;

$s =~ s/%\s*/ /sg;
$s =~ s/\n\s*?\n/\n/sg;
$s =~ s/^\s*\n//s;


#----- Read subsystem map -----------------------------------------------------


if (defined $map) {
	open(MAP, $map) || die "$map: $!";
	while (<MAP>) {
		next if /^#/;
		s/([^\\])#.*/$1/;
		next if /^\s*$/;
		die "bad syntax" unless /^(\S+)\s+(.*?)\s*$/;
		die "duplicate subsystem" if defined $map{$2};
		$map{$2} = $1;
		$map_unused{$2} = 1;
	}
	close MAP;
}


#----- Read legacy assignment map ---------------------------------------------


if (defined $legacy) {
	open(LEGACY, $legacy) || die "$map: $!";
	my $last = undef;
	while (<LEGACY>) {
		next if /^#/;
		s/([^\\])#.*/$1/;
		next if /^\s*$/;

		die "bad syntax" unless
		    /^(\S.*?\S|\S)?\s+(\S+)\s+((taken\s+)?(GPIO\d+|gpio_\d+)|-|shared)\s*$/;

		my $sys = defined $1 ? $1 : $last;
		my $key = "$sys|$2";
		my $dsc = $3;
		my $taken = $4;
		my $gpio = $5;
		$last = $sys;
		$key =~ s/\\//g;

		die "duplicate legacy entry \"$key\""
		    if defined $leg_unused{$key};

		if ($dsc eq "-") {
			$leg{$key} = "---";
		} elsif ($dsc eq "shared") {
			$leg{$key} = "shared";
		} else {
			$gpio =~ s/^GPIO(\d+)$/gpio_$1/;
			$gpio =~ s/_/\\_/g;

			if ($taken) {
				$leg{$key} = "($gpio)";
			} else {
				$leg{$key} = $gpio;
			}
		}
		$leg_unused{$key} = 1;
#		print STDERR "key = '$key' ($leg{$key})\n";
	}
	close LEGACY;
}


#----- Output -----------------------------------------------------------------


for (split(/\s*\\\\\s*?\n/, $s)) {
	($t = $_) =~ s/^\s*//;
	@f = split(/\s*&\s*/, $t);
	$sys = $f[0] unless $f[0] eq "";
	$f[0] = "" if $f[0] eq $last;
	$last = $f[0];
	if ($f[0] ne "") {
		$board = $map{$f[0]};
		die "\"$f[0]\" is not mapped" unless defined $board;
		delete $map_unused{$f[0]};
	}

	if ($f[1] =~ /^\s*---\s*/) {
		$key = $f[2];
	} else {
		$key = $f[1];
	}
	$key =~ s/\s+//g;
	$key = "$sys|$key";
	$key =~ s/\\sch\b//g;	# \sch is a command
	$key =~ s/\\//g;
	$key =~ s/\$//g;

	die "no legacy for \"$key\"" unless defined $leg{$key};
	push(@f, $leg{$key});
	delete $leg_unused{$key};

	next unless defined $filter && $filter{$board};

	if ($f[2] =~ /^\s*---\s*/) {
		splice(@f, 2, 1);
	} else {
		splice(@f, 1, 1);
	}
	print join(" & ", @f), "\\\\\n";
}

for (keys %map_unused) {
	print STDERR "unused map entry \"$_\"\n";
	$die = 1;
}
for (keys %leg_unused) {
	print STDERR "unused legacy entry \"$_\"\n";
	$die = 1;
}

die if $die;
