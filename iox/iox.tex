\documentclass[11pt]{article}
\usepackage{a4}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{xfrac}	% for \sfrac
\usepackage{titlesec}   % for \sectionbreak
\usepackage{parskip}
\usepackage[hang,bottom]{footmisc}
\usepackage{footnote}
\usepackage{longtable}
\usepackage[all]{nowidow}
\usepackage[numbib]{tocbibind}  % add references to PDF TOC
%\usepackage{scrextend}	% for \footref
\usepackage{fixfoot}
\usepackage[hidelinks,bookmarksnumbered=true,bookmarksopen,
    bookmarksopenlevel=2]{hyperref}


% Copyright notice in the footer (first page only)
\usepackage{fancyhdr}
\fancypagestyle{plain}{
  \fancyfoot{}
  \fancyfoot[L]{Copyright \textcopyright\ by the authors.}}
\renewcommand{\headrulewidth}{0pt}

\bibliographystyle{unsrt}
\newcommand{\sectionbreak}{\clearpage}
\renewcommand{\footnotemargin}{1.2em}

\def\iic{$\hbox{I}^2\hbox{C}$}
\def\TODO{{\bf TO DO}}

\title{Neo900 IO Expanders}
\author{J\"org Reisenweber%
\footnote{Design requirements.}
%~\url{<joerg@openmoko.org>} \\
,
Werner Almesberger%
\footnote{Specification details and illustrations.}
%~\url{<werner@almesberger.net>}
}
\date{December 31, 2016}

\begin{document}
\phantomsection\pdfbookmark{Neo900 IO Expanders}{firstpage}
\maketitle

The circuits of Neo900 are distributed over three principal boards called
UPPER, LOWER, and BOB (for ``Break-Out Board''). LOWER contains
the modem, most of the sensors and switches, the audio subsystem, etc.
UPPER contains the keyboard contacts, the CPU, memories, etc. BOB 
contains the memory card holder, Hackerbus, etc. The three boards are
interconnected with board-to-board (B2B) connectors, as shown in the following
stylized drawing:

\vspace{3mm}
\begin{center}
\includegraphics[scale=0.9]{boards.pdf}
\end{center}
\vspace{3mm}

Neo900 uses a large number of control and interrupt signals that are
handled by ordinary GPIO (for ``General-Purpose Input/Output'') pins.
The number of GPIO pins available on the CPU is limited, and any signal
traveling from the CPU to a peripheral on LOWER or BOB also needs to
cross the UPPER-LOWER B2B connector.

\newpage % @@@ ugly
In order to reduce the number of CPU pins and B2B contacts needed, 
we employ so-called IO expander chips:

\begin{center}
\includegraphics[scale=0.9]{expander.pdf}
\end{center}

An IO expander communicates with the CPU through \iic\ and typically
has one dedicated interrupt line. The IO expander provides a number
of IO pins that connect to peripherals.

In the following sections we briefly describe common features of
IO expander chips, examine a few examples of commonly used chips, and
then analyze the characteristics of the signals that connect to GPIOs
of the CPU or an IO expander.

% -----------------------------------------------------------------------------

\section{IO expander characteristics}

The type of IO expander we consider here consists of the function blocks
shown below: \iic\ interface gives access to a number of internal registers,
through which the functions of the chip are controlled. The port drivers
connect to the registers and also to the interrupt logic.

\begin{center}
\includegraphics[scale=0.9]{anatomy.pdf}
\end{center}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Pin circuit}

The following diagram illustrates the structure of the circuit at each
GPIO pin, with features that are only found in some of the chips we
looked at shown in grey:
an output driver that can be individually enabled when the pin is
used for output. In most chips, this driver has a push-pull configuration
while some only have open collector outputs. There is normally also a
configurable pull-up resistor, but only few chips have also pull-down
resistors.

The input driver connects to a register from which the pin status can
be read, and also connects to the interrupt logic. In all the chips
we considered, interrupts are edge-triggered. Some chips trigger on
both edges while others allow selection of rising, falling, or both
edges.

Some but not all IO expander chips allow interrupts to be enabled on a
per-pin basis. Some chips indicate pending interrupts through a register,
while others require the CPU to read the pin state (which means that fast
pulses and glitches may be missed).
All interrupts are or-ed to produce the interrupt signal to the CPU.

\begin{center}
\includegraphics[scale=0.9]{gpio.pdf}
\end{center}

The GPIO functions are thus very similar to that in a CPU, except that
interrupt handling is less flexible, and also that GPIO chips usually
lack pull-downs, which have become common in microcontrollers over the
last years.

Additional capabilities may include multiple voltage domains (e.g.,
for the host interface and for IO), allowing a GPIO expander to act as
level shifter.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Access timing}

We consider only IO expander chips that connect through \iic. The \iic\
interface imposes some constraints on access timing. The two system-wide
\iic\ busses in Neo900 operate at 400~kHz (I2C\#2) and 100~kHz (I2C\#3).
We assume that all GPIO expanders connect to I2C\#2.

A read or write operation to a byte-wide register typically consists of
three bytes: the 7-bit \iic\ device address, the direction bit, a
register number, and the value read or written. Each byte is followed by
an acknowledge bit. Furthermore, each read or write operation has one
start and one stop bit. The total number of bits per access is therefore
$2+3\times (8+1)=29$. At 400~kHz, this yields an access time of 72.5~$\mu$s,
and a maximum rate of 13\,793 operations per second.

A slow \iic\ device may also delay acknowledgement, extending the duration
of a transfer.

Since the \iic\ bus is shared among many devices, it may be occupied at the
time an access is attempted, and the operation will have to be deferred
until the on-going transfer terminates. Given that transfers between the
CPU and NFC or the RDS section of the FM transceiver may take in excess of
1000 bit times, such competing transfers may impose latencies in the order
of several milliseconds.

Worse, should an \iic\ transfer be attempted to a unresponsive device,
the kernel driver applies a hard-coded timeout of 1000~ms.%
\footnote{\url{OMAP_I2C_TIMEOUT} in \url{drivers/i2c/busses/i2c-omap.c}}

We should therefore use IO expanders only for signals that change infrequently,
where a typical latency of at least 10~ms can be tolerated, where variations
of this latency are acceptable,%
\footnote{I.e., a consistent high latency at a human interface may be
  perceived as more agreeable than a latency that is usually very short
  but occasionally jumps to a large value.}
and occasionally much longer latency does not cause severe malfunction.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{\iic\ addressing}

Each device on an \iic\ bus has a 7-bit address. This address must be
unique on that bus. Most \iic-capable chips can be configured to use
one of several hard-wired addresses. This configuration is typically
accomplished by connecting a number of address selection pins to ground,
VCC, or in some cases also SDA or SCL.

The IO expander chips we consider support between one (i.e., not
configurable) and 32 different addresses.

% -----------------------------------------------------------------------------

\section{IO expander chips}

This section examines the characteristics of several IO expander chips
available on the market.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Catalog search}

We searched the Digi-Key catalog for suitable IO expander chips in the
category ``Integrated Circuits (ICs)'', sub-category
``Interface -- I/O Expanders'', with 1\,474 entries.%
\footnote{As of 2016-06-05.}
We then refined the query as follows:

\begin{tabular}{ll|r}
  Parameter & Value & Parts \\
  \hline
  Mounting Type         & Surface Mount				& 1\,428 \\
  Interface		& \iic$\star$				& 1\,277 \\
  Packaging:		& $\neg$Digi-Reel, $\neg$Tape \& Reel	& 532 \\
  Interrupt Output	& Yes					& 469 \\
  Voltage -- Supply	& $\rm V_{min}\le 1.8~V$		& 155 \\
  Number of I/O		& $\ge 16$				& 79 \\
  Package		& $\rm \neg SIOC, \neg \star SOP$	& 45 \\
  Frequency		& $\rm\ge 400~kHz$			& 44 \\
\end{tabular}

Of these, 35 were in stock. Sorted by unit price for 1000 units, and ignoring
anything larger than $\rm 4\times4~mm$ or more expensive than USD 1.50, we
get this list:

\begin{tabular}{llccc}
  Manufacturer	& Part name		& Package & Size & Unit price \\
		&			&	& (mm)	& (USD) \\
  \hline
  Exar		& XRA1201P		& 24-QFN & $4\times4$	& 0.56 \\
  		& XRA1201		& 24-QFN & $4\times4$	& 0.56 \\
  NXP		& PCAL6416A		& 24-BGA & $3\times3$	& 0.78 \\
		&			& 24-QFN & $4\times4$	& 0.78 \\
  STM		& STMPE1600		& 24-QFN & $4\times4$	& 0.80 \\
  		& STMPE1801		& 25-BGA & $2\times2$	& 0.82 \\
  Semtech	& SX1503		& 28-QFN & $4\times4$	& 0.84 \\
  NXP		& PCA9539A		& 24-QFN & $4\times4$	& 0.86 \\
  		& PCAL6416A		& 24-BGA & $2\times2$	& 0.86 \\
  		& PCA6416A		& 24-QFN & $4\times4$	& 0.86 \\
  		& PCAL6416A		& 24-BGA & $2\times2$	& 0.86 \\
  		& PCA9535A		& 24-QFN & $4\times4$	& 0.86 \\
  TI		& TCA6416A		& 24-QFN & $4\times4$	& 0.89 \\
  STM		& STMPE1601		& 25-BGA & $3\times3$	& 0.89 \\
  NXP		& PCA9575		& 24-QFN & $4\times4$	& 0.90 \\
  TI		& TCA1116		& 24-QFN & $4\times4$	& 0.95 \\
  		& TCA9539		& 24-QFN & $4\times4$	& 0.95 \\
  		& TCA9555		& 24-QFN & $4\times4$	& 0.95 \\
  		& TCA6416A		& 24-BGA & $3\times3$	& 0.96 \\
  Exar		& XRA1203		& 24-QFN & $4\times4$	& 1.00 \\
  		& XRA1207		& 24-QFN & $4\times4$	& 1.00 \\
  TI		& TCA6418		& 25-BGA & $2\times2$	& 1.02 \\
  		& TCA9535		& 24-QFN & $4\times4$	& 1.07 \\
  Microchip	& MCP23018		& 24-QFN & $4\times4$	& 1.14 \\
  Maxim		& MAX7325		& 24-QFN & $4\times4$   & 1.19 \\
  Semtech	& SX1509B		& 28-QFN & $4\times4$	& 1.22 \\
		& SX1509QB		& 28-QFN & $4\times4$	& 1.22 \\
  STM		& STMPE2401		& 36-BGA & $3.5\times3.5$ & 1.46 \\
\end{tabular}

Except for the STMPE1801 and the TCA6418, which have 18 GPIOs,
all the above expander chips have 16 GPIO pins.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


\subsection{Current consumption}

The following table shows the supply current for ``idle'' and ``active''
states, according to the respective data sheet. We define ``idle'' as the
absence of \iic\ or any other IO activity, and ``active'' as some amount of
traffic on the \iic\ bus, operating at $f_{\rm SCL}=400\rm~kHz$.
Conditions for published ``active'' state consumptions vary widely, and the
values should therefore be considered as indicators (or
absence thereof) for characteristics that may need further examination
when a given chip is considered.

\begin{savenotes}
\begin{tabular}{llccccc}
  Manufacturer	& Part name	& \multicolumn{4}{c}{Current consumption
				  ($\rm\mu A$)}
				& Voltage (V) \\
		&		& \multicolumn{2}{c}{Idle}
				& \multicolumn{2}{c}{Bus active} & \\
		&		& Typ & Max & Typ & Max \\
  \hline
  Exar		& XRA1201/P	& ---	& 1	& ---	& 50	& 1.8 \\
		& XRA1203	& ---	& 1	& ---	& 50	& 1.8 \\
		& XRA1207	& ---	& 1	& ---	& 50	& 1.8 \\
  Maxim		& MAX7325	& 0.9	& 1.9	& 23	& 55	& 3.3 \\
  Microchip	& MCP23018	& ---	& 1	& ---	& 1000	& 1.8--5.5 \\
  NXP		& PCA6416A	 & 0.5	& 1.7	&   4	&   9	& 1.65-2.3 \\
		& PCA9535A	& 0.5	& 1.7	&   4	&   9	& 1.65--2.3 \\
  		& PCA9539A	& 0.5	& 1.7	&   4	&   9	& 1.65--2.3 \\
		& PCA9575	& 0.25	& 2	& 135	& 200	& 3.6 \\
		& PCAL6416A	& 0.5	& 1.7	&   4	&   9	& 1.65--2.3 \\
  Semtech	& SX1503	& ---	& 2	& ---	& 7	& $<2$ \\
		& SX1509B/QB	& 3	& 9	& ?	& ?	& 3.3 \\
  STM		& STMPE1600	& 0.25	& 1	& 135	& 200	& 1.8--3.3 \\
		& STMPE1801	& ---	& 0.5	& 28	& 55	& 1.8 \\
  TI		& TCA6416A	& 0.5	& 1.7	& 4	& 9	& 1.65--2.3 \\
		& TCA6418	& ---	& 13	& ---	& 25	& 1.65--3.6 \\
		& TCA9535	& 0.4	& 2.2	& 5	& 11	& 1.95 \\
		& TCA9539	& 0.4	& 2.2	& 5	& 11	& 1.95 \\
		& TCA9555	& 0.5	& 1	& 5	& 11	& 1.95 \\
\end{tabular}
\end{savenotes}

We choose operating conditions that include an environmental temperature of
$\rm 25~^\circ C$.  We prefer specifications for exactly 1.8~V, but the
published values may apply to a wide range or a higher voltage, as
indicated in the ``Voltage'' column, above.

The above table does not include chips that the manufacturer declared as
obsolete or for which only insufficient documentation exists.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\DeclareFixedFootnote{\viopp}{Each port (8 bits) has individual IO voltage.}


\subsection{Products by vendor}

IO expanders made by the same company usually share a common set of
basic features, or show some form of design evolution. We revisit
the aspect of progressive design improvements in section \ref{evolution}.


\subsubsection{Exar}

All Exar chips have push-pull outputs (despite Digi-Key claiming that
some are only open drain)
and each pin has an individually programmable pull-up resistor.
Interrupts can be generated on the raising or
falling edge, or on both edges, and each pin has an individual interrupt mask
bit.

\begin{tabular}{lccl}
  Part name	& \iic		& Pull-up	& Other \\
		& addresses	& on reset	& \\
  \hline
  XRA1201	& 32		& off		& \\
  XRA1201P	& 32 		& on		& \\
  XRA1203	& 16		& off		& reset input \\
  XRA1207	&  4		& off		& reset input, level shifter \\
\end{tabular}

Exar also indicate that their IO expanders are ``pin and software compatible''
with the following chips by other manufacturers:

\begin{tabular}{llll}
  Exar  & NXP & TI & Maxim  \\
  \hline
  XRA1201P	& PCA9555	& TCA9555	& MAX7311, MAX7318 \\
  XRA1201	& PCA9535	& TCA9535	& MAX7312 \\
  XRA1203	& PCA9539	& TCA9539	& --- \\
  XRA1207	& ---		& TCA6416	& --- \\
\end{tabular}

We can therefore refer to the NXP section (\ref{nxp}) for further details.


\subsubsection{Maxim}

The MAX7325 has a number of rather eccentric features: one port is push-pull
and output-only while the other port is open drain with pull-up resistors
whose configuration depends on the \iic\ address (!) selection. Likewise,
the reset level (i.e., high or low) of the output-only ports is determined
by the address selection. Registers are selected not in the usual way
of transmitting a register number, but through the \iic\ address (this
chip occupies two device addresses), and position-dependent semantics 
of multi-byte reads or writes.


\subsubsection{Microchip}

The MCP23018 has only open drain outputs,
supports \iic\ speeds of up to 3.4~MHz,
and features one interrupt line per port.
Otherwise, it is similar to the XRA1203.

\begin{tabular}{lccl}
  Part name	& \iic		& Pull-up	& Other \\
		& addresses	& on reset	& \\
  \hline
  MCP23018	& 8		& off		& reset input,
						  two interrupt lines \\
\end{tabular}


\subsubsection{NXP}
\label{nxp}

The NXP PCA series chips PCA6416A, PCA9535A, and PCA9539A
have push-pull outputs. There are no integrated
pull-up or -down resistors. Interrupts are generated on either edge, and
cannot be masked. The CPU has to determine the interrupt status by reading
the inputs~--~there is no interrupt status register.

The PCA9575 has pull-up and -down resistors, where pull resistors are
enabled per port but the direction (i.e., up or down) can be
selected per pin. The PCA9575 also has interrupt mask and status
registers.

\begin{savenotes}
\begin{tabular}{lccl}
  Part name	& \iic		& Pull		& Other \\
		& addresses	& on reset	& \\
  \hline
  PCA6416A	& 2		& ---		& reset input, level shifter \\
  PCA9535A	& 8		& ---		& \\
  PCA9539A	& 4		& ---		& reset input \\
  PCA9575	& 1		& off		& reset input, level shifter%
\viopp \\
\end{tabular}
\end{savenotes}

The PCAL series is an update of the PCA series, adding
improved pull resistor and interrupt handling, like in the PCA9575,
but with per-pin pull enable.


\subsubsection{Semtech}

The SX1503 has pull-up and -down resistors, interrupt mask, and edge
selection (rising, falling, both). All of these features can be
individually set per pin. As an unusual extra, some GPIOs can be
configured to act as simple programmable logic devices (PLD).

SX1509B and SX1509QB seem to differ only in part name and package
marking. They are similar to the SX1503, but have a LED controller
and a keyboard controller instead of PLD.

\begin{savenotes}
\begin{tabular}{lccl}
  Part name	& \iic		& Pull		& Other \\
		& addresses	& on reset	& \\
  \hline
  SX1503	& 1		& off		& reset input, level shifter%
\viopp, PLD \\
  SX1509B/QB	& 4		& off		& reset input, level shifter%
\viopp, LED, keyboard \\
\end{tabular}
\end{savenotes}


\subsubsection{STM}

The STMPE1600 and the STMPE1801 are similar to the PCA family, except that
they have an interrupt mask. The STMPE1801 also allows edge selection,
has 18 GPIOs instead of the usual 16, and features a keyboard controller.
The STMPE1600 has no pull resistors. The STMPE1801 has individually
programmable pull-up resistors in GPIO mode, and an additional set of
pull-down resistors only used in keyboard mode. The data sheet does not
mention the reset state of the pull-ups.

The STMPE1601 and STMPE2401 found in the catalog search are not recommended
for new designs.

\begin{tabular}{lccl}
  Part name	& \iic		& Pull		& Other \\
		& addresses	& on reset	& \\
  \hline
  STMPE1600	& 8		& ---		& \\
  STMPE1801	& 1		& ?		& reset input, keyboard \\
\end{tabular}


\subsubsection{TI}

The chips TCA6416A, TCA9535, and TCA9539
seem to be functionally identical to their NXP counterparts,
PCA6416A, PCA9535A, and PCA9539A, respectively.
 
The TCA1116 seems to be an obsolete design that is similar to the
TCA9539. The data sheet is truncated and says that a full data sheet is
available only on request.

The TCA6418 has per-pin pull-down resistors (but no pull-up),
an interrupt mask, per-pin selectable edge polarity, and supports
an \iic\ speed of up to 1~MHz.

The TCA9555 is identical to the TCA9535, except that it
contains hard-wired pull-up resistors.

\begin{tabular}{lccl}
  Part name	& \iic		& Pull		& Other \\
		& addresses	& on reset	& \\
  \hline
  TCA6418	& 1		& off		& reset input\\
  TCA9555	& 8		& up		& \\
\end{tabular}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Evolution}
\label{evolution}

There are two clear development trends in the PCA/TCA type of chip families:
within a technology generation, different chip variants trade
\iic\ address selection pins for a reset input and a separate IO voltage
input (level shifting).

There is a big technological leap from the PCA/TCA generation to the
compatible chips by Exar, adding pull-up resistors, interrupt masks,
interrupt edge selection, and also making more efficient use of the
\iic\ address selection pins by adding the option to connect them to
SDA or SCL, in addition to VCC or ground.

The NXP PCAL series is an update of the PCA series, with improvements similar
to those by Exar plus pull-down resistors,
but still lacks edge selection and
does not use the improved address selection technique.

\begin{center}
\includegraphics[scale=0.9]{evol.pdf}
\end{center}

The PCA9575 is somewhere between generations, adding some improvements
but lacking others. A few more chips are relatively small variations
of members of the major IO expander chip families.
We left out the few chips that would not fit nicely into this pattern, but
which are also of less interest. Chips with difficult availability are
shown in grey.

% -----------------------------------------------------------------------------

\section{Signal classification}

The following sections contain a classification of all signals that can
connect either to an IO expander or the CPU. The names of function blocks
and signals are those used in the Neo900 block diagram \cite{BD} unless
noted otherwise. Signal names are given for block diagram and schematics.

Signal types are as seen by the CPU or IO expander, i.e., an ``input''
is a signal sent from a peripheral towards the CPU.

We classify speed requirements in the following thee categories:
\begin{description}
  \item[slow] for signals that can tolerate latencies in the order of
    hundreds of milliseconds. This would typically be enable or reset
    signals for peripherals that are expected to take some time to initialize,
    or any buttons that request major device configuration changes, such as
    opening the display slider.
  \item[medium] for signals that can tolerate latencies of tens of
    milliseconds. When interacting with a device, the shortest response
    delays and delay variations a human user is able to perceive are in
    this range.
  \item[fast] for signals where any delay is unwelcome. An example would be
    interrupts from fast peripherals such as WLAN, where latency directly
    impacts throughput. Also for signals related to performing any sort of
    emergency
    shutdown, minimum latency may be desirable even if the peripheral
    effecting the shutdown adds substantial delays of its own.

    Furthermore, some ``fast'' signals may have a high rate of change,
    which would increase occupancy of the \iic\ bus and delay other
    operations.
\end{description}

We consider ``slow'' signals as generally suitable for use with an IO
expander, and ``fast'' signals as generally unsuitable. ``Medium''
signals have to be decided on a case-by-case basis.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

%\DeclareFixedFootnote{\bd}{Name as used in the Neo900 block diagram
%\cite{BD} unless noted otherwise.}

\DeclareFixedFootnote{\sch}{Name as used in the Neo900 schematics
\cite{SCH}.}

%\DeclareFixedFootnote{\both}{Name as used in the Neo900 schematics \cite{SCH}
%and block diagram \cite{BD}.}


\newenvironment{ioxgen}[2]%
  {\begin{savenotes}\begin{tabular}{l|ll|ll}%
  Function#1 & \multicolumn{2}{l|}{Signal#2} & Type & Speed \\
  & (block) & (schematics) \\
  \hline\rule{0pt}{12pt}\ignorespaces}%
  {\end{tabular}\end{savenotes}}

\newenvironment{ioxdef}%
  {\begin{ioxgen}{}{}}%
  {\end{ioxgen}}

\newenvironment{ioxaltgen}%
  {\begin{savenotes}\begin{tabular}{l|ll|l}%
  Function & \multicolumn{2}{l|}{Signal} & Connected to $\ldots$ \\
  & (block) & (schematics) \\
  \hline\rule{0pt}{12pt}\ignorespaces}%
  {\end{tabular}\end{savenotes}}

\newenvironment{ioxalt}%
  {Related items that connect to other peripherals and are therefore not
   considered in this document:\par
  \begin{ioxaltgen}}
  {\end{ioxaltgen}}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Power}

Battery charger and fuel gauge have a small number of configuration and
status signals, all of which are low-speed.

\begin{ioxdef}
  Batt charger		& INT		& CHG\_INT	& interrupt & slow \\
			& OTG		& CHG\_OTG	& output    & slow \\
  Fuel gauge		& GPOUT		& BQ\_GPOUT	& input/output & slow\\ 
\end{ioxdef}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Human input sensors}

There is a large number of buttons and sensors that detect actions of
the user. Most of them should tolerate a moderate amount of latency.
Exceptions are discussed below.

\begin{ioxdef}
  Lock			& SCREEN\_LCK	& SCREEN\_LOCK	& interrupt & slow \\
  Slide mag. sensor	& SLIDE\_SW	& SLIDE\_SW	& interrupt & slow \\
  Capture		& CAM\_CAP$[0]$	& CAM\_CAP\_1	& interrupt & slow \\
 			& CAM\_CAP$[1]$	& CAM\_CAP\_2	& interrupt & medium \\
  Cam cover		& cam\_d11	& CAM\_COVER\_INT & interrupt & slow \\
  Stylus		& stylus	& STYLUS\_INT	& interrupt & slow \\
  Kbd scan		& KEYIRQ	& KEYIRQ	& interrupt & medium%
\footnote{The minimum debounce time is 25~ms (section 8.6.2.15 of
\cite{TCA818}), the maximum debounce time is 60~ms (section 6.9).} \\
  3.5 mm		& HEADPH\_IND	& HEADPH\_IND	& interrupt & slow \\
			& present	& MIC\_nPRESENT	& interrupt & slow \\
  Batt. lid mag.	& BATT\_LID	& BATT\_LID	& interrupt & slow \\
  uSD card		& CD		& SD\_CD	& interrupt & slow \\
  Touch scrn ctrl	& TSC\_RST	& TSC\_RST	& output & slow%
\footnote{Reset time is 13~ms (section A.7 of \cite{CRTOUCHDS}).
The minimum duration of the reset pulse is not specified.} \\
			& PEN\_INT	& PEN\_INT	& interrupt & medium%
\footnote{The highest sampling rate of the resistive touch screen is
10 points per second (section 4.2.3.4 of \cite{CRTOUCHDS}. Neo900 does not
use the chip's capacitive system.} \\
  Main flex connector	& PROXY		& PROXY		& interrupt & slow \\
\end{ioxdef}

We consider keyboard and touch screen to be medium-speed interrupt sources.
The capture button has two levels: the first initiates camera configuration
(focus, etc.) and is usually not very timing-critical. The second level
releases the shutter, which we consider medium-speed.

\begin{ioxalt} 
  Vol $+/-$		& ---		& VOL\_UP	& Keyboard scanner \\
			& ---		& VOL\_DOWN	& Keyboard scanner \\
  Power			& POWERON	& POWERON	& Companion chip \\
\end{ioxalt}

The keyboard controller requires a reset pulse with a minimum duration of
120~$\mu$s. The time to perform a reset is also 120~$\mu$s (section 6.7 of
\cite{TCA818}). We use nRESWARM (instead of a dedicated reset signal),
which has a default duration of about 183~$\mu$s.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Environmental sensors}

\begin{ioxdef}
  Main flex connector	& ALS\_INT	& ALS\_INT	& interrupt	& slow%
\footnote{Maximum interrupt rate is at most once (see page 16 of \cite{TSL2563}
per integration time (13.7, 101, or 402~ms, table 6).} \\
  Accel			& INT1	& SENS\_INT1		& interrupt & fast \\
			& INT2	& SENS\_INT2		& interrupt & fast \\
  9-Axis		& INT1	& SENS\_INT1		& interrupt & fast \\
			& INT2	& SENS\_INT2		& interrupt & fast \\
\end{ioxdef}

The accelerometer and the 9-axis sensor have a total of eight different
interrupt outputs. The following drawing illustrates how these outputs
are merged into the two signals that go to the CPU:

\begin{center}
\includegraphics[scale=0.9]{shint.pdf}
\end{center}

The resistors at the barometer side are needed because the interrupts of
the barometer pins have push-pull outputs while all other interrupts can
be configured to work as open collector. We therefore let the barometer
outputs act as pull-up for the other interrupt lines. The additional
pull-ups (on the right side) are need in case the barometer interrupts
are disabled, which sets them high impedance.

The LIS302DL (``Accel'' \cite{LIS302DL}) can generate interrupts for the
following events:
\begin{itemize}
  \item availability of sample data (up to 400 samples per second),
  \item free fall detection, and
  \item click detection.
\end{itemize}
Free fall and click detection presumably operate at a granularity determined
by the sample rate. Any of the three event types can be assigned to either
interrupt line.

The BX055 9-axis sensor \cite{BX055} contains an accelerometer module that
can provide up to 10~ksamples/s (section 5.2.1), a gyroscope module with up
to 2~ksamples/s (section 7.3), as well as additional modules operating at
lower rates, and various filtering options.
Accelerometer and gyroscope each have two interrupt outputs, which are
connected to the two interrupt lines in Neo900.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Audio}

\begin{ioxdef}
  Mic/TV		& TVOUT\_EN	& TVOUT\_EN	& output & slow \\
  Headphone amplifier	& HEADPH\_EN	& HEADPH\_EN	& output & slow \\
  Stereo audio codec	& CODEC\_RST	& CODEC\_nRESET	& output & slow%
\footnote{Minimum duration is 10 ns (page 22 of \cite{TLV320AIC34}), with
no upper limit.} \\
\end{ioxdef}

We assume that users will expect only ``slow'' responses when setting
these audio-related configuration and reset signals. The chips have no
unusual or otherwise noteworthy timing requirements.

\begin{ioxalt} 
  Speaker amplifier	& en	& SPEAKER\_EN	& Companion chip (GPIO7) \\
\end{ioxalt} 

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{ECI}

There is no publicly accessible specification of the ECI protocol and its
timing. The best available resource appears to be an MSc thesis by Jussi
Hannula \cite{ECI-Thesis}.

A patch adding ECI support \cite{ECI-Drv} has been submitted for inclusion
into the mainline Linux kernel but was apparently never accepted.
In any case, this
driver assumes an unknown ECI controller device and therefore does not
reveal any details of the ECI wire protocol.

The kernel for N-series Nokia devices accesses ECI through an ACI component
in the TWL5031 chip.%
\footnote{\url{https://github.com/pali/linux-n900/blob/v2.6.32-nokia/drivers/mfd/twl5031-aci.c}}
 TWL5031 may be an alias for TPS65951 \cite{TPS65951DM}.
No further public information about ACI on TWL5031 or TPS65951 seems to exist.

\begin{ioxdef}
  ECI			& ECI$[0]$	& ECI0		& input	& fast \\
			& ECI$[1]$	& ECI1		& input	& fast \\
			& ECI$[2]$	& ECI\_OUT	& output & fast \\
			& ECI\_EN	& ECI\_EN	& output & slow \\
\end{ioxdef}

Given that \cite{ECI-Thesis} mentions a close similarity between \iic\ and
ECI,%
\footnote{Section 3.2 begins with
``{\em From the electronics point of view, the ECI-bus is very
much like the \iic-bus protocol. $[\ldots]$
Basically, a
single \iic-bus protocol's signal line is excluded from the design.}''}
and that the ECI implementation presented in the paper operates at 400~kHz,
we conservatively assume that ECI may run (even though empirical evidence
suggesting that ECI may operate at low rates is said to exist) at similar
speeds and thus qualifies as ``fast''.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{WLAN and Bluetooth}

Given that the transmission time of 802.11n frames is measured in tens of
microseconds, it is safe to consider interrupt latency requirements of
the WLAN/BT module to be in the ``fast'' category.

\begin{ioxdef}
  WLAN/BT		& WLAN\_EN	& WLAN\_EN	& output & slow%
\footnote{WLAN\_EN acts as reset signal, with a power-up time of 5~ms
(figure 5.3 in section 5.22.3 of \cite{WL1837}. WLAN\_EN also needs to
be deasserted at least 10~$\mu$s before the VBAT and VIO voltage rails
drop, lest the device be damaged (section 5.22.2). This timing requirement
requires dedicated hardware and is outside the scope of this document.} \\
			& WLAN\_IRQ	& WLAN\_IRQ	& interrupt & fast \\
			& BT\_EN	& BT\_EN	& output & slow%
\footnote{BT\_EN acts as reset signal, with a maximum initialization time
of 100~ms (figure 5-5 in section 5.22.5 of \cite{WL1837}.} \\
\end{ioxdef}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{FM}

\begin{ioxdef}
  FM/TX			& FM\_nINT	& FM\_nINT	& int./output & medium\\
			& FM\_nRST	& FM\_nRST	& output & slow \\
\end{ioxdef}

The Si4721 uses interrupts mainly to indicate the following types of events:
\begin{itemize}
  \item command completion (for all commands, no matter how long or short),
  \item the crossing of a signal strength threshold, and
  \item RDS FIFO state changes.
\end{itemize}

In all cases, the use of interrupts is optional. Among the interrupt sources
in the transceiver, we assume the RDS FIFO to be the source of
the most timing-critical type of event (i.e., to prevent overruns).
The documentation \cite{Si4720,AN332} is inconclusive regarding FIFO
characteristics.

However, programming examples mention transfer of one RDS group while
another is being received. According to \cite{RDS-WP}, RDS ``A'' groups
can arrive at a rate of up to 11.4 groups per second in pre-2.0 RDS.
Without further background research, we take this as our reference
rate and thus assume that interrupt latency should be significantly
below one ``A'' group time, i.e., 87.7~ms.

The GPO2/nINT pin of the FM transceiver acts as configuration input (!)
at reset, and must be held low to select the \iic\ interface.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Modem}

\begin{ioxdef}
  Modem monitor		& ALERT		& INA231\_INT	& interrupt & fast \\
			& EN		& MODEM\_EN	& output & fast \\
  Modem			& EMERG\_OFF	& MODEM\_EMERG	& output & fast \\
  			& RING		& RING		& interrupt & slow \\
			& PWR\_IND	& PWR\_IND	& interrupt & slow \\
			& LC\_IND	& LC\_IND	& interrupt & slow%
\footnote{Use case is unclear -- we already have independent current
monitoring through the modem monitor.} \\
			& STATUS	& STATUS	& interrupt & slow%
\footnote{STATUS is not only an on/off indication but can also indicate
additional details by blinking at 1 Hz with duty cycles ranging from
1\% (10~ms) to 50\% (section 18.5 of \cite{PHS8P-AT}).} \\
			& WAKEUP	& MODEM\_WAKEUP	& interrupt & medium \\
			& MODEM\_IGT	& MODEM\_IGT	& output & slow%
\footnote{Minimum pulse width is 100~ms for activation (section 3.3.1 of
\cite{PHS8E-HW}), 2.1~s if used for deactivation (section 3.3.4).
Figure 7 of section 3.3.4 seems to suggest that the impulse used for
activation should not exceed one second, but there is no mention of
such a limit elsewhere.} \\
			& VMIC	& MODEM\_VMIC\_SENSE	& interrupt & slow \\
  Modem TX monitor	& --- & CELL\_DETECT\_IRQ	& interrupt & fast \\
  USB PHY		& RESETB	& ---		& output & slow \\
\end{ioxdef}

We classify all modem monitor signals and EMERG\_OFF as ``fast'',
assuming that upon
detection of unexpected activity, corrective action (e.g., a shutdown)
may be initiated without delay, even if not all of these signals may
cause immediate cessation of a potentially undesired operation.

\begin{ioxalt}
  Modem TX monitor	& ---	& ADC1 (analog)	& Companion chip \\
			& ---	& ADC2 (analog)	& Companion chip \\
\end{ioxalt}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{GPS}

The modem uses its VGNSS output to enable the amplifier for the GPS
antenna signal. We add two signals controlled by the CPU that allow
it to detect whether the modem is trying to use the GPS antenna,
and to override the modem.

\begin{center}
\includegraphics[scale=0.9]{vgnss.pdf}
\end{center}

If the CPU tri-states VGNSS\_override, the modem controls the LNA
and the GPS kill switch. The CPU ``kills'' GPS by driving VGNSS\_override
low. In either case, VGNSS\_sense tracks which configuration the modem
requests.

\begin{tabular}{cc|cc}
  VGNSS\_override & VGNSS	& Enable	& VGNSS\_sense	\\
  (CPU)		& (Modem)	& (LNA)		& (CPU)		\\
  \hline
  Z		& H		& H		& H \\
  Z		& L		& L		& L \\
  L		& H		& L		& H \\
  L		& L		& L		& L \\
\end{tabular}


Neither detection nor intervention are particularly timing-critical
since VGNSS\_override can simply be kept low unless GPS use is authorized.

\begin{ioxdef}
  GPS kill		& ---	& VGNSS\_SENSE	& interrupt & slow \\
			& ---	& VGNSS\_OVERRIDE & output & slow \\
\end{ioxdef}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{LED drivers}

\begin{ioxdef}
  Fancy RGB LED driver	& RGB\_INT	& RGB\_INT	& interrupt & slow \\
			& RGB\_CTRL\_EN	& RGB\_CTRL\_EN	& output & slow \\
\end{ioxdef}

The minimum delay after activating RGB\_CTRL\_EN is 500~$\mu$s
(section 7.4.1 of \cite{LP55231}).
Note that EN does not affect the \iic\ interface, and
the functionality of the EN line is also available through the CHIP\_EN
register bit, with resulting internal enable signal being
EN \&\& CHIP\_EN (section 7.4.1).

% Kernel (4.6) driver (drivers/leds/leds-lp55xx-common.c and leds-lp5523.c)
% asserts both EN and CHIP_EN at initialization and deasserts EN at driver
% de-initialization.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{NFC}

\begin{ioxdef}
  NFC			& rst		& NFC\_nRESET	& output & slow \\
			& int		& NFC\_INT	& interrupt & fast \\
			& swd$[0]$	& NFC\_SWD\_DIO	& input/output & slow\\
			& swd$[1]$	& NFC\_SWD\_CLK	& output & slow \\
\end{ioxdef}

NFC operates at data rates of up to 848~kbps.%
\footnote{See section 7.1.2 of \cite{Neo900NFC}.}
While the dedicated
microcontroller can perform timing-critical operations on behalf of the
CPU, the ability to respond rapidly to NFC events may still be desirable.

The SWD signal are for use during development and are not timing-critical.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Second SIM}

\begin{ioxgen}{}{%
\footnote{Names as defined in sections 3.1 and 3.2 of \cite{Neo900-SIMSW}.}}
  SIM switch		& ---		& MUX\_STROBE	& output & slow%
\footnote{The SIM switch circuit stretches the STROBE signal to at least
500~ms.} \\
			& ---		& MUX\_SEL	& output & slow \\
			& ---		& MUX\_CPU\_nMODEM & output & slow \\
			& ---		& CPU\_3V\_n1V8	& output & slow \\
			& ---		& CPU\_PWR\_EN	& output & medium \\
			& ---		& CPU\_SIM\_RST	& output & medium \\
			& ---		& CPU\_SIM\_CLK	& output & fast \\
			& ---		& CPU\_SIM\_IO	& input/output & fast\\
  SIM \#2		& cd		& CPU\_CD\_2	& interrupt & medium \\
\end{ioxgen}

According to sections 5.2.3 and 8.3 of \cite{ISO7816-3}, the
SIM CLK frequency must be at least 1~MHz and can be as high as 20~MHz.
The data rate at the IO pin can be negotiated, with a maximum of
$\rm f_{max} \times \sfrac{D}{F} = 860~kHz$ with
$\rm D = 64$, $\rm F = 372$, and $\rm f_{max} = 5~MHz$.

CPU\_PWR\_EN, RST, and CPU\_CD\_2 should operate at least at ``medium''
speed in order to facilitate clean shutdown on card removal.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Main camera}

\begin{ioxdef}
  Main cam conn		& cam\_d3	& CAM\_MAIN\_SHDN & output & slow%
\footnote{cam\_d3 connects to the XSHUTDOWN pin of the camera module:
\url{http://natisbad.org/N900/ref/VS6555.pdf}} \\
  Cam switch		& CAM\_B\_EN	& CAM\_B\_EN	& output & slow \\
  Flash LED driver	& en		& FLASH\_EN	& output & slow \\
\end{ioxdef}

The following two signals connect to the CPU but are not used as GPIOs:

\begin{ioxaltgen}
  Flash LED driver	& int	& FLASH\_INT	& Camera function block of CPU\\
			& strobe & FLASH\_STROBE& Camera function block of CPU\\
\end{ioxaltgen}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Hackerbus}

\begin{ioxgen}{}{\footnote{Names are either from the Hackerbus
white paper \cite{Neo900-HB} or the block diagram \cite{BD}.}}
  Hackerbus		& ---	& HB\_A	& input/output	& fast \\
			& ---	& HB\_B	& input/output	& fast \\
			& ---	& HB\_C	& input/output	& fast \\
			& ---	& HB\_D	& input/output	& fast \\
  HB USB PHY		& RST	& ---	& output	& slow \\
\end{ioxgen}

For a maximum of flexibility, all Hackerbus GPIOs are connected --~through
level shifters~-- to the CPU, thus allowing them to be used for input,
output, and interrupts.

% -----------------------------------------------------------------------------

\section{GPIO overview}

The following tables gives an overview of the GPIOs described in this document.
The compatibility column indicates the GPIO number of the corresponding
signal in the N900. ``---'' indicates that a signals is new in Neo900 or
has no obvious N900 counterpart. GPIOs that are used for different function
blocks in Neo900 are shown in parentheses.


\subsection{UPPER board}

\begin{longtable}[l]{lllll}
  Function &  Signal & Type & Speed & N900 \\
  \hline
  \endhead
\input upper.inc
\end{longtable}


\subsection{LOWER board}

\begin{longtable}[l]{lllll}
  Function & Signal & Type & Speed & N900 \\
  \hline
  \endhead
\input lower.inc
\end{longtable}


\subsection{BOB}

\begin{longtable}[l]{lllll}
  Function & Signal & Type & Speed & N900 \\
  \hline
  \endhead
\input bob.inc
\end{longtable}

% -----------------------------------------------------------------------------

\clearpage
\begin{thebibliography}{8}
\input iox.bbl
\end{thebibliography}

\end{document}
