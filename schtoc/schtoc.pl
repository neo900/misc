#!/usr/bin/perl
#
# schtoc.pl - EAGLE and KiCad schematics (.sch) to PDF with PDF TOC
#
# Written 2015-2016 by Werner Almesberger
# Copyright 2015-2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


sub pdf_tocify
{
	local ($s, @toc) = @_;

	# Analyze trailer

	die "no trailer" unless
	    $s =~ m|^(trailer\s+<<[^>]+/Size\s+)(\d+)([^>]+>>)|m;
	my ($t0, $top, $t1) = ($1, $2, $3);

	# Extract Page objects (@@@ should use Pages object)

	my $t = $s;
	my @page = ();
	while ($t =~ m|(\d+\s+\d+)\s+obj\s+<<\s+/Type\s+/Page\s+|) {
		push(@page, $1);
		$t = $';
	}

	# Generate outline items

	my $n = $top + 1;
	my $obj = "";
	for (my $i = 0; $i <= $#toc; $i++) {
		$obj .= "$n 0 obj\n<<\n\t/Title ($toc[$i])\n";
		$obj .= "\t/Parent $top 0 R\n";
		$obj .= "\t/Prev " . ($n - 1) . " 0 R\n" if $i;
		$obj .= "\t/Next " . ($n + 1) . " 0 R\n" if $i != $#toc;
		$obj .= "\t/Dest [$page[$i] R /Fit]\n>>\nendobj\n";
		$n++;
	}

	# Generate outline (root)

	my $cat = "$top 0 obj\n<<\n";
	$cat .= "\t/Count " . ($n - $top - 1) . "\n";
	$cat .= "\t/First " . ($top + 1) . " 0 R\n";
	$cat .= "\t/Last " . ($n - 1) . " 0 R\n";
	$cat .= ">>\nendobj\n";

	# Add pointer to Outlines directory to catalog

	my $out = "/Outlines $top 0 R\n";

	if ($zero) {
		$out .= "/PageLabels << /Nums [ 0 << /S /D /St 0 >> ] >>\n";
	}
	$s =~ s|<<\s+/Type\s+/Catalog\s+[^>]+|$&$out|m;

	# Remove xref, trailer, and startxref

	$s =~ s/^xref\s+.*//ms;

	# Append the outline

	$s .= $cat . $obj;

	# Index objects

	my $pos = 0;
	my $t = $s;
	my @obj_pos = ();
	my @obj_gen = ();
	while ($t =~ m|^(\d+)\s+(\d+)\s+obj\s+|m) {
		$pos += length $`;
		$obj_pos[$1] = $pos;
		$obj_gen[$1] = $2;
		$pos += length $&;
		$t = $';
	}

	#
	# Regenerate xref
	#
	# Section 3.4.3 page 94 of
	# "PDF Reference, sixth edition", "Adobe Portable Document Format
	# Version 1.7", November 2006
	# says that EOL in the xref table is marked with two characters.
	# If using only \n, it has to be preceded by a space.
	#

	my $xref_pos = length $s;

	$s .= "xref\n0 $n\n";
	$s .= sprintf("%010d 65535 f \n", 0);
	for (my $i = 1; $i != $n; $i++) {
		die "no object $i" unless defined $obj_pos[$i];
		$s .= sprintf("%010d %05d n\n", $obj_pos[$i], $obj_gen[$1]);
	}

	# Regenerate trailer

	$s .= "$t0$n$t1\n";

	# Regenerate startxref

	$s .= "startxref\n$xref_pos\n%%EOF\n";

	return $s;
}


# HTML + UTF-8 -> PDF

sub hutf_to_pdf
{
	local ($s) = @_;

	$s =~ s/&amp;/&/g;		# &amp; -> &
	$s =~ s/\302\265/\265/g;	# UTF-8 mu to PDF (see [1] Appendix D)

	return $s;
}

#
# [1] PDF Reference
# http://www.adobe.com/content/dam/Adobe/en/devnet/acrobat/pdfs/pdf_reference_1-7.pdf
#


sub get_eagle_toc
{
	local ($s) = @_;

	my @res = ();
	my $n = 0;
	SHEET: while ($s =~ /<sheet>(.*?)<\/sheet>/s) {
		my $t = $1;

		$s = $';
		$n++;
		while ($t =~
		    /<text.*size="(\d(\.\d+)?).*layer="94">([^<]+)/) {
			if ($1 > 4) { # @@@ arbitrary threshold
				my $entry = &hutf_to_pdf($3);

				$entry = "$n $entry" if $numbers;
				push(@res, $entry);
				next SHEET;
			}
			$t = $';
		}
		push(@res, "?");
	}
	return @res;
}


sub get_kicad_toc
{
	local ($s) = @_;

	my @res = ();
	my $n = 0;

	die unless $s =~ /Title\s+"([^}]*?)"/;
	push(@res, $1);
	while ($s =~ /\$Sheet*(.*?)\$EndSheet/s) {
		my $t = $1;

		$s = $';
		$n++;
		die unless $t =~ /F0\s+"([^"]*?)"/s;
		$entry = $numbers ? "$n $1" : $1;
		push(@res, $entry);
	}
	return @res;
}


sub usage
{
	print STDERR <<"EOF";
usage: $0 [-n] [-z] eagle.sch
       $0 [-n] [-z] kicad.sch kicad.pdf
EOF
	exit 1;
}


while ($ARGV[0] =~ /^-/) {
	my $opt = shift @ARGV;

	if ($opt eq "-n") {
		$numbers = 1;
	} elsif ($opt eq "-z") {
		$zero = 1;
	} else {
		&usage;
	}
}
&usage unless $#ARGV <= 1;
$file = $ARGV[0];
$tmp = "_tmp.pdf";

open(SCH, $file) || die "$file: $!";
$sch = join("", <SCH>);
close SCH;

if ($#ARGV == 0) {
	@toc = &get_eagle_toc($sch);

	unlink("$tmp");
	system("eagle", "-C",
	    "print 20 -1 file ./$tmp landscape sheets all; quit;", $file) &&
	    die "system: $!";
} else {
	@toc = &get_kicad_toc($sch);
	$tmp = $ARGV[1];
}

open(PDF, $tmp) || die "$tmp: $!";
$pdf = join("", <PDF>);
close PDF;

print &pdf_tocify($pdf, @toc);
