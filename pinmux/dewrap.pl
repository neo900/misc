#!/usr/bin/perl
#
# dewrap.pl - Merge wrapped ball names (and make list space-separated)
#
# Written 2016 by Werner Almesberger
# Copyright 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


sub usage
{
	print STDERR <<"EOF";
usage: $0 [-q] [file ...]

  -q	quote each non-empty field
EOF
	exit(1);
}


$quote = 0;
while ($ARGV[0] =~ /^-./) {
	if ($ARGV[0] eq "-q") {
		$quote = 1;
	} else {
		&usage;
	}
	shift @ARGV;
}

while (<>) {
	my @a = ();

	chop;
	while (length $_) {
		if (/^"([^"]+)"(,|$)/) {
			push(@a, $1);
			$_ = $';
			push(@a, "") if $2 eq "," && $' eq "";
			die if $1 =~ /,/ && $#a > 1;
		} elsif (/^,/) {
			push(@a, "");
			push(@a, "") if $_ eq ",";
			$_ = $';
		} else {
			die;
		}
	}

	if ($#f != -1) {
		for $i (0, 1) {
			if ($f[$#f][$i] =~ /,$/) {
				$f[$#f][$i] .= $a[$i];
				$a[$i] = "";
			}
		}
		next if join("", @a) eq "";
	}

	push(@f, [ @a ]);
}

for (@f) {
	for $i (0, 1) {
		@{ $_ }[$i] =~ s/\s*,\s*/ /g;
	}
	print join(",",
	    map { $_ eq "" || !$quote ? $_ : "\"$_\"" } @{ $_ }), "\n"
}
