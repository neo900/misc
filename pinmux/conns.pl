#!/usr/bin/perl
#
# conns.pl - Generate ASCII drawing for BB-xM connector assignment
#
# Written 2016 by Werner Almesberger
# Copyright 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

sub usage
{
	print STDERR <<"EOF";
usage: $0 connector-spec ...

connector-spec: Pnum:contacts:orientation
orientation: location of pin 1, ul (upper left), ll, or lr
EOF
	exit(1);
}

for (@ARGV) {
	&usage unless /^P(\d+):(\d+):(ul|ll|lr)$/;
	die "connecor P$1 specified multiple times" if defined $conn[$1];
	$contacts[$1] = $2;
	$orient[$1] = $3;
}

while (<STDIN>) {
	@a = split(/\s+/);
	die unless $a[2] =~ /^P(\d+)_(\d+)$/;
	die "unknown connector P$1" unless defined $contacts[$1];
	die "P$1_$2 used multiple times" if defined $use[$1][$2];
	$use[$1][$2] = $a[0];
	$fn[$1][$2] = $a[1];
	my $len = length($a[0]) + length($a[1]);
	$max = $len if $len > $max;
}

for $p (keys @use) {
	my @u = @{ $use[$p] };
	my @f = @{ $fn[$p] };

	next unless defined $use[$p];
	printf("%*sP$p\n", $max + 5, "");
	for (0..($contacts[$p] - 1)) {
		my $o = $orient[$p];

		if ($o eq "ul") {
			$n = $_ + 1;
		} elsif ($o eq "lr") {
			$n = $contacts[$p] - $_;
		} elsif ($o eq "ll") {
			$n = $contacts[$p] - ($_ ^ 1);
		}
		if (!($_ & 1)) {	# left
			printf("%*s ---[ %2d  ",
			    $max + 1, "$u[$n] $f[$n]", $n);
		} else {		# right
			printf("%2d ]--- %-*s\n",
			    $n, $max, "$f[$n] $u[$n]");
		}
	}
	print "\n";
}
