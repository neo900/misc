#!/usr/bin/perl
#
# csv2map.pl - Generate pin map from "raw" cbp.csv
#
# Written 2016 by Werner Almesberger
# Copyright 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


while (<>) {
	chop;

	my @a = split(/,/, $_);

	$ball = $a[0] if $a[0] ne "";

	next if $ball eq "NA";
	$fn = $a[2];
	$type = $a[4];
	$reset = $a[5] if $a[5] ne "";
	$reset_rel = $a[6] if $a[6] ne "";

	next if $type eq "-";
	$fn = $type if $type eq "GND" || $type eq "PWR" || $type =~ /^POP:/;

	next if $fn =~ /safe_mode/;

	for (split(/\s+/, $ball)) {
		push(@{ $fn{$_} }, $fn);
		$rst{$_} = "$reset$reset_rel";
	}
}


sub cmp
{
	local ($a, $b) = @_;

	die "bad ball name $a" unless $a =~ /^([A-Z]+)(\d+)$/;
	my ($aa, $an) = ($1, $2);
	die "bad ball name $b" unless $b =~ /^([A-Z]+)(\d+)$/;
	my ($ba, $bn) = ($1, $2);

	return $an <=> $bn if $aa eq $ba;
	return $aa cmp $ba;
}


print "#\n# *** MACHINE-GENERATED. DO NOT EDIT ! ***\n#\n\n";
for (sort { &cmp($a, $b) } keys %fn) {
	my @f = @{ $fn{$_} };

	print "$_\t$rst{$_}\t", join(" ", @f), "\n";
}
