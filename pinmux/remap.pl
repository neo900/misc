#!/usr/bin/perl

sub usage
{
	print STDERR "usage: $0 remap.txt [map.txt]\n";
	exit(1);
}


&usage unless $#ARGV == 1;

open(RE, $ARGV[0]) || die "$ARGV[0]: $!";
while (<RE>) {
	s/#.*//;
	next if /^\s*$/;
	die unless /^(\S+)\s+(\S+)(\s+(\S+))?/;
	die "duplicate contact $1" if $contact{$1};
	$contact{$1} = 1;
	warn "duplicate function $2:$4" if exists $fn{$2} && $fn{$2} ne $4;
	$fn{$2} = $4;
	push(@{ $map{$4} }, $1) if defined $3;
}
close(RE);

shift @ARGV;
while (<>) {
	my $orig = $_;

	s/#.*//;
	if (/^\s*$/) {
		print $orig;
		next;
	}
	die unless /^(\S+)\s+/;
	next unless defined $map{$1};
	for (@{ $map{$1} }) {
		print "$_\t$'";
	}
	delete $map{$1};
}

for (keys %map) {
	warn "unmatched pin $_\n";
}
