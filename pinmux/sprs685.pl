#!/usr/bin/perl
#
# sprs685.pl - Extract pin information from data sheet
#
# Written 2016 by Werner Almesberger
# Copyright 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

	
@width = (
	10,	# BALL BOTTOM
	11,	# BALL TOP
	21,	# PIN NAME
	11,	# MODE
	12,	# TYPE
	12,	# BALL RESET STATE
	12,	# BALL RESET REL. STATE
	10,	# RESET REL. MODE
	11,	# POWER
	11,	# HYS
	11,	# BUFFER STRENGTH (mA)
	12,	# PULLUP/DOWN TYPE
	0,	# IO CELL
);


@key = (
	"BOTTOM",	#  0	BALL BOTTOM
	"BALL TOP",	#  1	BALL TOP
	"PIN NAME",	#  2	PIN NAME
	"MODE",		#  3	MODE
	"TYPE",		#  4	TYPE
	"STATE",	#  5	BALL RESET STATE
	"STATE",	#  6	BALL RESET REL. STATE
	"REL. MODE",	#  7	RESET REL. MODE
	"POWER",	#  8	POWER
	"HYS",		#  9	HYS
	"STRENGTH",	# 10	BUFFER STRENGTH (mA)
	"PULLUP",	# 11	PULLUP/DOWN TYPE
	"IO CELL"	# 12	IO CELL
);


sub usage
{
	print STDERR <<"EOF";
usage: $0 [-c] [-d] [file ...]

  -c	output each line in comma-separated value format, without processing
  -d	enable debugging output
EOF
	exit(1);
}


while ($ARGV[0] =~ /^-/) {
	if ($ARGV[0] eq "-c") {
		$csv = 1;
	} elsif ($ARGV[0] eq "-d") {
		$debug = 1;
	} else {
		&usage;
	}
	shift @ARGV;
}

$skip = 1;
$scan = 0;

while (<>) {
	my @a = ();
	my $t = $_;

	chop $t;

	# Detect in which area of the page we are

	$skip = 1 if /^\014/;	# ^L
	$skip = 1 if /The usage/;	# below the end of the table
	if ($skip) {
		if (/^\s+BALL/) {
			$skip = 0;
			$scan = 1;
			@pos = ();
		}
		next;
	}

	# Look for table heading fragments

	if ($scan) {
		my $p = 0;

		if ($t =~ /^\s+/) {
			$p = length $&;	
			$t = $';
		}
		while ($t =~ /^\S+\s*/) {
			my ($this, $tail) = ($&, $');

			MATCH: for (my $i = 0; $i <= $#key; $i++) {
				if (!defined $pos[$i] && $t =~ /^$key[$i]/) {
					$pos[$i] = $p;
					last MATCH;
				}
			}
			$p += length $this;
			$t = $tail;
		}
	}

	# Calculate column positions

	if ($scan && /STATE \[6\]/) {
		my $last = undef;

		@width = ();
		for (my $i = 0; $i <= $#pos; $i++) {
			die "$key[$i - 1] ($last) >= $key[$i] ($pos[$i])"
			    if defined $last && $last >= $pos[$i];
			push(@width, $pos[$i] - $last) if defined $last;
			$last = $pos[$i];
		}
		$scan = 0;
		next;
	}

	next if $scan;

	# Extract columns

	for (@width) {
		push(@a, substr($t, 0, $_, ""));
	}

	if ($csv) {
		for (@a) {
			$_ =~ s/^\s+//;
			$_ =~ s/\s+$//;
		}
		print '"', join('","', @a), "\"\n";
		next;
	}

	# Patch up wrong gusses by pdftotext

	if ($a[1] !~ /\s$/ && $a[2] =~ /^\s/) {
		$a[1] .= " ";
		$a[2] = $';
	}

	# Another wrong guess

	if ($a[2] eq ",") {
		$a[1] .= ",";
		$a[2] = "";
	}

	# Filter out bogus lines

	next unless $a[0] =~ /\s$/ || "$a[1]$a[2]" eq "";
						# BALL BOTTOM
	next unless $a[1] =~ /\s$/ || $a[2] eq "";
						# BALL TOP
#	next unless $a[2] =~ /^\S.*\s$/;	# PIN NAME
#	next unless $a[4] =~ /^\S/;		# TYPE

	# Clean up field content

	# @@@ AF15, AE15, AF14, AG14 have bogus IO CELL

	for (@a) {
		$_ =~ s/(\s*\(\d+\))*\s*$//;
		$_ =~ s/\($//;	# vdds_mmc1(
	}

	# Freaky stuff like "hsusb1_ data3"

	$a[2] =~ s/\s//g;

	# Filter out non-IO entries

#	next if $a[4] eq "PWR";
#	next if $a[4] eq "GND";
	if ($a[4] eq "-") {
		undef $type;
		next;
	}

	# Fix wrong guesses by pdftotext

	if ($a[0] =~ /,\s*NA/) {
		$a[0] = $`;
		splice(@a, 1, 0, "NA");
	}

	# Filter out top balls without bottom ball

	next if $a[0] eq "NA";

	# Debug output

	print STDERR join("|", @a), "\n" if $debug;

	$pin = $a[0] if $a[0] ne "";
	$type = $a[2] if $a[2] ne "" && $a[4] ne "";
	$type = $a[4] if $a[4] eq "PWR" || $a[4] eq "GND";
	$pin =~ s/,\s*$//;
	next if $type eq "";
	for $p (split(/,\s*/, $pin)) {
		push(@{ $fn{$p} }, $type)
		    unless grep({ $_ eq $type } @{ $fn{$p} });
	}
}


sub cmp
{
	local ($a, $b) = @_;

	die "bad ball name $a" unless $a =~ /^([A-Z]+)(\d+)$/;
	my ($aa, $an) = ($1, $2);
	die "bad ball name $b" unless $b =~ /^([A-Z]+)(\d+)$/;
	my ($ba, $bn) = ($1, $2);

	return $an <=> $bn if $aa eq $ba;
	return $aa cmp $ba;
}


for (sort { &cmp($a, $b) } keys %fn) {
	my @f = @{ $fn{$_} };

	print "$_\t", join(" ", @f), "\n";
}
