#!/usr/bin/perl
#
# pinmux.pl - Check pin assignment and generate reports
#
# Written 2016 by Werner Almesberger
# Copyright 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


sub usage
{
	print STDERR <<"EOF";
usage: $0 [-a] [-d] [-f [-f]] [-p] [-q name ...] [-Q name ...]
	map.txt [file ...]

  -a	show assigned pins
  -d	enable debugging output
  -f	show free (unassigned) pins
  -f -f	only show yet unassigned functions
  -p	show power pins
  -q name
	show pins with function/type matching /^name\$/
  -Q name
	like -q, but fail if the query never matches
EOF
	exit(1);
}


while ($ARGV[0] =~ /^-/) {
	if ($ARGV[0] eq "-a") {
		$assigned = 1;
	} elsif ($ARGV[0] eq "-d") {
		$debug = 1;
	} elsif ($ARGV[0] eq "-f") {
		$free++;
	} elsif ($ARGV[0] eq "-p") {
		$power = 1;
	} elsif ($ARGV[0] =~ /^-[Qq]$/) {
		$unmatched{$ARGV[1]} = 1 if $ARGV[0] eq "-Q";
		shift @ARGV;
		&usage unless defined $ARGV[0];
		push(@query, $ARGV[0]);
	} else {
		&usage;
	}
	shift @ARGV;
}


$map = shift @ARGV;
&usage unless defined $map;

open(MAP, $map) || die "$map: $!";
while (<MAP>) {
	chop;
	s/#.*//;
	next if /^\s*$/;

	my @a = split(/\s+/, $_);
	my $pin = shift @a;

	$rst{$pin} = shift @a;
	$fn{$pin} = [ @a ];
	for (@a) {
		push(@{ $rev{$_} }, $pin);
	}
}
close MAP;


sub conflict
{
	local ($s) = @_;

	warn "$s\n";
	$conflicts++;
}


while (<>) {
	print STDERR "$_" if $debug;
	chop;
	s/#.*//;
	next if /^\s*$/;

	my @a = split(/\s+/, $_);

	if ($a[0] eq "") {
		die unless defined $use;
		shift @a;
	} else {
		$use = shift @a;
	}

	my @pin = ();
	my @details = ();

	for my $s (@a) {
		# We want to be able to concatenate names like this:
		# BLOCK_signal
		# In cpp, this could be done with ##, but that's rather
		# awkward. Much easier to use BLOCK.signal in cpp, and turn
		# the . into a _ in this script.

		$s =~ s/\./_/g;

		my $rst = undef;
		if ($s =~ /<([^)]+)>$/) {
			$rst = $1;
			$s = $`;
		}

		if ($s =~ /:/) {
			my ($fn, $p) = ($`, $');
			my $found = 0;

			die "unknown function \"$fn\"" unless defined $rev{$fn};
			die "unknown pin \"$p\"" unless defined $fn{$p};
			die "function $fn is not available on pin $p" unless
			    grep($_ eq $p, @{ $rev{$fn} }) == 1;
			push(@pin, $p);
			push(@details, $fn);
		} else {
			die "unknown function \"$s\"" unless defined $rev{$s};

			my @pins = @{ $rev{$s} };

			die "function $s has multiple pin options: " .
			    join(" ", @pins)
			    if $#pins;
			push(@pin, $pins[0]);
			push(@details, $s);
		}

		next unless defined $rst;

		my $pin = $pin[$#pin];

		$rst = $rst . $rst if length $rst == 1;
		die "invalid reset configuration \"$rst\"" if length $rst != 2;
		die "$pin ($details[$#details]) has reset configuration " .
		    "$rst{$pin}:, not $rst"
		    unless $rst{$pin} eq $rst;
	}

	for (my $i = 0; $i <= $#pin; $i++) {
		my $p = $pin[$i];
		my $d = $details[$i];

		&conflict("$use: pin $p is already in use by $pin_use{$p}")
		    if defined $pin_use{$p};
		&conflict(
		    "$use: function $d is already assigned to pin $fn_use{$d}")
		    if defined $fn_use{$d};
		$pin_use{$p} = "$use:$details[$i]:$p";
		$fn_use{$d} = $p;
	}
}
die if $conflicts;


#
# We allow "ball" names to be either the form ABC123 (for BGA balls) but also
# P12_34 (for connectors).
#

sub cmp
{
	local ($a, $b) = @_;

	die "bad ball name $a" unless $a =~ /^([A-Z_0-9]+?)(\d+)$/;
	my ($aa, $an) = ($1, $2);
	die "bad ball name $b" unless $b =~ /^([A-Z_0-9]+?)(\d+)$/;
	my ($ba, $bn) = ($1, $2);

	return $an <=> $bn if $aa eq $ba;
	return $aa cmp $ba;
}


sub split
{
	local ($s, @w) = @_;

	my @a = split(/:/, $s);
	my $s = "";

	for (@a) {
		my $w = ($w[0] + 7) & ~7;
		my $left = $w - length $_;

		$s .= $_;
		if ($w == 0) {
			# do nothing
		} elsif ($left <= 0) {
			$s .= " ";
		} else {
			$s .= "\t" x (($left + 7) >> 3);
		}
		shift @w;
	}
	return $s;
}


if ($assigned) {
	my @w = ();

	for (values %pin_use) {
		my $i = 0;

		for (split(/:/, $_)) {
			$w[$i] = length $_ if length $_ > $w[$i];
			$i++;
		}
	}
	pop @w;

	for (sort values %pin_use) {
		print &split($_, @w), "\n";
	}
}

if ($free || $power) {
	for (sort { &cmp($a, $b) } keys %fn) {
		next if defined $pin_use{$_};

		my @a = @{ $fn{$_} };

		next if $a[0] =~ /^POP:/;
		if ($a[0] eq "GND" || $a[0] eq "PWR") {
			next unless $power;
		} else {
			next unless $free;
		}

		@a = grep(!defined $fn_use{$_}, @a) if $free > 1;
		print "$_\t$rst{$_}\t", join(" ", @a), "\n";
	}
}

if ($#query != -1) {
	PIN: for $p (sort { &cmp($a, $b) } keys %fn) {
		my @a = @{ $fn{$p} };

		QUERY: {
			for $f (@a) {
				for $q (@query) {
					next unless $f =~ /^$q$/;
					delete $unmatched{$q};
					last QUERY;
				}
			}
			next PIN;
		}

		print "$p\t", join(" ", @a), "\n";
	}
	die "unmatched query/queries: ",
	    join(", ", map("\"$_\"", keys %unmatched)), "\n"
	    if keys %unmatched;
}
