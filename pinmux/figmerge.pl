#!/usr/bin/perl
#
# figmerge.pl - Merge multiple FIG files into one
#
# Written 2016 by Werner Almesberger
# Copyright 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

#
# @@@ Known bug: color objects are only allowed in the first object.
#

sub usage
{
	print STDERR <<"EOF";
usage: $0 [-s from to ...] [file ...]

  -s from to	replace text "from" (regex) with "to"
  -S from to	like -s, but fail if there is no match
EOF
	exit 1;
}


while ($ARGV[0] =~ /^-./) {
	if ($ARGV[0] =~ /^-[Ss]$/) {
		$unmatched{$ARGV[1]} = 1 if $ARGV[0] eq "-S";
		shift @ARGV;
		&usage unless defined $ARGV[1];
		$subst{$ARGV[0]} = $ARGV[1];
		shift @ARGV;
	} else {
		&usage;
	}
	shift @ARGV;
}

$first = 1;
$skip = 0;
while (<>) {
	if (/^#FIG/) {
		$skip = !$first;
		$first = 0;
	}
	if ($skip) {
		$skip = 0 if /^1200\s/;
		next;
	}
	for $s (keys %subst) {
		next unless /^4\s/;
		delete $unmatched{$s} if /$s/;
		s/$s/$subst{$s}/;
	}
	print;
}

die "unmatched pattern(s): ", join(", ", map("\"$_\"", keys %unmatched)), "\n"
    if keys %unmatched;
