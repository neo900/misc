#!/usr/bin/perl
#
# easize.pl - Calculate board space taken up by components
#
# Written 2015 by Werner Almesberger
# Copyright 2015 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#


# Unit is micrometer


sub usage
{
	print STDERR "usage: $0 [-f] library.scr clear.def partlist.txt\n";
	exit(1);
}


if ($ARGV[0] eq "-f") {
	$fig = 1;
	shift @ARGV;
}

&usage if $ARGV[0] =~ /^-/;
&usage unless $#ARGV == 2;


# ----- Helper functions ------------------------------------------------------


sub rot
{
	local ($deg, $x, $y) = @_;

	my $a = $deg / 180.0 * 3.1415926;
	my $s = sin($a);
	my $c = cos($a);

	return ($c * $x - $s * $y, $c * $y + $s * $x);

}


# ----- FIG output ------------------------------------------------------------


$POLY_TOP_COLOR = 31;	# gold
$POLY_TOP_DEPTH = 80;
$POLY_BOT_COLOR = 31;	# gold
$POLY_BOT_DEPTH = 110;

$RECT_TOP_COLOR = 18;	# dark red
$RECT_TOP_DEPTH = 90;
$RECT_BOT_COLOR = 9;	# dark blue
$RECT_BOT_DEPTH = 120;

$BB_TOP_COLOR = 5;		# magenta
$BB_TOP_DEPTH = 70;
$BB_BOT_COLOR = 3;		# cyan
$BB_BOT_DEPTH = 100;

$ZOOM = 1;


sub fig_header
{
	print "#FIG 3.2\nLandscape\nCenter\nMetric\nA4\n";
	print "100.00\nSingle\n-2\n1200 2\n";
}


sub poly
{
	local ($fg, $depth, @p) = @_;

	# object_code sub_type line_style thickness
	print "2 1 0 2";
	# pen_color fill_color depth pen_style
	print " $fg -1 $depth -1";
	# area_fill style_val join_style cap_style radius
	print " -1 0.000 0 0 -1";
	# forward_arrow backward_arrow
	print " 0 0 ";
	# npoints
	print (($#p + 1) / 2);
	print "\n\t";

	while ($#p != -1) {
		die if $#p == 0;
		my $x = shift @p;
		my $y = shift @p;

		&coord($x, $y);
	}
	print "\n";
}


sub fig_poly
{
	local ($top, @p) = @_;

	return unless $fig;

	&poly($top ? $POLY_TOP_COLOR : $POLY_BOT_COLOR,
	    $top ? $POLY_TOP_DEPTH : $POLY_BOT_DEPTH, @p);
}


sub coord
{
	local ($x, $y) = @_;

	print int($x / 25.4 / 1000 * 1200 * $ZOOM + 0.5);
	print " ";
	print int(-$y / 25.4 / 1000 * 1200 * $ZOOM + 0.5);
	print " ";
}


sub fig_comment
{
	local ($s) = @_;

	return unless $fig;

	print "# $s\n";
}


sub fig_rect
{
	local ($top, $x0, $y0, $x1, $y1) = @_;

	return unless $fig;

	die if $x0 == $x1 && $y0 == $y1;

	# object_code sub_type line_style thickness
	print "2 2 0 0";
	# pen_color
	print " -1 ";
	# fill_color
	print $top ? $RECT_TOP_COLOR : $RECT_BOT_COLOR;
	print " ";
	# depth
	print $top ? $RECT_TOP_DEPTH : $RECT_BOT_DEPTH;
	# pen_style
	print " -1";
	# area_fill style_val join_style cap_style radius
	print " 20 0.000 0 0 -1";
	# forward_arrow backward_arrow npoints
	print " 0 0 5\n\t";

	&coord($x0, $y0);
	&coord($x1, $y0);
	&coord($x1, $y1);
	&coord($x0, $y1);
	&coord($x0, $y0);
	print "\n";
}


sub fig_bb
{
	local ($top, $x0, $y0, $x1, $y1) = @_;

	return unless $fig;

	&poly($top ? $BB_TOP_COLOR : $BB_BOT_COLOR,
	    $top ? $BB_TOP_DEPTH : $BB_BOT_DEPTH,
	    $x0, $y0,
	    $x1, $y0,
	    $x1, $y1,
	    $x0, $y1,
	    $x0, $y0);
}


sub fig_begin
{
	local ($part, $pkg) = @_;
}


sub fig_end
{
#	print "-6\n";
}


&fig_header if $fig;


# ----- Read the library-generating script ------------------------------------


sub add
{
	push(@{ $pkg{$curr_pkg} }, [ $curr_layer, @_ ]);
}


sub add_poly
{
	local ($rot, $poly) = @_;

	(my $tmp = $') =~ s/[();]//g;

	my @p = split(/\s+/, $tmp);
	die unless $#p & 1;

	my @pr = ();
	while ($#p >= 0) {
		$x = shift @p;
		$y = shift @p;
		($x, $y) = &rot($rot, $x, $y);
		&add($x, $y);
		push(@pr, $x, $y);
	}
	push(@{ $fig{$curr_pkg} }, [ \&fig_poly, @pr ]);
}


sub box
{
	local ($x0, $y0, $x1, $y1) = @_;

	if ($x0 == $x1 && $y0 == $y1) {
		warn "zero-sized rect ($x0 $y0) ($x1 $y1) in $curr_pkg";
		return;
	}
	&add($x0, $y0);
	&add($x1, $y1);
	push(@{ $fig{$curr_pkg} }, [ \&fig_rect,
	    $x0 < $x1 ? $x0 : $x1, $y0 < $y1 ? $y0 : $y1,
	    $x0 < $x1 ? $x1 : $x0, $y0 < $y1 ? $y1 : $y0 ]);
}


sub add_smd
{
	local ($w, $h, $rot, $x, $y) = @_;

	($dx, $dy) = &rot($rot, $w / 2.0, $h / 2.0);
	&box($x - $dx, $y - $dy, $x + $dx, $y + $dy);
}


sub add_pad
{
	local ($w, $shape, $rot, $x, $y) = @_;

	if ($w < 1) {
		#
		# @@@ hack: pick Restring(min) from the design rules.
		#
		$w = $drill + 400;
		print STDERR "$curr_pkg: set pad width to $w\n";
	}

	my $h = $w;
	$w = 2 * $w if $shape eq "Long";

	#
	# @@@ quirk: "pad" makes through-hole components. We don't have a lot
	# of these and in most cases, they are not where we'd be concerned
	# about space. So we treat them as being on the respective copper
	# layer.
	#
	$curr_layer = 1;
	&add_smd($w, $h, $rot, $x, $y);
}


open(SCR, $ARGV[0]) || die "$ARGV[0]: $!";
undef $curr_pkg;
while (<SCR>) {
	if (/^Edit\s+'(.*?)\.PAC'/) {
		die "duplicate package \"$1\"" if defined $pkg{$1};
		$curr_pkg = $1;
		next;
	}
	next unless defined $curr_pkg;

	if (/^DESCRIPTION\b/) {
		undef $curr_pkg;
		next;
	}

	if (/^Layer (\d+)/) {
		$curr_layer = $1;
		next;
	}

	if (/^Wire\s+\d+(\.\d*)?\s+/) {
		&add_poly(0, $');
		next;
	}
	die "bad Wire" if /^Wire/;

	if (/^Rect\s+R(\d+(\.\d+)?)\s+/) {
		&add_poly($1, $');
		next;
	}
	die "bad Rect" if /^Rect/;

	if (/^SMD\s+(\d+(\.\d+)?)\s+(\d+(\.\d+)?)\s+-?\d+\s+R(\d+(\.\d+))\s+[^']*'[^']*'\s+\((-?\d+(\.\d+)?)\s+(-?\d+(\.\d+)?)\)/) {
		&add_smd($1, $3, $5, $7, $9);
		next;
	}
	die "bad SMD" if /^SMD/;

	if (/^Pad\s+(\d+(\.\d+)?)\s+(\S+)\s+R(\d+(\.\d+))\s+[^']*'[^']*'\s+\((-?\d+(\.\d+)?)\s+(-?\d+(\.\d+)?)\)/) {
		&add_pad($1, $3, $4, $6, $8);
		next;
	}
	die "bad Pad" if /^Pad/;

	if (/^Change Drill (\d+(\.\d*)?);/) {
		$drill = $1;
		next;
	}
	die "bad Change Drill" if /^Change Drill/;

	if (/^Text\s+[MRS]\S+\s+'([^']*)'/) {
		my $t = $1;
		next unless $t =~ /HEIGHT/;
		if ($t =~ /^HEIGHT>\s+(\d*\.?\d+)(mm)?/) {
			die "bad height unit" if $2 eq "" && $1;
			$h{$curr_pkg} = "$1$2";
		} elsif ($t =~ /^>HEIGHT:\s+(\d*\.?\d+)(mm)?/) {
			die "bad height unit" if $2 eq "" && $1;
			$h{$curr_pkg} = "$1$2";
		} else {
			warn "bad height \"$t\"";
		}
		next;
	}
	die "bad Text" if /^Text/;
}
close SCR;


# ----- Read extra clearances -------------------------------------------------


open(CLEAR, $ARGV[1]) || die "$ARGV[1]: $!";
while (<CLEAR>) {
	s/#.*\n//;
	next if /^\s*$/;
	die "bad clearance" unless /^(\d+)\s+(\d+(\.\d*)?|-)\s*$/;
	die "duplicate clearance for layer $1" if defined $clear[$1];
	$clear[$1] = $2;
}
close CLEAR;


# ----- Compute bounding boxes ------------------------------------------------


for $pkg (keys %pkg) {
	my ($x0, $x1, $y0, $y1) = (0, 0, 0, 0);
	for (@{ $pkg{$pkg} }) {
		my @p = @{ $_ };
		my $clr = $clear[shift @p];

		next if $clr eq "-";

		for (my $i = 0; $i < $#p; $i += 2) {
			my $x = $p[$i];
			my $y = $p[$i + 1];

			$x0 = $x - $clr if $x0 > $x - $clr;
			$x1 = $x + $clr if $x1 < $x + $clr;
			$y0 = $y - $clr if $y0 > $y - $clr;
			$y1 = $y + $clr if $y1 < $y + $clr;
		}
	}
	$bb{$pkg} = [ $x0, $y0, $x1, $y1 ];
}


# ----- Read part list --------------------------------------------------------


sub part
{
	local ($part, $value, $pkg, $lib, $pos, $orient) = @_;

	die "unknown package $pkg" unless defined $bb{$pkg};
if (!$warned{$pkg} && !defined $h{$pkg}) {
warn "no height for package $pkg ($part $value)\n";
$warned{$pkg} = 1;
}
	my ($x0, $y0, $x1, $y1) = @{ $bb{$pkg} };
	my $x_mm = abs($x1 - $x0) / 1000;
	my $y_mm = abs($y1 - $y0) / 1000;
	my $area_mm2 = $x_mm * $y_mm;

	$part = "-" if $part eq "";
	$value = "-" if $value eq "";
	my $s = sprintf("%-35s %-14s %-32s %7.3f %7.3f %8.3f",
	    $pkg, $part, $value, $x_mm, $y_mm, $area_mm2);
	push(@res, [ $area_mm2, $part, $s ]);

	$total_mm2 += $area_mm2;

	die unless $pos =~ /\((-?\d+\.?\d*)\s+(-?\d+\.?\d*)\)/;
	my ($px, $py) = ($1 * 1000, $2 * 1000);

	&fig_begin($part, $pkg);

	die "bad orientation \"$orient\""
	    unless $orient =~ /^(M?)R(\d+(\.\d*)?)$/;
	my $top = $1 ne "M";
	my $rot = $2;

	($x0, $x1) = (-$x1, -$x0) unless $top;
	($x0, $y0) = &rot($rot, $x0, $y0);
	($x1, $y1) = &rot($rot, $x1, $y1);
	&fig_comment("$pkg-$part");
	&fig_bb($top, $px + $x0, $py + $y0, $px + $x1, $py + $y1);

	for (@{ $fig{$pkg} }) {
		my @a = @{ $_ };
		my @res = ();
		my $fn = shift @a;

		while ($#a != -1) {
			die if $#a == 0;
			my $x = shift @a;
			my $y = shift @a;

			$x = -$x unless $top;
			($x, $y) = &rot($rot, $x, $y);
			push(@res, $px + $x, $py + $y);
		}
		&$fn($top, @res);
	}
	&fig_end();
}


undef @pos;
open(PART, $ARGV[2]) || die "$ARGV[2]: $!";
while (<PART>) {
	next if /^\s*$/;
	if (/^Part\s/) {
		my $off = 0;

		s/\(mm\)/    /;
		while (/^\S+\s*/) {
			push(@pos, $off);
			$off += length $&;
			$_ = $';
		}
		next;
	}
	next if $#pos == -1;

	my @d = ();
	for (my $i = 0; $i <= $#pos; $i++) {
		my $tmp;

		if ($i == $#pos) {
			$tmp = substr($_, $pos[$i]);
		} else {
			$tmp = substr($_, $pos[$i], $pos[$i + 1] - $pos[$i]);
		}
		$tmp =~ s/\s*$//;
		push(@d, $tmp);
	}
	&part(@d);
}
close PART;


# ----- Sorted output ---------------------------------------------------------


sub by_area_or_name
{
	my @ra = @{ $a };
	my @rb = @{ $b };
	my $d = $rb[0] <=> $ra[0];

	return $d if $d;
	if ($ra[1] =~ /^([A-Z]+)(\d+)$/) {
		my ($ta, $na) = ($1, $2);

		if ($rb[1] =~ /^([A-Z]+)(\d+)$/) {
			my ($tb, $nb) = ($1, $2);

			$d = $ta cmp $tb;
			return $d if $d;
			return $na <=> $nb;
		}
	}
	return $ra[1] cmp $rb[1];
}


sub output_list
{
	print sprintf("%-35s %-14s %-32s %-7s %-7s %-8s\n",
	    "# Package", "Part", "Value", "X(mm)", "Y(mm)", "X*Y(mm^2)");
	for (sort by_area_or_name @res) {
		my @r = @{ $_ };

		print "$r[2]\n";
	}
	print "area = ", $total_mm2, " mm^2\n";
}


&output_list unless $fig;
