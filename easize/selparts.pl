#!/usr/bin/perl

sub usage
{
	print STDERR "usage: $0 eagle-partlist selection\n";
	exit(1);
}

&usage unless $#ARGV == 1;

open(SEL, $ARGV[1]) || die "$ARGV[1]: $!";
while (<SEL>) {
	chop;
	s/#.*//;
	s/\s.*$//;
	next if /^$/;
	$sel{$_} = 1;
}
close SEL;

open(LIST, $ARGV[0]) || die "$ARGV[0]: $!";
while (<LIST>) {
	print;
	last if /^Part\s/;
}
while (<LIST>) {
	print unless /^\S+/ && !$sel{$&};
}
close LIST;
