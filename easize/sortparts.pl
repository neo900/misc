#!/usr/bin/perl
# Sort part numbers (component references) with names apnn by page, where
# a = one or more letters
# p = one or more page digits
# nn = component number, always two digits

while (<>) {
	if (/^([A-Z]+)(\d+)(\d\d)$/) {
		push(@s, [ $1, $2, $3 ]);
	} else {
		print;
	}
}


sub srt
{
	local ($a, $b) = @_;

	local ($aa, $ap, $an) = @{ $a };
	local ($ba, $bp, $bn) = @{ $b };

	return $ap <=> $bp if $ap != $bp;
	return $aa cmp $ba if $aa ne $ba;
	return $an <=> $bn;
}


for (sort { &srt($a, $b) } @s) {
	local ($a, $p, $n) = @{ $_ };

	print "$a$p$n\n";
}
