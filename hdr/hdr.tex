\documentclass[11pt]{article}
\usepackage{a4}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{wasysym}
\usepackage[hang,bottom]{footmisc}
\usepackage{titlesec}	% for \sectionbreak
\usepackage{parskip}
\usepackage{wasysym}	% for \diameter
\usepackage[numbib]{tocbibind}	% add references to PDF TOC
\usepackage[hidelinks,bookmarksnumbered=true,bookmarksopen]{hyperref}

\newcommand{\sectionbreak}{\clearpage}

\renewcommand{\footnotemargin}{1.2em}

\def\r#1{$\diameter\,\rm #1$}
\def\sq#1{$\Square\,\rm #1$}

\title{Header Part Selection Overview \\
 {\bf Interim version}}
\author{Werner Almesberger}
\date{December 1, 2016}

\def\TODO{{\bf TO DO}}
\def\todo#1{\begin{center}{\bf TO DO:} #1\hfill~\end{center}}

\def\iic{$\hbox{I}^2\hbox{C}$}

\newenvironment{tab}{\vskip4mm\qquad\begin{minipage}{430pt}}%
{\end{minipage}\vskip4mm\noindent}

\begin{document}
\phantomsection\pdfbookmark{Header Part Selection Overview}{firstpage}
\maketitle

This document lists various families of headers with 50 mil and 100 mil
pitch. The purpose of this overview is to compile the characteristics
of parts available on the market with sufficient detail to allow
the selection of suitable connectors for the Neo900 Hackerbus and
the BeagleBoard-xM interface of the Neo900 v2 prototype.

In order to limit this survey to a reasonable amount of possible items,
we only considered parts that were in stock at Digi-Key at the time of
the search.%
\footnote{Most of the searches were done in early 2015;
  female through-hole 1.27 mm single row parts were added in January 2016.}
We also exclude angled, shrouded, or blade connectors.

\def\Y{$\CIRCLE$}
\def\N{$\Circle$}
\def\P{$\LEFTcircle$}

This document currently only covers the part of the above search space
we consider of prime interest for Hackerbus and the v2 prototype:

\begin{tabular}{ll|cccc}
  Pitch	& Rows	& \multicolumn{2}{c}{Male} & \multicolumn{2}{c}{Female} \\
	&	& TH & SMT & TH & SMT \\
  \hline
  1.27 mm	& Single	& \Y & \N & \Y & \Y \\
  		& Dual		&\Y & \N & \N & \Y \\
  \hline
  2.54 mm	& Single	& \N & \N & \N & \N \\
  		& Dual		& \P & \N & \N & \N \\
\end{tabular}

(\Y~included in this document. \N~not included. \P~partially included.)

% -----------------------------------------------------------------------------

\section{Conventions}

The queries
that led to the respective parts are included in this document.
In queries, $\neg$ means negation and $\star$ is a wildcard.

Unless explicitly indicated otherwise, all dimensions
are in {\bf millimeters}. Please note that parameters in the Digi-Key
catalog are not necessarily identical with the ones shown in this
document, e.g., because of different interpretation of vendor documentation.

Pin types are indicated as \r{} (round pin) or \sq{} (square pin) and -- if
specified - the corresponding size or size range ({\em minimum}/{\em maximum}).
In parts that have protruding elements, the respective dimensions are
also indicated as {\em minimum}/{\em maximum}. Please note that all dimensions
are nominal values that do not include tolerances.

In part names, letters in italics indicate the following codes:

\begin{description}
  \item[\em c] Number of columns (dual-row only -- we always use {\em n}
    for single-row)
  \item[\em n] Number of pins
  \item[\em p] Plating / finish / termination code
  \item[\em x] Other characteristics of minor relevance
\end{description}

% -----------------------------------------------------------------------------

\section{Male, through-hole}

This is perhaps the best-known type of header. The following drawing
shows a cross-section with the key characteristics we examine:

\begin{center}
\includegraphics[scale=0.9]{th-male.pdf}
\end{center}

\begin{description}
  \item[A] Contact length above the mold. Note that the contacts are
    chamfered and one should therefore not expect them to make
    reliable contacts at their very end.
  \item[B] Height above the PCB of the plastic mold, including any 
    stilts or spacers. If the pin has a widened
    base that extends above the mold, the height of this base is
    considered to be part of {\bf B} as well.
  \item[C] Contact length below the mold. This part usually passes
    through the PCB and is soldered on the other side, but other
    arrangements are possible.
  \item[D] Width of the mold. For double-row headers, this is the
    width across both rows.
\end{description}

{\bf Query A:} Digi-Key category ``Connectors, Interconnects'', sub-category
``Rectangular - Board to Board Connectors - Headers, Male Pins'': 2\,047,
  of which 979 were in stock at the time of the query.

\begin{tabular}{ll|r}
  Parameter & Value & Parts \\
  \hline
  Mounting Type		& Through Hole				& 472 \\
  Connector Type	& $\neg$ Shrouded			& 349 \\
\end{tabular}

{\bf Query B:} ``Connectors, Interconnects'', sub-category
``Rectangular Connectors - Headers, Male Pins'': 110\,344,
of which 28\,676 were in stock.

\begin{tabular}{ll|r}
  Parameter & Value & Parts \\
  \hline
  Mounting Type		& Through Hole				& 16\,003 \\
  Connector Type	& $\neg$ Shrouded 			& 12\,290 \\
\end{tabular}

If the ``Part status'' option is available (2016), we filter the above
queries with

\begin{tabular}{ll|r}
  Parameter & Value \\
  \hline
  Part Status		& Active \\
\end{tabular}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Male through-hole: 1.27 mm pitch, single row}

Proceeding with query A (349 parts):
\nopagebreak

\begin{tabular}{ll|r}
  Parameter & Value & Parts \\
  \hline
  Pitch			& 1.27 mm	& 15 \\
\end{tabular}

All parts found are dual-row.

Proceeding with query B (12\,290 parts):
\nopagebreak

\begin{tabular}{ll|r}
  Parameter & Value & Parts \\
  \hline
  Pitch			& 1.27 mm	& 201 \\
  Number of Rows	& 1		& 56 \\
  Contact Type		& Male Pin	& 21 \\
\end{tabular}

This yields the following families:

% m-th-127-single
\begin{tabular}{llc|c|cccc}
  Manufacturer & Part name & Reference & Pin & A & B & C & D \\
  \hline
  Preci-Dip	& 850-{\em pp}-{\em nnn}-10-001101
	& \href{http://www.precidip.com/AppHost/9695,1/Scripts/Modules/Catalog/default.aspx?s=850-10-004-10-001101&i=520&p=171&pdf=1}{Catalog}
	& \r{0.41} & 3 & 2.2 & 2.9 & 2.2 \\
  Mill-Max	& 850-{\em pp}-{\em nnn}-10-001000
	& \href{https://www.mill-max.com/assets/pdfs/metric/034M.PDF}{Catalog}
	& \r{0.41} & 3.0 & 2.21 & 2.9 & 2.21 \\
  Sullins	& GRPB{\em nn}1VWVN-RC
	& \href{http://www.sullinscorp.com/drawings/71_GRPB___1VWVN-RC,_10957-C.pdf}{Drawing}
	& \sq{0.40} & 3.00 & 1.00 & 2.30 & 2.14 \\
\end{tabular}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Male through-hole: 1.27 mm pitch, dual row}

Proceeding with query A (349 parts):
\nopagebreak

\begin{tabular}{ll|r}
  Parameter & Value & Parts \\
  \hline
  Pitch			& 1.27 mm	& 14 \\
  Row Spacing		& 1.27 mm	& 10 \\
\end{tabular}

This yields the following families:
\nopagebreak

% m-th-127-dual
\begin{tabular}{llc|c|cccc}
  Manufacturer & Part name & Reference & Pin & A & B & C & D \\
  \hline
  Harwin	& M50-350{\em cc}{\em pp}
	& \href{http://harwin.com/includes/pdfs/M50-350.pdf}{Drawing}
	& \sq{0.40} & 3.00 & 1.00 & 2.30 & 3.40 \\
\end{tabular}

Proceeding with query B (12\,290  parts):
\nopagebreak

\begin{tabular}{ll|r}
  Parameter & Value & Parts \\
  \hline
  Pitch			& 1.27 mm		& 201 \\
  Number of Rows	& 2			& 145 \\
  Connector Type	& Header Unshrouded	& 137 \\
\end{tabular}

This yields the following families:
\nopagebreak

% m-th-127-dual
\begin{tabular}{llc|c|cccc}
  Manufacturer & Part name & Reference & Pin & A & B & C & D \\
  \hline
  FCI		& 20021111-000{\em nn}T{\em p}LF
	& \href{http://portal.fciconnect.com/Comergent//fci/drawing/20021111.pdf}{Drawing}
	& \sq{0.41} & 3.05 & 2.50 & 2.30 & 3.43 \\
  W\"urth	& 6220{\em nn}21121
	& \href{http://katalog.we-online.de/em/datasheet/6220xx21121.pdf}{Drawing}
	& \sq{0.4} & 3.8 & 1.5 & 2.3 & 3.4 \\
  Mill-Max	& 852-{\em pp}-{\em nnn}-10-001000
	& \href{https://www.mill-max.com/assets/pdfs/metric/034M.PDF}{Catalog}
	& \r{0.41} & 3.0 & 2.11 & 3.0 & 3.05 \\
  Preci-Dip	& 852-{\em pp}-{\em nnn}-10-001101
	& \href{http://www.precidip.com/AppHost/9695,1/Scripts/Modules/Catalog/default.aspx?s=852-10-024-10-001101&i=522&p=172&pdf=1}{Catalog}
	& \r{0.41} & 3 & 2.1 & 3 & 3.25 \\
  Samtec	& FTS-1{\em nn}-01-{\em p}-D
	& \href{http://www.samtec.com/documents/webfiles/cpdf/FTS-1XX-XX-XX-XX-XX-MKT.pdf}{Drawing}
	& \sq{0.41} & 3.05 & 0.86 & 2.29 & 3.43 \\
  Samtec	& FTS-1{\em nn}-03-{\em p}-D
	& idem
	& \sq{0.41} & 1.65 & 0.86 & 2.29 & 3.43 \\
  Sullins	& GRPB{\em nn}2VWVN-RC
	& \href{http://media.digikey.com/pdf/Data%20Sheets/Sullins%20PDFs/GRPB_%20_2VWVN-RC%2010954-C.pdf}{Drawing}
	& \sq{0.40} & 3.00 & 1.00 & 2.30 & 3.40 \\
\end{tabular}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

%% \subsection{Male through-hole: 2.54 mm pitch, single row}
%% 
%% Proceeding with query A (349 parts):
%% 
%% \begin{tabular}{ll|r}
%%   Parameter & Value & Parts \\
%%   \hline
%%   Pitch			& 2.54 mm	& 317 \\
%%   Number of Rows	& 1		& 253 \\
%% \end{tabular}
%% 
%% This yields the following families:
%% 
%% \begin{tabular}{llc|cccc}
%%   Manufacturer & Part name & Reference & A & B & C & D \\
%%   \hline
%%   Samtec		& BBL-1{\em nn}-{\em p}-E
%% 	& \href{http://www.samtec.com/ftppub/cpdf/bxx-mkt.pdf}{Drawing}
%% 	& 2.66 & 1.78 & 3.18 & 2.44 \\
%%   Samtec		& BBL-1{\em nn}-{\em p}-E
%% 	& \href{http://www.samtec.com/ftppub/cpdf/bxx-mkt.pdf}{Drawing}
%% 	& 3.09 & 1.78 & 3.18 & 2.44 \\
%%   Samtec		& BBS-1{\em nn}-{\em p}-A \\
%% 	& \href{http://www.samtec.com/ftppub/cpdf/bxx-mkt.pdf}{Drawing}
%% 	& 3.15 & 5.35 & 3.18 & 2.44 \\
%%   Samtec		& TS-1{\em nn}-{\em p}-A \\
%% 	& \href{http://cloud.samtec.com/catalog_english/TS.PDF}{Catalog}
%% 	& 4.22 & $\approx$ 3.1 & 3.96 & 2.54 \\
%% \end{tabular}
%% 
%% Proceeding with query B (12\,289 parts):
%% 
%% \begin{tabular}{ll|r}
%%   Parameter & Value & Parts \\
%%   \hline
%%   Pitch			& 2.54 mm	& 10\,691 \\
%%   Number of Rows	& 1		& 5\,551 \\
%%   Packaging		& $\neg$ Digi-Reel, $\neg$ TR & 5\,542 \\
%%   Number of Positions	& 40, 50, 64	& 124 \\
%%   Color			& Black		& 119 \\
%%   Termination		& Solder	& 111 \\
%%   Lead-Free		& Yes		& 99 \\
%%   RoHS-Compliant	& Yes		& 93 \\
%% \end{tabular}
%% 
%% This yields the following families:
%% 
%% \begin{tabular}{llc|cccc}
%%   Manufacturer & Part name & Reference & A & B & C & D \\
%%   \hline
%%   Mill-Max		& 350-{\em pp}-1{\em nn}-00-001000
%% 	& \href{http://www.mill-max.com/assets/pdfs/metric/084M.PDF}{Catalog}
%% 	& 4.22 & 3.93 & 3.96 & 2.54 \\
%%   Mill-Max		& 350-{\em pp}-1{\em nn}-00-006000
%% 	& \href{http://www.mill-max.com/assets/pdfs/metric/046M.PDF}{Catalog}
%% 	& 3.61 & 3.61 & 2.79 & 2.54 \\
%% \end{tabular}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Male through-hole: 2.54 mm pitch, dual row}

Proceeding with query A (258 parts):
\nopagebreak

\begin{tabular}{ll|r}
  Parameter & Value & Parts \\
  \hline
  Pitch			& 2.54 mm	& 218 \\
  Row Spacing		& 2.54 mm	& 62 \\
\end{tabular}

This yields the following family:
%\nopagebreak

% m-th-254-dual
\begin{tabular}{llc|c|cccc}
  Manufacturer & Part name & Reference & Pin & A & B & C & D \\
  \hline
  Samtec	& TD-1{\em nn}-{\em p}-A
	& \href{http://suddendocs.samtec.com/catalog_english/td.pdf}{Catalog}
	& \r{0.46} & 4.22 & 3.10 & 3.96 & 5.08 \\
\end{tabular}

Proceeding with query B (11\,179 parts):
\nopagebreak

\begin{tabular}{ll|r}
  Parameter & Value & Parts \\
  \hline
  Pitch			& 2.54 mm		& 9,434 \\
  Number of Rows	& 2			& 4,386 \\
\end{tabular}

Due to the large number of hits, we~--~somewhat arbitrarily~--~limit
the results to

\begin{tabular}{ll|r}
  Parameter & Value & Parts \\
  \hline
  Number of Positions	& $\le$ 20		& 3,335 \\
  Contact Mating Length & $\le$ 5.8 mm		& 317 \\
\end{tabular}

Note: most 2.54~mm headers have a contact length of 5.84~mm. Since this
is already too long for any chance of finding a compatible 1.27~mm pair,
we limit the search to shorter pins.

Furthermore, we consider only parts that are not a ``Value Added Item''
and exclude Preci-Dip (which generally seem to mirror Mill-Max).
This yields the following families:

%% m-th-254-dual
%\begin{tabular}{llc|c|cccc}
%  Manufacturer & Part name & Reference & Pin & A & B & C & D \\
%  \hline
%  Amphenol/FCI	& 67996-{\em p}{\em nn}HLF
%		& \href{http://portal.fciconnect.com/Comergent//fci/drawing/67996.pdf}{Drawing}
%		& \sq{0.64) & 5.84 & 2.54 & 2.41 & 4.83 \\
%  Amphenol/FCI	& 67997-{\em p}{\em nn}HLF
%		& \href{http://portal.fciconnect.com/Comergent//fci/drawing/67996.pdf}{Drawing}
%		& \sq{0.64) & 5.84 & 2.54 & 3.05 & 4.83 \\
%  Amphenol/FCI	& 68602-{\em p}{\em nn}HLF
%		& \href{http://portal.fciconnect.com/Comergent//fci/drawing/67996.pdf}{Drawing}
%		& \sq{0.64) & 5.84 & 2.54 & 3.81 & 4.83 \\
%  Amphenol/FCI	& 68691-{\em p}{\em nn}HLF
%		& \href{http://portal.fciconnect.com/Comergent//fci/drawing/67996.pdf}{Drawing}
%		& \sq{0.64) & 6.48 & 2.54 & 3.05 & 4.83 \\
%  Amphenol/FCI	& 68692-{\em p}{\em nn}HLF
%		& \href{http://portal.fciconnect.com/Comergent//fci/drawing/67996.pdf}{Drawing}
%		& \sq{0.64) & 5.84 & 2.54 & 2.54 & 4.83 \\
%  Amphenol/FCI	& 77313-{\em p}18-{\em nn}LF
%		& \href{http://portal.fciconnect.com/Comergent//fci/drawing/77313.pdf}{Drawing}
%		& \sq{0.62} & 5.84 & 2.54 & 3.05 & 4.83 \\

\begin{tabular}{llc|c|cccc}
  Manufacturer & Part name & Reference & Pin & A & B & C & D \\
  \hline
  3M		& 9612{\em nn}-6404-AR
	& \href{http://multimedia.3m.com/mws/media/548112O/3mtm-pin-strip-header-ts2181.pdf}{Drawing}
	& \sq{0.64} & 5.5 & 2.5 & 3.3 & 5.0 \\
  Harwin	& M20-976{\em nn}{\em pp}
	& \href{https://cdn.harwin.com/pdfs/M20-976.pdf}{Drawing}
	& \sq{0.64} & 5.80 & 2.54 & 3.1 & 5.08 \\
  Mill-Max	& 435-{\em pp}-2{\em nn}-00-160000
	& \href{https://www.mill-max.com/assets/pdfs/metric/053M.PDF}{Drawing}
	& \r{0.46} & 3.18 & 1.78 & 2.64 & 5.08 \\
  Mill-Max	& 802-{\em pp}-0{\em nn}-10-001000
	& \href{https://www.mill-max.com/assets/pdfs/metric/053M.PDF}{Drawing}
	& \r{0.61} & 4.95 & 3.18 & 3.0 & 5.08 \\
  Mill-Max	& 802-{\em pp}-0{\em nn}-10-002000
	& \href{https://www.mill-max.com/assets/pdfs/metric/055M.PDF}{Drawing}
	& \r{0.61} & 3.61 & 3.18 & 3.0 & 5.08 \\
  Mill-Max	& 802-{\em pp}-0{\em nn}-62-001000
	& \href{https://www.mill-max.com/assets/pdfs/metric/057M.PDF}{Drawing}
	& \r{0.76} & 4.85 & 2.79 & 3.65 & 5.08 \\
  Samtec	& TLW-1{\em cc}-05-{\em p}-D
	& \href{http://suddendocs.samtec.com/catalog_english/tlw_th.pdf}{Catalog}
	& \sq{0.64} &  2.67 & 1.52 & 4.32 & 5.03 \\
  Samtec	& TLW-1{\em cc}-06-{\em p}-D
	& \href{http://suddendocs.samtec.com/catalog_english/tlw_th.pdf}{Catalog}
	& \sq{0.64} &  2.67 & 1.52 & 3.43 & 5.03 \\
  Samtec	& TSW-1{\em cc}-05-{\em p}-D
	& \href{http://suddendocs.samtec.com/catalog_english/tsw_th.pdf}{Catalog}
	& \sq{0.64} &  2.67 & 2.54 & 3.30 & 5.08 \\
\end{tabular}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\section{Male, SMT}

The following drawing
shows a cross-section with the key characteristics we consider:

\begin{center}
\includegraphics[scale=0.9]{smt-male.pdf}
\end{center}

\begin{description}
  \item[A] Contact length above the mold.
  \item[B] Height above the PCB of the top of the plastic mold.
  \item[C] Height of the solder pads.
  \item[D] Width of the mold. For double-row headers, this is the
    width across both rows.
  \item[E] Width of the entire component, including pads.
\end{description}

\TODO

% -----------------------------------------------------------------------------

\section{Female, through-hole}

The following drawing
shows a cross-section with the key characteristics we consider:

\begin{center}
\includegraphics[scale=0.9]{th-female.pdf}
\end{center}

\begin{description}
  \item[A] Distance of contact point from the top of the component.
  \item[B] Height above the PCB of the top of the component.
  \item[C] Contact length below the mold.
  \item[D] Width of the mold. For double-row headers, this is the
    width across both rows.
  \item[Z] Maximum insertion depth. Omitted if pins can enter or exit at the
    bottom.
\end{description}

{\bf Query A:} Digi-Key category ``Connectors, Interconnects'', sub-category
``Rectangular - Board to Board Connectors - Headers, Receptacles,
  Female Sockets'': 7\,080, of which 2\,521 were in stock.

\begin{tabular}{ll|r}
  Parameter & Value & Parts \\
  \hline
  Mounting Type		& Through Hole				& 1\,735 \\
  Termination		& Solder				& 1\,628 \\
\end{tabular}

{\bf Query B:} ``Connectors, Interconnects'', sub-category
``Rectangular Connectors - Headers, Receptacles, Female Sockets'':
37\,044, of which 9\,342 were in stock.

\begin{tabular}{ll|r}
  Parameter & Value & Parts \\
  \hline
  Mounting Type		& Through Hole $\star$			& 6\,952 \\
  Mounting Type		& $\neg$ Right Angle			& 5\,217 \\
  Packaging		& $\neg$ TR				& 5\,193 \\
  Contact Type		& Female Socket, Forked			& 5\,187 \\
  Positions Loaded	& All					& 5\,185 \\
\end{tabular}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Female through-hole: 1.27 mm pitch, single row}

There are no single-row 1.27 mm pitch parts for query A.

Proceeding with query B (5\,185 parts):
\nopagebreak

\begin{tabular}{ll|r}
  Parameter & Value & Parts \\
  \hline
  Pitch                 & 1.27 mm       & 103 \\
  Number of Rows	& 1		& 31 \\
\end{tabular}

% f-th-127-single
\begin{tabular}{llc|c|cccccc}
  Manufacturer & Part name & Ref & Pin & A & B & C & D & Z \\
  \hline
  TE/AMP	& 5-104192-{\em b}
	& \href{http://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=104192&DocType=Customer+Drawing&DocLang=English}{Drw}
	& \sq{}	& ? & 8.89 & 2.54 & 2.49 & ? \\
  Mill-Max	& 851-{\em pp}-{\em nnn}-10-001000
	& \href{https://www.mill-max.com/assets/pdfs/metric/034M.PDF}{Cat}
	& \r{0.38/0.51} & 1.91 & 4.09 & 2.51 & 2.21 & 3.05 \\
  Preci-Dip	& 851-{\em pp}-{\em nnn}-10-001101
	& \href{http://www.precidip.com/AppHost/9696,1/Scripts/Modules/Catalog/Default.aspx?c=7&i=173&p=72&pdf=1&dsku=851-PP-NNN-10-001101}{Cat}
	& \r{0.38/0.50} & 2.5 & 4.1 & 2.3 & 2.2 & 3.45 \\
  Preci-Dip	& 851-{\em pp}-{\em nnn}-10-001101
	& \href{http://www.precidip.com/AppHost/9696,1/Scripts/Modules/Catalog/Default.aspx?c=7&i=181&p=76&pdf=1&dsku=851-PP-009-10-477101}{Cat}
	& \r{0.38/0.50} & 2.5 & 8.8 & 2.3 & 2.2 & 3.7 \\
  Sullins	& LPPB{\em nn}1{\em ppp}N-RC
	& \href{http://www.sullinscorp.com/drawings/72_LPPB___1NFFN-RC,_10955.pdf}{Drw}
	& \sq{0.4} & 2.05 & 4.50 & 3.00 & 2.20 & ? \\
\end{tabular}

Note: drawings of Preci-Dip parts are of extremely poor quality and
some dimensions may therefore be incorrect.

%%\subsection{Female through-hole: 1.27 mm pitch, doublerow}
%%
%%Proceeding with query A (1\,628 parts):
%%\nopagebreak
%%
%%\begin{tabular}{ll|r}
%%  Parameter & Value & Parts \\
%%  \hline
%%  Pitch                 & 1.27 mm       & 9 \\
%%\end{tabular}

% -----------------------------------------------------------------------------

\section{Female, SMT}

The following drawing
shows a cross-section with the key characteristics we consider:

\begin{center}
\includegraphics[scale=0.9]{smt-female.pdf}
\end{center}

\begin{description}
  \item[A] Distance of contact point from the top of the component.
  \item[B] Height above the PCB of the top of the component.
  \item[C] Height of the solder pads.
  \item[D] Width of the mold. For double-row headers, this is the
    width across both rows.
  \item[E] Width including pads. Note that E is sometimes smaller than D.
  \item[Z] Maximum insertion depth. Omitted if pins can enter or exit at the
    bottom.
\end{description}

{\bf Query A:} Digi-Key category ``Connectors, Interconnects'', sub-category
``Rectangular - Board to Board Connectors - Headers, Receptacles,
  Female Sockets'': 7\,107, of which 2\,528 were in stock.

\begin{tabular}{ll|r}
  Parameter & Value & Parts \\
  \hline
  Mounting Type		& Surface Mount				& 524 \\
  Packaging		& $\neg$ Digi-Reel, $\neg$ TR		& 481 \\
\end{tabular}

{\bf Query B:} ``Connectors, Interconnects'', sub-category
``Rectangular Connectors - Headers, Receptacles, Female Sockets'':
36\,198, of which 9\,429 were in stock.

\begin{tabular}{ll|r}
  Parameter & Value & Parts \\
  \hline
  Mounting Type		& Surface Mount;  Surface Mount, Through Board
								& 1\,868\\
  Packaging		& $\neg$ Digi-Reel, $\neg$ TR		& 1\,549 \\
  Contact Type		& Female Socket $\star$			& 1\,497 \\
\end{tabular}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Female SMT: 1.27 mm pitch, single row}

Proceeding with query A (481 parts):
\nopagebreak

\begin{tabular}{ll|r}
  Parameter & Value & Parts \\
  \hline
  Pitch			& 1.27 mm	& 221 \\
\end{tabular}

All parts found have two or more rows.

Proceeding with query B (1\,497 parts):
\nopagebreak

\begin{tabular}{ll|r}
  Parameter & Value & Parts \\
  \hline
  Pitch			& 1.27 mm	& 166 \\
  Number of Rows	& 1		& 16 \\
\end{tabular}

This yields the following families:
\nopagebreak

% f-smt-127-single
\begin{tabular}{llc|c|cccccc}
  Manuf. & Part name & Ref & Pin & A & B & C & D & E & Z \\
  \hline
  Mill-Max	& 399-{\em pp}-0{\em nn}-21-300000
	& \href{http://www.mill-max.com/assets/pdfs/metric/041M.PDF}{Cat}
	& \r{0.38/0.56} & 1.91 & 3.0 & 0.51 & 2.21 & 1.83 & 2.54 \\
  Mill-Max	& 851-{\em pp}-0{\em nn}-30-001000
	& \href{http://www.mill-max.com/assets/pdfs/metric/037M.PDF}{Cat}
	& \r{0.38/0.51} & 1.91 & 5.26 & ? & 2.21 & 3.02 & 3.05 \\
  Preci-Dip	& 851-{\em pp}-{\em nnn}-30-001191
	& \href{http://www.precidip.com/AppHost/9696,1/Scripts/Modules/Catalog/Default.aspx?c=7&i=345&p=123&pdf=1&dsku=851-PP-NNN-30-001101}{Cat}
	& \r{0.35/0.50} & ? & 5.15 & 0.44 & 2.2 & 3.4 & ? \\
  Preci-Dip	& 851-{\em pp}-{\em nnn}-30-001101
	& \href{http://www.precidip.com/AppHost/9696,1/Scripts/Modules/Catalog/Default.aspx?c=7&i=345&p=123&pdf=1&dsku=851-PP-NNN-30-001101}{Cat}
	&  \r{0.35/0.50} & 2.5 & 5.15 & 0.33 & 2.2 & 3.4 & 3.45 \\
  Sullins	& LPPB{\em nn}1NFS{\em x}-RC
	& \href{http://www.sullinscorp.com/drawings/86_LPPB___1NFS_-RC,_10965.pdf}{Drw}
	& \sq{0.40} & 2.05 & 4.65 & 0.15 & 2.20 & 3.10 & 2.05 \\
\end{tabular}

Note: documentation of the Preci-Dip 851-{\em pp}-{\em nnn}-30-001101
and -001191 is only partially readable.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Female SMT: 1.27 mm pitch, dual row}

Proceeding with query A (484 parts):
\nopagebreak

\begin{tabular}{ll|r}
  Parameter & Value & Parts \\
  \hline
  Pitch			& 1.27 mm	& 223 \\
  Number of Rows	& 2		& 211 \\
  Row spacing		& 1.27 mm	& 201 \\
  Features		& $\neg$ Board Lock	& 181 \\
  Color			& Black		& 173 \\
\end{tabular}

This yields the following families:
\nopagebreak

% f-smt-127-dual
\begin{tabular}{llc|c|cccccc}
  Manufacturer & Part name & Reference & Pin & A & B & C & D & E & Z \\
  \hline
  Samtec	& CLP-1{\em cc}-02-{\em p}-D 
	& \href{http://www.samtec.com/ftppub/cpdf/CLP-DMKT.PDF}{Drawing}
	& \sq{} & $\approx$0.75 & 2.28 & ? & 3.05 & 4.32 & -- \\
  Harwin	& M50-310{\em cc}{\em pp}
	& \href{http://harwin.com/includes/pdfs/M50-310.pdf}{Drawing}
	& \sq{} & 1.00 & 4.60 & 0.15 & 3.10 & 4.80 & ? \\
  Harwin	& M50-312{\em cc}{\em pp}
	& \href{http://harwin.com/includes/pdfs/M50-312.pdf}{Drawing}
	& \sq{} & 1.43 & 3.60 & 0.15 & 3.10 & 4.50 & ? \\
  Harwin	& M50-315{\em cc}{\em pp}
	& \href{http://harwin.com/includes/pdfs/M50-315.pdf}{Drawing}
	& ? & 1.20 & 2.20 & 0.15 & 3.40 & 4.50 & -- \\
  Harwin	& M50-430{\em cc}{\em pp}
	& \href{http://harwin.com/includes/pdfs/M50-430.pdf}{Drawing}
	& \sq{} & 1.09 & 4.60 & 0.20 & 3.10/3.60 & 4.80 & ? \\
\end{tabular}

Proceeding with query B (1\,496 parts):
\nopagebreak

\begin{tabular}{ll|r}
  Parameter & Value & Parts \\
  \hline
  Pitch			& 1.27 mm	& 166 \\
  Number of Rows	& 2		& 150 \\
  Features		& $\neg$ Board Lock	& 143 \\
\end{tabular}

This yields the following families:
\nopagebreak

% f-smt-127-dual
{\small
\begin{tabular}{llc|c|cccccc}
  Manuf. & Part name & Ref & Pin & A & B & C & D & E & Z \\
  \hline
  Harting	& 1521{\em nnn}{\em x}601{\em xxx}
	& \href{https://b2b.harting.com/files/download/PRD/PDF_TS/1521XXXX601_XXX_100227879DRW002B.PDF}{Drw}
	& \sq{} & ? & 6.25 & ? & ? & ? 7.4 & ? \\
  FCI		& 20021321-{\em x}0{\em x}{\em nn}{\em xx}LF
	& \href{http://portal.fciconnect.com/Comergent//fci/drawing/20021321.pdf}{Drw}
	& \sq{} & $\approx$1.2 & 4.50 & ? & 3.00 & 4.50 & ? \\
  Mill-Max	& 853-{\em pp}-{\em nnn}-30-001000
	& \href{http://www.mill-max.com/assets/pdfs/metric/037M.PDF}{Cat}
	& \r{0.38/0.51} & 1.91 & 5.26 & ? & 3.05 & 4.29 & 3.05 \\
  Samtec	& FLE-1{\em cc}-01-{\em p}-DV
	& \href{http://www.samtec.com/documents/webfiles/cpdf/FLE-1XX-XX-XX-DV-X-MKT1.pdf}{Drw}
	& \sq{} & $\approx$1.2 & 4.55 & ? & 3.33 & 4.59 & -- \\
  Sullins	& LPPB{\em cc}2NFS{\em x}-RC
	& \href{http://www.sullinscorp.com/drawings/88_LPPB___2NFSS-RC,_11017.pdf}{Drw}
	& \sq{0.40} & 2.05 & 4.65 & 0.15 & 3.10 & 4.50 & -- \\
  Sullins	& SFH31-NP{\em p}B-D{\em cc}-SP-{\em x}
	& \href{http://www.sullinscorp.com/drawings/152_SFH31-NP_B-D___-SP-___,_C11165.pdf}{Drw}
	& \sq{} & 2.00 & 4.40 & 0.15 & 3.10/3.60 & 4.50 & -- \\
  Samtec	& SFML-1{\em cc}-{\em x}2-{\em p}-D
	& \href{http://www.samtec.com/documents/webfiles/cpdf/SFML-1XX-XX-XXX-D-XXX-MKT.pdf}{Drw}
	& \sq{} & $\approx$2.6 & 4.70 & 0.13 & 3.05/3.68 & 4.19 & 4.06 \\
\end{tabular}}

Products taller than 9 mm (Harting) were excluded.

\end{document}
