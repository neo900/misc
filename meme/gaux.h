/*
 * gaux.h - Auxiliary graphics functions
 *
 * Written 2014, 2016 by Werner Almesberger
 * Copyright 2014, 2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef GAUX_H
#define GAUX_H

#include "SDL.h"


SDL_Surface *sdl_surface(Uint32 flags, int width, int height);
Uint32 get_pixel(const SDL_Surface *s, Sint16 x, Sint16 y, Uint32 null);

#endif /* !GAUX_H */
