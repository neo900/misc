#
# Example: slicing plane for n900-pcb-unstacked-bottom-50um.stl
#
# Setup:
#
# STL=../../scans/stl/n900-pcb-unstacked-bottom-50um.stl
# PLANE=plane.gp
#
# Generate the outlines:
#
# ./meme -s $PLANE $STL >out.gp
# ./meme -s $PLANE -n $STL >out2.gp
#
# To visualize:
#
# ./meme -o out.gp -o out2.gp $STL
#

# upper left
13.050       -55.800      1.525
12.800       -53.350      1.575

# upper right
98.850       -56.600      1.500
99.850       -51.400      1.550

# lower right
107.950      -13.300      1.725
105.550      -7.200       1.775
100.250      -9.500       1.750

# lower left
3.800        -12.500      2.125
11.750       -9.050       2.100
9.950        -2.850       2.175

# middle
45.200       -21.400      1.750
52.100       -18.200      1.775

# middle right
106.500      -41.000      1.650
104.400      -40.700      1.650

# global offset
-0.30

#
# Global offset:
#
# -0.2 produces large errors (edge is found well within the PCB area) in the
#   lower right corner
# -0.25 produces a few small errrors (a few small areas near the edge are
#   found to be below the surface) in the lower right corner
# -0.3 resulted in visual inspection revealing only one artefact
#
# Note that going from -0.25 to -0.3 moves the interpolated edge by about
# 20-30 um to the outside. If using the needle model, only insignificant
# changes occur.
#
#
