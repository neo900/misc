/*
 * stl.h - STL file reading
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef STL_H
#define	STL_H

#include <stdio.h>


void stl_load_file(FILE *file);
void stl_load(const char *name);

#endif /* !STL_H */
