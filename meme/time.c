/*
 * time.h - Timing of program execution
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stddef.h>
#include <sys/time.h>

#include "time.h"


void time_start(struct time *t)
{
	gettimeofday(&t->tv, NULL);
}


void time_stop(struct time *t)
{
	struct timeval now;

	gettimeofday(&now, NULL);
	t->tv.tv_sec -= now.tv_sec;
	t->tv.tv_usec -= now.tv_usec;
}


unsigned time_ms(const struct time *t)
{
	return -t->tv.tv_sec*1000 - t->tv.tv_usec/1000;
}
