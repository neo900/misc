/*
 * plane.c - Intersect mesh with planes
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdlib.h>
#include <stdio.h>

#include "mesh.h"
#include "plane.h"


#define	ALLOC	1024	/* must be power of two */


struct cut {
	int coord;
	struct point *res;
	unsigned n;
};


static void add_point(struct cut *cut, int x, int y)
{
	struct point *p;

	if (!cut->res)
		cut->res = calloc(ALLOC, sizeof(struct point));

	/* this tests if we're at a 2^n boundary */
	if (cut->n >= ALLOC && (cut->n & (cut->n - 1)) == 0)
		cut->res = realloc(cut->res, cut->n * 2 * sizeof(struct point));
	p = &cut->res[cut->n];
	p->x = x;
	p->y = y;
	cut->n++;
}


/* ----- XZ plane ---------------------------------------------------------- */


static bool cut_xz(const struct vertex *a, const struct vertex *b, void *user)
{
	struct cut *cut = user;
	int y = cut->coord;
	int dx, dy, dz;
	float f;;

	if ((a->y > y || b->y < y) && (a->y < y || b->y > y))
		return 0;

	if (a->y == b->y) {
		add_point(cut, (a->x + b->x) / 2, (a->z + b->z) / 2);
		return 0;
	}

	dx = a->x - b->x;
	dy = a->y - b->y;
	dz = a->z - b->z;
	f = (y - b->y)/(float) dy;
	add_point(cut, b->x + f * dx, b->z + f * dz);

	return 0;
}


static int comp_x(const void *a, const void *b)
{
	const struct point *pa = a;
	const struct point *pb = b;

	if (pa->x < pb->x)
		return -1;
	if (pa->x > pb->x)
		return 1;
	return 0;
}


struct point *plane_xz(int y, unsigned *n)
{
	struct cut cut = {
		.coord	= y,
		.res	= NULL,
		.n	 = 0,
	};

	edge_foreach(cut_xz, &cut);
	qsort(cut.res, cut.n, sizeof(struct point), comp_x);
	*n = cut.n-1;
	return cut.res;
}


/* ----- YZ plane ---------------------------------------------------------- */


static bool cut_yz(const struct vertex *a, const struct vertex *b, void *user)
{
	struct cut *cut = user;
	int x = cut->coord;
	int dx, dy, dz;
	float f;;

	if ((a->x > x || b->x < x) && (a->x < x || b->x > x))
		return 0;

	if (a->x == b->x) {
		add_point(cut, (a->z + b->z) / 2, (a->y + b->y) / 2);
		return 0;
	}

	dx = a->x - b->x;
	dy = a->y - b->y;
	dz = a->z - b->z;
	f = (x - b->x)/(float) dx;
	add_point(cut, b->z + f * dz, b->y + f * dy);

	return 0;
}


static int comp_y(const void *a, const void *b)
{
	const struct point *pa = a;
	const struct point *pb = b;

	if (pa->y < pb->y)
		return -1;
	if (pa->y > pb->y)
		return 1;
	return 0;
}


struct point *plane_yz(int x, unsigned *n)
{
	struct cut cut = {
		.coord	= x,
		.res	= NULL,
		.n	 = 0,
	};

	edge_foreach(cut_yz, &cut);
	qsort(cut.res, cut.n, sizeof(struct point), comp_y);
	*n = cut.n-1;
	return cut.res;
}
