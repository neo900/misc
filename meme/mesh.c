/*
 * mesh.c - 3D mesh data structures
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include "util.h"
#include "mesh.h"


#define	VHASH_TABLE	(1 << 20)
#define	EHASH_TABLE	(1 << 20)

#define	CHUNK	65536


struct v {
	struct vertex v;
	struct v *next;
};

struct e {
	struct edge e;
	struct e *next;
};


static struct v *vhash[VHASH_TABLE];
static struct e *ehash[EHASH_TABLE];


static struct edge *edges = NULL;
struct facet *facets = NULL;


#define	VHASH(x, y, z)	((x) % VHASH_TABLE)
#define	EHASH(a, b)	((long) (a) % EHASH_TABLE)


static int vused = 0;
static struct v *vchunk;

static int eused = 0;
static struct e *echunk;

static int fused = 0;
static struct facet *fchunk;


const struct vertex *vertex_add(int x, int y, int z)
{
	struct v **h = &vhash[VHASH(x, y, z)];
	struct v *v;

	for (v = *h; v; v = v->next)
		if (v->v.x == x && v->v.y == y && v->v.z == z)
			return &v->v;

	if (vused == CHUNK)
		vused= 0;
	if (!vused) {
		vchunk = calloc(CHUNK, sizeof(struct v));
	}
	v = &vchunk[vused++];
//	v = alloc_type(struct v);
	v->v.x = x;
	v->v.y = y;
	v->v.z = z;
	v->next = *h;
	*h = v;
	return &v->v;
}


void vertex_foreach(bool (*fn)(struct vertex *v, void *user), void *user)
{
	struct v *v;
	int i;

	for (i = 0; i != VHASH_TABLE; i++)
		for (v = vhash[i]; v; v = v->next)
			if (fn(&v->v, user))
				return;
}


void vertex_foreach_const(bool (*fn)(const struct vertex *v, void *user),
    void *user)
{
	const struct v *v;
	int i;

	for (i = 0; i != VHASH_TABLE; i++)
		for (v = vhash[i]; v; v = v->next)
			if (fn(&v->v, user))
				return;
}


void edge_foreach(bool (*fn)(const struct vertex *a, const struct vertex *b,
    void *user), void *user)
{
	const struct edge *e;

	for (e = edges; e; e = e->next)
		if (fn(e->a, e->b, user))
			return;
}


static void edge_add(const struct vertex *a, const struct vertex *b)
{
	const struct vertex *tmp;
	struct e **h;
	struct e *e;

	if (a > b) {
		tmp = a;
		a = b;
		b = tmp;
	}
	h = &ehash[EHASH(a, b)];
	for (e = *h; e; e = e->next)
		if (e->e.a == a && e->e.b == b)
			return;

	if (eused == CHUNK)
		eused = 0;
	if (!eused) {
		echunk = calloc(CHUNK, sizeof(struct e));
	}
	e = &echunk[eused++];
//	e = alloc_type(struct e);
	e->e.a = a;
	e->e.b = b;
	e->e.next = edges;
	edges = &e->e;
	e->next = *h;
	*h = e;
}


const struct facet *facet_add(const struct vertex *a, const struct vertex *b,
    const struct vertex *c)
{
	struct facet *f;

	if (fused == CHUNK)
		fused= 0;
	if (!fused) {
		fchunk = calloc(CHUNK, sizeof(struct facet));
	}
	f = &fchunk[fused++];
//	f = alloc_type(struct facet);
	f->v[0] = a;
	f->v[1] = b;
	f->v[2] = c;

	edge_add(a, b);
	edge_add(b, c);
	edge_add(c, a);

	f->next = facets;
	facets = f;
	return f;
}


void mesh_init(void)
{
}
