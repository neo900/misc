/*
 * overlay.h - Overlay in Gnuplot format
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef OVERLAY_H
#define	OVERLAY_H

struct path;

struct overlay {
	const char *name;
	void (*xform)(float x, float y, float *res_x, float *res_y);
	struct path *paths;
};


void overlay_draw(const struct overlay *overlay,
    void (*moveto)(void *user, int x, int y),
    void (*lineto)(void *user, int x, int y), void *user);

void overlay_reload(struct overlay *overlay);

void overlay_load(struct overlay *overlay, const char *name,
    void (*xform)(float x, float y, float *res_x, float *res_y));

#endif /* !OVERLAY_H */
