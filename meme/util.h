/*
 * util.h - Common utility functions
 *
 * Written 2006, 2009, 2010, 2014 by Werner Almesberger
 * Copyright 2006, 2009, 2010, 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef UTIL_H
#define UTIL_H

#include <stddef.h>
#include <stdlib.h>


#define alloc_size(s)					\
    ({	void *alloc_size_tmp = malloc(s);		\
	if (!alloc_size_tmp)				\
		abort();				\
	alloc_size_tmp; })

#define alloc_type(t) ((t *) alloc_size(sizeof(t)))


#define container(ptr, type, member) \
        ((type *) ((void *) (ptr) - offsetof(type, member)))

#endif /* !UTIL_H */
