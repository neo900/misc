/*
 * gui.c - User interface
 *
 * Written 2014-2016 by Werner Almesberger
 * Copyright 2014-2016 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#include "SDL.h"
#include "SDL_gfxPrimitives.h"

#include "util.h"
#include "time.h"
#include "mesh.h"
#include "overlay.h"
#include "marker.h"
#include "plane.h"
#include "gaux.h"
#include "screen.h"
#include "text.h"
#include "gui.h"


#define	XRES		640
#define	YRES		480

#define	SDL_SURFACE	SDL_HWSURFACE

#define	CURSOR_R	5

#define	MARK_WIDTH	2
#define	MARK_LEN	20

#define	EDGE_RGBA		0x000000ff
#define	CURSOR_RGBA		0xff202080
#define	PROFILE_RGBA		0x2020ffff
#define	MARK_A_RGBA		0x00ff00ff
#define	MARK_B_RGBA		0x3030ffff
#define	CONTRAST_RGBA		0xffe080ff
#define	EQUAL_RGBA		0xf080ffff
#define	OVERLAY_MIX_RGBA	0xffc01060
#define	OVERLAY_ALONE_RGBA	0xffc010ff
#define	MARKER_LINE_RGBA	0x00ff00ff

#define	DEPTH_RANGE	0xc0
#define	DEPTH_OFFSET	0x30


struct marker marker;

static int x_orig, y_orig;
static Sint16 xres, yres;
static int x_min, x_max, y_min, y_max, z_min, z_max;
static int zoom;
static bool show_grid = 0;
static bool show_profiles = 0;
static bool show_overlay = 0;
static bool show_mesh = 1;
static const struct vertex *cursor;
static const struct overlay *overlay = NULL;


/* ----- Coordinate transform ---------------------------------------------- */


static inline int x_model2screen(int x)
{
	return ((x-x_orig) >> zoom)+(xres >> 1);
}


static inline int y_model2screen(int y)
{
	return ((y-y_orig) >> zoom)+(yres >> 1);
}


static inline int x_screen2model(int x)
{
	return ((x - (xres >> 1)) << zoom)+x_orig;
}


static inline int y_screen2model(int y)
{
	return ((y - (yres >> 1)) << zoom)+y_orig;
}


static int coord(int x, int y, Sint16 *res_x, Sint16 *res_y)
{
	x = x_model2screen(x);
	y = y_model2screen(y);

	if (x < -32768 || x > 32767)
		return -1;
	if (y < -32768 || y > 32767)
		return -1;

	*res_x = x;
	*res_y = y;

	return x >= 0 && x < xres && y >= 0 && y < yres;
}


static inline int xz_z_model2screen(int z)
{
	/* center at cursor in lower 1/4 of screen */
	return yres-(yres >> 3) - ((z-cursor->z) >> zoom);
}


static int coord_xz(int x, int y, Sint16 *res_x, Sint16 *res_y, Sint16 ymin)
{
	x = x_model2screen(x);
	y = xz_z_model2screen(y);

	if (x < -32768 || x > 32767)
		return -1;
	if (y < -32768 || y > 32767)
		return -1;

	*res_x = x;
	*res_y = y;

	return x >= 0 && x < xres && y >= ymin && y < yres;
}


static inline int yz_z_model2screen(int z)
{
	/* center at cursor in right 1/4 of screen */
	return xres-(xres >> 3) - ((z-cursor->z) >> zoom);
}


static int coord_yz(int x, int y, Sint16 *res_x, Sint16 *res_y, Sint16 xmin)
{
	x = yz_z_model2screen(x);
	y = y_model2screen(y);

	if (x < -32768 || x > 32767)
		return -1;
	if (y < -32768 || y > 32767)
		return -1;

	*res_x = x;
	*res_y = y;

	return x >= xmin && x < xres && y >= 0 && y < yres;
}


/* ----- Depth gradient ---------------------------------------------------- */


static Uint32 *default_z_grad, *zoom_z_grad;
static const Uint32 *z_grad;


static void init_z_grad(void)
{
	int i;
	int div;
	uint8_t col;

	default_z_grad = calloc(3*(z_max-z_min)+1, sizeof(Uint32));
	div = 3*(z_max-z_min);
	for (i = 0; i <= 3*(z_max-z_min); i++) {
		col = i*DEPTH_RANGE/div + DEPTH_OFFSET;
		default_z_grad[i] = col * 0x1010100 + 0xff;
	}
	z_grad = default_z_grad;
}


static void calc_z_grad(int center, unsigned c_zoom)
{
	unsigned i;
	int col;
	uint8_t mid = DEPTH_RANGE >> 1;

fprintf(stderr, "center %d zoom %u\n", center, c_zoom);
	if (!zoom_z_grad)
		zoom_z_grad = calloc(3*(z_max-z_min)+1, sizeof(Uint32));
	for (i = 0; i <= 3*(z_max-z_min); i++) {
		col = ((int) i-center*3)/(1 << c_zoom)+mid;
		if (col < 0 || col >= DEPTH_RANGE)
			zoom_z_grad[i] = CONTRAST_RGBA;
		else
			zoom_z_grad[i] = (col+DEPTH_OFFSET) * 0x1010100 + 0xff;
	}
	z_grad = zoom_z_grad;
}


static void reset_z_grad(void)
{
	z_grad = default_z_grad;
}


/* ----- Contrast adjustment ----------------------------------------------- */


static bool contrast_mode = 0;
static int contrast_center;
static unsigned contrast_zoom = 1;


static void contrast_change(bool lower)
{
	if (cursor)
		contrast_center = cursor->z - z_min;
	if (lower) {
		/* limit zoom to 1/12 of the Z range */
		if (1 << contrast_zoom > (z_max-z_min)/4)
			return;
		contrast_zoom++;
	} else {
		if (contrast_zoom == 1)
			return;
		contrast_zoom--;
	}
	calc_z_grad(contrast_center, contrast_zoom);
}


static void begin_contrast(void)
{
	contrast_mode = 1;
	if (cursor)
		contrast_center = cursor->z - z_min;
	contrast_zoom = 1;
	while (DEPTH_RANGE << contrast_zoom < 3*(z_max-z_min))
		contrast_zoom++;
	calc_z_grad(contrast_center, contrast_zoom);
}


static void end_contrast(bool modifier)
{
	contrast_mode = 0;
	if (!modifier)
		reset_z_grad();
}


/* ----- Equal height ------------------------------------------------------ */


static int equal = INT_MAX;


static void begin_equal(void)
{
	if (cursor)
		equal = cursor->z;
}


static void end_equal(void)
{
	equal = INT_MAX;
}


#include <assert.h>


/*
 * Draw line between sides AC and BC
 */

static void equal_line(SDL_Surface *s,
    const struct vertex *a, const struct vertex *b, const struct vertex *c,
    Sint16 xa, Sint16 ya, Sint16 xb, Sint16 yb, Sint16 xc, Sint16 yc)
{
	float fa, fb;

assert(a->z != c->z);
assert(b->z != c->z);
	fa = (float) (equal - c->z) / (a->z - c->z);
	fb = (float) (equal - c->z) / (b->z - c->z);

	aalineColor(s, xc+(xa-xc)*fa, yc+(ya-yc)*fa,
	    xc+(xb-xc)*fb, yc+(yb-yc)*fb, EQUAL_RGBA);
}


static inline bool between(int v, int a, int b)
{
	return (a <= v && b >= v) || (b <= v && a >= v);
}


/*
 * This duplicates a lot of things from surface_draw, but there doesn't seem
 * to be an easy way around that problem.
 */

static void equal_draw(SDL_Surface *s)
{
	const struct facet *f;
	Sint16 xa, ya, xb, yb, xc, yc;
	int a, b, c;
	bool ab, ac, bc;
	
	for (f = facets; f; f = f->next) {
		a = coord(f->v[0]->x, f->v[0]->y, &xa, &ya);
		if (a < 0)
			continue;
		b = coord(f->v[1]->x, f->v[1]->y, &xb, &yb);
		if (b < 0)
			continue;
		c = coord(f->v[2]->x, f->v[2]->y, &xc, &yc);
		if (c < 0)
			continue;
		if (!(a || b || c))
			continue;

		ab = f->v[0]->z == equal && f->v[1]->z == equal;
		ac = f->v[0]->z == equal && f->v[2]->z == equal;
		bc = f->v[1]->z == equal && f->v[2]->z == equal;

		if (ab && ac) {
			filledTrigonColor(s, xa, ya, xb, yb, xc, yc,
			    EQUAL_RGBA);
			continue;
		}
		if (ab) {
			aalineColor(s, xa, ya, xb, yb, EQUAL_RGBA);
			continue;
		}
		if (ac) {
			aalineColor(s, xa, ya, xc, yc, EQUAL_RGBA);
			continue;
		}
		if (bc) {
			aalineColor(s, xb, yb, xc, yc, EQUAL_RGBA);
			continue;
		}

		ab = between(equal, f->v[0]->z, f->v[1]->z);
		ac = between(equal, f->v[0]->z, f->v[2]->z);
		bc = between(equal, f->v[1]->z, f->v[2]->z);
		if (!(ab || ac || bc))
			continue;
		if (ac && bc) {
			equal_line(s, f->v[0], f->v[1], f->v[2],
			    xa, ya, xb, yb, xc, yc);
			continue;
		}
		if (ab && ac) {
			equal_line(s, f->v[1], f->v[2], f->v[0],
			    xb, yb, xc, yc, xa, ya);
			continue;
		}
		if (ab && bc) {
			equal_line(s, f->v[0], f->v[2], f->v[1],
			    xa, ya, xc, yc, xb, yb);
			continue;
		}
abort();
	}
}


/* ----- Draw the mesh ----------------------------------------------------- */


static void surface_draw(SDL_Surface *s)
{
	const struct facet *f;
	Sint16 xa, ya, xb, yb, xc, yc;
	int a, b, c;
	int z_off = z_min*3;
	
	for (f = facets; f; f = f->next) {
		a = coord(f->v[0]->x, f->v[0]->y, &xa, &ya);
		if (a < 0)
			continue;
		b = coord(f->v[1]->x, f->v[1]->y, &xb, &yb);
		if (b < 0)
			continue;
		c = coord(f->v[2]->x, f->v[2]->y, &xc, &yc);
		if (c < 0)
			continue;
		if (!(a || b || c))
			continue;
		filledTrigonColor(s, xa, ya, xb, yb, xc, yc,
		    z_grad[f->v[0]->z + f->v[1]->z + f->v[2]->z - z_off]);
	}
}


static bool edge_draw(const struct vertex *a, const struct vertex *b,
    void *user)
{
	SDL_Surface *s = user;
	Sint16 xa, ya, xb, yb;
	int res_a, res_b;

	res_a = coord(a->x, a->y, &xa, &ya);
	if (res_a < 0)
		return 0;
	res_b = coord(b->x, b->y, &xb, &yb);
	if (res_b < 0)
		return 0;
	if (!(res_a || res_b))
		return 0;
	aalineColor(s, xa, ya, xb, yb, EDGE_RGBA);
	return 0;
}


static void edges_draw(SDL_Surface *s)
{
	edge_foreach(edge_draw, s);
}


static void draw(SDL_Surface *s)
{
	SDL_FillRect(s, NULL, SDL_MapRGB(s->format, 0xff, 0xff, 0xff));
	if (show_mesh)
		surface_draw(s);
	if (show_grid)
		edges_draw(s);
	if (equal != INT_MAX)
		equal_draw(s);
}


/* ----- Backing store ----------------------------------------------------- */


static struct change {
	SDL_Surface *s;
	SDL_Rect r;
	struct change *prev;
	struct change *next;
} changes = {
	.prev = &changes,
	.next = &changes,
};


static void change(SDL_Surface *s, int x, int y, int w, int h)
{
	struct change *c;

	if (x < 0) {
		w += x;
		x = 0;
	}
	if (y < 0) {
		h += y;
		y = 0;
	}
	if (x+w >= xres)
		w = xres-x-1;
	if (y+h >= yres)
		h = yres-y-1;

	if (h <= 0 || w <= 0)
		return;

	c = alloc_type(struct change);
	c->s = sdl_surface(SDL_SURFACE, w, h);
	if (!c->s) {
		fprintf(stderr, "sdl_surface: %s\n", SDL_GetError());
		exit(1);
	}

	c->r.x = x;
	c->r.y = y;
	c->r.w = w;
	c->r.h = h;
	if (SDL_BlitSurface(s, &c->r, c->s, NULL)) {
		fprintf(stderr, "SDL_BlitSurface: %s\n", SDL_GetError());
		exit(1);
	}

	/* append */
	c->next = &changes;
	c->prev = changes.prev;
	changes.prev->next = c;
	changes.prev = c;
}


static void apply_changes(SDL_Surface *s)
{
	const struct change *c;

	for (c = changes.next; c != &changes; c = c->next)
		SDL_UpdateRect(s, c->r.x, c->r.y, c->r.w, c->r.h);
}


static void no_changes(void)
{
	changes.next = &changes;
	changes.prev = &changes;
}


static void rollback_changes(SDL_Surface *s)
{
	struct change *c, *prev;

	for (c = changes.prev; c != &changes; c = prev) {
		prev = c->prev;
		if (SDL_BlitSurface(c->s, NULL, s, &c->r)) {
			fprintf(stderr, "SDL_BlitSurface: %s\n",
			    SDL_GetError());
			exit(1);
		}
		SDL_FreeSurface(c->s);
		SDL_UpdateRect(s, c->r.x, c->r.y, c->r.w, c->r.h);
		free(c);
	}
	no_changes();
}


static void discard_changes(void)
{
	struct change *c, *next;

	for (c = changes.next; c != &changes; c = next) {
		next = c->next;
		SDL_FreeSurface(c->s);
		free(c);
	}
	no_changes();
}


/* ----- Cursor handling --------------------------------------------------- */


static void hide_cursor(SDL_Surface *s)
{
	rollback_changes(s);
}


static void show_cursor(SDL_Surface *s, const struct vertex *v)
{
	int x = x_model2screen(v->x);
	int y = y_model2screen(v->y);

	change(s, x-CURSOR_R, y-CURSOR_R, 2*CURSOR_R+1, 2*CURSOR_R+1);
	change(s, 0, y, xres-1, 1);
	change(s, x, 0, 1, yres-1);
	SDL_LockSurface(s);
	aalineColor(s, 0, y, xres-1, y, CURSOR_RGBA);
	aalineColor(s, x, 0, x, yres-1, CURSOR_RGBA);
	filledCircleColor(s, x, y, CURSOR_R, CURSOR_RGBA);
	SDL_UnlockSurface(s);
}


/* ----- Profiles ---------------------------------------------------------- */


static void show_xz_profile(SDL_Surface *s)
{
	struct point *xz;
	unsigned n, i;
	Sint16 ymin;
	Sint16 xa = 0, ya = 0, xb = 0, yb = 0;
		/* initialize because gcc seems to "see" that they can be left
		   uninitialized by coord_xz but then fails to notice that
		   we track this via the return value. */
	int a, b;

	xz = plane_xz(cursor->y, &n);

	ymin = yres - (yres >> 2)-1;
	a = coord_xz(xz[0].x, xz[0].y, &xa, &ya, ymin);
	change(s, 0, ymin, xres, yres >> 2);
	SDL_LockSurface(s);
	for (i = 1; i < n; i++) {
		b = coord_xz(xz[i].x, xz[i].y, &xb, &yb, ymin);
		if (a >= 0 && b >= 0 && (a || b))
			aalineColor(s, xa, ya, xb, yb, PROFILE_RGBA);
		xa = xb;
		ya = yb;
		a = b;
	}
	aalineColor(s, 0, yres - (yres >> 3), xres-1, yres - (yres >> 3),
	    PROFILE_RGBA);
	SDL_UnlockSurface(s);

	free(xz);
}


static void show_yz_profile(SDL_Surface *s)
{
	struct point *yz;
	unsigned n, i;
	Sint16 xmin;
	Sint16 xa = 0, ya = 0, xb = 0, yb = 0;
		/* initialize because gcc seems to "see" that they can be left
		   uninitialized by coord_yz but then fails to notice that
		   we track this via the return value. */
	int a, b;

	yz = plane_yz(cursor->x, &n);

	xmin =  xres - (xres >> 2)-1;
	a = coord_yz(yz[0].x, yz[0].y, &xa, &ya, xmin);
	change(s, xmin, 0, xres >> 2, yres);
	SDL_LockSurface(s);
	for (i = 1; i < n; i++) {
		b = coord_yz(yz[i].x, yz[i].y, &xb, &yb, xmin);
		if (a >= 0 && b >= 0 && (a || b))
			aalineColor(s, xa, ya, xb, yb, PROFILE_RGBA);
		xa = xb;
		ya = yb;
		a = b;
	}
	aalineColor(s, xres - (xres >> 3), 0, xres - (xres >> 3), yres-1,
	    PROFILE_RGBA);
	SDL_UnlockSurface(s);

	free(yz);
}


/* ----- Find nearest vertex ----------------------------------------------- */


struct proximity {
	int x, y;
	const struct vertex *best;
	float best_d;
};


#if 0
static bool proximity_fn(const struct vertex *v, void *user)
{
	struct proximity *p = user;
	float d = hypotf(v->x - p->x, v->y - p->y);

	if (!p->best || d < p->best_d) {
		p->best_d = d;
		p->best = v;
	}
	return 0;
}

#else


static bool proximity_fn(const struct vertex *v, void *user)
{
	struct proximity *p = user;
	float d;

	if (p->best &&
	    (abs(v->x - p->x) >= p->best_d || abs(v->y - p->y) >= p->best_d))
		return 0;

	d = hypotf(v->x - p->x, v->y - p->y);
	if (p->best && d >= p->best_d)
		return 0;

	p->best_d = d;
	p->best = v;
	return 0;
}
#endif


static const struct vertex *find_nearest(int x, int y)
{
	struct proximity p = {
		.x	= x,
		.y	= y,
		.best	= NULL,
	};
//	struct time t;

//	time_start(&t);
	vertex_foreach_const(proximity_fn, &p);
//	time_stop(&t);
//	fprintf(stderr, "%.3f\t", time_ms(&t)/1000.0);
	return p.best;
}


/* ----- Add text change --------------------------------------------------- */


#include <stdarg.h>


static void text_change(SDL_Surface *s, int x, int y, const char *fmt, ...)
{
	SDL_Surface *t;
	SDL_Rect r;
	va_list ap;

	va_start(ap, fmt);
	t = vtextf(fmt, ap);
	va_end(ap);

	change(s, x, y, t->w, t->h);
	r.x = x;
	r.y = y;
	r.w = t->w;
	r.h = t->h;
	if (SDL_BlitSurface(t, NULL, s, &r)) {
		fprintf(stderr, "SDL_BlitSurface: %s\n", SDL_GetError());
		exit(1);
	}
	SDL_FreeSurface(t);
}


/* ----- Markers ----------------------------------------------------------- */


static const struct vertex *mark_a, *mark_b;
static bool curr_mark;


static void show_marker(SDL_Surface *s, const struct vertex *v, Uint32 color)
{
	int x = x_model2screen(v->x);
	int y = y_model2screen(v->y);

	change(s, x-MARK_LEN, y-MARK_WIDTH, 2*MARK_LEN+1, 2*MARK_WIDTH+1);
	change(s, x-MARK_WIDTH, y-MARK_LEN, 2*MARK_WIDTH+1, 2*MARK_LEN+1);
	thickLineColor(s, x-MARK_LEN, y, x+MARK_LEN, y, MARK_WIDTH, color);
	thickLineColor(s, x, y-MARK_LEN, x, y+MARK_LEN, MARK_WIDTH, color);
}


static void show_meas(SDL_Surface *s, int x, int y,
    const struct vertex *a, const struct vertex *b,
    Uint32 color)
{
	float dx, dy, dz;

	dx = abs(a->x - b->x) / 1000.0;
	dy = abs(a->y - b->y) / 1000.0;
	dz = abs(a->z - b->z) / 1000.0;

#define	LS 12

	text_color(color);
	if (dx) {
		text_change(s, x, y, "X  %7.3f", dx);
		y += LS;
	}
	if (dy) {
		text_change(s, x, y, "Y  %7.3f", dy);
		y += LS;
	}
	if (dz) {
		text_change(s, x, y, "Z  %7.3f", dz);
		y += LS;
	}
	if (dx && dy) {
		text_change(s, x, y, "XY %7.3f", hypotf(dx, dy));
		y += LS;
	}
	if (dx && dz) {
		text_change(s, x, y, "XZ %7.3f", hypotf(dx, dz));
		y += LS;
	}
	if (dy && dz) {
		text_change(s, x, y, "YZ %7.3f", hypotf(dy, dz));
		y += LS;
	}
	if (dx && dy && dz) {
		text_change(s, x, y, "3D %7.3f", hypotf(hypotf(dx, dy), dz));
		y += LS;
	}
}


static void show_markers(SDL_Surface *s)
{
	if (mark_a)
		show_marker(s, mark_a, MARK_A_RGBA);
	if (mark_b)
		show_marker(s, mark_b, MARK_B_RGBA);
	if (mark_a && mark_b)
		show_meas(s, 10, 10, mark_a, mark_b, 0x000000ff);
}


static bool select_marker(const struct vertex *v)
{
	if (v == mark_a) {
		mark_a = NULL;
		curr_mark = 0;
		return 1;
	}
	if (v == mark_b) {
		mark_b = NULL;
		curr_mark = 1;
		return 1;
	}
	if (!mark_a && !mark_b) {
		curr_mark = 0;
		return 0;
	}
	if (mark_a && mark_b)
		return 0;
	if (mark_a)
		curr_mark = 1;
	else
		curr_mark = 0;
	return 0;
}


static void print_marker(FILE *file)
{
	const struct vertex *mark;
	double dx, dy, dz;
	char name;

	if (curr_mark) {
		mark = mark_b;
		name = 'B';
	} else {
		mark = mark_a;
		name = 'A';
	}
	fprintf(file, "%c:\tX  %.3f\tY  %.3f\tZ  %.3f\n", name,
	    mark->x / 1000.0, mark->y / 1000.0, mark->z / 1000.0);
	if (!mark_a || !mark_b)
		return;

	dx = abs(mark_a->x - mark_b->x) / 1000.0;
	dy = abs(mark_a->y - mark_b->y) / 1000.0;
	dz = abs(mark_a->z - mark_b->z) / 1000.0;

	fprintf(file, "D:\tX  %.3f\tY  %.3f\tZ  %.3f\n", dx, dy, dz);
	fprintf(file, "  \tXY %.3f\tXZ %.3f\tYZ %.3f\n",
	    hypot(dx, dy), hypot(dx, dz), hypot(dy, dz));
	fprintf(file, "   \tXYZ %.3f\n", hypot(hypot(dx, dy), dz));
}


static void log_marker(const char *logfile)
{
	FILE *file;

	file = fopen(logfile, "a");
	if (!file) {
		perror(logfile);
		return;
	}
	print_marker(file);
	if (fclose(file) == EOF)
		perror(logfile);
}


static void set_marker(const struct vertex *v, const char *logfile)
{
	if (select_marker(v))
		return;

	if (curr_mark)
		mark_b = cursor;
	else
		mark_a = cursor;

	print_marker(stderr);
	if (logfile)
		log_marker(logfile);
}


static void clear_markers(void)
{
	mark_a = mark_b = NULL;
}


static void marker_next(const char *logfile)
{
	mark_a = mark_b ? mark_b : cursor;
	mark_b = 0;
	curr_mark = 0;

	print_marker(stderr);
	if (logfile)
		log_marker(logfile);
}


/* ----- Overlay ----------------------------------------------------------- */


static Sint16 ovl_x, ovl_y;
static int ovl_ok;


static void moveto(void *user, int x, int y)
{
	ovl_ok = coord(x, y, &ovl_x, &ovl_y);
}


static void lineto(void *user, int x, int y)
{
	SDL_Surface *s = user;
	Sint16 to_x, to_y;
	int ok;

	ok = coord(x, y, &to_x, &to_y);
	if (ovl_ok >= 0 && ok >= 0 && (ovl_ok || ok))
		aalineColor(s, ovl_x, ovl_y, to_x, to_y,
		    show_mesh ? OVERLAY_MIX_RGBA : OVERLAY_ALONE_RGBA);
#if 0
	/*
	 * Something's wrong with thickLineColor. It places the lines with a
	 * substantial offset from the correct position, or even makes them
	 * veer off the screen.
	 */
		thickLineColor(s, ovl_x, ovl_y, to_x, to_y,
		    OVERLAY_WIDTH, OVERLAY_RGBA);
#endif
	ovl_ok = ok;
	if (ok >= 0) {
		ovl_x = to_x;
		ovl_y = to_y;
	}
}


static void draw_overlay(SDL_Surface *s, const struct overlay *over)
{
	if (over)
		overlay_draw(over, moveto, lineto, s);
}


/* ----- Markers ----------------------------------------------------------- */


#define TEE_MARK	3	/* half the mark length */

#define	TEXT_OFFSET_X	-5
#define	TEXT_OFFSET_Y	-7
#define	TEXT_DIST	10


static void marker_line(void *user, int xa, int ya, int xb, int yb,
    float nx, float ny)
{
	SDL_Surface *s = user;
	Sint16 ax, ay, bx, by;
	int ok_a, ok_b;

	ok_a = coord(xa, ya, &ax, &ay);
	ok_b = coord(xb, yb, &bx, &by);
	if (ok_a < 0 || ok_b < 0)
		return;
	if (!ok_a && !ok_b)
		return;
	aalineColor(s, ax - TEE_MARK * nx, ay - TEE_MARK * ny,
	    ax + TEE_MARK * nx, ay + TEE_MARK * ny, MARKER_LINE_RGBA);
	aalineColor(s, bx - TEE_MARK * nx, by - TEE_MARK * ny,
	    bx + TEE_MARK * nx, by + TEE_MARK * ny, MARKER_LINE_RGBA);
	aalineColor(s, ax, ay, bx, by, MARKER_LINE_RGBA);
}


#define	TEXT_BG_RGBA		0xffffffff
#define	TEXT_BORDER_RGBA	0xffffffc0
#define	MASK_GB			0x00ffff00	/* works with RGBA and ABGR */


static void text_with_outline(SDL_Surface *s, const SDL_Surface *t,
    Sint16 x, Sint16 y)
{
	Uint16 ix, iy;
	int8_t dx, dy;
	Uint32 col;
	uint8_t v, v2;

	for (iy = 0; iy != t->h; iy++)
		for (ix = 0; ix != t->w; ix++) {
			col = get_pixel(t, ix, iy, TEXT_BG_RGBA);
			if ((col ^ TEXT_BG_RGBA) & MASK_GB) {
				v = col >> 16;
				v2 = (255 + v) >> 1;
				pixelRGBA(s, x + ix, y + iy, v, v2, v, 255);
				continue;
			}
			col = TEXT_BG_RGBA;
			for (dy = -1; dy <= 1; dy++)
				for (dx = -1; dx <= 1; dx++)
					if (dx || dy)
						col &= get_pixel(t,
						    ix + dx, iy + dy,
						    TEXT_BG_RGBA);
			if ((col ^ TEXT_BG_RGBA) & MASK_GB)
				pixelColor(s, x + ix, y + iy, TEXT_BORDER_RGBA);
		}
}


static void marker_text(void *user, int x, int y, float nx, float ny,
    const char *txt)
{
	SDL_Surface *s = user;
	Sint16 xx, yy;
	SDL_Surface *t;

	if (coord(x, y, &xx, &yy) <= 0)
		return;

	t = text(txt);
	text_with_outline(s, t, xx + TEXT_OFFSET_X + TEXT_DIST * nx,
	    yy + TEXT_OFFSET_Y + TEXT_DIST * ny);
        SDL_FreeSurface(t);
}


static void draw_marker(SDL_Surface *s)
{
	marker_draw(&marker, marker_line, marker_text, s);
}


/* ----- Last mouse position  ---------------------------------------------- */


static int mouse_x, mouse_y;


static void update_mouse_pos(Uint16 x, Uint16 y)
{
	mouse_x = x_screen2model(x);
	mouse_y = y_screen2model(y);
}


static void init_mouse_pos(void)
{
	mouse_x = (x_min+x_max)/2;
	mouse_y = (y_min+y_max)/2;
}


/* ----- Scaling ----------------------------------------------------------- */


static void auto_center(void)
{
	x_orig = (x_min + x_max) >> 1;
	y_orig = (y_min + y_max) >> 1;
}


static void auto_zoom(bool full)
{
	float f = full ? 1 : 0.9;

	for (zoom = 0; (x_max - x_min) >> zoom > xres * f ||
	    (y_max - y_min) >> zoom > yres * f; zoom++);
}


static void auto_scale(bool full)
{
	auto_center();
	auto_zoom(full);
}


static void zoom_in(int x, int y)
{
	if (contrast_mode) {
		contrast_change(0);
		return;
	}
	if (zoom == 1)
		return;
	zoom--;
	x_orig = (x_orig+x)/2;
	y_orig = (y_orig+y)/2;
}


static void zoom_out(int x, int y)
{
	if (contrast_mode) {
		contrast_change(1);
		return;
	}
	if (1 << (zoom+1) > x_max-x_min && 1 << (zoom+1) > y_max-y_min)
		return;
	zoom++;
	x_orig = 2*x_orig-x;
	y_orig = 2*y_orig-y;
}


/* ----- Event handling ---------------------------------------------------- */


static void motion_event(SDL_Surface *s, SDL_MouseMotionEvent *motion)
{
	int x = x_screen2model(motion->x);
	int y = y_screen2model(motion->y);

	if (cursor)
		hide_cursor(s);
	cursor = find_nearest(x, y);
	show_cursor(s, cursor);
	if (show_profiles) {
		show_xz_profile(s);
		show_yz_profile(s);
	}
	apply_changes(s);
}


static void button_event(SDL_MouseButtonEvent *button, const char *logfile)
{
	switch (button->button) {
	case 1:
		if (!cursor)
			break;
		set_marker(cursor, logfile);
		break;
	case 2:
		x_orig = mouse_x;
		y_orig = mouse_y;
		break;
	case 3:
		clear_markers();
		break;
	case 4: /* wheel forward */
		zoom_in(mouse_x, mouse_y);
		break;
	case 5: /* wheel backward */
		zoom_out(mouse_x, mouse_y);
		break;
	}
}


/* ----- Determine model extrema ------------------------------------------- */


static bool extrema_vertex(const struct vertex *v, void *user)
{
	bool *first = user;

	if (*first) {
		x_min = x_max = v->x;
		y_min = y_max = v->y;
		z_min = z_max = v->z;
		*first = 0;
	} else {
		if (x_min > v->x)
			x_min = v->x;
		if (x_max < v->x)
			x_max = v->x;
		if (y_min > v->y)
			y_min = v->y;
		if (y_max < v->y)
			y_max = v->y;
		if (z_min > v->z)
			z_min = v->z;
		if (z_max < v->z)
			z_max = v->z;
	}
	return 0;
}


static void extrema(void)
{
	bool first = 1;

	vertex_foreach_const(extrema_vertex, &first);
}


/* ----- Timers ------------------------------------------------------------ */


#include <time.h>
#include <sys/time.h>


static struct timer {
	struct timeval expires;
	struct timer *next;
	void (*fn)(void *user);
	void *user;
} *timers = NULL;


static void cancel_timer(const struct timer *t)
{
	struct timer **anchor;

	for (anchor = &timers; *anchor; anchor = &(*anchor)->next)
		if (*anchor == t) {
			*anchor = t->next;
			return;
		}
}


static int timeval_cmp(const struct timeval *a, const struct timeval *b)
{
	if (a->tv_sec < b->tv_sec)
		return -1;
	if (a->tv_sec > b->tv_sec)
		return 1;
	if (a->tv_usec < b->tv_usec)
		return -1;
	if (a->tv_usec > b->tv_usec)
		return 1;
	return 0;
}


static void start_timer(struct timer *t,
    void (*fn)(void *user), void *user, unsigned delta_ms)
{
	struct timer **anchor;

	cancel_timer(t);
	gettimeofday(&t->expires, NULL);

	t->expires.tv_usec += 1000*delta_ms;
	t->expires.tv_sec -= t->expires.tv_usec/1000000;
	t->expires.tv_usec %= 1000000;

	t->fn = fn;
	t->user = user;

	for (anchor = &timers;
	    *anchor && timeval_cmp(&t->expires, &(*anchor)->expires) >= 0;
	    anchor = &(*anchor)->next);
	t->next = *anchor;
	*anchor = t;
}


static void run_timers(void)
{
	struct timeval now;
	struct timer *t;

	while (timers) {
		gettimeofday(&now, NULL);
		if (timeval_cmp(&now, &timers->expires) < 0)
			return;
		t = timers;
		timers = t->next;
		t->fn(t->user);
	}
}


/* ----- Event polling ----------------------------------------------------- */


static void sdl_event(SDL_Event *ev)
{
	struct timespec ts = {
		.tv_sec = 0,
		.tv_nsec = 20*1000*1000, // 20 ms
	};

	while (1) {
		if (!timers) {
			SDL_WaitEvent(ev);
			return;
		}

		if (SDL_PollEvent(ev))
			return;
		run_timers();
		nanosleep(&ts, NULL);
	}
}


/* ----- Main loop --------------------------------------------------------- */


static struct motion {
	SDL_Surface *s;
	SDL_MouseMotionEvent event;
} motion;

static struct timer motion_timer;


static void delayed_motion(void *user)
{
	struct motion *m = user;

	motion_event(m->s, &m->event);
}


static bool event_loop(SDL_Surface **surf, const char *logfile,
    struct overlay overlays[], unsigned n_overlays)
{
	SDL_Surface *s = *surf;
	SDL_Event event;
	unsigned i;

	cancel_timer(&motion_timer);
	while (1) {
		sdl_event(&event);

		switch (event.type) {
		case SDL_MOUSEMOTION:
			update_mouse_pos(event.motion.x, event.motion.y);
			if (cursor)
				hide_cursor(s);
			show_markers(s);
			cursor = NULL;
			motion.s = s;
			motion.event = event.motion;
			start_timer(&motion_timer, delayed_motion, &motion,
			    50);
			break;
		case SDL_MOUSEBUTTONDOWN:
			update_mouse_pos(event.button.x, event.button.y);
			button_event(&event.button, logfile);
			return 0;
		case SDL_VIDEORESIZE:
			xres = event.resize.w;
			yres = event.resize.h;
			*surf = SDL_SetVideoMode(xres, yres, 0,
			    SDL_SURFACE | SDL_RESIZABLE);
			return 0;
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym) {
			case SDLK_c:
				begin_contrast();
				return 0;
			case SDLK_d:
				dump_screen(*surf);
				break;
			case SDLK_e:
				begin_equal();
				return 0;
			case SDLK_g:
				show_grid = !show_grid;
				return 0;
			case SDLK_m:
				show_mesh = !show_mesh;
			case SDLK_n:
				marker_next(logfile);
				return 0;
			case SDLK_o:
				show_overlay = !show_overlay;
				return 0;
			case SDLK_p:
				show_profiles = !show_profiles;
				return 0;
			case SDLK_q:
				return 1;
			case SDLK_r:
				for (i = 0; i != n_overlays; i++)
					overlay_reload(overlays + i);
				marker_reload(&marker);
				return 0;
			case SDLK_1:
				if (n_overlays)
					overlay = overlays + 0;
				return 0;
			case SDLK_2:
				if (n_overlays > 1)
					overlay = overlays + 1;
				return 0;
			case SDLK_3:
				if (n_overlays > 2)
					overlay = overlays + 2;
				return 0;
			default:
				break;
			}
			if (event.key.keysym.unicode & 0xFF80)
				break;
			switch (event.key.keysym.unicode) {
			case '*':
				auto_scale(false);
				return 0;
			case '+':
				zoom_in(mouse_x, mouse_y);
				return 0;
			case '-':
				zoom_out(mouse_x, mouse_y);
				return 0;
			}
			break;
		case SDL_KEYUP:
			switch (event.key.keysym.sym) {
			case SDLK_c:
				end_contrast(event.key.keysym.mod & KMOD_SHIFT);
				return 0;
			case SDLK_e:
				end_equal();
				return 0;
			default:
				break;
			}
			break;
		case SDL_QUIT:
			return 1;
		}
	}
	return 0;
}


void gui_dump(struct overlay overlays[], unsigned n_overlays,
    const struct dump_params *prm)
{
	SDL_Surface *surf;

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "SDL_init: %s\n", SDL_GetError());
		exit(1);
	}
	atexit(SDL_Quit);

	surf  = sdl_surface(SDL_SWSURFACE, prm->xres, prm->yres);

	xres = surf->w;
	yres = surf->h;

	extrema();
//	init_mouse_pos();
	init_z_grad();

	if (prm->enhance) {
		begin_contrast();
		calc_z_grad(prm->enh_z - z_min, contrast_zoom - prm->enh_scale);
	}
	if (prm->middle) {
		x_orig = prm->mx;
		y_orig = prm->my;
	} else {
		auto_center();
	}

	if (!prm->zoom_abs)
		auto_zoom(true);
	if (prm->zoom_abs)
		zoom = prm->zoom;
	if (prm->zoom_rel)
		zoom -= prm->zoom;

	SDL_LockSurface(surf);
	draw(surf);
	draw_overlay(surf, n_overlays ? overlays : NULL);
	draw_marker(surf);
	SDL_UnlockSurface(surf);
	SDL_UpdateRect(surf, 0, 0, 0, 0);

	if (prm->cursor) {
		cursor = find_nearest(prm->cx, prm->cy);
		if (cursor) {
			if (prm->profile) {
				show_xz_profile(surf);
				show_yz_profile(surf);
			}
			show_cursor(surf, cursor);
			apply_changes(surf);
		}
	}

	dump_screen(surf);
}


void gui(const char *logfile, struct overlay overlays[], unsigned n_overlays)
{
	SDL_Surface *surf;

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "SDL_init: %s\n", SDL_GetError());
		exit(1);
	}
	atexit(SDL_Quit);

	surf = SDL_SetVideoMode(XRES, YRES, 0, SDL_SURFACE | SDL_RESIZABLE);
	if (!surf) {
		fprintf(stderr, "SDL_SetVideoMode: %s\n", SDL_GetError());
		exit(1);
	}

	xres = surf->w;
	yres = surf->h;

	SDL_EnableUNICODE(1);

	extrema();
	init_mouse_pos();
	init_z_grad();
	auto_scale(false);

	if (n_overlays)
		overlay = overlays;

	while (1) {
struct time t;
		discard_changes();
		SDL_LockSurface(surf);
time_start(&t);
		draw(surf);
time_stop(&t);
fprintf(stderr, "%.3f s @ zoom %d\n", time_ms(&t)/1000.0, zoom);
		if (show_overlay) {
			draw_overlay(surf, overlay);
			draw_marker(surf);
		}
		SDL_UnlockSurface(surf);
		SDL_UpdateRect(surf, 0, 0, 0, 0);

		show_markers(surf);
		if (cursor) {
			if (show_profiles) {
				show_xz_profile(surf);
				show_yz_profile(surf);
			}
			show_cursor(surf, cursor);
//			apply_changes(surf);
		}
		apply_changes(surf);

		if (event_loop(&surf, logfile, overlays, n_overlays))
			break;
	}
}
