/*
 * overlay.c - Overlay in Gnuplot format
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "util.h"
#include "overlay.h"


struct point {
	float x, y;
	struct point *next;
};

struct path {
	struct point *points;
	struct path *next;
};


void overlay_draw(const struct overlay *overlay,
    void (*moveto)(void *user, int x, int y),
    void (*lineto)(void *user, int x, int y), void *user)
{
	const struct path *path;
	const struct point *p;

	for (path = overlay->paths; path; path = path->next) {
		p = path->points;
		moveto(user, p->x, p->y);
		for (p = p->next; p; p = p->next)
			lineto(user, p->x, p->y);
	}
}


static void overlay_load_file(struct overlay *overlay, FILE *file,
    void (*xform)(float x, float y, float *res_x, float *res_y))
{
	struct path *path = NULL;
	struct point *p;
	int lineno = 0;
	char buf[1024];
	int n;
	double x, y, z;

	overlay->paths = NULL;
	while (fgets(buf, sizeof(buf), file)) {
		lineno++;
		if (*buf == '#')
			continue;
		n = sscanf(buf, "%lf %lf %lf\n", &x, &y, &z);
		switch (n) {
		case -1:
			path = NULL;
			continue;
		case 2:
			/* fall through */
		case 3:
			break;
		default:
			fprintf(stderr, "invalid data at line %d\n", lineno);
			exit(1);
		}

		if (!path) {
			path = alloc_type(struct path);
			path->next = overlay->paths;
			path->points = NULL;
			overlay->paths = path;
		}
		p = alloc_type(struct point);
		if (xform) {
			xform(x, y, &p->x, &p->y);
		} else {
			p->x = x;
			p->y = y;
		}
		p->next = path->points;
		path->points = p;
	}
}


static void overlay_do_load(struct overlay *overlay)
{
	FILE *file;

	file = fopen(overlay->name, "r");
	if (!file) {
		perror(overlay->name);
		exit(1);
	}
	overlay_load_file(overlay, file, overlay->xform);
	fclose(file);
}


static void free_points(struct path *path)
{
	struct point *p, *next;

	for (p = path->points; p; p = next) {
		next = p->next;
		free(p);
	}
	path->points = NULL;
}


static void free_paths(struct overlay *overlay)
{
	struct path *path, *next;

	for (path = overlay->paths; path; path = next) {
		next = path->next;
		free_points(path);
		free(path);
	}
	overlay->paths = NULL;
}


void overlay_reload(struct overlay *overlay)
{
	free_paths(overlay);
	overlay_do_load(overlay);
}


void overlay_load(struct overlay *overlay, const char *name,
    void (*xform)(float x, float y, float *res_x, float *res_y))
{
	overlay->name = strdup(name);
	if (!overlay->name) {
		perror("strdup");
		exit(1);
	}
	overlay->xform = xform;
	overlay_do_load(overlay);
}
