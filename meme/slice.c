/*
 * slice.c - Intersect the mesh with a plane
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_multifit.h>

#include "util.h"
#include "mesh.h"
#include "slice.h"


#define	EPSILON		1e-6
#define	MAX_WARN	100


struct point {
	double x, y, z;
	struct point *next;
};

static struct point *points = NULL;


/* ----- Slicing plane ----------------------------------------------------- */


/* z(x, y) = cx * x + cy * y + z0 */

static double cx, cy, z0;


static void slice_plane(void)
{
	const struct point *p;
	unsigned n = 0;
	gsl_multifit_robust_workspace *work;
	gsl_vector *y, *c;
	gsl_matrix *x, *cov;

	for (p = points; p; p = p->next)
		n++;

	if (n < 3) {
		cx = cy = 0;
		z0 = points->z;
		return;
	}

	x = gsl_matrix_alloc(n, 3);
	y = gsl_vector_alloc(n);
	n = 0;
	for (p = points; p; p = p->next) {
		gsl_vector_set(y, n, p->z);
		gsl_matrix_set(x, n, 0, p->x);
		gsl_matrix_set(x, n, 1, p->y);
		gsl_matrix_set(x, n, 2, 1);
		n++;
	}

	c = gsl_vector_alloc(3);
	cov = gsl_matrix_alloc(3, 3);

	work = gsl_multifit_robust_alloc(gsl_multifit_robust_default, n, 3);
	gsl_multifit_robust(x, y, c, cov, work);
	gsl_multifit_robust_free(work);

	cx = gsl_vector_get(c, 0);
	cy = gsl_vector_get(c, 1);
	z0 = gsl_vector_get(c, 2);

	gsl_matrix_free(x);
	gsl_vector_free(y);
	gsl_vector_free(c);

	fprintf(stderr, "plane: z(x, y) = %g * x + %g * y + %g\n",
	    cx, cy, z0 / 1000);
}


/* ----- Slice the mesh ---------------------------------------------------- */


static bool above(const struct vertex *v)
{
	return v->z > z0 + cx * v->x + cy * v->y;
}


/* --- Interpolation --- */


static bool inter_solve(const struct vertex *va, const struct vertex *vb,
    double *res)
{
	double dx, dy, dz;
	double m, a;

	dx = vb->x - va->x;
	dy = vb->y - va->y;
	dz = vb->z - va->z;

	/*
	 * We solve this equation for t:
	 *
	 * x(t) = t * dx + va->x;
	 * y(t) = t * dy + va->y;
	 * z(t) = t * dz + va->z;
	 * cx * x(t) + cy * y(t) + z0 = z(t)
	 *
	 * Calculate m and a such that
	 * m * t + a = 0
	 */

	m = cx * dx + cy * dy - dz;
	a = cx * va->x + cy * va->y - va->z + z0;

	if (fabs(m) < EPSILON)
		return 0;

	*res = -a / m;
	return 1;
}


static void inter_point(void (*fn)(double x, double y, double z, bool last),
    const struct vertex *va, const struct vertex *vb, double t, bool last)
{
	double dx, dy, dz;

	dx = vb->x - va->x;
	dy = vb->y - va->y;
	dz = vb->z - va->z;

	fn((va->x + t * dx) / 1000, (va->y + t * dy) / 1000,
	    (va->z + t * dz) / 1000, last);
}


/* --- Needle step --- */

/*
 * Known bug: we don't take into account horizontal needle deflection.
 * E.g., when slightly beyond a vertical drop, the needle may bend as
 * it plunges, thus reporting a lower height than a perfectly rigid
 * needle would.
 *
 * If this happens, e may become larger than d and we simply go back to
 * using the last point before the edge.
 */


#define	NEEDLE_TIP_R	40	/* 40 um */
#define	NEEDLE_SIDE	14	/* dy = 14 * dx */


static double step_calc(const struct vertex *va, const struct vertex *vb)
{
	static unsigned warnings = 0;
	double dx, dy, dz;
	double d, e;

	if (va->z > vb->z)
		return 1 - step_calc(vb, va);

	dx = vb->x - va->x;
	dy = vb->y - va->y;
	dz = vb->z - va->z;

	d = hypot(dx, dy);

	if (dz < NEEDLE_TIP_R) {
		/*
		 * From:
		 * e^2 + (r - z)^2 = r^2
		 */
		e = sqrt(2 * NEEDLE_TIP_R * dz - dz * dz);
	} else {
		e = (dz - NEEDLE_TIP_R) / NEEDLE_SIDE + NEEDLE_TIP_R;
	}

	if (e > d) {
		if (warnings < MAX_WARN) {
			fprintf(stderr, "e > d: %f, %f; dz = %f\n", e, d, dz);
			warnings++;
		} else if (warnings == MAX_WARN) {
			fprintf(stderr, "...\n");
			warnings++;
		}
		return 1;
	} else {
		return fabs(d) < EPSILON ? 0 : e / d;
	}
}


static void step_point(void (*fn)(double x, double y, double z, bool last),
    const struct vertex *va, const struct vertex *vb, double t, bool last)
{
	double dx, dy;

	dx = vb->x - va->x;
	dy = vb->y - va->y;

	fn((va->x + t * dx) / 1000, (va->y + t * dy) / 1000, vb->z / 1000,
	    last);
}


static void segment(void (*fn)(double x, double y, double z, bool last),
    const struct vertex *va, const struct vertex *vb, const struct vertex *vc,
    bool needle)
{
	double tb, tc;

	if (needle) {
		tb = step_calc(va, vb);
		tc = step_calc(va, vc);
		if (tb > EPSILON || tc > EPSILON) {
			step_point(fn, va, vb, tb, 0);
			step_point(fn, va, vc, tc, 1);
		}
	} else {
		if (inter_solve(va, vb, &tb) && inter_solve(va, vc, &tc)) {
			inter_point(fn, va, vb, tb, 0);
			inter_point(fn, va, vc, tc, 1);
		}
	}
}


void slice_run(void (*fn)(double x, double y, double z, bool last), bool needle)
{
	const struct facet *f;
	bool a, b, c;

	slice_plane();
	for (f = facets; f; f = f->next) {
		a = above(f->v[0]);
		b = above(f->v[1]);
		c = above(f->v[2]);
		if (a && b && c)
			continue;
		if (!a && !b && !c)
			continue;
		if ((b && c) || (!b && !c))
			segment(fn, f->v[0], f->v[1], f->v[2], needle);
		if ((a && c) || (!a && !c))
			segment(fn, f->v[1], f->v[0], f->v[2], needle);
		if ((a && b) || (!a && !b))
			segment(fn, f->v[2], f->v[0], f->v[1], needle);
	}
}


/* ----- Points that define the slicing plane ------------------------------ */


static void slice_load_file(FILE *file)
{
	struct point *p;
	int lineno = 0;
	char buf[1024];
	int n;
	double x, y, z, zoff;
	double zglobal = 0;

	p = alloc_type(struct point);
	while (fgets(buf, sizeof(buf), file)) {
		lineno++;
		if (*buf == '#')
			continue;
		n = sscanf(buf, "%lf %lf %lf %lf\n", &x, &y, &z, &zoff);
		switch (n) {
		case -1:
			continue;
		case 1:
			zglobal = x * 1000;
			continue;
		case 4:
			z += zoff;
			/* fall through */
		case 3:
			break;
		default:
			fprintf(stderr, "invalid data at line %d\n", lineno);
			exit(1);
		}

		p->x = x * 1000;
		p->y = y * 1000;
		p->z = z * 1000;
		p->next = points;
		points = p;
		p = alloc_type(struct point);
	}
	free(p);

	for (p = points; p; p = p->next)
		p->z += zglobal;
}


void slice_load(const char *name)
{
	FILE *file;

	file = fopen(name, "r");
	if (!file) {
		perror(name);
		exit(1);
	}
	slice_load_file(file);
	fclose(file);
}
