/*
 * slice.h - Intersect the mesh with a plane
 *
 * Written 2015 by Werner Almesberger
 * Copyright 2015 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef SLICE_H
#define	SLICE_H

#include <stdbool.h>


void slice_run(void (*fn)(double x, double y, double z, bool last),
    bool needle);
void slice_load(const char *name);

#endif /* !SLICE_H */
