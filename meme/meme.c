/*
 * meme.c - Measmesh command-line processing and setup
 *
 * Written 2014-2016 by Werner Almesberger
 * Copyright 2014-2016 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>

#include "time.h"
#include "mesh.h"
#include "stl.h"
#include "screen.h"
#include "overlay.h"
#include "marker.h"
#include "slice.h"
#include "gui.h"


#define	N_OVERLAYS	3


static bool count_edge(const struct vertex *a, const struct vertex *b,
    void *user)
{
	unsigned *count = user;

	(*count)++;
	return 0;
}


static unsigned count_edges(void)
{
	unsigned count = 0;

	edge_foreach(count_edge, &count);
	return count;
}


static void slice_out(double x, double y, double z, bool last)
{
	printf("%f %f %f\n%s", x, y, z, last ? "\n" : "");
}


static void overlay_xform(float x, float y, float *res_x, float *res_y)
{
	*res_x = round(x * 1000);
	*res_y = round(y * 1000);
}


static void usage(const char *name)
{
	fprintf(stderr,
"usage: %s [-d dump.bmp] [-l logfile] [-m marker.gp] [-o overlay.gp ...]\n"
"          [-D xres yres [-C cx cy] [-E z zoom] [-M mx my] [-P] [-Z zoom]]\n"
"          [file.stl]\n"
"       %s -s plane.gp [-n] [file.stl]\n\n"
"  -d dump.bmp\n"
"       write screen dump to specified file (default: meme-NNNN.bmp)\n"
"  -l logfile\n"
"       write measurement data to the specified file\n"
"  -m marker.gp\n"
"       show markers specified in the file\n"
"  -n   when slicing, try to compensate for the scanning needle geometry\n"
"  -o overlay.gp\n"
"       display the specified overlays. The option can be repeated.\n"
"  -s   slice the mesh with a plane. This is non-interactive.\n\n"
"  -D xres yres\n"
"       non-interactively dump a screenshot of mesh and adornments\n"
"  -C cx cy\n"
"       show the cursor at the specified position\n"
"  -E z zoom\n"
"       enhance contrast around Z by the specified zoom factor\n"
"  -M mx my\n"
"       set the middle of the screen to the specified position\n"
"  -P   show profiles\n"
"  -Z zoom\n"
"        set the specified zoom level. If prefixed with - or +, zoom out or\n"
"        in from default zoom\n"
	    , name, name);
	exit(1);
}


int main(int argc, char **argv)
{
	int c;
	const char *logfile = NULL;
	const char *slice = NULL;
	bool needle = 0;
	struct overlay overlays[N_OVERLAYS];
	unsigned n_overlays = 0;
	struct time t_load;
	int args = 0;
	bool headless_dump = 0;
	int dump_cursor = 0, dump_middle = 0, dump_enhance = 0, dump_zoom = 0;

	struct dump_params prm = {
		.middle = 0,
		.zoom_abs = 0,
		.zoom_rel = 0,
		.cursor = 0,
		.enhance = 0,
		.profile = 0,
	};

	while ((c = getopt(argc, argv, "d:l:m:no:s:DCEMPZ")) != EOF)
		switch (c) {
		case 'd':
			screen_dump_name = optarg;
			break;
		case 'l':
			logfile = optarg;
			break;
		case 'm':
			marker_load(&marker, optarg, overlay_xform);
			break;
		case 'n':
			needle = 1;
			break;
		case 'o':
			if (n_overlays == N_OVERLAYS) {
				fprintf(stderr, "too many overlays\n");
				exit(1);
			}
			overlay_load(overlays + n_overlays, optarg,
			    overlay_xform);
			n_overlays++;
			break;
		case 's':
			slice = optarg;
			break;
		case 'D':
			headless_dump = 1;
			args += 2;
			break;
		case 'C':
			if (!headless_dump)
				usage(*argv);
			prm.cursor = 1;
			dump_cursor = args;
			args += 2;
			break;
		case 'E':
			if (!headless_dump)
				usage(*argv);
			prm.enhance = 1;
			dump_enhance = args;
			args += 2;
			break;
		case 'M':
			if (!headless_dump)
				usage(*argv);
			prm.middle = 1;
			dump_middle = args;
			args += 2;
			break;
		case 'P':
			if (!headless_dump)
				usage(*argv);
			prm.profile = 1;
			break;
		case 'Z':
			if (!headless_dump)
				usage(*argv);
			dump_zoom = args;
			args++;
			break;
		default:
			usage(*argv);
		}

	if (headless_dump) {
		if (argc - optind < args)
			usage(*argv);
		prm.xres = atoi(argv[optind]);
		prm.yres = atoi(argv[optind + 1]);
		if (prm.cursor)
			overlay_xform(atof(argv[optind + dump_cursor]),
			    atof(argv[optind + dump_cursor + 1]),
			    &prm.cx, &prm.cy);
		if (prm.enhance) {
			prm.enh_z = atof(argv[optind + dump_enhance]) * 1000;
			prm.enh_scale = atoi(argv[optind + dump_enhance + 1]);
		}
		if (prm.middle)
			overlay_xform(atof(argv[optind + dump_middle]),
			    atof(argv[optind + dump_middle + 1]),
			    &prm.mx, &prm.my);
		if (dump_zoom) {
			prm.zoom = atof(argv[optind + dump_zoom]);
			if (strchr("+-", *argv[optind + dump_zoom]))
				prm.zoom_rel = 1;
			else
				prm.zoom_abs = 1;
		}
		optind += args;
	}

	switch (argc - optind) {
	case 0:
		time_start(&t_load);
		stl_load_file(stdin);
		time_stop(&t_load);
		break;
	case 1:
		time_start(&t_load);
		stl_load(argv[optind]);
		time_stop(&t_load);
		break;
	default:
		usage(*argv);
	}

	fprintf(stderr, "STL loaded in %.3f s\n", time_ms(&t_load)/1000.0);
	fprintf(stderr, "%u edges\n", count_edges());

	if (slice) {
		slice_load(slice);
		slice_run(slice_out, needle);
		exit(0);
	}

	if (headless_dump) {
		gui_dump(overlays, n_overlays, &prm);
		exit(0);
	}

	gui(logfile, overlays, n_overlays);

	return 0;
}
