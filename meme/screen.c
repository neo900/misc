/*
 * screen.c - Screen dump
 *
 * Written 2015-2016 by Werner Almesberger
 * Copyright 2015-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "SDL.h"

#include "screen.h"


#define	BASE_NAME	"meme-"


const char *screen_dump_name = NULL;


void dump_screen_file(SDL_Surface *s, const char *name)
{
	if (SDL_SaveBMP(s, name)) {
		fprintf(stderr, "SDL_SaveBMP: %s\n", SDL_GetError());
		exit(1);
	}
	fprintf(stderr, "Screen dump to %s\n", name);
}


static void dump_screen_unique(SDL_Surface *s)
{
	char tmp[sizeof(BASE_NAME) + 4 + 4];
	int n;

	for (n = 0; n <= 9999; n++) {
		snprintf(tmp, sizeof(tmp), BASE_NAME "%04d.bmp", n);
		if (access(tmp, R_OK)) {
			dump_screen_file(s, tmp);
			return;
		}
	}
	fprintf(stderr, "dump_screen: no available name\n");
	exit(1);
}


void dump_screen(SDL_Surface *s)
{
	if (screen_dump_name)
		dump_screen_file(s, screen_dump_name);
	else
		dump_screen_unique(s);
}
