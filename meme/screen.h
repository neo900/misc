/*
 * screen.h - Screen dump
 *
 * Written 2015-2016 by Werner Almesberger
 * Copyright 2015-2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef	SCREEN_H
#define	SCREEN_H

#include "SDL.h"


extern const char *screen_dump_name;


void dump_screen_file(SDL_Surface *s, const char *name);
void dump_screen(SDL_Surface *s);

#endif /* !SCREEN_H */
