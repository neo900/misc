/*
 * gaux.c - Auxiliary graphics functions
 *
 * Written 2014, 2016 by Werner Almesberger
 * Copyright 2014, 2016 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include "SDL.h"


SDL_Surface *sdl_surface(Uint32 flags, int width, int height)
{
	SDL_PixelFormat *fmt;

	fmt = SDL_GetVideoInfo()->vfmt;
	return SDL_CreateRGBSurface(flags, width, height,
	    fmt->BitsPerPixel, fmt->Rmask, fmt->Gmask, fmt->Bmask, fmt->Amask);
}


/*
 * Loosely based on
 * https://forums.libsdl.org/viewtopic.php?t=3653&sid=964c840d9a6461f79e00ccd745da6c1c
 */

Uint32 get_pixel(const SDL_Surface *s, Sint16 x, Sint16 y, Uint32 null)
{
	if (x < 0 || y < 0)
		return null;
	if (x >= s->w || y >= s->h)
		return null;
	return ((Uint32 *) s->pixels)[y * (s->pitch / sizeof(Uint32)) + x];
}
