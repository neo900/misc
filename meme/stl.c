/*
 * stl.c - STL file reading
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

#include "mesh.h"
#include "stl.h"


#define	MAX_LINE	100


enum state {
	s_init,
	s_facet,
	s_loop,
	s_vertices,
	s_endloop,
	s_endfacet,
};


void stl_load_file(FILE *file)
{
	char buf[MAX_LINE+1];
	enum state state = s_init;
	const struct vertex *v[3];
	int num_v = 0;
	char *s, *e;
	int n = 0;
	int end, got;
	float x, y, z;

	mesh_init();
	while (fgets(buf, sizeof(buf), file)) {
		n++;
		if (!(n & 1023))
			fprintf(stderr, "%d\r", n);

		for (s = buf; *s && isspace(*s); s++);
		e = strchr(s, 0);
		while (e != s && isspace(e[-1]))
				e--;
		*e = 0;
		end = 0;

		switch (state) {
		case s_init:
			sscanf(s, "solid %*s%n", &end);
			state = s_facet;
			break;
		case s_facet:
			if (!strncmp(s, "endsolid", 8))
				return;
			sscanf(s, "facet normal %*f %*f %*f%n", &end);
			state = s_loop;
			break;
		case s_loop:
			sscanf(s, "outer loop%n", &end);
			state = s_vertices;
			num_v = 0;
			break;
		case s_vertices:
			got = sscanf(s, "vertex %f %f %f%n", &x,&y, &z, &end);
			if (got < 3)
				break;
			v[num_v] = vertex_add(
			    roundf(x*1000), roundf(-y*1000), roundf(z*1000));
			if (++num_v == 3) {
				facet_add(v[0], v[1], v[2]);
				state = s_endloop;
			}
			break;
		case s_endloop:
			sscanf(s, "endloop%n", &end);
			state = s_endfacet;
			break;
		case s_endfacet:
			sscanf(s, "endfacet%n", &end);
			state = s_facet;
			break;
		}
		if (end != e-s) {
			fprintf(stderr, "cannot parse line %d (%d %ld)\n",
			     n, end, e-s);
			exit(1);
		}
	}
	fprintf(stderr, "incomplete STL file\n");
	exit(1);

}


void stl_load(const char *name)
{
	FILE *file;

	file = fopen(name, "r");
	if (!file) {
		perror(name);
		exit(1);
	}
	stl_load_file(file);
	fclose(file);
}
