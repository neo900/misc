/*
 * text.c - Text rendering
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdarg.h>
#include <stdio.h>

#include "SDL.h"
#include "SDL_Pango.h"

#include "gaux.h"
#include "text.h"


const char *text_font = "Courier Bold 10";


static SDLPango_Matrix matrix = {
	.m = {
		{ 255,	0,	0,	0 },
		{ 255,	0,	0,	0 },
		{ 255,	0,	0,	0 },
		{ 0,	255,	0,	0 }
	}
};


void text_color(Uint32 rgba)
{
	matrix.m[0][1] = rgba >> 24;
	matrix.m[1][1] = rgba >> 16;
	matrix.m[2][1] = rgba >> 8;
	matrix.m[3][1] = rgba;
}


SDL_Surface *text(const char *s)
{
	SDLPango_Context *pango;
	SDL_Surface *surf;
	int w, h;

	pango = SDLPango_CreateContext_GivenFontDesc(text_font);
	SDLPango_SetDefaultColor(pango, &matrix);
	SDLPango_SetMinimumSize(pango, 0, 0);
	SDLPango_SetMarkup(pango, s, -1);

	// surf = SDLPango_CreateSurfaceDrawWithLayout(pango, NULL);

	w = SDLPango_GetLayoutWidth(pango);
	h = SDLPango_GetLayoutHeight(pango);

	surf = sdl_surface(SDL_SWSURFACE, w, h);

	SDLPango_Draw(pango, surf, 0, 0);

	SDLPango_FreeContext(pango);

	return surf;
}


SDL_Surface *vtextf(const char *fmt, va_list ap) 
{
	char *tmp;
	int ignore;

	ignore = vasprintf(&tmp, fmt, ap);
	(void) ignore;

	return text(tmp);
}

SDL_Surface *textf(const char *fmt, ...)
{
	va_list ap;
	SDL_Surface *surf;

	va_start(ap, fmt);
	surf = vtextf(fmt, ap);
	va_end(ap);

	return surf;
}
