/*
 * mesh.h - 3D mesh data structures
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef MESH_H
#define	MESH_H


#include <stdbool.h>


struct vertex {
	int x, y, z;
};


struct edge {
	const struct vertex *a, *b;
	struct edge *next;
};


struct facet {
	const struct vertex *v[3];
	struct facet *next;
};


extern struct facet *facets;


const struct vertex *vertex_add(int x, int y, int z);

void vertex_foreach(bool (*fn)(struct vertex *v, void *user), void *user);
void vertex_foreach_const(bool (*fn)(const struct vertex *v, void *user),
    void *user);

void edge_foreach(bool (*fn)(const struct vertex *a, const struct vertex *b,
    void *user), void *user);

const struct facet *facet_add(const struct vertex *a, const struct vertex *b,
    const struct vertex *c);

void mesh_init(void);

#endif /* !MESH_H */
