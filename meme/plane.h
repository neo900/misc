/*
 * plane.h - Intersect mesh with planes
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef PLANE_H
#define	PLANE_H

#include "mesh.h"


struct point {
	int x, y;
};


struct point *plane_xz(int y, unsigned *n);
struct point *plane_yz(int x, unsigned *n);

#endif /* !PLANE_H */
