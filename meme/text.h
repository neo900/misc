/*
 * text.h - Text rendering
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef TEXT_H
#define	TEXT_H

#include <stdarg.h>

#include "SDL.h"


extern const char *text_font;


void text_color(Uint32 rgba);
SDL_Surface *text(const char *s);
SDL_Surface *textf(const char *fmt, ...);
SDL_Surface *vtextf(const char *fmt, va_list ap);

#endif /* !TEXT_H */
