/*
 * gui.c - User interface
 *
 * Written 2014-2016 by Werner Almesberger
 * Copyright 2014-2016 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef GUI_H
#define	GUI_H

#include <stdbool.h>

#include "overlay.h"
#include "marker.h"


extern struct marker marker;


struct dump_params {
	int xres, yres;

	bool middle;
	float mx, my;

	bool zoom_abs;	/* "zoom" contains a zoom value */
	bool zoom_rel;	/* add "zoom" to auto-zoom result */
	int zoom;

	bool cursor;
	float cx, cy;

	bool enhance;
	float enh_z;
	int enh_scale;

	bool profile;
};


void gui_dump(struct overlay overlays[], unsigned n_overlays,
    const struct dump_params *prm);
void gui(const char *logfile, struct overlay overlays[], unsigned n_overlays);

#endif /* !GUI_H */
