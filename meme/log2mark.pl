#!/usr/bin/perl
#
# log2mark.pl - Convert meme log output to markers
#
# Written 2016 by Werner Almesberger
# Copyright 2016 by Werner Almesberger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

#
# Typical use:
#
# meme -l log -m foo.mark ...
# log2mark.pl -f Z log
#
# Then copy entires output by log2mark.pl manually to foo.mark and add them to
# the display by reloading with R.
#


sub usage
{
	print STDERR <<"EOF";
usage: $0 [-f] tag [file ...]

  -f  follow file (like tail -f)
EOF
	exit(1);
}


while ($ARGV[0] =~ /^-./) {
	if ($ARGV[0] eq "-f") {
		$follow = 1;
	} else {
		&usage;
	}
	shift @ARGV;
}

$field = shift @ARGV;
&usage unless defined $field;

if ($follow) {
	$in = shift @ARGV;
	open(IN, $in) || die "$in: $!" if defined $in;
}

$d = 0;
while (1) {
	if (!defined $in) {
		$_ = <>;
		exit unless defined $_;
	} else {
		$_ = <IN>;
		if (!defined $_) {
			exit unless $follow;
			close IN;
			open(IN, $in) || die "$in: $!" if defined $in;
		}
	}

	chop;

	$d = /^D:/ if /^\S/;
	if (/^([AB]):\s+X\s+(\S+)\s+Y\s+(\S+)/) {
		if ($1 eq "A") {
			@a = ($2, $3);
		} else {
			@b = ($2, $3);
		}
		next;
	}
	next unless $d;
	/\S*\s+/;
	$tag = undef;
	for (split(/\s+/, $')) {
		if (defined $tag) {
			print "$a[0]\t$a[1]\t$b[0]\t$b[1]\t$_\n"
			    if $tag eq $field;
			undef $tag;
		} else {
			$tag = $_;
		}
	}
}
