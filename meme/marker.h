/*
 * marker.h - Markers
 *
 * Written 2016 by Werner Almesberger
 * Copyright 2016 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef MARKER_H
#define	MARKER_H

struct mark;

struct marker {
	const char *name;
	void (*xform)(float x, float y, float *res_x, float *res_y);
	struct mark *markers;
};


void marker_draw(const struct marker *marker,
    void (*line)(void *user, int xa, int ya, int xb, int yb,
    float nx, float ny),
    void (*text)(void *user, int x, int y, float nx, float ny, const char *txt),
    void *user);

void marker_reload(struct marker *marker);

void marker_load(struct marker *marker, const char *name,
    void (*xform)(float x, float y, float *res_x, float *res_y));

#endif /* !MARKER_H */
