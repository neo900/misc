/*
 * time.h - Timing of program execution
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 by Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#ifndef TIME_H
#define	TIME_H

#include <sys/time.h>


struct time {
	struct timeval tv;
};


void time_start(struct time *t);
void time_stop(struct time *t);
unsigned time_ms(const struct time *t);

#endif /* !TIME_H */
