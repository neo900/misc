2016-05-28 to 2016-11-21
------------------------

- GPIO_d now uses configurable VAUX3 as supply, not a fixed 2.7 V rail

- Documented alternative Hackerbus pin assignment

2015-02-25 to 2015-02-11
------------------------

- Hackerbus now uses a dedicated USB High-Speed host port, with D+/D-
  directly accessible on the main Hackerbus connector (replacing two
  GPIOs). This reduces the risk of upsetting regular USB (OTG) operation.

- Access to USB OTG is still possible, but now uses spring-loaded contacts
  on the user circuit. This allows the mating contacts in the Neo900 to be
  simple PCB holes (instead of large header pins), reducing the risk of
  interference and signal degradation.

- More details on current limits and over-current protection.

- The optional (shared with GPIO) regulated power rail is now 2.7 V instead
  of 3.3 V. This simplifies the circuit (we already have a 2.7 V rail for
  various components, but may not have a 3.3 V rail), and most chips that
  operate with 3.3 V also accept 2.7 V.

- Appendix describing the board-to-board connection between BOB and LOWER.
  We need to consider this also for the Hackerbus design as it affects the
  placement of items and each imposes constraints on the other.
