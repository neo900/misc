\documentclass[11pt]{article}
\usepackage{a4}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage[hang,bottom]{footmisc}
\usepackage{footnote}
\usepackage{titlesec}	% for \sectionbreak
\usepackage{fp}
\usepackage{numprint}
\usepackage{xfrac}
\usepackage{parskip}
\usepackage{enumitem}	% for style=nextline
\usepackage[all]{nowidow}
\usepackage[numbib]{tocbibind}	% add references to PDF TOC
\usepackage[hidelinks,bookmarksnumbered=true,
  bookmarksopen,bookmarksopenlevel=2]{hyperref}
\usepackage{collcell}

\def\FPfmt#1{\FPeval{fpfmttmp}{#1}\fpfmttmp{}}
\def\FPrnd#1#2{\FPfmt{round((#2):#1)}}
% known bug: want "ceil" not "round"
\def\ni#1{\FPeval{fpfmttmp}{round((#1 + 0.499):0)}\numprint{\fpfmttmp{}}}
\def\no#1{\FPeval{fpfmttmp}{round((#1):1)}\numprint{\fpfmttmp{}}}
\def\nf#1{\FPeval{fpfmttmp}{round((#1):2)}\numprint{\fpfmttmp{}}}
\npdecimalsign{.}

% awesome column-shifting hack by "egreg"
% http://tex.stackexchange.com/questions/29537/table-can-i-shift-a-column-by-half-the-height-of-a-row
\newcommand{\shiftdown}[1]{\smash{\raisebox{-.5\normalbaselineskip}{#1}}}
\newcolumntype{C}{>{\collectcell\shiftdown}c<{\endcollectcell}}

% http://tex.stackexchange.com/questions/2644/how-to-prevent-a-page-break-before-an-itemize-list
\makeatletter 
\newcommand\nobreakpar{\par\nobreak\@afterheading} 
\makeatother

% http://tex.stackexchange.com/questions/50352/inserting-a-small-vertical-space-in-a-table
\def\T{\rule{0pt}{12pt}}

% Copyright notice in the footer (first page only)
\usepackage{fancyhdr}
\fancypagestyle{plain}{
  \fancyfoot{}
  \fancyfoot[L]{Copyright \textcopyright\ by the authors.}}
\renewcommand{\headrulewidth}{0pt}

\newcommand{\sectionbreak}{\clearpage}
\renewcommand{\footnotemargin}{1.2em}

\title{Neo900 Hackerbus \\
  {\large\bf PRELIMINARY --  SUBJECT TO CHANGE WITHOUT FURTHER NOTICE}}
\author{J\"org Reisenweber%
\footnote{Concept and design requirements.}
%~\url{<joerg@openmoko.org>} \\
,
Werner Almesberger%
\footnote{Specification details and illustrations.}
%~\url{<werner@almesberger.net>}
}
\date{November 21, 2016}

\def\TODO{{\bf TO DO}}
\def\todo#1{\begin{center}{\bf TO DO:} #1\hfill~\end{center}}

\def\iic{$\hbox{I}^2\hbox{C}$}

\newenvironment{tab}{\vskip4mm\qquad\begin{minipage}{430pt}}%
{\end{minipage}\vskip4mm\noindent}

\begin{document}
\phantomsection\pdfbookmark{Neo900 Hackerbus}{firstpage}
\maketitle

The Hackerbus is an interface that allows user-provided circuits to
connect directly to power rails and various signals in the Neo900.

Warning: Hackerbus gives access to signals that can upset the operation
of the Neo900 and incorrect use may cause permanent damage inside
and outside the device. Use with caution !

Characteristics beyond what is specified in this document should be
obtained by examining the schematics and the data sheets of the
respective components.

% -----------------------------------------------------------------------------

\section{Pin assignment}
\label{hbpins}

Hackerbus uses two connectors, one for power and various signals and
one for USB OTG data, which are arranged around the memory card on the
Break-Out-Board (BOB).

The following drawing gives a rough overview of connector locations
and shows the pin assignment:

\begin{center}
\includegraphics[scale=0.9]{pins.pdf}
\end{center}

% Nov 26 20:36:51 <DocScrutinizer05> several of the pins have a solder option to reassign them. Alternative signals include audio-in/out, USB ID-pin, I2C bus #2, you-name-it
% Nov 26 20:43:32 <DocScrutinizer05> at least two of them GPIOs are capable to trigger SoC_IRQ/WAKE

The following table describes the functions of the pins on the
Hackerbus main connector:
\nopagebreak

\begin{tabular}{l|p{12cm}}
  \bf Hackerbus pin	& \bf Description \\
  \hline
  VBAT\_RAW		& Direct connection to the battery (charging allowed) \\
  VBAT\_SWITCHED	& Like VBAT\_RAW but switched off when system is
			  powered down \\
			& \bf Reverse-feed \underline{not} allowed~! \\
  VBUS\_OTG		& USB OTG bus voltage (reverse-feed allowed) \\
  GND			& System ground and return for power and all signals
			  but NFC and audio \\
  \hline
  USB\_HB\_DP		& USB differential data, positive \\
  USB\_HB\_DM		& USB differential data, negative \\
  \hline
  UART\_TX		& UART3, Transmit data (output from CPU) \\
  UART\_RX		& UART3, Receive data (input to CPU) \\
  UART\_CTS		& UART3, Clear To Send (input to CPU) \\
  UART\_RTS		& UART3, Ready To Send (output from CPU) \\
  \hline
  GPIO\_a		& TBD \\
  GPIO\_b		& TBD \\
  GPIO\_c		& TBD \\
  GPIO\_d/VDD		& TBD / VAUX3 rail \\
  \hline
  NFC\_ANT		& NFC antenna, positive \\
  NFC\_GND		& NFC antenna, ground or negative \\
			& \bf Do not connect to any other GND~! \\
\end{tabular}

Note that the UART signals may also be used as general IOs
and that their role further depends on the configuration
of the infrared (IR) subsystem \cite{Neo900-IR}.

\todo{The exact function of UART\_RX will depend on the implementation
of IR RX, which will be decided at a later time.}

The Hackerbus USB OTG through-hole pads give access to signals from the
Neo900 Micro USB connector:
\nopagebreak

\begin{tabular}{l|p{12cm}}
  \bf Hackerbus pin	& \bf Description \\
  \hline
  USB\_OTG\_DP		& USB differential data, positive \\
  USB\_OTG\_DN		& USB differential data, negative \\
\end{tabular}

Further details on how Hackerbus connects to USB can be found in
section \ref{usb}.

% Nov 26 20:55:57 <wpwrak> so we want to allow HB to feed power into VBUS ?
% Nov 26 20:56:26 <DocScrutinizer05> sure

% Nov 25 03:33:06 <DocScrutinizer05> and we need to clearly state that it's absolute nogo to feed power to Vbatt_switched

% Nov 26 20:58:21 <wpwrak> fair enough. VBUS_RAW max current on HB ?
% Nov 26 20:58:30 <wpwrak> discharge / charge
% Nov 26 20:58:40 <wpwrak> err, VBAT_RAW
% Nov 26 20:58:46 <DocScrutinizer05> 1C

% IO voltage:
% Nov 26 22:42:16 <DocScrutinizer05> [2014-11-23 Sun 21:24:43] <DocScrutinizer05> maybe we have an unused LDO in twl4030 still, even a programmable one? ;-)
% Nov 26 22:42:50 <wpwrak> that would be neat, yet VHB :)

% -----------------------------------------------------------------------------

\section{Alternate pin assignments}
\label{alt}

Some Hackerbus pins are prepared for modifications that allow alternative
signals to be assigned to them. The following list shows the pins that
can be reassigned:

\begin{tabular}{lll}
  \bf Group	& \bf Signal \\
  \hline
  UART		& HB\_TX\_IRTX \\
		& HB\_CTS\_RCTX \\
		& UART3\_RTS \\
		& UART3\_RX\_IRRX \\
  GPIO		& HB\_A \\
		& HB\_B \\
		& HB\_C \\
		& HB\_D \\
\end{tabular}

The circuit of these signals is located on the LOWER PCB and looks as
follows:

\begin{center}
\includegraphics[scale=0.9]{alt.pdf}
\end{center}

The resistor {\bf A} can be removed to separate the signal from the CPU.
The alternative
signal can be attached with a jumper wire either at point {\bf B} (if digital)
or point {\bf D} (if analog).
The trace can be cut between level shifter and LOWER-BOB connector ({\bf C})
to separate the pin also
from the level shifter (see section \ref{level}), but we know of no
technical need for doing this.
The following alternative signals are available on a patch
field near the LOWER-BOB connector:

\begin{tabular}{lll}
  \bf Group		& \bf Signal		& \bf Type \\
  \hline
  \iic\T		& I2C3\_SDA		& Digital \\
  			& I2C3\_SCL		& Digital \\
  USB			& OTG\_ID		& Analog \\
  Audio line in/out	& HB\_LINE\_OUT\_L	& Analog \\
  			& HB\_LINE\_OUT\_R	& Analog \\
  			& HB\_LINE\_IN		& Analog \\
  Audio jack		& HB\_JACK\_1L		& Analog \\
			& HB\_JACK\_2R		& Analog \\
			& HB\_JACK\_3M		& Analog \\
			& HB\_JACK\_4GND	& Analog \\
  Digital microphone	& HB\_DMIC\_CLOCK	& Digital \\
			& HB\_DMIC\_DATA	& Digital \\
\end{tabular}

To choose an alternative assignment, the circuit of the respective
pin has to be modified as follows:
\nobreakpar
\begin{description}[style=nextline]
  \item[Digital signal]
    If the original signal is shared with other functions (e.g., UART),
    remove the resistor at {\bf A}. Then connect the new
    signal with a jumper wire between the patch field and point {\bf B}.
  \item[Positive analog signal]
    If the original signal is shared with other functions (e.g., UART),
    remove the resistor at {\bf A}.
    Solder the jumper wire with the
    new signal to the respective pin on the LOWER-BOB connector ({\bf D}).
  \item[Negative analog signal]
    If the analog signal can become negative (with respect to ground),
    conflict with the clamp diode in the CPU may occur. Therefore,
    remove the resistor at {\bf A} and
    solder the jumper wire to {\bf D}.
    Note that the ESD protection may also interfere with the signal.
\end{description}

Note that, if the resistor is not removed, the CPU typically has to
tri-state the respective signal.

% Nov 26 02:47:56 <DocScrutinizer05> I2C will not have the OVP
% Nov 26 02:48:22 <DocScrutinizer05> neither will audio-in/out or any of the other tentative 2nd level signals

% Nov 26 20:36:51 <DocScrutinizer05> several of the pins have a solder option to reassign them. Alternative signals include audio-in/out, USB ID-pin, I2C bus #2, you-name-it

% <DocScrutinizer05> we need to define the "TBD" stuff, at least preliminary. And I wonder if we should keep the levelshifters for SECONDARY I2C
% <wpwrak> (I2C) yes, that should work. we don't even need R1 there because it's already somewhere. saves us some important microcents ;-)

% -----------------------------------------------------------------------------

\section{USB access}
\label{usb}

Hackerbus gives access to two USB buses: the data signals of a
High-Speed host port are available on the Hackerbus connector. This
USB port is used exclusively for Hackerbus.

Furthermore, the data signals of the Neo900 USB OTG port (High-Speed)
are available on contacts accessible from the Hackerbus, VBUS of the OTG
port is available on the Hackerbus connector, and the ID signal of OTG
is available as an alternate pin assignment. (See section \ref{alt}.)

\begin{center}
\includegraphics[scale=0.9]{usb.pdf}
\end{center}

\todo{Define and indicate alternate pin assignment for USB ID.}

Note that there is no bus supply voltage (VBUS) on the dedicated USB
interface of Hackerbus. User circuits can be powered from the 2.7 V
rail or from any of the other supplies.

% -----------------------------------------------------------------------------

\section{Physical placement}

The following drawing illustrates the precise placement of components
on the Break-Out-Board (BOB):

\begin{center}
\includegraphics[scale=0.9]{area.pdf}
\end{center}

The main connector is soldered to the bottom of the BOB and the
header pins pass through holes in the PCB, entering the receptacle
from the bottom. The USB OTG contacts are on the LOWER board
of Neo900 and can be accessed through an opening in the BOB.

Please note that the Hackerbus connector and contacts are covered by plastic
structures in the N900 case and are therefore only accessible
when the case is removed or if the respective plastic structures
have been cut.

% Nov 23 18:03:46 <DocScrutinizer05> we have an "unused" area of 14x6mm right "above" the uSD card

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Hackerbus main connector}

On the Neo900 side, the principal connector for Hackerbus is a
Harwin M50-3151042 \cite{M50-315}
female connector with 20 contacts organized in a $10\times 2$ array. 
The connector has a 1.27 mm pitch and is mounted underneath the Neo900
break-out board (BOB).

The vertical stacking of the main connector is illustrated in the following
diagram:%
\footnote{The drawing is approximately to scale but dimensions drawn can be
  off by up to 0.15~mm in real-world coordinates.}

\begin{center}
\includegraphics[scale=0.9]{conn-main.pdf}
\end{center}

The male header shown as an example in the drawing has the dimensions of
the FCI 20021111-00020T4LF through-hole connector,%
\footnote{\url{http://portal.fciconnect.com/Comergent/fci/drawing/20021111.pdf}}
with a contact length of 3.05 mm and a plastic mold height of 2.5 mm.
(See also \cite{Neo900-Hdr}.)
% Sullins GRPB102VWVN-RC

% Nov 26 20:51:11 <DocScrutinizer05> anyway this http://www.digikey.com/product-detail/en/M50-3150842/952-1373-5-ND/2264354 receptacle will sit *under* the uSD card board and the board has holes for the plug's posts to connect to it

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Hackerbus USB OTG contacts}

On the Neo900 side, the USB OTG contacts for Hackerbus consist of two
through-hole pads in the LOWER board.
Cut-outs in the BOB allow them to be accessed with pogo pins on the
user board.

The vertical stacking of the USB OTG connection is illustrated in the following
diagram:

\begin{center}
\includegraphics[scale=0.9]{conn-usb.pdf}
\end{center}

The characteristics of the pogo pins depend on the placement of the
user board. Clearances and hole diameters are designed for the
Mill-Max 914 series. \cite{M025}
The following table shows minimum, middle, and maximum length of the pogo
pins in this series. The length corresponds to the distance between
the facing sides of LOWER and the user board.

\def\pogo#1#2#3{\FPeval{#1max}{#2 + 0.71}%
\FPeval{#1mid}{#1max - 1.14}%
\FPeval{#1min}{max(#2 + 0.71 - 2.29, #3)}}

\pogo{a}{6.96}{4.7}
\pogo{b}{7.72}{6.15}
\pogo{c}{8.48}{6.91}
\pogo{d}{9.25}{7.67}

\begin{tabular}{l|rrr}
  Part number & \multicolumn{3}{c}{Length (mm)} \\
  & Min. & Mid & Max. \\
\hline
0914-{\bf 0}-15-20-77-14-11-0	& \nf{amin} & \nf{amid} & \nf{amax} \\
0914-{\bf 1}-15-20-77-14-11-0	& \nf{bmin} & \nf{bmid} & \nf{bmax} \\
0914-{\bf 2}-15-20-77-14-11-0	& \nf{cmin} & \nf{cmid} & \nf{cmax} \\
0914-{\bf 3}-15-20-77-14-11-0	& \nf{dmin} & \nf{dmid} & \nf{dmax} \\
\end{tabular}

% Nov 23 23:55:09 <DocScrutinizer05> [2014-11-23 Sun 22:24:23] <DocScrutinizer05> USB D+/- go directly through holes at _side_ of uSD to two postconn jacks next to USB conn on LOWER

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Mechanical coupling with battery cover}

If the user circuit connecting to Hackerbus is mechanically coupled
with the battery cover, the movement when opening and closing the
battery compartment has to be considered. The following drawing shows
approximately how the parts involved move:

\begin{center}
\includegraphics[scale=1.0]{cover.pdf}
\end{center}

The cover has a hook that goes into an opening in the case. When
opening or closing the cover it rotates around that hook, which
results in anything mechanically connected to the cover to perform
the same rotation.

The drawing shows the path along which a header pin plugging into the
Hackerbus main connector moves. This rotation may make it difficult
to connect extensions whose Hackerbus connector is rigidly coupled
with the battery cover. These difficulties would be more pronounced for
the Hackerbus USB connector since it is closer to the axis around which
the battery cover rotates.

Possible ways to avoid this issue include not coupling the extension
with the battery lid, using a flexible coupling, or removing the
hook on the cover.

% -----------------------------------------------------------------------------

\section{Overcurrent protection}
\label{overcurr}

To protect traces and other components from excessive current, VBAT\_RAW
is equipped with a resettable fuse. Traces and contacts between battery
and Hackerbus are designed to be able to permanently conduct at least
the trip current of the fuse.

\begin{center}
\includegraphics[scale=1.0]{fuse.pdf}
\end{center}

% Nov 26 20:59:37 <DocScrutinizer05> we will implement a 1.5A polyfuse anyway
% NB: polyfuse seems to be trademarked

% Nov 26 22:56:42 <wpwrak> (ask about the polyfuse) so we don't publish the OCP limits ?
% Nov 26 22:58:00 <DocScrutinizer51> you're free to pulish the polyfuse datasheet which is the OCP spec for that line


% [2014-11-26 Wed 05:20:12] <DocScrutinizer05> oooh, there are nice FET switches with integrated OCP
% [2014-11-26 Wed 05:22:17] <DocScrutinizer05> whe could use those for V18 and V33
% [2014-11-26 Wed 05:23:21] <DocScrutinizer05> should also conveniently protect the core system from bad things that happen on hackerbus, like short on VIO_18

Note that the presence of this fuse does not guarantee that loads
exceeding the trip current will not lead to malfunction. Furthermore,
the battery is a user-provided item and needs to be evaluated separately
by prospective users of VBAT\_RAW. The maximum current available on
VBAT\_RAW depends not only on the fuse but also on the characteristics
and condition of the battery.

The design of Neo900 is intended to support the following maximum currents
without suffering damage or disturbing normal system operation, provided
that suitable power sources are available:

\begin{tabular}{l|rr}
  Hackerbus pin	& \multicolumn{2}{c}{Continuous current (A)} \\
	& Forward & Reverse \\
  \hline
  VBAT\_RAW		& 0.5	& 1.5 \\
  VBAT\_SWITCHED	& 0.3	& --- \\
  VBUS\_OTG		& 0.5	& 0.5 \\
  GPIO\_d/VDD		& 0.2 	& --- \\
\end{tabular}

% DocScrutinizer05> a 500mA for VBATT would be nice
% <wpwrak> VBAT_RAW or VBAT_SWITCHED ?
% <DocScrutinizer05> 1A would be great
% <DocScrutinizer05> VBAT_SWITCHED is low power
% <DocScrutinizer05> max 300mA
% <wpwrak> i have 1.5 A (charging) for VBAT_RAW, 100 mA for VBAT_SWITCHED, 0.5 A for VBUS_OTG, and 100 mA for GPIO_d/3V3 (or whatever voltage we end up putting there)
% <DocScrutinizer05> ok :)

All these limits apply to individual rails. Predicting behaviour when
drawing or injecting significant current
on multiple rails
at the same time
is beyond the scope of this document.

Please note that none of these parameters are guaranteed, and further
constraints may apply.

Peripherals using VBAT\_RAW should connect to both VBAT\_RAW contacts
with traces that \underline{each} are capable of carrying the full
maximum current the application demands.

% -----------------------------------------------------------------------------

%% \section{Overvoltage protection}
%% 
%% \todo{Q: Do we still want/need this if we add level shifters ? \\
%%   A: No.}
%% 
%% All IO signals going to the CPU (whose IO pins operate at 1.8 V) are
%% protected against overvoltage by the following circuit:
%% 
%% \begin{center}
%% \includegraphics[scale=0.9]{clamp.pdf}
%% \end{center}
%% 
%% Diodes D1 and D2 clamp the signal to the rails and resistor R2 limits
%% the current through the diodes. Since the diodes allow the signal to
%% overshoot the rail's nominal voltage by the diode's forward voltage,
%% resistor R1 further limits the current into the CPU.
%% 
%% Limits and characteristics of the circuit are to be determined
%% by the user.

% -----------------------------------------------------------------------------

\section{Level shifters}
\label{level}

Level shifters are provided to interface with circuits operating in other
domains than 1.8 V. The circuit is as follows, for each primary UART and
GPIO signal:

\begin{center}
\includegraphics[scale=0.9]{shift.pdf}
\end{center}

The principle of operation is briefly described in \cite{LvlShift-Short}
and further details can be found in \cite{LvlShift-Long}.
R1 is usually an internal pull-up resistor in the CPU of $10~{\rm k}\Omega$
or higher.
R2 limits the current when CPU and external circuit drive the Hackerbus
signal in a conflicting way.
R2 is $100~\Omega$. D1 protects against ESD and polarity inversion.

Note that our circuit differs from the NXP design in that Neo900 does not
provide a pull-up resistor on the high-voltage side. If the user
circuit needs a pull-up, e.g., to drive a logic input, it therefore has
to provide one itself. If an external pull-up is to be used, it should be
dimensioned such that it does not deliver more than 1.8 mA into the
Neo900 if the corresponding Hackerbus signal is driven low (0 V).
A value of $10~{\rm k}\Omega$ or larger is recommended.

% <DocScrutinizer05> and 100R for series protective R sounds fine to me
% <DocScrutinizer05> it vould add a 0.2V to low logic level when pulling a 2mA
% <DocScrutinizer05> or (thanks captain obvious ;-) a 0.1V for 1mA
% <DocScrutinizer05> I2C input level: VIL  Low level input voltage  - 0.5  0.3 x vdds  V
% <DocScrutinizer05> Vdds=1V8, *0.3=0.54V,
% <DocScrutinizer05> I2C output level: V Low-level output voltage open-drain at 3-mA sink current  0  0.2 x vdds  V OL
% <DocScrutinizer05> margin: 0.18V
% <DocScrutinizer05> so 100R series is fine with external pullup in the range of not creating more than 1.8mA into I2C on low logic level
% <DocScrutinizer05> for the other GPIO I think we simply copy that

Power, NFC, USB, and secondary signals (other than I2C) are not equipped
with level shifters.

Further limits and characteristics of this circuit are to be determined
by the user.

% -----------------------------------------------------------------------------

%% \section{Idea for HB IO voltage}
%% 
%% \TODO
%% (see below)
%% 
%% Either find spare programmable voltage regulator and make HB fully
%% configurable or use switch to select either 1.8 V or 2.7 V (or none).
%% Switch should let HB user connect own supply if no internal supply
%% is selected. (I.e., switch does not admit reverse current.)
%% 
%% \begin{center}
%% \includegraphics[scale=0.9]{dist.pdf}
%% \end{center}

% -----------------------------------------------------------------------------

\section{Switchable power rail}

% <wpwrak> drop 1.8 V ? :)
% <DocScrutinizer05> so scratch 1V8
% <DocScrutinizer05> :-) yes

The VHB power rail (corresponding to the VAUX3 regulator) can be switched
to the GPIO\_d pin under
software control. If not operating as power rail,
GPIO\_d can be used for regular IO.
The following drawing illustrates the circuit:

\begin{center}
\includegraphics[scale=0.9]{rail.pdf}
\end{center}

VAUX3 provides up to 200~mA. The voltage can be set by software to
1.5, 1.8, 2.5, 2.8, or 3.0~V.

The connection to the CPU is protected by the same circuit
as all other IOs available on the Hackerbus.
When GPIO\_d is configured as power rail, the CPU should set the
corresponding GPIO to High-Z without pull-up or -down.
The CPU may read the GPIO, but the resulting value is undefined.

% -----------------------------------------------------------------------------

\appendix
\section{LOWER-BOB connection}

The design of Hackerbus also affects the connection between LOWER and BOB.
We therefore discuss characteristics of the connection in this appendix,
and provide a design that harmonizes with Hackerbus.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Signals}
\label{signals}

In addition to the 20 signals of the Hackerbus main connector described
in section \ref{hbpins}, we have the following signals to other components
on BOB:

\begin{savenotes}
\begin{tabular}{lcll}
  Signal(s)		& Number	& Connects to $\ldots$ &
  Maximum average current \\
  \hline
  SD-CMD,CLK,DATx	& 6		& Memory card holder \\
  SD-VDD		& 1		& Memory card holder & 100 mA (?) \\
  SD-CD			& 1		& Memory card holder \\
  I2C-SDA,SCL		& 2		& Flash, camera cover sensor \\
  CAM-COVER		& 1		& Camera cover sensor \\
  BATT-LID		& 1		& Battery lid sensor \\
  2V7			& 1		& Camera and battery sensor & 50 mA%
\footnote{The MLX90248 hall switch for camera cover sensing has a maximum
  current consumption of 5~mA.
  The TMD2671 proximity sensor used on the battery lid has a supply
  current of 3~mA plus LED pulses of up to 100~mA with a duty cycle of
  $\rm\sfrac{7.2~\mu s}{16.3~\mu s}=44\%$.} \\
  PRIVACY-R,G,B		& 3		& Privacy LED \\
  FLASH-A,K		& 2		& Flash LEDs & 300 mA%
\footnote{Absolute maximum peak pulsed LED forward current in table 4 of
\url{http://www.lumileds.com/uploads/461/DS209-pdf}} \\
  GND			& 1		& All of the above
					& Sum of the above, 450 mA \\
  \cline{2-2}
			& 19 \\
\end{tabular}
\end{savenotes}

We expect that each contact of the board-to-board connector can support
a maximum continuous current of 0.5~A (section \ref{connsel}). None of
the above signals exceeds this limit.

This brings the total number of contacts required to 39:
20 for Hackerbus plus another 19 for the remaining functions of BOB.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Connector characteristics}

The most important parameter is the distance between boards. Measurements
based on 3D scans of a N900%
\footnote{\url{https://neo900.org/git/scans/plain/data/stl/n900-rear-open-nobat-100um.stl.bz2}}
yielded an approximate distance of 5.2-5.3~mm between the bottom
(i.e., the
battery-facing side) of LOWER and the bottom of BOB. Given a PCB
thickness of 0.8~mm, this means that we need connectors with a stacking height
of approximately 4.4-4.5~mm.

To ensure precise placement of the connectors, positioning pins (``board
guides'') are desirable.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Connector selection}
\label{connsel}

A Digi-Key catalog search yielded the following connector families with
good availability of parts:

\begin{savenotes}
\begin{tabular}{l|cccc}
  Series	& Stacked	& Pitch	& Current	& Cycles \\
		& height	& (mm)	& per pin	& (max.) \\
		& (mm)		&	& (A) \\
  \hline
  FCI Conan%
\footnote{\url{http://portal.fciconnect.com/Comergent//fci/drawing/91900.pdf}}
		& 4.5		& 1.0	& 1.0		& 30 \\
  Hirose DF9%
\footnote{\url{http://media.digikey.com/pdf/Data\%20Sheets/Hirose\%20PDFs/DF9.pdf}}
		& 4.3 		& 1.0	& 0.5		& 30/100%
\footnote{Tin vs. gold plating.} \\
  Harwin M40%
\footnote{\url{http://cdn.harwin.com/pdfs/M40-600.pdf} \\
\url{http://cdn.harwin.com/pdfs/M40-620.pdf}}
		& 4.3		& 1.0	& 0.5		& 30 \\
\end{tabular}
\end{savenotes}

All three have a contacts in a trapezoidal arrangement with $N$ contacts
in one row and $N+1$ in the other, and very similar footprints.
Positioning pins and solder retention are optional in some series:

\begin{tabular}{l|ll}
  Series	& Positioning pin length & Solder retention \\
		& (mm) \\
  \hline
  FCI Conan	& 0.5 mm, optional	& yes \\
  Hirose DF9	& 1.0 mm, optional	& optional \\
  Harwin M40	& 1.0 mm		& yes \\
\end{tabular}

Hirose DF9 and Harwin M40 appear to have exactly the same geometry.
FCI Conan has nearly the same contact arrangement, but the positioning
pins are placed differently and the overall length is reduced.
Note that the length of the positioning pins of DB9 and M40 exceeds
the PCB thickness.

The stacking height of DF9/M40 (4.3~mm) matches the desired height of
4.5~mm within measurement and manufacturing tolerances.
Conan matches the height exactly.

We will only consider connectors with at least 15 contacts and a
footprint not exceeding the available PCB width of 17 mm.


% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Connector dimensions}

The following tables list the dimensions of the ``bounding boxes'' of the
bodies and the footprints of the three connector families selected in the
previous section. This stylized drawing illustrates the parameters we
consider:

\begin{center}
\includegraphics[scale=0.9]{dimen.pdf}
\end{center}

For the long sides we have the length of the
body, and the edge-to-edge distance of metal tab and footprint of
the solder retention. The body length is either the length of the
plastic mold (in the case of Conan, where the mold protrudes) or
the distance between the ends of the solder retention tabs
(DF9/M40, where the tabs protrude), whichever is larger.

\begin{tabular}{lc|ccc}
  Series	& Contacts & \multicolumn{2}{c}{Body (mm)} & Solder retention \\
		&	& \multicolumn{2}{c}{Solder retention}
						& footprint (mm) \\
		&	& with & without \\
   \hline
   FCI Conan	& 15	& 12.30	& ---	& 11.84 \\
		& 21	& 15.30	& ---	& 14.84 \\
		& 25	& 17.30	& ---	& 16.84 \\
   Hirose DF9	& 15	& 13.1	& 11.3	& 14.0 \\
		& 17	& 14.1  & 12.3	& 15.0 \\
		& 19	& 15.1  & 13.3	& 16.0 \\
		& 21	& 16.1	& 14.3	& 17.0 \\
   Harwin M40	& 15	& 13.10	& ---	& 14.00 \\
		& 21	& 16.10	& ---	& 17.00	\\
\end{tabular}

Only configurations with solder retention tabs and with 15 and 21 pins are
available from more than one source.

For the short sides, we consider the width of the mold, the length of
the protruding contacts (if any), and the edge-to-edge distance across the
PCB pads for the contacts.

\begin{tabular}{l|ccc}
  Series	& Mold	& Contacts	& Footprint \\
		& (mm)	& (mm)		& (mm) \\
  \hline
  FCI Conan	& 5.59	& ---		& 5.85 \\
  Hirose DF9	& 4.2	& 5.6		& 6.0 \\
  Harwin M40	& 4.20	& 5.60		& 6.00 \\
\end{tabular}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Common footprint}

With the above connectors, we have the choice between the common DF9/M40
design that would need a customization step (i.e., trimming of the
positioning pins), and the less common Conan design that could be used
without modification.

The following drawings illustrate the differences in footprint geometry
and the location and size of the positioning pin:

\begin{center}
\includegraphics[scale=0.9]{m40.pdf}
\end{center}

DF9/M40 (above) is generally larger than Conan (below) and places the
positioning pins on the center line.

\begin{center}
\includegraphics[scale=0.9]{conan.pdf}
\end{center}

From the above we can derive a common footprint design that should
be able to accommodate either type of connector, permitting postponing
the component choice until shortly before production and thus reducing
sourcing risk:

\begin{center}
\includegraphics[scale=0.9]{fp.pdf}
\end{center}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Placement overview}

The following overview drawing shows the dimensions of BOB, the placement of
the Hackerbus main connector (M50), the two board-to-board connectors
(DF9/M40), and the memory card.

For each connector, the body is shown as a rectangle and additional lines
mark the extent of protruding elements, such as pins or solder tabs.
For the memory card, the bounding box and the pad area are shown.

\begin{center}
\includegraphics[scale=0.8,trim=80 70 30 50,clip]{bob.pdf}
\end{center}

All dimensions are in millimeters.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Hypothetical pin assignment}

To verify that all the required signals can be accommodated in a reasonable
way, we provide a hypothetical pin assignment. This assignment is intended
as a model reference only and should not constrain the pin assignment choices
made for the actual circuit.

As described in section \ref{overcurr}, Hackerbus can load VBAT\_RAW
with up to 1.5~A. Ground return current is the sum of VBAT\_RAW,
VBUS\_OTG (0.5~A), and the signals described in section \ref{signals} (0.5~A),
yielding a total of 2.5~A.
With each contact being able to carry up to 0.5~A (section \ref{connsel}),
we should therefore provide at least a total of 3 pins for VBAT\_RAW and
at least 5 pins for ground.

\begin{tabular}{cC|cC}
  \multicolumn{2}{c}{Connector A} & \multicolumn{2}{c}{Connector B} \\
  \hline
  FLASH-A	& FLASH-K	&	GND		& 2V7 \\
  VBAT\_RAW	& VBAT\_RAW	&	CAM-COVER	& BATT-LID \\
  VBAT\_RAW	& USB\_HB\_DP   &	I2C-SDA		& I2C-SCL \\
  USB\_HB\_DM	& GND		&	PRIVACY-R	& PRIVACY-G \\
  GND		& NFC\_GND	&	PRIVACY-B	& SD-DAT0 \\
  NFC\_ANT	& GND		&	SD-DAT1		& SD-CLK \\
  GND		& VBUS\_OTG	&	SD-CD		& SD-VDD \\
  VBAT\_SWITCHED & GPIO\_d/VDD	&	SD-CMD		& SD-DAT3 \\
  UART\_TX	& UART\_RX	&	SD-DAT2		& GPIO\_c \\
  UART\_CTS	& UART\_RTS	&	GPIO\_a		& GPIO\_b \\
  GND		&		&	GND \\
\end{tabular}

% -----------------------------------------------------------------------------

\begin{thebibliography}{2}
\input hb.bbl
\end{thebibliography}

\end{document}


%Pins HB#1
%	Vbat (raw)	2
%	Vbat (switched)	1
%	VBUS		1
%	GPIO/1V8	1	(p-FET to turn on, enabler on IO-EXP 1.8, PD)
%	GPIO/3V3	1	(p-FET to turn on, enabler on IO-EXP 3.3, PD)
%	GND		4
%
%	NFC		2
%
%	Main
%	--------------
%	UART		4
%	GPIO/INT/WAKE	4
%
%	Alt
%	--------------
%	I2C		2
%	Audio L/R/GND	3
%	USB ID		1
%
%Pins HB#2
%	USB D+/D-	2
%	GND		1
%
%UART RX conflict:

% <wpwrak> the problem would only exist on UART3.RX. one simple option would be to make it input-only, add a small series resistor, and let the clamp do the rest
% <DocScrutinizer05> so let's keep the LS for UART signals, but explicitly explan that they spoil the wired-or concept and how to bridge LS when planning to use UART_1V8 with wire-or mode?
% <wpwrak> or we could make IR RX open-collector, pulling is illuminated. then you'd just have to keep it in the dark during HB use
% <wpwrak> with LS, it becomes output-only. so yes, that's the 3rd option
% <wpwrak> 4th: add some sort of output enable to IR RX
% <DocScrutinizer05> :nod: we'll keep all 4 in mind, and mention them in HB
% <DocScrutinizer05> #4 with "TBD"
