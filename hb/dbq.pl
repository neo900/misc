#!/usr/bin/perl
$curr = undef;
while (<>) {
	if (/^%\s+((m|f)-(th|smt)-(127)-(single|dual))\s*$/) {
		$tbl{$1} = [ $2, $3, $4, $5 ];
		$curr = $1;
		$next = 0;
		next;
	}
	next unless defined $curr;

	if (/begin{tabular}/) {
		die if $next;
		$next = 1;
		undef $s;
		next;
	}
	next unless $next;

	if (/end{tabular}/) {
		undef $curr;
		next;
	}

	next if /\\hline/;

	$s .= $_;
	next unless /\\\\\s*$/;

	$s =~ s/^\s*//;
	$s =~ s/\s*\\\\\s*//;

	$s =~ s/\\href{[^}]*}{[^}]*}//;
	@a = split(/\s*\&\s*/s,  $s);
	@a = map($_ eq "?" ? undef : $_, @a);
	@a = map($_ eq "--" ? 999 : $_, @a);
	for (@a) {
		s/\$\\approx\$\s*//;
	}
	$mn = $a[0];
	$mn =~ s/\\"//;

	$pn = $a[1];
	$pn =~ s/{\\em\s*//g;
	$pn =~ s/\s*}//g;

	push(@{ $db{$curr} },
	    { A => $a[4], B => $a[5], Z => $a[9],
	    name => "$mn $pn", s => $s })
	    unless $next == 1;
	$next++;
	undef $s;
}

$bob_pcb = 0.8;				# BOB PCB thickness
$bob_h = 5.0;				# BOB top above LOWER top
$bob_clear = $bob_h - 1.5 - 0.5;	# BOB to LOWER component clearance
$main_overlap = 0.5;			# minimum overlap of main connector

# $mf = "Harwin M50-315ccpp";
# $uf = "Samtec CLP-1cc-02-p-D";

for (@{ $db{"m-th-127-dual"} }) {
	my %mm = %{ $_ };

	next if $mm{"name"} ne $mm && defined $mm;
	next if $mm{"A"} > $bob_pcb + $bob_clear;

	for (@{ $db{"f-smt-127-dual"} }) {
		my %mf = %{ $_ };

		next if $mf{"name"} ne $mf && defined $mf;
		next unless defined $mf{"A"};

		# Main: Male header pins must pass contact point
		next if defined $mf{"A"} &&
		    $mm{"A"} - $bob_pcb < $mf{"B"} - $mf{"A"} + $main_overlap;

		$first = 1;
		for (@{ $db{"m-th-127-single"} }) {
			my %um = %{ $_ };

			next if $um{"name"} ne $um && defined $um;

			for (@{ $db{"f-smt-127-single"} }) {
				my %uf = %{ $_ };

				next if $uf{"name"} ne $uf && defined $uf;
				next unless defined $uf{"A"};
				next unless defined $uf{"Z"};

				# USB must not be taller than Main
				next if $um{"B"} + $uf{"B"} > $bob_h + $mm{"B"};

				$tip = $bob_h + $mm{"B"} - $um{"A"} - $um{"B"};
				$in = $uf{"B"} - $tip;

				# USB: Male header pin must pass contact point
				next if $in < $uf{"A"} + $main_overlap;

				# USB: Male header pin must not be too long
				next if $in > $uf{"Z"};

				print "$mm{'name'}\t[$mm{'B'}]\t",
				    "$mf{'name'}\t[$mf{'B'}]\n"
				    if $first;
				$first = 0;
				print "\t",
				    "$um{'name'}\t[$um{'B'}]\t",
				    "$uf{'name'}\t[$uf{'B'}]\n";
			}
		}
	}
}
