#!/usr/bin/perl

sub usage
{
	print STDERR "usage: $0 [-u] file.tex file.bib ...\n";
	exit(1);
}


if ($ARGV[0] eq "-u") {
	$warn_unused = 1;
	shift @ARGV;
}

&usage if $ARGV[0] =~ /^-/;
&usage unless $#ARGV >= 1;

$tex = shift @ARGV;

open(TEX, $tex) || die "$tex: $!";
$t = join("", <TEX>);
close TEX;

while ($#ARGV != -1) {
	$bib = shift @ARGV;
	open(BIB, $bib) || die "$bib: $!";
	$b .= join("", <BIB>);
	close BIB;
}

while ($t =~ /\\cite\{([^}]+)\}/s) {
	for (split(/,/, $1)) {
		next if defined $seen{$_};
		$seen{$_} = 1;
		push(@c, $_);
	}
	$t = $';
}

while ($b =~ /\\bibitem\{([^}]+)\}/s) {
	die "duplicate entry \"$1\"" if defined $bib{$1};
	$bib{$last} .= $` if defined $last;
	$last = $1;
	$bib{$1} = $&;
	$b = $';
}
$bib{$last} .= $b if defined $last;

for (@c) {
	die "undefined citation \"$_\"" unless defined $bib{$_};
	print $bib{$_};
	$used{$_} = 1;
}

if ($warn_unused) {
	for (sort keys %bib) {
		print STDERR "warning: reference \"$_\" was not used\n"
		    unless $used{$_};
	}
}
