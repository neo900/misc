\documentclass[11pt]{article}
\usepackage{a4}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{fp}
\usepackage{titlesec}	% for \sectionbreak
\usepackage{parskip}	% for non-indented paragraphs
\usepackage[hang,bottom]{footmisc}
			% non-ragged indentation of footnotes; send to bottom
\usepackage{footnote}
\usepackage[all]{nowidow}
%\usepackage[hidelinks,bookmarks=true]{hyperref}
\usepackage[numbib]{tocbibind}  % add references to PDF TOC
\usepackage[hidelinks,bookmarksnumbered=true,bookmarksopen]{hyperref}

% Copyright notice in the footer (first page only)
\usepackage{fancyhdr}
\fancypagestyle{plain}{
  \fancyfoot{}
  \fancyfoot[L]{Copyright \textcopyright\ by the authors.}}
\renewcommand{\headrulewidth}{0pt}

\newcommand{\sectionbreak}{\clearpage}

\def\FPfmt#1{\FPeval{fpfmttmp}{#1}\fpfmttmp{}}
\def\FPrnd#1#2{\FPfmt{round((#2):#1)}}

\def\iic{$\hbox{I}^2\hbox{C}$}

\title{Neo900 Infrared Subsystem}
\author{J\"org Reisenweber%
\footnote{Concept and design.}
%~\url{<joerg@openmoko.org>} \\
,
Werner Almesberger%
\footnote{Specification details and illustrations.}
%~\url{<werner@almesberger.net>}
}
\date{December 22, 2016}

\def\TODO{{\bf TO DO}}
\def\todo#1{\begin{center}{\bf TO DO:} #1\hfill~\end{center}}

\def\degree{\,^{\circ}}
\newenvironment{tab}{\vskip4mm\qquad\begin{minipage}{430pt}}%
{\end{minipage}\vskip4mm\noindent}

\begin{document}
\phantomsection\pdfbookmark{Neo900 Infrared Subsystem}{firstpage}
\maketitle

\noindent
This document describes the infrared (IR) system of Neo900.
It is intended to be used both as tentative specification and as
documentation of actual functionality, and may be revised as
focus shifts over time from requirements to implementation.

% -----------------------------------------------------------------------------

\section{System overview}

The IR subsystem consists of a high-powered transmitter and a receiver.
It connects to UART3 of the DM3730 CPU, where hardware-assisted
IrDA\footnote{\url{http://en.wikipedia.org/wiki/Infrared_Data_Association}}
(SIR) and
CIR\footnote{\url{http://en.wikipedia.org/wiki/Consumer_IR}}
(``Consumer IR'')
functions are available.

The IR subsystem also connects to the ``Hackerbus'' which can use the
signals to communicate with the CPU, be it as UART or as GPIOs, or to
access the IR system. For advanced
signal processing, the analog output of the IR sensor connects to an
unused input of the audio codec. The following drawing shows the various
components:

\begin{center}
\includegraphics[scale=0.9]{sys.pdf}
\end{center}
%
The signal named bq.GPIO is not only a control input but it also determines
the configuration after CPU reset. It is described in section
\ref{reset}.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{IR modes}
\label{modes}

The IR subsystem supports both CIR and
IrDA operation. In CIR mode, it can receive incoming
IR signals and thus act as a ``learning'' remote control.
Furthermore, the IR transceiver can be configured to send and receive
unmodulated data from the UART. We call this ``IR-UART'' mode.

% CIR trumps IrDA
%
% Jul 07 23:03:38 <DocScrutinizer05> wpwrak: I'd be willing to sacrifice full
% IrDA compatibility by simply using a photo transistor plus "discrete" filter
% gear for RX, and only the CIR wavelength IR LED for TX

These characteristics mean that some design conflicts are unavoidable, e.g.,
because CIR and IrDA operate at different wavelengths. Where a conflict
arises, highest priority is given to CIR transmission, followed by CIR
reception, and finally IrDA.

Likewise, since the IR data path is shared between CPU and Hackerbus,
conflicting configurations are possible. Their resolution is described
in sections \ref{conflict} and \ref{undriven}.

Note that we don't consider IR-UART reception and IrDA to be very important
features. If their implementation should prove to be overly troublesome,
they could therefore be omitted.

% -----------------------------------------------------------------------------

\section{Physical layer}

The following sections describe the main characteristics of the physical
layers of CIR, IrDA, and IR-UART, and then define a set of hardware
characteristics that achieve compatibility with all these modes of operation.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{CIR}

While there is no common standard for CIR, the characteristics of most IR
remote controls typically lie in the following ranges%
\footnote{Based on the following Wikipedia article:
\url{http://en.wikipedia.org/w/index.php?title=Consumer_IR&oldid=615137350\#Technical_information}}%
,
with amplitude-shift keying (ASK) being the most common form of modulation,
although other types of modulation are possible:

\begin{tab}
\begin{tabular}{l|cll}
Parameter  & Value & Unit & \\
\hline
Wavelength	& 870, 930--950	& nm \\
Data rate	& 4--120	& bps \\
Carrier		& 33-60		& kHz \\
Beam angle	& $\ge 50$	& $\hbox to 0pt{}\degree$
			& (estimate for IR remote) \\
\end{tabular}
\end{tab}%

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{IrDA}

IrDA (SIR) uses a pulse modulation with UART-like framing and has the
following characteristics:\footnote{Based on the following Wikipedia
article:
\url{http://en.wikipedia.org/w/index.php?title=Infrared_Data_Association&oldid=615436526\#IrPHY}}

\begin{tab}
\begin{tabular}{l|cll}
Parameter  & Value & Unit \\
\hline
Wavelength	& 850--900	& nm \\

Data rate	& 9.6-115.2	& kbps \\
Duty cycle	& 3/16		& & ``0`` bit \\
		& 0/16		& & ``1`` bit \\
Pulse width	& 1.6--19.5	& $\mu$s \\
Beam angle	& $\ge 15$	& $\hbox to 0pt{}\degree$ \\
\end{tabular}
\end{tab}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{IR-UART}

Last but not least, IR-UART mode uses on-off keying (OOK), and we can define
the following characteristics:

\begin{savenotes}
\begin{tab}
\begin{tabular}{l|cll}
Parameter  & Value & Unit \\
\hline
Data rate	& 9.6--115.2	& kbps (typical) \\
Duty cycle	& 9.1--91 	& \% &%
\footnote{The duty cycle range of 9.1--91\% (1/11 to 10/11) is for 8 bits with
parity.
The duty cycle range would be 10--90\% for the more common 8 bits
without parity format.} \\
\end{tabular}
\end{tab}%
\end{savenotes}%

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Combined characteristics}
\label{combined}

Combining the specifications from the preceding sections, we obtain the
following common characteristics for transmitter and receiver:

% Minimum beam angle
%
% Jul 08 00:07:39 <DocScrutinizer05> I think 50° are absolute minmum

% Wavelength selection
%
% Jul 07 23:28:15 <DocScrutinizer05> I'd prefer CIR wavelength rather than
% IrDA wavelength

\begin{tab}
\begin{tabular}{l|cll}
Parameter  & Value & Unit \\
\hline
Wavelength	& 940		& nm	& transmit \\
		& 850--950	& nm	& receive \\
Frequency	& 0.9--312	& kHz \\
Beam angle	& $\ge 50$	& $\hbox to 0pt{}\degree$ \\
\end{tabular}
\end{tab}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Transmit power}

While we do not know the radiant flux density required at the IR receivers
of consumer devices and therefore cannot specify the minimum
radiant intensity of the transmitter, we expect performance of the
IR transmitter to be on par with the average TV remote control.
For comparison, the N900 has a range of only approximately 2~m, which
we would consider too short.

% Expected range
%
% Jul 07 23:12:51 <DocScrutinizer05> I'd expect it to be on par with the
% average TV remote
%
% Jul 07 23:14:14 <DocScrutinizer05> N900 has a range of hardly 2m, that's
% inacceptable

% High-current operation
%
% Jul 07 23:29:55 <DocScrutinizer05> 300mA pulsed sounds good

% We no longer try to "over-drive" the LED. It would be too much of a hassle,
% with little benefit.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Received signal processing}

% High-pass RX filter
%
% Jul 07 23:57:01 <DocScrutinizer05> for the RX we want a phototransistor
% with filter gear to implement a highpass with cutoff @ ~20kHz

% RX connections
%
% Jul 09 16:43:40 <DocScrutinizer05> the detector should get connected to UART3
% RX, via proper signal shaping/filtering.
% and possibly also to audio-in

On the receiving side, a high-pass filter suppresses the effects of ambient
light.%
\footnote{The receiver is designed to be insensitive to constant ambient light
like strong sunlight, as well as common incandescent and fluorescent lamps.
LED lamps may create interference when they are PWM-controlled and create a
sufficient brightness level at the receiver diode.}
A Schmitt-trigger then detects the strong pulses generated by the
remote control and sends the resulting digital signal to the RX
line. The following drawing shows the general structure of the receiver
circuit. For details, please refer to section \ref{irrx}.

\begin{center}
\includegraphics[scale=0.9]{filter.pdf}
\end{center}

The series resistor ensures that CPU and Hackerbus can override the IR
receiver. When the receiver is not illuminated or when it is disabled,
the circuit outputs a high level and thus acts as a pull-up for the
RX line.

The unfiltered analog signal should also be sent to an unused input of
the audio codec. Further details on the circuit can be found in
section \ref{irrx}.

% Analog IR RX TBD
%
% Jul 17 03:43:57 <DocScrutinizer05> IR RX: I have nothing specific in mind,
% and I don't think we need to specify it right now. Will care about it later

% 
% Jul 23 15:48:26 <DocScrutinizer05> "ToDo HPF" we don't use HighPass
% filtering for UART mode, at least not with 20kHz, rather a DC-offset
% (aka background light) blocking filter at maybe 1Hz..100Hz (TBD)
%
% Jul 23 15:48:51 <DocScrutinizer05> This applies to UART mode only. SIR
% needs a 20kHz HPF
% Jul 23 15:49:21 <DocScrutinizer05> err, CIR needs that. SIR I'm currently
% clueless
%


% -----------------------------------------------------------------------------

\section{Configurations}
\label{cfg}

The IR subsystem can be configured for several different modes of operation.
The following sections detail the various configurations.

In each case the configuration of both the transmitter and the receiver
is shown. In many real-life situations, only one direction will be
used at a time while the other is deactivated. A deactivated transmitter
or receiver should behave as described in section \ref{off}.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Reset}
\label{reset}

After a reset, the
configuration of the IR system is determined by the state of the
GPIO pin of the bq27200 battery fuel gauge.
We call this pin ``bq.GPIO'' in the rest of the document.
The fuel gauge chip is directly connected to the battery and retains its
state during normal system reset.

Note that the bq27200 does not preserve the entire state of bq.GPIO
across power-on reset: while the direction of the pin is determined
by EEPROM content and is therefore retained across power-cycling,
the open-collector output is always set to ``1'', i.e., it is high-Z.

If bq.GPIO is high-Z and the CPU's control signals are in their reset
state, 
both transmitter and receiver shall be active and operate in IR-UART mode.
The purpose of this mode is to allow the CPU's ROM boot loader to try to boot
from UART3 via infrared.

If bq.GPIO is ``0'' and the CPU's control signals are in their reset state,
the IR subsystem shall be deactivated as described in section
\ref{off}.

When the CPU has left its reset state and can actively control the IR
subsystem, it is also able to change the state of bq.GPIO. bq.GPIO can
therefore be used (or ignored) as the implementation sees fit.

The following table summarizes the IR configuration in power-down and
after CPU reset:

\begin{tab}
\begin{tabular}{ll|l|l}
bq.GPIO		& CPU.controls	& \multicolumn{2}{c}{IR configuration} \\
		&		& \multicolumn{1}{c|}{TX} &
					\multicolumn{1}{c}{\qquad RX \qquad~} \\
\hline
``1'' (High-Z)	& reset state	& IR-UART		& on \\
		& active	& defined by controls	& on \\
``0''		& reset state	& off			& off \\
		& active	& defined by controls	& off \\
\end{tabular}
\end{tab}%
%
Use of the data signals in IR-UART mode is further explained in
section \ref{cfg:iruart}.
Note that Hackerbus can override the receiver (see section \ref{cfg:uart}
for details) and the CPU can therefore
boot from (wired) UART even if the IR-UART configuration is selected.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{IR deactivated}
\label{off}

The CPU can deactivate the transmitter and receiver, either both together
or one at a time,%
\footnote{See sections \ref{cfglogic} and \ref{irrx}.}
through the control signals to the IR subsystem.

When deactivated, the IR subsystem must draw as little current as possible.
The transmitter must ignore its data inputs, e.g., TX and CTS, and under no
circumstance activate the IR LED. The receiver must not drive the RX line
and use as little power as possible for any filters and/or amplifiers.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{CIR mode}

In CIR mode the CPU uses CTS as an active-high output%
\footnote{Note that CTS is an input at the CPU when UART3 is used as
regular UART.}
for transmit data.
The CIR module in the CPU could also use RTS for configuration purposes,
but we seem to have no use for this and the IR circuit must ignore its
state.
The use of RX in CIR mode is not clearly specified and it seems safe to
use it
similar to IrDA mode.
TX is not used and can be assigned to other tasks. The IR circuit must
therefore ignore the state of TX when in CIR mode.

CIR mode configuration is illustrated in the following drawing:
\begin{center}
\includegraphics[scale=0.9]{cir.pdf}
\end{center}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{IrDA mode}

In IrDA (SIR) mode, the CPU uses TX as active-high output and RX as
either\footnote{Depending on the setting of MDR2\_REG.RRXINVERT.}
active-low or active-high input.
RTS could again be used for configuration but is not
relevant in our application. CTS is not used in IrDA mode.
The IR circuit must therefore ignore the state of RTS and CTS when in
IrDA mode.

IrDA mode configuration is illustrated in the following drawing:
\begin{center}
\includegraphics[scale=0.9]{irda.pdf}
\end{center}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{IR-UART mode}
\label{cfg:iruart}

In IR-UART mode, TX and RX both are active-low. RTS and CTS are not used.
The routing of the TX and RX signals is the same as in IrDA mode.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{UART mode}
\label{cfg:uart}

The IR subsystem is inactive in UART mode and must behave as described
in section \ref{off}.
While we call it ``UART mode'', this configuration
applies to any setup where the UART signals are used for some form of
communication with the Hackerbus that does not involve IR.

UART mode configuration is illustrated in the following drawing:
\begin{center}
\includegraphics[scale=0.9]{uart.pdf}
\end{center}

Please note that, in order to avoid conflicts with use of the RX line,
at least one of the following must be true:

\begin{itemize}
  \item the IR sensor is disabled by setting bq.GPIO to ``0'',
  \item it is not illuminated by a light source that excites the
    receiver, or
  \item a pull-up resistor overriding R7 (see section \ref{irrx}) is added
    to the RX line.
\end{itemize}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Mode summary}
\label{modesum}

\def\unspec{---}
\def\outpos{$\stackrel{\strut\sqcap}{\longrightarrow}$}
\def\outneg{$\stackrel{\strut\sqcup}{\longrightarrow}$}
\def\inppos{$\stackrel{\strut\sqcap}{\longleftarrow}$}
\def\inpneg{$\stackrel{\strut\sqcup}{\longleftarrow}$}
\def\inpeit{$\stackrel{\strut\sqcup/\sqcap}{\longleftarrow}$}
\def\outany{$\longrightarrow$}
\def\inpany{$\longleftarrow$}
\def\posact{$\sqcap$}
\def\negact{$\sqcup$}
\def\eitact{$\sqcup/\sqcap$}

The following table summarizes the configuration of the I/O pins at
the CPU in the various IR modes:

\begin{tab}
\begin{tabular}{l|ccccc}
Signal & \multicolumn{5}{c}{IR subsystem mode} \\
        & Off     & CIR     & IrDA    & IR-UART & UART \\
\hline
TX	& \unspec & \unspec & \outpos & \outneg & \outneg \\
CTS	& \unspec & \outpos & \unspec & \unspec & \inpneg \\
RTS	& \unspec & \unspec & \unspec & \unspec & \outneg \\
RX	& \unspec & \inpeit & \inpeit & \inpneg & \inpneg \\
\end{tabular}
\end{tab}
%
{\outany} indicates an output from
the CPU, {\inpany} an input. An active-high signal (i.e., light is emitted
or received on the signal being ``1'') is marked with
{\posact} while an active-low (light on ''0'') signal is marked with
{\negact}.
A signal where the CPU can be configured for either polarity is marked
with {\eitact}. An unused signal is marked with {\unspec}.

% -----------------------------------------------------------------------------

\section{Conflict resolution}
\label{conflict}

%
% Conflict resolution rules:
%
% Jul 15 21:49:18 <DocScrutinizer05> wpwrak: both, and collision is a
% non-defined case, but we will make sure that the UART TX has a series R
% so it can get overridden by HB, given HB driver has lower ESR. Same for
% RX from photodiode which HB can override before it raches UART

When a line is driven as output from multiple sources, the following
conflict resolution strategy shall be applied:

\begin{tab}
\begin{tabular}{l|ll|l}
Line	& Dominant	& Subordinate	& Applicable IR system states \\
\hline
TX	& Hackerbus	& CPU		& IrDA, IR-UART \\
CTS	& Hackerbus	& CPU		& CIR \\
RX	& Hackerbus or CPU & IR sensor	& CIR, IrDA, IR-UART \\
\end{tabular}
\end{tab}%
%
Conflicts between CPU and Hackerbus on RTS or RX result in undefined
behaviour and should be avoided. Conflict increases overall power
consumption and should be avoided during normal system operation.

Precedence is established by series resistors next to the CPU for TX and
CTS, and a series resistor on the digital output of the IR receiver,
before entering the RX line.

% -----------------------------------------------------------------------------

\section{Undriven signals}
\label{undriven}

The IR circuit must be tolerant to a UART signal being driven by neither
CPU, Hackerbus, or the IR receiver. Whether this tolerance is achieved by
pulling the signal to a safe state or by applying a more general
protection against incorrect inputs is left as an implementation choice.

Control signals that float after CPU reset and that are not ignored by the
IR subsystem must be pulled to a safe state.

% -----------------------------------------------------------------------------

\section{Optoelectronics}

The following sections discuss the selection of the optoelectronic devices,
and their physical placement.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Possible component selection}

% Agreement on VSMB2948SL LED
%
% Jul 08 04:20:17 <DocScrutinizer05> I think
% http://www.vishay.com/docs/83498/vsmb2948sl.pdf
% is a better match for our purposes

Taking into account the requirements specified in section \ref{modes},
available space (see section \ref{placement}), and general component
availability, we consider the Vishay VSMB2948SL%
\footnote{\url{http://www.vishay.com/docs/83498/vsmb2948sl.pdf}}
IR LED and the Vishay VEMD10940F%
\footnote{\url{http://www.vishay.com/docs/83493/vemd2023slx01.pdf}}
photodiode suitable for our purpose.

\begin{tab}
\begin{tabular}{l|cl|cc}
Parameter	& Requirement	& Unit	& Transmitter	& Receiver \\
		& 		& 	& VSMB2948SL	& VEMD10940F \\
\hline
Wavelength	& 850--950	& nm	& 940		& 780--1050 \\
Beam angle	& $\ge$ 50 & $\hbox to 0pt{}\degree$ & 50 & 150 \\
Peak frequency 	& $\ge$ 0.3	& MHz	& 23		&  $\approx 0.2$ \\
\end{tabular}
\end{tab}%
%
Note that the peak frequency of 0.2~MHz for the receiver is an estimate
based on the specified rise and fall times of 100~ns each, which are
specified for a reverse voltage of 10~V and a load resistance of
$\rm 1~k\Omega$ whereas we
operate at 1.8~V and with a load of $\rm 22~k\Omega$. (See section
\ref{irrx}.)

% Daylight filter
%
% Jul 08 00:54:44 <DocScrutinizer05> don't worry about "color2 filtering,
% we already ahve a apparently completely black window

The characteristics of the daylight filter integrated in the IR
window of the N900 case are unknown,
our design will add multiple internal light sources
that may affect the photodiode, and there is also the possibility of
outside light reaching the diode through the spacer frame. We therefore
consider it desirable for the photodiode to contain its own filter and
not have to rely on external filtering and shielding.

% Photo-transistor, speed:
%
% Jul 08 03:27:28 <DocScrutinizer05> wpwrak:
% http://www.everlight.com/datasheets/PT26-51B-TR8_datasheet.pdf
% says "rise/fall-time 15us" - seems it's a tad slow for carrier frequencies
% >30kHz

% Photo-transistor, access to base:
%
% Jul 09 16:40:41 <DocScrutinizer05> re phototransistor: we prolly should
% use a photodiode then, they are faster than a phototransistor without
% proper discharge resistor on base

We considered the use of a phototransistor, but found that the readily
available SMT parts all had rise and fall times in the order of 15 $\mu$s,
which would limit the maximum signal frequency to only about 30 kHz.
They do not allow improving response time by biasing the transistor.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Component placement}
\label{placement}

The N900 case has a window with an integrated IR filter of unknown
characteristics. The window is approximately 5.6 mm $\times$ 5.3 mm. 

The window is designed for a single IR LED but we will have a LED
and a photodiode. Since IR components are generally large, it won't
be possible to place both ``nicely'' side by side. However, as the
following drawing illustrates, both components can ``see'' the window
if placed behind each other:

% 10 point font, nominal is 10*0.9
% 12/10*.9 = 1.08
\begin{center}
\includegraphics[scale=1.08]{window.pdf}
\end{center}
%
The transmitter should be closer to the window since its performance
is more important than that of the receiver.

The following drawing shows a {\bf bottom view} (i.e., we look from the
PCB at the bottom of the components and into the plastic case) of the
chamber that contains
the IR diodes. The indicated placement maintains a clearance of at
least 5 mm between components and between components and walls.

% 16 point font, nominal is 12*0.9
% 12/16*.9 = 0.675
\begin{center}
\includegraphics[scale=0.675]{chamber.pdf}
\end{center}

% -----------------------------------------------------------------------------

\section{Transmitter circuit}

The transmitter circuit consists of the configuration
logic that chooses which signal to output depending on the
configuration requested by the CPU and the bq.GPIO input,
the LED driver that amplifies
the output of the configuration logic, and finally the LED.

\begin{center}
\includegraphics[scale=0.9,]{txcirc.pdf}
\end{center}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Configuration logic}
\label{cfglogic}

The configuration logic selects either TX, CTS, or ``off'' depending on the
mode of operation (section \ref{cfg}). The polarity of the TX signal depends
on whether we operate in IR-UART or IrDA mode (section \ref{modesum}).
Finally, if the configuration inputs are in their reset state, the mode is
determined by bq.GPIO (section \ref{reset}).

With the assumption that the configuration inputs default to ``0'' after
reset, we can accomplish all this with the following circuit:

\begin{center}
\includegraphics[scale=0.9,]{logic.pdf}
\end{center}

The table below shows how the various modes are selected:

\begin{tab}
\begin{tabular}{l|cccc|c|l}
  System state & \multicolumn{3}{c}{IR\_TX\_$\,...$} & bq.GPIO
	& IR\_TX\_LED  & Mode \\
		& A & B & INV	&	&		& \\
  \hline
  \rule{0pt}{12pt}\ignorespaces
  Reset		& 0 & 0 & 0	& 0	& 0		& Off or UART \\
  		& 0 & 0 & 0	& 1	& $\neg$TX	& IR-UART \\
  \hline
  \rule{0pt}{12pt}\ignorespaces
  Configured	& 1 & 0 & 0	& X	& 0		& Off or UART \\
		& 1 & 1 & 0	& X	& CTS		& CIR \\
		& 0 & 1 & 0	& X	& $\neg$TX	& IR-UART \\
		& 0 & 1 & 1	& X	& TX		& IrDA \\
\end{tabular}
\end{tab}%
%
Since the circuit is too complicated to be implemented efficiently
with discrete logic, we use a
Silego SLG46533 mixed-signal array \cite{SLG46533}. This is the
same chip also used for power selection in the Neo900 SIM switch
\cite{Neo900-SIMSW}.%
\footnote{Since this chip can also communicate over \iic, we will
  to use that instead of dedicated external configuration signals.}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{LED driver}
\label{leddrv}

We drive the transmit LED through an n-FET in the current-limiting
configuration shown below.

The underlying concept is that the diode current flowing through R2
raises the voltage at the source (S) of T1 until $\rm V_{GS}$
is at the level where the FET admits exactly the desired current.

\begin{center}
\includegraphics[scale=0.9,]{leddrv.pdf}
\end{center}

\FPeval{Vbat}{4.3}
\FPeval{Voh}{1.8}
\FPeval{Vgs}{1.4}
\FPeval{Vf}{1.35}
\FPeval{Vr2}{Voh - Vgs}
\FPeval{R2}{4.7}
\FPeval{Iled}{0.09}	% mA
\FPeval{Vds}{Vbat - Vf - Iled * R2}

The maximum continuous forward current of the IR LED (VSMB2948SL) is 100~mA.
We design the circuit to operate at no more than \FPrnd{0}{Iled * 1000}~mA,
so that even a LED accidently left turned on permanently will not overheat.

The DMN26D0UT n-FET%
\footnote{\url{http://www.diodes.com/_files/datasheets/ds31854.pdf}}
is rated at 300~mW, a continuous $\rm I_D$ current of 230~mA, and
admits \FPrnd{0}{Iled * 1000}~mA when $\rm V_{GS}$ is at about
\FPrnd{1}{Vgs}~V (figure 2 in the data sheet).

With a voltage of $\rm V_{OH}=\FPrnd{1}{Voh}~V$ on IR\_TX\_LED,
resistor R2 has to drop
$\rm V_{OH}-V_{GS}=\FPrnd{2}{Vr2}~V$ to reach the desired equilibrium.
This corresponds to a resistance of $\FPrnd{2}{Vr2/Iled}~\Omega$.
We therefore use a part with a nominal resistance of $4.7~\Omega$.

Power dissipation in R2 is
$\rm {I_{LED}}^2\cdot R2=\FPrnd{0}{Iled * Iled * R2 * 1000}~mW$ and
thus requires no special consideration.
With battery voltage $\rm V_{BAT}=\FPrnd{1}{Vbat}~V$ and LED forward
voltage $\rm V_F=\FPrnd{2}{Vf}~V$, the voltage drop across the FET is
$\rm V_{DS}=V_{BAT}-V_F-I_{LED}\cdot R2=\FPrnd{2}{Vds}~V$, and the
power dissipation $\rm \FPrnd{0}{Vds * Iled * 1000}~mW$.

Resistor R3 acts with the FET gate capacitance as low-pass filter to
control the slew rate. Using $\rm C_{ISS}\approx 15~pF$ at
$\rm V_{DS}\approx 4 V$, we get a cut-off frequency of about 1~MHz
with $\rm R=10~kOhm$. (See also section \ref{combined}.)

% -----------------------------------------------------------------------------

\section{Receiver circuit}
\label{irrx}

The following diagram shows the IR receiver circuit:

\begin{center}
\includegraphics[scale=0.89]{rx.pdf}
\end{center}

When the receiver is enabled (analog switch U2 is closed), D1 is
reverse-biased through R6.
R6 pulls the signal at {\bf B} close to ground when the diode is not
illuminated, and lets the voltage raise when D1 is illuminated and
the reverse current through it increases.

The high-pass filter is implemented by comparing the signal from the
sensor ({\bf B} and {\bf C}) with the same signal after passing the
low-pass filter formed by R5 and C1 ({\bf A}).

When the diode is not illuminated, R5 also ensures that the voltage
at {\bf A} is higher than at {\bf C}, thus adding a threshold for
signal detection.

The comparator U1 operates as inverting Schmitt-trigger, with
R1 and R2 determining the hysteresis.

When the receiver is disabled (analog switch U2 is open) and the
sensor is not illuminated,
the voltage divider formed by R3 and R4 (with some contribution
from R1) lets C1 charge to the expected operating point.
R6 pulls {\bf C} to ground in this case.

R7 ensures that CPU and Hackerbus can override the IR receiver,
especially when disabled or not illuminated.

The analog comparator is part of the mixed-signal array.
(See section \ref{cfglogic}.)

% -----------------------------------------------------------------------------

\clearpage
\begin{thebibliography}{8}
\input ir.bbl
\end{thebibliography}

\end{document}

% Jul 09 16:47:10 <DocScrutinizer05> re any multiplexing: I expect the whole IR section to get powered down by asserting a GPIO pin to a non-POR-default state, so the IR is active by default after power on reset. By connecting the whole IR via some series resistors to the UART, I expect anything on hackerbus to be able to override what comes from IR, particularly when IR is powered down
