#!/usr/bin/perl
#
# Quick and dirty tester for the TX config logic circuit
#

die "usage: $0 A B INV bq\n" unless $#ARGV == 3;

($a, $b, $inv, $bq) = @ARGV;

for $tx (0, 1) {
	for $cts (0, 1) {
		$t = !$tx & $bq;
		$m = $a ? $cts : !$tx;
		$o = ($t & !$a) | ($b & $m);
		$res = $o ^ $inv;

		print "$tx $cts -> $t $m $o -> $res\n";
		$is_off++ unless $res;
		$is_tx++ if $res == $tx;
		$is_not_tx++ if $res != $tx;
		$is_cts++ if $res == $cts;
		$is_not_cts++ if $res != $cts;
	}
}

print "off $is_off\n";
print "tx $is_tx\n";
print "!tx $is_not_tx\n";
print "cts $is_cts\n";
print "!cts $is_not_cts\n";

print "\n";

print "off\n" if $is_off == 4;
print "TX\n" if $is_tx == 4;
print "!TX\n" if $is_not_tx == 4;
print "CTS\n" if $is_cts == 4;
print "!CTS\n" if $is_not_cts == 4;
