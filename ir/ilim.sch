<Qucs Schematic 0.0.18>
<Properties>
  <View=0,0,1107,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=ilim.dat>
  <DataDisplay=ilim.dpl>
  <OpenDisplay=0>
  <Script=ilim.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <.TR TR1 1 70 40 0 77 0 0 "lin" 1 "0" 1 "1 ms" 1 "1100" 0 "Trapezoidal" 0 "2" 0 "1 ns" 0 "1e-16" 0 "150" 0 "0.001" 0 "1 pA" 0 "1 uV" 0 "26.85" 0 "1e-3" 0 "1e-6" 0 "1" 0 "CroutLU" 0 "no" 0 "yes" 0 "0" 0>
  <IProbe Pr1 1 790 120 -26 16 0 0>
  <Switch S1 1 690 120 -26 11 0 0 "off" 0 "100 us" 1 "0" 0 "1e12" 0 "26.85" 0 "1e-6" 0>
  <R R2 1 550 120 -30 -62 1 0 "6.8 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
  <GND * 1 880 280 0 0 0 0>
  <Diode D1 1 880 210 24 -48 0 1 "1e-17 A" 1 "1.3" 1 "21 pF" 1 "0.5" 0 "0.7 V" 0 "0.5" 0 "0.0 fF" 0 "0.0" 0 "2.0" 0 "0.5 Ohm" 1 "0.0 ps" 0 "0" 0 "0.0" 0 "1.0" 0 "1.0" 0 "0" 0 "1 mA" 0 "26.85" 0 "3.0" 0 "1.11" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "26.85" 0 "1.0" 0 "normal" 0>
  <R R1 1 330 120 -34 -66 1 0 "27 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
  <GND * 1 400 280 0 0 0 0>
  <GND * 1 240 280 0 0 0 0>
  <C C1 1 400 210 17 -26 1 3 "33 uF" 1 "" 0 "neutral" 0>
  <Vdc V1 1 240 210 18 -26 0 1 "4.2 V" 1>
</Components>
<Wires>
  <720 120 760 120 "" 0 0 0 "">
  <580 120 660 120 "" 0 0 0 "">
  <820 120 880 120 "" 0 0 0 "">
  <880 120 880 180 "" 0 0 0 "">
  <880 240 880 280 "" 0 0 0 "">
  <360 120 400 120 "" 0 0 0 "">
  <240 120 300 120 "" 0 0 0 "">
  <240 120 240 180 "" 0 0 0 "">
  <400 120 520 120 "" 0 0 0 "">
  <400 120 400 180 "" 0 0 0 "">
  <400 240 400 280 "" 0 0 0 "">
  <240 240 240 280 "" 0 0 0 "">
  <880 120 880 120 "Vled" 910 90 0 "">
</Wires>
<Diagrams>
  <Rect 121 643 736 263 3 #c0c0c0 1 00 1 0 0.0001 0.001 0 0 0.1 0.5 0 0 0.2 2 315 0 225 "" "" "">
	<"Vled.Vt" #0000ff 3 3 0 0 1>
	<"Pr1.It" #ff0000 3 3 0 0 0>
  </Rect>
</Diagrams>
<Paintings>
  <Text 470 130 12 #000000 0 "1.2 W peak (try 0.4 W)">
  <Text 720 310 12 #000000 0 "Very rough VSMB2948SL approximation">
  <Text 310 130 12 #000000 0 "1/4 W">
  <Text 470 160 12 #000000 0 "Maybe ESR10EZPJ6R8">
</Paintings>
