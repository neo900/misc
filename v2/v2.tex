\documentclass[11pt]{article}
\usepackage{a4}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{xfrac}	% for \sfrac
\usepackage{titlesec}   % for \sectionbreak
\usepackage{parskip}
\usepackage[hang,bottom]{footmisc}
\usepackage{footnote}
\usepackage{longtable}
\usepackage[all]{nowidow}
\usepackage[numbib]{tocbibind}  % add references to PDF TOC
%\usepackage{scrextend}	% for \footref
\usepackage{fixfoot}
\usepackage[hidelinks,bookmarksnumbered=true,bookmarksopen,
    bookmarksopenlevel=2]{hyperref}

% http://tex.stackexchange.com/questions/50352/inserting-a-small-vertical-space-in-a-table
\def\T{\rule{0pt}{12pt}}

% Copyright notice in the footer (first page only)
\usepackage{fancyhdr}
\fancypagestyle{plain}{
  \fancyfoot{}
  \fancyfoot[L]{Copyright \textcopyright\ by the authors.}}
\renewcommand{\headrulewidth}{0pt}

\bibliographystyle{unsrt}
\newcommand{\sectionbreak}{\clearpage}
\renewcommand{\footnotemargin}{1.2em}

\def\iic{$\hbox{I}^2\hbox{C}$}
\def\iis{$\hbox{I}^2\hbox{S}$}
\def\TODO{{\bf TO DO}}

\title{Neo900 v2 Prototype}
\author{J\"org Reisenweber%
\footnote{Design requirements.}
%~\url{<joerg@openmoko.org>} \\
,
Werner Almesberger%
\footnote{Specification details and illustrations.}
%~\url{<werner@almesberger.net>}
}
\date{January 29, 2017}

\begin{document}
\phantomsection\pdfbookmark{Neo900 v2 Prototype}{firstpage}
\maketitle

The Neo900 v2 prototype is the forthcoming first major implementation
of large portions of the Neo900 design. The prototype is intended to:
\begin{itemize}
  \item confirm mechanical compatibility with the N900 design,
  \item validate design and implementation of the various circuits
    and connectors, mainly on LOWER, and
  \item test software compatibility with the Neo900 architecture.
\end{itemize}

In v2, we reduce the complexity of UPPER by deferring integration of
the OMAP (the principal CPU or SoC) and related components to a later
prototype, and use a BeagleBoard-xM (``BB-xM'' \cite{BBxM}) as
``brain'' for v2.
This complexity reduction serves the following purposes:
\begin{itemize}
  \item limit the risk of design flaws in UPPER that may prevent the prototype
    from operating and would delay testing of LOWER,
  \item reduce the layout effort for v2, and
  \item lower the cost of producing v2.
\end{itemize}

% -----------------------------------------------------------------------------

\section{Overall system structure}

The following drawing illustrates the structure of the various parts
of the Neo900 v2 prototype.
Note that this is greatly simplified and abstracted, and a number of
items (e.g., camera, battery) are not shown.

\begin{center}
\includegraphics[scale=0.4]{exp.pdf}
\end{center}

There are three principal boards of v2 proper: the LOWER board contains
most of the peripherals, including modem, WLAN/BT, and audio. LOWER interfaces
mechanically with the N900 case. The UPPER board has the keyboard, the
display connector, the camera interfaces, and some sensors. Memory card,
the flash LEDs, and a
few sensors are on BOB (the Break-Out Board). BOB also features the
Hackerbus interface
\cite{Neo900-HB}
that connects to user-provided external circuits.

In the final version of Neo900, UPPER will also contain the CPU, the
companion chip that provides voltage regulators and various other
peripheral functions, and the memories. In v2, all this resides on a
BeagleBoard-xM that attaches to UPPER.

In v2, UPPER extends far beyond the rear of the N900 case, where it
connects to the bottom side of BB-xM. An additional adapter board
(see section \ref{cambrd})
makes a connection between UPPER and the camera connector on the top side
of BB-xM.

A more detailed top view can be found in section \ref{xy}. The vertical
stacking is explained in section \ref{stacking}.

\filbreak
This drawing shows the flow of electrical signals between the boards:
\nobreak
\begin{center}
\includegraphics[scale=0.9]{estack.pdf}
\end{center}

Since BB-xM does not provide all the functionality we need, UPPER of v2 also
contains circuits for adapting and extending what BB-xM provides. These
adaptations are discussed in more detail in section \ref{v2diff}.

% -----------------------------------------------------------------------------

\section{BB-xM connections}

The v2 prototype of Neo900 connects to BB-xM through various 2.54~mm
and 1.27~mm headers. Most of these headers are located at the bottom
of BB-xM.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Connector locations}

The following illustration shows where on BB-xM the connectors we use are
located, when looking at the board from the top:

\begin{center}
\includegraphics[scale=1.0]{loc-conn.pdf}
\end{center}

Except for the S-Video connector P4 and the camera connector P10,
all are on the bottom of BB-xM. P4 is a side-facing mini-DIN connector
from which we can feed the analog TV output signal to Neo900 v2.

The exact placement of the headers can be found in section
\ref{bbxmgeo}, the numbers of pins and pitch (between rows and columns)
are as follows:

\begin{tabular}{lrll}
  Reference & Pins & Pitch	& Name \\
  \hline
  P9	& 28	& 2.54~mm	& Expansion Connector \\
  P10	& 34	& 1.27~mm	& Camera Connector \\
  P17	& 20	& 1.27~mm	& Aux Access Header \\
  P18	& 4	& 1.27~mm	& Audio Access Header \\
  P11, P13 & 20	& 1.27~mm	& LCD RGB Interface \\
\end{tabular}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Component selection}

The BB-xM documentation provides part numbers only for the following
two connectors:

\begin{savenotes}
\begin{tabular}{ll}
  Reference%
\footnote{In the BB-xM revision C schematics from \\
  \url{http://beagleboard.org/static/BB-xM_REV_C-2011-05-23.zip}}
 & Part number \\
  \hline
  P9\T & LSWSS-114-D-02-T-LF%
\footnote{Data sheet:
  \url{https://s3.amazonaws.com/mle-assets/parts/LSWSS-1/LSWSS-1-D+REV+D.pdf}}
  \\
  P10 & F618-MG -D051-XX-CF358%
\footnote{The original data sheet does not seem to be available from the
  manufacturer, but this copy can still be found: \\
  \url{http://markmail.org/download.xqy?id=l7gxdc4kfmshw7dy&number=1} \\
  A similar (without positioning posts) and still active part from the
  same manufacturer is specified here: \\
  \url{http://php2.twinner.com.tw/files/cherngweei/P605-SGN-030_023-XX.pdf}}
  \\
\end{tabular}
\end{savenotes}

The Aux Access Header, the two LCD headers, and the Audio Access Header
are just specified as ``HDR {\em n}X2\_1.27mm''. All these connectors
are female, and we assume that they have an insertion depth of at least
3~mm, like to similar parts we examined for \cite{Neo900-HB}.

Since all the connectors on the bottom side of BB-xM connect to UPPER,
their mated height has to match. The following illustration shows
the Expansion Connector and the 1.27~mm connectors:

\begin{center}
\includegraphics[scale=0.8]{bb-conn.pdf}
\end{center}

We select the following parts to mate with the BB-xM connectors:

\begin{savenotes}
\begin{tabular}{lllr}
  Reference & Manufacturer	& Part number		& Positions \\
  \hline
  P9\T		& Mill-Max	& 435-40-228-00-160000	& 28 \\
		&		& 435-40-272-00-160000	& 72 \\
  P10		& Preci-Dip	& 852-80-034-10-001101	& 34 \\
		& Mill-Max	& 852-10-100-10-001000	& 100 \\
  P11, P13, P17	& Mill-Max	& 852-10-020-10-001000	& 20 \\
		&		& 852-10-100-10-001000	& 100 \\
  P18		& W\"urth	& 62200421121		& 4 \\
		& Mill-Max	& 852-10-100-10-001000	& 100 \\
\end{tabular}
\end{savenotes}

All these connectors are ``breakaway'' headers, and in some cases only
strips with a large number of contacts (e.g., 100) are manufactured and
are then cut (be it by the manufacturer, the distributor, the board
assembler, etc.)
to the number of pins the respective application needs.
The above table shows both the
customized part and~--~in case the customized part should turn out
to be difficult to source and we need to take care of customization
ourselves~--~the larger off-the-shelf header.

Further information about connector characteristics can be found in
\cite{Neo900-Hdr}.

% -----------------------------------------------------------------------------

\section{Board placements}
\label{xy}

The UPPER board of the v2 prototype of Neo900 extends beyond the N900
case and overlaps with the bottom side of BB-xM. The following
drawing (top view) shows the approximate placement of BB-xM, N900 and display,
the extended UPPER board, and the adapter board for the camera connector:

\begin{center}
\includegraphics[scale=0.8]{loc-xy.pdf}
\end{center}

The parts of UPPER that reach remote connectors are deliberately made of
narrow PCB strips. These strips allow the PCB to flex a little and thus
adapt to mechanical tolerances.

In the above drawing, the dimensions and the shape of UPPER are only
indicative, and the board outline chosen by layout may differ.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Finger openings}

UPPER has openings at the rear end of the N900 case that allow the
operator to access the buttons located there. The openings should
allow for the insertion of fingers of at least 15$\times$18~mm,
with the fingers placed on the center of the button or, in the case
of the volume button, on the two small protruding knobs.

The drawing below illustrates the general idea:

\begin{center}
\includegraphics[scale=0.8]{finger.pdf}
\end{center}

The exact location and width of the finger holes should be determined
from the placement of the respective switches on the PCB, which is outside
the scope of this document.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Camera adapter board}
\label{cambrd}

Since the camera connector (P10) is placed on top of BB-xM, we cannot
plug into it directly from UPPER. Instead, we use a small adapter board
that is attached to UPPER with a 50~mil (1.27~mm) ribbon cable.

\begin{center}
\includegraphics[scale=0.4]{ribbon.pdf}
\end{center}

The exact size and shape of the board is to be determined by layout.
Note that some signals on the P10 are not used by v2 or are redundant,
so the ribbon cable could have less than 34 conductors.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\filbreak
\subsection{Vertical stacking}
\label{stacking}

The following drawing shows the vertical stacking of the v2 system,
including the Neo900 boards, N900 parts, and BeagleBoard-xM:
\nopagebreak
\begin{center}
\includegraphics[scale=0.6]{vert.pdf}
\end{center}

Dimensions are in mm. The drawing is only approximate and some artistic
liberties have been taken especially in the horizontal direction.
Further details on the stacking of items in Neo900 can be found in
\cite{Neo900-Stacking}.

The drawing also shows the various PCB surfaces: B1 (towards the case
bottom) and B2 are the surfaces of BOB, S1 (towards the battery) and S2 are
of LOWER, and S3 and S4 (towards the display) are of UPPER.

% -----------------------------------------------------------------------------

\section{Differences to Neo900}
\label{v2diff}

Some signals of the OMAP processor and the companion chip are not available
on any connector of BB-xM or are used in different ways than in Neo900.
We therefore have to replace or modify some functions, assign them to
different pins, or just omit them. The following sections detail the
principal differences. 

GPIO assignments are normally not specified in this document. They can be
found in the schematics%
\footnote{\url{https://neo900.org/stuff/werner/tmp/ee/pdf/neo900.pdf}}
and in our BB-xM pin assignment table:\\
\url{https://neo900.org/git/misc/tree/pinmux/bb-xm.assign}

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Functions performed by BB-xM}

BB-xM provides the CPU (OMAP), its companion chips, and the system memories
required for the system to run. All these chips are tightly interconnected
(with over 100 signals) and ``outsourcing'' them to BB-xM greatly reduces
the complexity of the UPPER board of the Neo900 v2 prototype.

By using the same CPU as in Neo900 and disturbing N900 compatibility
of the Neo900 design as little as possible, v2 can still be able to run
certain non-open code written for N900, should the need for doing so arise.

The key functions provided by BB-xM are:
\begin{itemize}
  \item Running the Linux environment, test code, and selected other code,
  \item some voltage regulators (see section \ref{power}), provided by
    the companion chip,
  \item various digital protocols (UART, \iic, \iis, SPI, $\ldots$),
    implemented by function blocks in the CPU,
  \item an ADC input, and
  \item last but not least, GPIOs.
\end{itemize}

The other peripherals on BB-xM are only used for access and operation of
BB-xM itself.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Memories}

The v2 prototype relies on BB-xM to provide most of the memories.
The following table compares the memories available in N900,
BB-xM (Neo900 v2), and the Neo900 design beyond v2:

\begin{tabular}{llll}
  Memory	& N900		& BB-xM / Neo900 v2 & Neo900 $\rm > v2$ \\
  \hline
  RAM		& 256~MB	& 512~MB	& 1~GB \\
  NAND		& 256~MB	& ---		& 512~MB \\
  eMMC		& 32~GB		& ---		& To be defined \\
  Memory card	& MMC\#1	& \iic		& MMC\#1 \\
\end{tabular}

BB-xM has a memory card interface that is occupied by a card containing the
boot and system files. The memory card connector of Neo900 is also accessible
in the v2 prototype, but only through an \iic-attached IO expander.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Power supplies}
\label{power}

In v2, we use a mixed supply architecture where part of the system is supplied
from BB-xM (i.e., USB) and the rest from the charger circuit of Neo900
(i.e., USB or battery). Furthermore, v2 has regulators for power rails
that will be supplied by the companion chip in later versions, but where
the corresponding rail is not suitable (or unavailable) on BB-xM.

Power rails that are implemented differently in v2 and the post-v2
design are shown in the drawing below:

\begin{center}
\includegraphics[scale=0.8]{rails.pdf}
\end{center}

A 5~V DC adapter (regulated) can be used instead of USB to provide
BB-xM with power. While we do not expect to need this, we show it as a
possible choice.

% http://tex.stackexchange.com/questions/9937/underscore-makes-text-go-past-end-of-line-into-margins
\renewcommand\_{\textunderscore\allowbreak}
The remaining rails (VBUS, BATT, VBAT\_RAW, VBUS\_OTG, VMODEMx, VGNSS,
VBAT\_SWITCHED, and VSIM\_x) are the same as in the post-v2 design.
Their structure is shown in the power tree \cite{Neo900-Pwr}.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{IO expander}
\label{v2iox}

The number of GPIOs available on the BB-xM connectors is slightly too low
for Neo900 v2. We therefore add an IO expander to provide additional GPIOs.
The role of IO expanders in Neo900 is discussed in detail in
\cite{Neo900-iox}.

Note that the IO expander (XRA1201P) enables pull-ups on all GPIOs on reset,
which may differ from reset behaviour of the corresponding pin on OMAP.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Keyboard controller}

The companion chip contains a $8\times 8$ keyboard matrix controller,
which we plan to use in Neo900. Since access to the companion chip
(in BB-xM) is very limited in the v2 prototype, the prototype uses a
dedicated \iic-attached controller chip (TCA8418%
\footnote{\url{http://www.ti.com/lit/ds/symlink/tca8418.pdf}})
instead.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Modem USB}

The modem module communicates with the CPU through the USB\#1 host interface
and UART\#1.
The USB\#1 setup in Neo900 is shown on the left-hand side of the following
drawing:

\begin{center}
\includegraphics[scale=0.9]{modemusb-final.pdf}
\hspace{10mm}
\includegraphics[scale=0.9]{modemusb-v2.pdf}
\end{center}

Since there is no CPU and therefore no ULPI bus on v2, we instead
add a Micro USB B connector that can be connected to any regular
USB port~--~e.g., one of the USB ports of BB-xM, as shown on the
right-hand side of the above drawing.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Hackerbus USB}

Neo900 provides a dedicated USB host interface on the Hackerbus.
This interface consists of a PHY connected to the USB\#2 ULPI
bus of the CPU, similar to modem USB.

Since we have no ULPI on v2, and the Hackerbus USB interface has
no other function in Neo900 than being available on Hackerbus,
we omit this functionality in v2.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsubsection{PCM routing}

Neo900 has the following bidirectional digital audio busses:

\begin{tabular}{lll}
  Bus			& Type		& Port \\
  \hline
  Audio codec\T		& \iis		& McBSP2 \\
  Bluetooth		& PCM		& McBSP3 \\
  Modem			& PCM		& McBSP4 \\
\end{tabular}

\filbreak
Each audio bus is served by a serial port (``Multichannel Buffered
Serial Port'', McBSP) in the CPU:
\nobreak
\begin{center}
\includegraphics[scale=0.9]{pcm-final.pdf}
\end{center}

BB-xM only gives access to two of these ports. We therefore share
McBSP3 between Bluetooth and modem, and insert a Silego mixed-signal
matrix to act as programmable multiplexer:

\begin{center}
\includegraphics[scale=0.9]{pcm-v2.pdf}
\end{center}

The Silego chip is configured through \iic.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Audio master clock}

N900 uses the HFCLKOUT output (19.2 MHz) of the companion chip as master
clock for the audio codec (CODEC\_MCLK).
BB-xM uses a 26~MHz crystal instead of 19.2~MHz,
and HFCLKOUT is not available on any connector.

While the very flexible PLL in the audio codec should make it possible to
produce a suitable clock also from a different source, we add a dedicated
crystal oscillator for maximum compatibility.

The oscillator is controlled with an enable signal (HFCLK\_EN)
from the v2 IO expander. This signal also drives a LED,
and can thus be used to test basic operation of the \iic~\#3 bus.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{32.768 Hz clock}

The 32.768 kHz clock generated by the companion chip is not available
on any connector of the BB-xM. We use a dedicated crystal oscillator
for it in v2.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Camera strobe}

The camera strobe signal (cam\_strobe, ball D25, controlled by the
Camera Image Signal Processor function block in the CPU) is used in Neo900
to trigger the camera flash. BB-xM does not make this signal available
on any connector.

In order to be able to test the flash function, even if without proper
synchronization with camera capture, we use a GPIO instead.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{TV output}

The analog TV output signal (cvideo1\_out, ball Y28, connected to the DAC
of the Display Subsystem in the CPU) is only available on the S-Video
connector (P4) of BB-xM.

In order to test the signal routing in v2, we connect TVOUT\_U to
a footprint for a 100 mil 2-pin through-hole header on UPPER.
An adapter cable can then connect this to the analog video output
of BB-xM.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Vibration motor}

The outputs of the vibration motor driver in the companion chip
(VIBRAP F16 and VIBRAM G15) are not available on any connector of
BB-xM. We therefore do not support use of the vibration motor in v2.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{UART flow control signals}

Neo900 uses the following UARTs, each consisting of the
data lines TX and RX, and the flow control lines CTS and RTS:

\begin{tabular}{lll}
  Destination	& Name prefix	& Port \\
  \hline
  Modem		& UART1		& UART\#1 \\
  Bluetooth	& BT\_UART	& UART\#2 \\
  IR, Hackerbus	& UART3		& UART\#3 \\
\end{tabular}

The data lines of all three UARTs are available on BB-xM connectors.
However, CTS and RTS are only available for UART\#2. We therefore
provide the missing CTS and RTS signals on GPIOs in v2.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{MMC/SD interface}

The memory card of Neo900 is connected to the MMC\#1 bus.
BB-xM connects this bus to the ``uSD connector'' (a card holder) on BB-xM and
does not make it available on any other connector. This interface is
typically occupied for operation of the BB-xM.

In order to be able to test connectivity, the signals of the Neo900 memory
card holder are connected in v2 to an IO expander.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Hackerbus GPIOs}

Since the number of signals available on the BB-xM connectors is limited,
the CPU-side signals (HB\_A to HB\_D) of the four Hackerbus GPIOs are
connected to the v2 IO expander. While this limits the use of these GPIOs,
it still allows testing of overall connectivity.

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{ADC multiplexing}

The following signals pass from LOWER to the ADC in the companion chip:

\begin{tabular}{ll}
  Signal		& Origin \\
  \hline
  ADC\_1		& Inbound cellular antenna power detector \\
  ADC\_2		& Outbound cellular antenna power detector \\
  BATTID		& Battery multipurpose contact \\
  BATTEMP\_COMPANION	& Battery temperature sensor \\
  ECI\_ADC		& ECI signal \\
  VSIM\_SENSE		& SIM current sensor \\
\end{tabular}

ADC\_1 and ADC\_2 are ``nice to have'' but can be omitted if
the number of contacts on the LOWER-UPPER connection should turn
out to be insufficient.

BATTID should connect to ADCIN0, BATTEMP\_COMPANION to ADCIN1, which
are both designed for the respective function.
ECI\_ADC should follow the N900 design and connect to ADCIN2 (ball G3).
The other signals connect to any of the general-purpose inputs ADCIN2
to ADCIN7.

\begin{center}
\includegraphics[scale=0.9]{adc-final.pdf}
\end{center}

BB-xM only provides a single ADC input, ADCIN6 on the Auxiliary Expansion
Header. While this limits the use of at least some of the signals, we can
still monitor them for correct levels. To that end, v2 uses an analog
multiplexer to select which of the available analog signals gets sent
to the BB-xM:

\begin{center}
\includegraphics[scale=0.9]{adc-v2.pdf}
\end{center}

The analog multiplexer is configured through three control signals
from the v2-specific IO expander (see section \ref{v2iox}).

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

\subsection{Speaker amplifier enable}

Neo900 uses signal SPEAKER\_EN to enable the speaker amplifier.
This corresponds to IHF\_EN in N900. IHF\_EN is connected to GPIO7
(ball N14) of the companion chip.

BB-xM does not connect ball N14. We therefore
connect SPEAKER\_EN in v2 to the v2-specific IO expander
(see section \ref{v2iox}).

% -----------------------------------------------------------------------------

\section{BB-xM geometry}
\label{bbxmgeo}

The following diagram shows the location of pin 1 of each connector
on BB-xM, relative to the center of the lower left mounting hole.
Furthermore, the size of the connector outlines (on silk screen) is
shown. Distances are in mm.%
\footnote{To show sizes in mil, like in the original BB-xM design, load
\url{https://neo900.org/git/misc/tree/v2/bbxm.fpd}
with fped and change the unit by clicking on the box
in the lower right corner.}

\begin{center}
\includegraphics[scale=0.7,trim=20 110 25 100,clip,angle=90]{bbxm.pdf}
\end{center}

The board is shown from above.
The position of the board edges relative to the mounting holes is
only an approximation.

% -----------------------------------------------------------------------------

\clearpage
\begin{thebibliography}{8}
\input v2.bbl
\end{thebibliography}

\end{document}
